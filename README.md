# Isos Technology Tech Assessment

## Objectives:

1. Install Atlassian Jira Service Desk
2. Configure Database
3. Import Sample Data
4. Document procedure and issues
5. Identify 3 Next Steps and additional tools to improve the procedure.

### V1 - Jira Service Desk on CentOS VM

This installation utilizes a Vagrant VM running CentOS7 and a PostgreSQL database server.

[V1 - Jira Service Desk Install - Click Here](https://bitbucket.org/jgbiz/isos-technology-tech-assessment/src/master/versions/V1.md)

### V2 - Jira Service Desk - Ansible Automated installation

Using Ansible and Vagrant, automatically install PostgreSQL database server, Postfix mail server and download & install Jira Service Desk. Copy `response.varfile` for unattended installation, copy database configuration `dbconfig.xml` and import a jira-clean.sql database. Perform checks for successful download and existence of Jira to prevent errors if provisioning multiple times.

[V2 - Ansible Jira Service Desk Install - Click Here](https://bitbucket.org/jgbiz/isos-technology-tech-assessment/src/master/versions/V2.md)

### V3 - EC2 Instance with Dynamic Inventory Population (in progress)

Using ec2.py script, load inventory dynamically and run the playbook against this inventory.

[V3 - EC2 Dynamic Inventory - Click Here](https://bitbucket.org/jgbiz/isos-technology-tech-assessment/src/master/versions/V3.md)

### Next Steps
1.  Automate the PostgreSQL database installation, database and user creation with Ansible. Conceal passwords with encryption.
2.  Automate the Jira installation with Ansible. Install Jira with configuration options and remove the need for the manual browser setup.
3.  Automate a Jira installation on AWS EC2 and RDS, ideally with a CloudFormation template to automate the stack generation as well. Populate host list dynamically using EC2.py or Ansible Tower.


### Issues
#### V1:
* Jira Setup Error - Jira failed to configure the first attempt
    * Threw errors for system plugins failing to start and database collation/database version not supported.
    * CentOS7 Ships with PostgreSQL 7.2, so I had to add a repository source and reinstall with a newer version 9.*
* Database Setup Error - Ident authentication failed.
    * I had to add a step in my database installation and configuration that changed the default Ident authentication type to Trust or MD5.
* Configure Shared Folders in Vagrant Error
    * The original vagrant box that I used did not ship with guest additions.
    * A manual install of guest additions failed and broke the SSH service.
    * Switching to the Bento/CentOS-7 box solved this error, as it shipped with guest additions configured
* Jira Administrator Password
    * Without an admin user I had to boot Jira in recovery mode and setup a user account with admin privileges.

#### V2:
* Custom PostgreSQL Role `createdb` - failed to create user and database.
    * Due to HBA authentication error, need to script an override of the line in HBA Auth section for `local, peer` to `local, md5`
    * Utilized ansible-galaxy postgresql role instead
* Become user errors - permissions errors
  * I used `become: yes` &  `become_user:` to obtain the proper leverage.
* running `vagrant provision` multiple times
    * I utilized the `register` option with `debug` modules to check to see if files/folders existed prior to certain steps to prevent errors.
* Postfix and mail - issues testing email
    * Need to set up an email relay using SMTP.

#### V3:
* Utilizing dynamic inventory
    * I'm able to execute ec2.py and produce a list of inventory from AWS, but not yet able to run the playbook against this.

### Screenshots of V1
![Jira Admin Database 1](img/system-database-info-1.jpg)

![Jira Admin Database 2](img/system-database-info-2.jpg)

![Jira Admin Database 3](img/system-database-info-3.jpg)
