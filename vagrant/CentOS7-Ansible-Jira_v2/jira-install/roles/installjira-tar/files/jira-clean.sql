--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: AO_0201F0_KB_HELPFUL_AGGR; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_0201F0_KB_HELPFUL_AGGR" (
    "COUNT" bigint,
    "ID" bigint NOT NULL,
    "SERVICE_DESK_ID" bigint,
    "START_TIME" bigint
);


ALTER TABLE public."AO_0201F0_KB_HELPFUL_AGGR" OWNER TO jira;

--
-- Name: AO_0201F0_KB_HELPFUL_AGGR_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_0201F0_KB_HELPFUL_AGGR_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_0201F0_KB_HELPFUL_AGGR_ID_seq" OWNER TO jira;

--
-- Name: AO_0201F0_KB_HELPFUL_AGGR_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_0201F0_KB_HELPFUL_AGGR_ID_seq" OWNED BY "AO_0201F0_KB_HELPFUL_AGGR"."ID";


--
-- Name: AO_0201F0_KB_VIEW_AGGR; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_0201F0_KB_VIEW_AGGR" (
    "COUNT" bigint,
    "ID" bigint NOT NULL,
    "SERVICE_DESK_ID" bigint,
    "START_TIME" bigint
);


ALTER TABLE public."AO_0201F0_KB_VIEW_AGGR" OWNER TO jira;

--
-- Name: AO_0201F0_KB_VIEW_AGGR_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_0201F0_KB_VIEW_AGGR_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_0201F0_KB_VIEW_AGGR_ID_seq" OWNER TO jira;

--
-- Name: AO_0201F0_KB_VIEW_AGGR_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_0201F0_KB_VIEW_AGGR_ID_seq" OWNED BY "AO_0201F0_KB_VIEW_AGGR"."ID";


--
-- Name: AO_0201F0_STATS_EVENT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_0201F0_STATS_EVENT" (
    "EVENT_KEY" character varying(255),
    "EVENT_TIME" bigint,
    "ID" bigint NOT NULL
);


ALTER TABLE public."AO_0201F0_STATS_EVENT" OWNER TO jira;

--
-- Name: AO_0201F0_STATS_EVENT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_0201F0_STATS_EVENT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_0201F0_STATS_EVENT_ID_seq" OWNER TO jira;

--
-- Name: AO_0201F0_STATS_EVENT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_0201F0_STATS_EVENT_ID_seq" OWNED BY "AO_0201F0_STATS_EVENT"."ID";


--
-- Name: AO_0201F0_STATS_EVENT_PARAM; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_0201F0_STATS_EVENT_PARAM" (
    "ID" bigint NOT NULL,
    "PARAM_NAME" character varying(127),
    "PARAM_VALUE" character varying(450),
    "STATS_EVENT_ID" bigint
);


ALTER TABLE public."AO_0201F0_STATS_EVENT_PARAM" OWNER TO jira;

--
-- Name: AO_0201F0_STATS_EVENT_PARAM_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_0201F0_STATS_EVENT_PARAM_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_0201F0_STATS_EVENT_PARAM_ID_seq" OWNER TO jira;

--
-- Name: AO_0201F0_STATS_EVENT_PARAM_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_0201F0_STATS_EVENT_PARAM_ID_seq" OWNED BY "AO_0201F0_STATS_EVENT_PARAM"."ID";


--
-- Name: AO_0AC321_RECOMMENDATION_AO; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_0AC321_RECOMMENDATION_AO" (
    "CATEGORY" character varying(255),
    "CUSTOM_FIELD_ID" bigint,
    "ID" character varying(255) NOT NULL,
    "NAME" character varying(255),
    "PERFORMANCE_IMPACT" double precision,
    "PROJECT_IDS" text,
    "RESOLVED" boolean,
    "TYPE" character varying(255)
);


ALTER TABLE public."AO_0AC321_RECOMMENDATION_AO" OWNER TO jira;

--
-- Name: AO_21D670_WHITELIST_RULES; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_21D670_WHITELIST_RULES" (
    "ALLOWINBOUND" boolean,
    "EXPRESSION" text NOT NULL,
    "ID" integer NOT NULL,
    "TYPE" character varying(255) NOT NULL
);


ALTER TABLE public."AO_21D670_WHITELIST_RULES" OWNER TO jira;

--
-- Name: AO_21D670_WHITELIST_RULES_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_21D670_WHITELIST_RULES_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_21D670_WHITELIST_RULES_ID_seq" OWNER TO jira;

--
-- Name: AO_21D670_WHITELIST_RULES_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_21D670_WHITELIST_RULES_ID_seq" OWNED BY "AO_21D670_WHITELIST_RULES"."ID";


--
-- Name: AO_21F425_MESSAGE_AO; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_21F425_MESSAGE_AO" (
    "CONTENT" text NOT NULL,
    "ID" character varying(255) NOT NULL
);


ALTER TABLE public."AO_21F425_MESSAGE_AO" OWNER TO jira;

--
-- Name: AO_21F425_MESSAGE_MAPPING_AO; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_21F425_MESSAGE_MAPPING_AO" (
    "ID" integer NOT NULL,
    "MESSAGE_ID" character varying(255) NOT NULL,
    "USER_HASH" character varying(255) NOT NULL
);


ALTER TABLE public."AO_21F425_MESSAGE_MAPPING_AO" OWNER TO jira;

--
-- Name: AO_21F425_MESSAGE_MAPPING_AO_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_21F425_MESSAGE_MAPPING_AO_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_21F425_MESSAGE_MAPPING_AO_ID_seq" OWNER TO jira;

--
-- Name: AO_21F425_MESSAGE_MAPPING_AO_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_21F425_MESSAGE_MAPPING_AO_ID_seq" OWNED BY "AO_21F425_MESSAGE_MAPPING_AO"."ID";


--
-- Name: AO_21F425_USER_PROPERTY_AO; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_21F425_USER_PROPERTY_AO" (
    "ID" integer NOT NULL,
    "KEY" character varying(255) NOT NULL,
    "USER" character varying(255) NOT NULL,
    "VALUE" character varying(255) NOT NULL
);


ALTER TABLE public."AO_21F425_USER_PROPERTY_AO" OWNER TO jira;

--
-- Name: AO_21F425_USER_PROPERTY_AO_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_21F425_USER_PROPERTY_AO_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_21F425_USER_PROPERTY_AO_ID_seq" OWNER TO jira;

--
-- Name: AO_21F425_USER_PROPERTY_AO_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_21F425_USER_PROPERTY_AO_ID_seq" OWNED BY "AO_21F425_USER_PROPERTY_AO"."ID";


--
-- Name: AO_2C4E5C_MAILCHANNEL; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_2C4E5C_MAILCHANNEL" (
    "CREATED_BY" character varying(255),
    "CREATED_TIMESTAMP" bigint DEFAULT 0,
    "ENABLED" boolean,
    "ID" integer NOT NULL,
    "MAIL_CHANNEL_KEY" character varying(255),
    "MAIL_CONNECTION_ID" integer,
    "MAX_RETRY_ON_FAILURE" integer DEFAULT 0,
    "MODIFIED_BY" character varying(255),
    "PROJECT_ID" bigint DEFAULT 0,
    "UPDATED_TIMESTAMP" bigint DEFAULT 0
);


ALTER TABLE public."AO_2C4E5C_MAILCHANNEL" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILCHANNEL_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_2C4E5C_MAILCHANNEL_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_2C4E5C_MAILCHANNEL_ID_seq" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILCHANNEL_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_2C4E5C_MAILCHANNEL_ID_seq" OWNED BY "AO_2C4E5C_MAILCHANNEL"."ID";


--
-- Name: AO_2C4E5C_MAILCONNECTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_2C4E5C_MAILCONNECTION" (
    "CREATED_TIMESTAMP" bigint DEFAULT 0,
    "EMAIL_ADDRESS" character varying(255) NOT NULL,
    "FOLDER" character varying(255) NOT NULL,
    "HOST_NAME" character varying(255) NOT NULL,
    "ID" integer NOT NULL,
    "PASSWORD" character varying(255) NOT NULL,
    "PORT" integer DEFAULT 0 NOT NULL,
    "PROTOCOL" character varying(255) NOT NULL,
    "PULL_FROM_DATE" bigint DEFAULT 0,
    "TIMEOUT" bigint DEFAULT 0,
    "TLS" boolean DEFAULT false,
    "UPDATED_TIMESTAMP" bigint DEFAULT 0,
    "USER_NAME" character varying(255) NOT NULL
);


ALTER TABLE public."AO_2C4E5C_MAILCONNECTION" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILCONNECTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_2C4E5C_MAILCONNECTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_2C4E5C_MAILCONNECTION_ID_seq" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILCONNECTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_2C4E5C_MAILCONNECTION_ID_seq" OWNED BY "AO_2C4E5C_MAILCONNECTION"."ID";


--
-- Name: AO_2C4E5C_MAILGLOBALHANDLER; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_2C4E5C_MAILGLOBALHANDLER" (
    "CREATED_TIMESTAMP" bigint DEFAULT 0,
    "HANDLER_TYPE" character varying(255),
    "ID" integer NOT NULL,
    "MODULE_COMPLETE_KEY" character varying(255),
    "UPDATED_TIMESTAMP" bigint DEFAULT 0
);


ALTER TABLE public."AO_2C4E5C_MAILGLOBALHANDLER" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILGLOBALHANDLER_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_2C4E5C_MAILGLOBALHANDLER_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_2C4E5C_MAILGLOBALHANDLER_ID_seq" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILGLOBALHANDLER_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_2C4E5C_MAILGLOBALHANDLER_ID_seq" OWNED BY "AO_2C4E5C_MAILGLOBALHANDLER"."ID";


--
-- Name: AO_2C4E5C_MAILHANDLER; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_2C4E5C_MAILHANDLER" (
    "CREATED_TIMESTAMP" bigint DEFAULT 0,
    "HANDLER_TYPE" character varying(255),
    "ID" integer NOT NULL,
    "MAIL_CHANNEL_ID" integer,
    "MODULE_COMPLETE_KEY" character varying(255),
    "UPDATED_TIMESTAMP" bigint DEFAULT 0
);


ALTER TABLE public."AO_2C4E5C_MAILHANDLER" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILHANDLER_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_2C4E5C_MAILHANDLER_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_2C4E5C_MAILHANDLER_ID_seq" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILHANDLER_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_2C4E5C_MAILHANDLER_ID_seq" OWNED BY "AO_2C4E5C_MAILHANDLER"."ID";


--
-- Name: AO_2C4E5C_MAILITEM; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_2C4E5C_MAILITEM" (
    "CREATED_TIMESTAMP" bigint DEFAULT 0,
    "FROM_ADDRESS" character varying(255),
    "ID" integer NOT NULL,
    "MAIL_CONNECTION_ID" integer DEFAULT 0 NOT NULL,
    "STATUS" character varying(255),
    "SUBJECT" character varying(255),
    "UPDATED_TIMESTAMP" bigint DEFAULT 0
);


ALTER TABLE public."AO_2C4E5C_MAILITEM" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILITEMAUDIT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_2C4E5C_MAILITEMAUDIT" (
    "CREATED_TIMESTAMP" bigint DEFAULT 0,
    "FROM_ADDRESS" character varying(255),
    "HANDLER_NAME_KEY" character varying(255),
    "ID" integer NOT NULL,
    "ISSUE_KEY" character varying(255),
    "MAIL_CHANNEL_ID" integer DEFAULT 0,
    "MAIL_CHANNEL_NAME" character varying(255),
    "MAIL_ITEM_ID" integer,
    "MESSAGE" character varying(255),
    "NO_OF_RETRY" integer DEFAULT 0,
    "RESULT_STATUS" character varying(255),
    "SUBJECT" character varying(255),
    "UPDATED_TIMESTAMP" bigint DEFAULT 0
);


ALTER TABLE public."AO_2C4E5C_MAILITEMAUDIT" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILITEMAUDIT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_2C4E5C_MAILITEMAUDIT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_2C4E5C_MAILITEMAUDIT_ID_seq" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILITEMAUDIT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_2C4E5C_MAILITEMAUDIT_ID_seq" OWNED BY "AO_2C4E5C_MAILITEMAUDIT"."ID";


--
-- Name: AO_2C4E5C_MAILITEMCHUNK; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_2C4E5C_MAILITEMCHUNK" (
    "ID" integer NOT NULL,
    "MAIL_ITEM_ID" integer,
    "MIME_MSG_CHUNK" text,
    "MIME_MSG_CHUNK_IDX" integer DEFAULT 0
);


ALTER TABLE public."AO_2C4E5C_MAILITEMCHUNK" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILITEMCHUNK_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_2C4E5C_MAILITEMCHUNK_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_2C4E5C_MAILITEMCHUNK_ID_seq" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILITEMCHUNK_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_2C4E5C_MAILITEMCHUNK_ID_seq" OWNED BY "AO_2C4E5C_MAILITEMCHUNK"."ID";


--
-- Name: AO_2C4E5C_MAILITEM_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_2C4E5C_MAILITEM_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_2C4E5C_MAILITEM_ID_seq" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILITEM_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_2C4E5C_MAILITEM_ID_seq" OWNED BY "AO_2C4E5C_MAILITEM"."ID";


--
-- Name: AO_2C4E5C_MAILRUNAUDIT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_2C4E5C_MAILRUNAUDIT" (
    "CREATED_TIMESTAMP" bigint DEFAULT 0,
    "FAILURE_MESSAGE" character varying(255),
    "ID" integer NOT NULL,
    "MAIL_CHANNEL_NAME" character varying(255),
    "MAIL_CONNECTION_ID" integer DEFAULT 0 NOT NULL,
    "NO_OF_RETRY" integer DEFAULT 0,
    "RUN_OUTCOME" character varying(255),
    "UPDATED_TIMESTAMP" bigint DEFAULT 0
);


ALTER TABLE public."AO_2C4E5C_MAILRUNAUDIT" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILRUNAUDIT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_2C4E5C_MAILRUNAUDIT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_2C4E5C_MAILRUNAUDIT_ID_seq" OWNER TO jira;

--
-- Name: AO_2C4E5C_MAILRUNAUDIT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_2C4E5C_MAILRUNAUDIT_ID_seq" OWNED BY "AO_2C4E5C_MAILRUNAUDIT"."ID";


--
-- Name: AO_319474_MESSAGE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_319474_MESSAGE" (
    "CLAIMANT" character varying(127),
    "CLAIMANT_TIME" bigint,
    "CLAIM_COUNT" integer NOT NULL,
    "CONTENT_TYPE" character varying(255) NOT NULL,
    "CREATED_TIME" bigint NOT NULL,
    "EXPIRY_TIME" bigint,
    "ID" bigint NOT NULL,
    "MSG_DATA" text,
    "MSG_ID" character varying(127) NOT NULL,
    "MSG_LENGTH" bigint NOT NULL,
    "PRIORITY" integer NOT NULL,
    "QUEUE_ID" bigint NOT NULL,
    "VERSION" integer NOT NULL
);


ALTER TABLE public."AO_319474_MESSAGE" OWNER TO jira;

--
-- Name: AO_319474_MESSAGE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_319474_MESSAGE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_319474_MESSAGE_ID_seq" OWNER TO jira;

--
-- Name: AO_319474_MESSAGE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_319474_MESSAGE_ID_seq" OWNED BY "AO_319474_MESSAGE"."ID";


--
-- Name: AO_319474_MESSAGE_PROPERTY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_319474_MESSAGE_PROPERTY" (
    "ID" bigint NOT NULL,
    "LONG_VALUE" bigint,
    "MESSAGE_ID" bigint NOT NULL,
    "NAME" character varying(450) NOT NULL,
    "PROPERTY_TYPE" character varying(1) NOT NULL,
    "STRING_VALUE" character varying(450)
);


ALTER TABLE public."AO_319474_MESSAGE_PROPERTY" OWNER TO jira;

--
-- Name: AO_319474_MESSAGE_PROPERTY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_319474_MESSAGE_PROPERTY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_319474_MESSAGE_PROPERTY_ID_seq" OWNER TO jira;

--
-- Name: AO_319474_MESSAGE_PROPERTY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_319474_MESSAGE_PROPERTY_ID_seq" OWNED BY "AO_319474_MESSAGE_PROPERTY"."ID";


--
-- Name: AO_319474_QUEUE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_319474_QUEUE" (
    "CLAIMANT" character varying(127),
    "CLAIMANT_TIME" bigint,
    "CREATED_TIME" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "MESSAGE_COUNT" bigint NOT NULL,
    "MODIFIED_TIME" bigint NOT NULL,
    "NAME" character varying(255) NOT NULL,
    "PURPOSE" character varying(450) NOT NULL,
    "TOPIC" character varying(255)
);


ALTER TABLE public."AO_319474_QUEUE" OWNER TO jira;

--
-- Name: AO_319474_QUEUE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_319474_QUEUE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_319474_QUEUE_ID_seq" OWNER TO jira;

--
-- Name: AO_319474_QUEUE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_319474_QUEUE_ID_seq" OWNED BY "AO_319474_QUEUE"."ID";


--
-- Name: AO_319474_QUEUE_PROPERTY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_319474_QUEUE_PROPERTY" (
    "ID" bigint NOT NULL,
    "LONG_VALUE" bigint,
    "NAME" character varying(450) NOT NULL,
    "PROPERTY_TYPE" character varying(1) NOT NULL,
    "QUEUE_ID" bigint NOT NULL,
    "STRING_VALUE" character varying(450)
);


ALTER TABLE public."AO_319474_QUEUE_PROPERTY" OWNER TO jira;

--
-- Name: AO_319474_QUEUE_PROPERTY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_319474_QUEUE_PROPERTY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_319474_QUEUE_PROPERTY_ID_seq" OWNER TO jira;

--
-- Name: AO_319474_QUEUE_PROPERTY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_319474_QUEUE_PROPERTY_ID_seq" OWNED BY "AO_319474_QUEUE_PROPERTY"."ID";


--
-- Name: AO_38321B_CUSTOM_CONTENT_LINK; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_38321B_CUSTOM_CONTENT_LINK" (
    "CONTENT_KEY" character varying(255),
    "ID" integer NOT NULL,
    "LINK_LABEL" character varying(255),
    "LINK_URL" character varying(255),
    "SEQUENCE" integer DEFAULT 0
);


ALTER TABLE public."AO_38321B_CUSTOM_CONTENT_LINK" OWNER TO jira;

--
-- Name: AO_38321B_CUSTOM_CONTENT_LINK_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_38321B_CUSTOM_CONTENT_LINK_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_38321B_CUSTOM_CONTENT_LINK_ID_seq" OWNER TO jira;

--
-- Name: AO_38321B_CUSTOM_CONTENT_LINK_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_38321B_CUSTOM_CONTENT_LINK_ID_seq" OWNED BY "AO_38321B_CUSTOM_CONTENT_LINK"."ID";


--
-- Name: AO_3B1893_LOOP_DETECTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_3B1893_LOOP_DETECTION" (
    "COUNTER" integer DEFAULT 0 NOT NULL,
    "EXPIRES_AT" bigint DEFAULT 0 NOT NULL,
    "ID" integer NOT NULL,
    "SENDER_EMAIL" text NOT NULL
);


ALTER TABLE public."AO_3B1893_LOOP_DETECTION" OWNER TO jira;

--
-- Name: AO_3B1893_LOOP_DETECTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_3B1893_LOOP_DETECTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_3B1893_LOOP_DETECTION_ID_seq" OWNER TO jira;

--
-- Name: AO_3B1893_LOOP_DETECTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_3B1893_LOOP_DETECTION_ID_seq" OWNED BY "AO_3B1893_LOOP_DETECTION"."ID";


--
-- Name: AO_4789DD_HEALTH_CHECK_STATUS; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_4789DD_HEALTH_CHECK_STATUS" (
    "APPLICATION_NAME" character varying(255),
    "COMPLETE_KEY" character varying(255),
    "DESCRIPTION" text,
    "FAILED_DATE" timestamp without time zone,
    "FAILURE_REASON" text,
    "ID" integer NOT NULL,
    "IS_HEALTHY" boolean,
    "IS_RESOLVED" boolean,
    "RESOLVED_DATE" timestamp without time zone,
    "SEVERITY" character varying(255),
    "STATUS_NAME" character varying(255) NOT NULL
);


ALTER TABLE public."AO_4789DD_HEALTH_CHECK_STATUS" OWNER TO jira;

--
-- Name: AO_4789DD_HEALTH_CHECK_STATUS_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_4789DD_HEALTH_CHECK_STATUS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_4789DD_HEALTH_CHECK_STATUS_ID_seq" OWNER TO jira;

--
-- Name: AO_4789DD_HEALTH_CHECK_STATUS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_4789DD_HEALTH_CHECK_STATUS_ID_seq" OWNED BY "AO_4789DD_HEALTH_CHECK_STATUS"."ID";


--
-- Name: AO_4789DD_PROPERTIES; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_4789DD_PROPERTIES" (
    "ID" integer NOT NULL,
    "PROPERTY_NAME" character varying(255) NOT NULL,
    "PROPERTY_VALUE" character varying(255) NOT NULL
);


ALTER TABLE public."AO_4789DD_PROPERTIES" OWNER TO jira;

--
-- Name: AO_4789DD_PROPERTIES_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_4789DD_PROPERTIES_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_4789DD_PROPERTIES_ID_seq" OWNER TO jira;

--
-- Name: AO_4789DD_PROPERTIES_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_4789DD_PROPERTIES_ID_seq" OWNED BY "AO_4789DD_PROPERTIES"."ID";


--
-- Name: AO_4789DD_READ_NOTIFICATIONS; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_4789DD_READ_NOTIFICATIONS" (
    "ID" integer NOT NULL,
    "IS_SNOOZED" boolean,
    "NOTIFICATION_ID" integer NOT NULL,
    "SNOOZE_COUNT" integer,
    "SNOOZE_DATE" timestamp without time zone,
    "USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_4789DD_READ_NOTIFICATIONS" OWNER TO jira;

--
-- Name: AO_4789DD_READ_NOTIFICATIONS_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_4789DD_READ_NOTIFICATIONS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_4789DD_READ_NOTIFICATIONS_ID_seq" OWNER TO jira;

--
-- Name: AO_4789DD_READ_NOTIFICATIONS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_4789DD_READ_NOTIFICATIONS_ID_seq" OWNED BY "AO_4789DD_READ_NOTIFICATIONS"."ID";


--
-- Name: AO_4789DD_TASK_MONITOR; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_4789DD_TASK_MONITOR" (
    "CLUSTERED_TASK_ID" character varying(255),
    "CREATED_TIMESTAMP" bigint DEFAULT 0,
    "ID" integer NOT NULL,
    "NODE_ID" character varying(255),
    "PROGRESS_MESSAGE" text,
    "PROGRESS_PERCENTAGE" integer,
    "SERIALIZED_ERRORS" text,
    "SERIALIZED_WARNINGS" text,
    "TASK_ID" character varying(255) NOT NULL,
    "TASK_MONITOR_KIND" character varying(255),
    "TASK_STATUS" text
);


ALTER TABLE public."AO_4789DD_TASK_MONITOR" OWNER TO jira;

--
-- Name: AO_4789DD_TASK_MONITOR_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_4789DD_TASK_MONITOR_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_4789DD_TASK_MONITOR_ID_seq" OWNER TO jira;

--
-- Name: AO_4789DD_TASK_MONITOR_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_4789DD_TASK_MONITOR_ID_seq" OWNED BY "AO_4789DD_TASK_MONITOR"."ID";


--
-- Name: AO_4AEACD_WEBHOOK_DAO; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_4AEACD_WEBHOOK_DAO" (
    "ENABLED" boolean,
    "ENCODED_EVENTS" text,
    "FILTER" text,
    "ID" integer NOT NULL,
    "JQL" character varying(255),
    "LAST_UPDATED" timestamp without time zone NOT NULL,
    "LAST_UPDATED_USER" character varying(255) NOT NULL,
    "NAME" text NOT NULL,
    "REGISTRATION_METHOD" character varying(255) NOT NULL,
    "URL" text NOT NULL,
    "PARAMETERS" text,
    "EXCLUDE_ISSUE_DETAILS" boolean
);


ALTER TABLE public."AO_4AEACD_WEBHOOK_DAO" OWNER TO jira;

--
-- Name: AO_4AEACD_WEBHOOK_DAO_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_4AEACD_WEBHOOK_DAO_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_4AEACD_WEBHOOK_DAO_ID_seq" OWNER TO jira;

--
-- Name: AO_4AEACD_WEBHOOK_DAO_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_4AEACD_WEBHOOK_DAO_ID_seq" OWNED BY "AO_4AEACD_WEBHOOK_DAO"."ID";


--
-- Name: AO_4E8AE6_NOTIF_BATCH_QUEUE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_4E8AE6_NOTIF_BATCH_QUEUE" (
    "AUTHOR_ID" bigint,
    "CONTEXT" character varying(63),
    "EVENT_TIME" bigint NOT NULL,
    "HTML_CONTENT" text,
    "ID" bigint NOT NULL,
    "ISSUE_ID" bigint,
    "PROJECT_ID" bigint NOT NULL,
    "RECIPIENT_ID" bigint NOT NULL,
    "SENT_TIME" bigint,
    "TEXT_CONTENT" text
);


ALTER TABLE public."AO_4E8AE6_NOTIF_BATCH_QUEUE" OWNER TO jira;

--
-- Name: AO_4E8AE6_NOTIF_BATCH_QUEUE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_4E8AE6_NOTIF_BATCH_QUEUE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_4E8AE6_NOTIF_BATCH_QUEUE_ID_seq" OWNER TO jira;

--
-- Name: AO_4E8AE6_NOTIF_BATCH_QUEUE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_4E8AE6_NOTIF_BATCH_QUEUE_ID_seq" OWNED BY "AO_4E8AE6_NOTIF_BATCH_QUEUE"."ID";


--
-- Name: AO_4E8AE6_OUT_EMAIL_SETTINGS; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_4E8AE6_OUT_EMAIL_SETTINGS" (
    "CSS" text,
    "HTML_LINGO_ID" bigint,
    "ID" integer NOT NULL,
    "PLAINTEXT_LINGO_ID" bigint,
    "PROJECT_ID" bigint NOT NULL,
    "SUBJECT_LINGO_ID" bigint
);


ALTER TABLE public."AO_4E8AE6_OUT_EMAIL_SETTINGS" OWNER TO jira;

--
-- Name: AO_4E8AE6_OUT_EMAIL_SETTINGS_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_4E8AE6_OUT_EMAIL_SETTINGS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_4E8AE6_OUT_EMAIL_SETTINGS_ID_seq" OWNER TO jira;

--
-- Name: AO_4E8AE6_OUT_EMAIL_SETTINGS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_4E8AE6_OUT_EMAIL_SETTINGS_ID_seq" OWNED BY "AO_4E8AE6_OUT_EMAIL_SETTINGS"."ID";


--
-- Name: AO_54307E_ASYNCUPGRADERECORD; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_ASYNCUPGRADERECORD" (
    "ACTION" character varying(255) NOT NULL,
    "CREATED_DATE" timestamp without time zone NOT NULL,
    "EXCEPTION" text,
    "ID" integer NOT NULL,
    "MESSAGE" text,
    "SERVICE_DESK_VERSION" character varying(255) NOT NULL,
    "UPGRADE_TASK_NAME" character varying(255) NOT NULL
);


ALTER TABLE public."AO_54307E_ASYNCUPGRADERECORD" OWNER TO jira;

--
-- Name: AO_54307E_ASYNCUPGRADERECORD_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_ASYNCUPGRADERECORD_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_ASYNCUPGRADERECORD_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_ASYNCUPGRADERECORD_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_ASYNCUPGRADERECORD_ID_seq" OWNED BY "AO_54307E_ASYNCUPGRADERECORD"."ID";


--
-- Name: AO_54307E_CAPABILITY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_CAPABILITY" (
    "CAPABILITY_NAME" character varying(255) NOT NULL,
    "CAPABILITY_VALUE" character varying(255) NOT NULL,
    "ID" integer NOT NULL,
    "SERVICE_DESK_ID" integer NOT NULL,
    "USER_KEY" character varying(255)
);


ALTER TABLE public."AO_54307E_CAPABILITY" OWNER TO jira;

--
-- Name: AO_54307E_CAPABILITY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_CAPABILITY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_CAPABILITY_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_CAPABILITY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_CAPABILITY_ID_seq" OWNED BY "AO_54307E_CAPABILITY"."ID";


--
-- Name: AO_54307E_CONFLUENCEKB; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_CONFLUENCEKB" (
    "APPLINKS_APPLICATION_ID" character varying(63),
    "APPLINK_NAME" character varying(255),
    "APPLINK_URL" text,
    "ID" integer NOT NULL,
    "SERVICE_DESK_ID" integer NOT NULL,
    "SPACE_KEY" character varying(255),
    "SPACE_NAME" character varying(255),
    "SPACE_URL" text
);


ALTER TABLE public."AO_54307E_CONFLUENCEKB" OWNER TO jira;

--
-- Name: AO_54307E_CONFLUENCEKBENABLED; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_CONFLUENCEKBENABLED" (
    "CONFLUENCE_KBID" integer NOT NULL,
    "ENABLED" boolean,
    "FORM_ID" integer NOT NULL,
    "ID" integer NOT NULL,
    "SERVICE_DESK_ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_CONFLUENCEKBENABLED" OWNER TO jira;

--
-- Name: AO_54307E_CONFLUENCEKBENABLED_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_CONFLUENCEKBENABLED_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_CONFLUENCEKBENABLED_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_CONFLUENCEKBENABLED_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_CONFLUENCEKBENABLED_ID_seq" OWNED BY "AO_54307E_CONFLUENCEKBENABLED"."ID";


--
-- Name: AO_54307E_CONFLUENCEKBLABELS; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_CONFLUENCEKBLABELS" (
    "CONFLUENCE_KBID" integer NOT NULL,
    "FORM_ID" integer NOT NULL,
    "ID" integer NOT NULL,
    "LABEL" character varying(255),
    "SERVICE_DESK_ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_CONFLUENCEKBLABELS" OWNER TO jira;

--
-- Name: AO_54307E_CONFLUENCEKBLABELS_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_CONFLUENCEKBLABELS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_CONFLUENCEKBLABELS_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_CONFLUENCEKBLABELS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_CONFLUENCEKBLABELS_ID_seq" OWNED BY "AO_54307E_CONFLUENCEKBLABELS"."ID";


--
-- Name: AO_54307E_CONFLUENCEKB_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_CONFLUENCEKB_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_CONFLUENCEKB_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_CONFLUENCEKB_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_CONFLUENCEKB_ID_seq" OWNED BY "AO_54307E_CONFLUENCEKB"."ID";


--
-- Name: AO_54307E_CUSTOMGLOBALTHEME; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_CUSTOMGLOBALTHEME" (
    "CONTENT_LINK_COLOR" character varying(255),
    "CONTENT_TEXT_COLOR" character varying(255),
    "CUSTOM_CSS" text,
    "HEADER_BADGE_BGCOLOR" character varying(255),
    "HEADER_BGCOLOR" character varying(255),
    "HEADER_LINK_COLOR" character varying(255),
    "HEADER_LINK_HOVER_BGCOLOR" character varying(255),
    "HEADER_LINK_HOVER_COLOR" character varying(255),
    "HELP_CENTER_TITLE" character varying(255),
    "ID" integer NOT NULL,
    "LOGO_ID" integer
);


ALTER TABLE public."AO_54307E_CUSTOMGLOBALTHEME" OWNER TO jira;

--
-- Name: AO_54307E_CUSTOMGLOBALTHEME_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_CUSTOMGLOBALTHEME_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_CUSTOMGLOBALTHEME_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_CUSTOMGLOBALTHEME_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_CUSTOMGLOBALTHEME_ID_seq" OWNED BY "AO_54307E_CUSTOMGLOBALTHEME"."ID";


--
-- Name: AO_54307E_CUSTOMTHEME; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_CUSTOMTHEME" (
    "HEADER_BGCOLOR" character varying(255),
    "HEADER_LINK_COLOR" character varying(255),
    "HEADER_LINK_HOVER_BGCOLOR" character varying(255),
    "HEADER_LINK_HOVER_COLOR" character varying(255),
    "ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_CUSTOMTHEME" OWNER TO jira;

--
-- Name: AO_54307E_CUSTOMTHEME_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_CUSTOMTHEME_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_CUSTOMTHEME_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_CUSTOMTHEME_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_CUSTOMTHEME_ID_seq" OWNED BY "AO_54307E_CUSTOMTHEME"."ID";


--
-- Name: AO_54307E_EMAILCHANNELSETTING; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_EMAILCHANNELSETTING" (
    "EMAIL_ADDRESS" character varying(255),
    "ID" integer NOT NULL,
    "LAST_PROCEEDED_TIME" bigint,
    "MAIL_CHANNEL_KEY" character varying(255) NOT NULL,
    "ON_DEMAND" boolean,
    "REQUEST_TYPE_ID" integer NOT NULL,
    "SERVICE_DESK_ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_EMAILCHANNELSETTING" OWNER TO jira;

--
-- Name: AO_54307E_EMAILCHANNELSETTING_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_EMAILCHANNELSETTING_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_EMAILCHANNELSETTING_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_EMAILCHANNELSETTING_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_EMAILCHANNELSETTING_ID_seq" OWNED BY "AO_54307E_EMAILCHANNELSETTING"."ID";


--
-- Name: AO_54307E_EMAILSETTINGS; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_EMAILSETTINGS" (
    "EMAIL_ADDRESS" character varying(255),
    "ENABLED" boolean,
    "ID" integer NOT NULL,
    "JIRA_MAIL_SERVER_ID" bigint NOT NULL,
    "LAST_PROCEEDED_TIME" bigint,
    "ON_DEMAND" boolean,
    "REQUEST_TYPE_ID" integer NOT NULL,
    "SERVICE_DESK_ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_EMAILSETTINGS" OWNER TO jira;

--
-- Name: AO_54307E_EMAILSETTINGS_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_EMAILSETTINGS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_EMAILSETTINGS_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_EMAILSETTINGS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_EMAILSETTINGS_ID_seq" OWNED BY "AO_54307E_EMAILSETTINGS"."ID";


--
-- Name: AO_54307E_GOAL; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_GOAL" (
    "CALENDAR_ID" integer,
    "DEFAULT_GOAL" boolean,
    "ID" integer NOT NULL,
    "JQL_QUERY" text,
    "POS" integer DEFAULT 0 NOT NULL,
    "TARGET_DURATION" bigint,
    "TIME_METRIC_ID" integer NOT NULL,
    "TIME_UPDATED_DATE" timestamp without time zone,
    "TIME_UPDATED_MS_EPOCH" bigint
);


ALTER TABLE public."AO_54307E_GOAL" OWNER TO jira;

--
-- Name: AO_54307E_GOAL_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_GOAL_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_GOAL_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_GOAL_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_GOAL_ID_seq" OWNED BY "AO_54307E_GOAL"."ID";


--
-- Name: AO_54307E_GROUP; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_GROUP" (
    "DELETED_TIME" bigint,
    "GROUP_NAME" character varying(127),
    "ID" integer NOT NULL,
    "ORDER" integer,
    "VIEWPORT_ID" integer
);


ALTER TABLE public."AO_54307E_GROUP" OWNER TO jira;

--
-- Name: AO_54307E_GROUPTOREQUESTTYPE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_GROUPTOREQUESTTYPE" (
    "GROUP_ID" integer,
    "ID" integer NOT NULL,
    "ORDER" integer,
    "REQUEST_TYPE_ID" integer
);


ALTER TABLE public."AO_54307E_GROUPTOREQUESTTYPE" OWNER TO jira;

--
-- Name: AO_54307E_GROUPTOREQUESTTYPE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_GROUPTOREQUESTTYPE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_GROUPTOREQUESTTYPE_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_GROUPTOREQUESTTYPE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_GROUPTOREQUESTTYPE_ID_seq" OWNED BY "AO_54307E_GROUPTOREQUESTTYPE"."ID";


--
-- Name: AO_54307E_GROUP_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_GROUP_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_GROUP_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_GROUP_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_GROUP_ID_seq" OWNED BY "AO_54307E_GROUP"."ID";


--
-- Name: AO_54307E_IMAGES; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_IMAGES" (
    "CONTENTS" text,
    "ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_IMAGES" OWNER TO jira;

--
-- Name: AO_54307E_IMAGES_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_IMAGES_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_IMAGES_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_IMAGES_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_IMAGES_ID_seq" OWNED BY "AO_54307E_IMAGES"."ID";


--
-- Name: AO_54307E_METRICCONDITION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_METRICCONDITION" (
    "CONDITION_ID" character varying(255) NOT NULL,
    "FACTORY_KEY" character varying(255) NOT NULL,
    "ID" integer NOT NULL,
    "PLUGIN_KEY" character varying(255) NOT NULL,
    "TIME_METRIC_ID" integer NOT NULL,
    "TYPE_NAME" character varying(255) NOT NULL
);


ALTER TABLE public."AO_54307E_METRICCONDITION" OWNER TO jira;

--
-- Name: AO_54307E_METRICCONDITION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_METRICCONDITION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_METRICCONDITION_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_METRICCONDITION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_METRICCONDITION_ID_seq" OWNED BY "AO_54307E_METRICCONDITION"."ID";


--
-- Name: AO_54307E_ORGANIZATION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_ORGANIZATION" (
    "ID" integer NOT NULL,
    "LOWER_NAME" character varying(255) NOT NULL,
    "NAME" character varying(255) NOT NULL,
    "SEARCH_NAME" character varying(255) NOT NULL
);


ALTER TABLE public."AO_54307E_ORGANIZATION" OWNER TO jira;

--
-- Name: AO_54307E_ORGANIZATION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_ORGANIZATION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_ORGANIZATION_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_ORGANIZATION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_ORGANIZATION_ID_seq" OWNED BY "AO_54307E_ORGANIZATION"."ID";


--
-- Name: AO_54307E_ORGANIZATION_MEMBER; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_ORGANIZATION_MEMBER" (
    "DEFAULT_ORGANIZATION" boolean DEFAULT false NOT NULL,
    "ID" integer NOT NULL,
    "ORGANIZATION_ID" integer,
    "USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_54307E_ORGANIZATION_MEMBER" OWNER TO jira;

--
-- Name: AO_54307E_ORGANIZATION_MEMBER_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_ORGANIZATION_MEMBER_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_ORGANIZATION_MEMBER_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_ORGANIZATION_MEMBER_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_ORGANIZATION_MEMBER_ID_seq" OWNED BY "AO_54307E_ORGANIZATION_MEMBER"."ID";


--
-- Name: AO_54307E_ORGANIZATION_PROJECT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_ORGANIZATION_PROJECT" (
    "ID" integer NOT NULL,
    "ORGANIZATION_ID" integer,
    "PROJECT_ID" bigint NOT NULL
);


ALTER TABLE public."AO_54307E_ORGANIZATION_PROJECT" OWNER TO jira;

--
-- Name: AO_54307E_ORGANIZATION_PROJECT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_ORGANIZATION_PROJECT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_ORGANIZATION_PROJECT_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_ORGANIZATION_PROJECT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_ORGANIZATION_PROJECT_ID_seq" OWNED BY "AO_54307E_ORGANIZATION_PROJECT"."ID";


--
-- Name: AO_54307E_OUT_EMAIL_SETTINGS; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_OUT_EMAIL_SETTINGS" (
    "EMAIL_SUBJECT_PREFIX_ENABLED" boolean NOT NULL,
    "ID" integer NOT NULL,
    "SERVICE_DESK_ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_OUT_EMAIL_SETTINGS" OWNER TO jira;

--
-- Name: AO_54307E_OUT_EMAIL_SETTINGS_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_OUT_EMAIL_SETTINGS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_OUT_EMAIL_SETTINGS_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_OUT_EMAIL_SETTINGS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_OUT_EMAIL_SETTINGS_ID_seq" OWNED BY "AO_54307E_OUT_EMAIL_SETTINGS"."ID";


--
-- Name: AO_54307E_PARTICIPANTSETTINGS; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_PARTICIPANTSETTINGS" (
    "AUTOCOMPLETE_ENABLED" boolean,
    "ID" integer NOT NULL,
    "MANAGE_ENABLED" boolean,
    "SERVICE_DESK_ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_PARTICIPANTSETTINGS" OWNER TO jira;

--
-- Name: AO_54307E_PARTICIPANTSETTINGS_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_PARTICIPANTSETTINGS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_PARTICIPANTSETTINGS_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_PARTICIPANTSETTINGS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_PARTICIPANTSETTINGS_ID_seq" OWNED BY "AO_54307E_PARTICIPANTSETTINGS"."ID";


--
-- Name: AO_54307E_QUEUE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_QUEUE" (
    "ID" integer NOT NULL,
    "JQL" text,
    "NAME" character varying(255) NOT NULL,
    "PROJECT_ID" bigint NOT NULL,
    "PROJECT_KEY" character varying(255) NOT NULL,
    "QUEUE_ORDER" integer
);


ALTER TABLE public."AO_54307E_QUEUE" OWNER TO jira;

--
-- Name: AO_54307E_QUEUECOLUMN; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_QUEUECOLUMN" (
    "COLUMN_ID" character varying(255),
    "COLUMN_ORDER" integer,
    "ID" integer NOT NULL,
    "QUEUE_ID" integer
);


ALTER TABLE public."AO_54307E_QUEUECOLUMN" OWNER TO jira;

--
-- Name: AO_54307E_QUEUECOLUMN_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_QUEUECOLUMN_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_QUEUECOLUMN_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_QUEUECOLUMN_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_QUEUECOLUMN_ID_seq" OWNED BY "AO_54307E_QUEUECOLUMN"."ID";


--
-- Name: AO_54307E_QUEUE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_QUEUE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_QUEUE_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_QUEUE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_QUEUE_ID_seq" OWNED BY "AO_54307E_QUEUE"."ID";


--
-- Name: AO_54307E_REPORT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_REPORT" (
    "CREATED_DATE" timestamp without time zone,
    "ID" integer NOT NULL,
    "NAME" character varying(255),
    "REPORT_ORDER" integer,
    "REPORT_TYPE" character varying(63),
    "SERVICE_DESK_ID" integer,
    "TARGET" bigint,
    "UPDATED_DATE" timestamp without time zone
);


ALTER TABLE public."AO_54307E_REPORT" OWNER TO jira;

--
-- Name: AO_54307E_REPORT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_REPORT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_REPORT_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_REPORT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_REPORT_ID_seq" OWNED BY "AO_54307E_REPORT"."ID";


--
-- Name: AO_54307E_SERIES; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_SERIES" (
    "COLOR" character varying(63),
    "CREATED_DATE" timestamp without time zone,
    "GOAL_ID" integer,
    "ID" integer NOT NULL,
    "JQL" text,
    "REPORT_ID" integer,
    "SERIES_DATA_TYPE" character varying(255),
    "SERIES_LABEL" character varying(63),
    "TIME_METRIC_ID" bigint,
    "UPDATED_DATE" timestamp without time zone
);


ALTER TABLE public."AO_54307E_SERIES" OWNER TO jira;

--
-- Name: AO_54307E_SERIES_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_SERIES_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_SERIES_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_SERIES_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_SERIES_ID_seq" OWNED BY "AO_54307E_SERIES"."ID";


--
-- Name: AO_54307E_SERVICEDESK; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_SERVICEDESK" (
    "CREATED_BY_USER_KEY" character varying(255),
    "CREATED_DATE" timestamp without time zone,
    "CREATED_WITH_EMPTY_PROJECT" boolean,
    "DISABLED" boolean,
    "ID" integer NOT NULL,
    "LEGACY_TRANSITION_DISABLED" boolean,
    "OPEN_CUSTOMER_ACCESS" integer,
    "PROJECT_ID" bigint,
    "PROJECT_KEY" character varying(255) DEFAULT 'N/A'::character varying NOT NULL,
    "PUBLIC_SIGNUP" integer,
    "VERSION_CREATED_AT" character varying(255)
);


ALTER TABLE public."AO_54307E_SERVICEDESK" OWNER TO jira;

--
-- Name: AO_54307E_SERVICEDESK_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_SERVICEDESK_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_SERVICEDESK_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_SERVICEDESK_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_SERVICEDESK_ID_seq" OWNED BY "AO_54307E_SERVICEDESK"."ID";


--
-- Name: AO_54307E_SLAAUDITLOG; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_SLAAUDITLOG" (
    "EVENT_TIME" bigint,
    "ID" bigint NOT NULL,
    "ISSUE_ID" bigint,
    "REASON" character varying(255),
    "SLA_ID" bigint
);


ALTER TABLE public."AO_54307E_SLAAUDITLOG" OWNER TO jira;

--
-- Name: AO_54307E_SLAAUDITLOGDATA; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_SLAAUDITLOGDATA" (
    "ID" bigint NOT NULL,
    "KEY" character varying(255),
    "SLA_AUDIT_LOG_ID" bigint NOT NULL,
    "VALUE" character varying(255)
);


ALTER TABLE public."AO_54307E_SLAAUDITLOGDATA" OWNER TO jira;

--
-- Name: AO_54307E_SLAAUDITLOGDATA_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_SLAAUDITLOGDATA_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_SLAAUDITLOGDATA_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_SLAAUDITLOGDATA_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_SLAAUDITLOGDATA_ID_seq" OWNED BY "AO_54307E_SLAAUDITLOGDATA"."ID";


--
-- Name: AO_54307E_SLAAUDITLOG_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_SLAAUDITLOG_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_SLAAUDITLOG_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_SLAAUDITLOG_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_SLAAUDITLOG_ID_seq" OWNED BY "AO_54307E_SLAAUDITLOG"."ID";


--
-- Name: AO_54307E_STATUSMAPPING; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_STATUSMAPPING" (
    "FORM_ID" integer,
    "ID" integer NOT NULL,
    "STATUS_ID" character varying(255),
    "STATUS_NAME" character varying(255)
);


ALTER TABLE public."AO_54307E_STATUSMAPPING" OWNER TO jira;

--
-- Name: AO_54307E_STATUSMAPPING_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_STATUSMAPPING_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_STATUSMAPPING_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_STATUSMAPPING_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_STATUSMAPPING_ID_seq" OWNED BY "AO_54307E_STATUSMAPPING"."ID";


--
-- Name: AO_54307E_SUBSCRIPTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_SUBSCRIPTION" (
    "ID" integer NOT NULL,
    "ISSUE_ID" bigint NOT NULL,
    "SUBSCRIBED" boolean NOT NULL,
    "USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_54307E_SUBSCRIPTION" OWNER TO jira;

--
-- Name: AO_54307E_SUBSCRIPTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_SUBSCRIPTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_SUBSCRIPTION_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_SUBSCRIPTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_SUBSCRIPTION_ID_seq" OWNED BY "AO_54307E_SUBSCRIPTION"."ID";


--
-- Name: AO_54307E_SYNCUPGRADERECORD; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_SYNCUPGRADERECORD" (
    "ACTION" character varying(255) NOT NULL,
    "CREATED_DATE" timestamp without time zone NOT NULL,
    "EXCEPTION" text,
    "ID" integer NOT NULL,
    "MESSAGE" text,
    "SERVICE_DESK_VERSION" character varying(255) NOT NULL,
    "UPGRADE_TASK_NAME" character varying(255) NOT NULL
);


ALTER TABLE public."AO_54307E_SYNCUPGRADERECORD" OWNER TO jira;

--
-- Name: AO_54307E_SYNCUPGRADERECORD_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_SYNCUPGRADERECORD_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_SYNCUPGRADERECORD_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_SYNCUPGRADERECORD_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_SYNCUPGRADERECORD_ID_seq" OWNED BY "AO_54307E_SYNCUPGRADERECORD"."ID";


--
-- Name: AO_54307E_THRESHOLD; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_THRESHOLD" (
    "ID" integer NOT NULL,
    "REMAINING_TIME" bigint NOT NULL,
    "TIME_METRIC_ID" integer NOT NULL
);


ALTER TABLE public."AO_54307E_THRESHOLD" OWNER TO jira;

--
-- Name: AO_54307E_THRESHOLD_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_THRESHOLD_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_THRESHOLD_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_THRESHOLD_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_THRESHOLD_ID_seq" OWNED BY "AO_54307E_THRESHOLD"."ID";


--
-- Name: AO_54307E_TIMEMETRIC; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_TIMEMETRIC" (
    "CREATED_DATE" bigint,
    "CUSTOM_FIELD_ID" bigint,
    "DEFINITION_CHANGE_DATE" timestamp without time zone,
    "DEFINITION_CHANGE_MS_EPOCH" bigint,
    "GOALS_CHANGE_DATE" timestamp without time zone,
    "GOALS_CHANGE_MS_EPOCH" bigint,
    "ID" integer NOT NULL,
    "NAME" character varying(255) NOT NULL,
    "SERVICE_DESK_ID" integer NOT NULL,
    "THRESHOLDS_CHANGE_MS_EPOCH" bigint,
    "THRESHOLDS_CONFIG_CHANGE_DATE" timestamp without time zone
);


ALTER TABLE public."AO_54307E_TIMEMETRIC" OWNER TO jira;

--
-- Name: AO_54307E_TIMEMETRIC_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_TIMEMETRIC_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_TIMEMETRIC_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_TIMEMETRIC_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_TIMEMETRIC_ID_seq" OWNED BY "AO_54307E_TIMEMETRIC"."ID";


--
-- Name: AO_54307E_VIEWPORT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_VIEWPORT" (
    "DESCRIPTION" text,
    "ID" integer NOT NULL,
    "KEY" character varying(255) NOT NULL,
    "LOGO_ID" integer,
    "NAME" character varying(255) NOT NULL,
    "PROJECT_ID" bigint NOT NULL,
    "SEND_EMAIL_NOTIFICATIONS" boolean,
    "THEME_ID" integer
);


ALTER TABLE public."AO_54307E_VIEWPORT" OWNER TO jira;

--
-- Name: AO_54307E_VIEWPORTFIELD; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_VIEWPORTFIELD" (
    "DESCRIPTION" text,
    "DISPLAYED" boolean,
    "FIELD_ID" character varying(255) NOT NULL,
    "FIELD_ORDER" integer,
    "FIELD_TYPE" character varying(255) NOT NULL,
    "FORM_ID" integer,
    "ID" integer NOT NULL,
    "LABEL" character varying(255),
    "REQUIRED" boolean
);


ALTER TABLE public."AO_54307E_VIEWPORTFIELD" OWNER TO jira;

--
-- Name: AO_54307E_VIEWPORTFIELDVALUE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_VIEWPORTFIELDVALUE" (
    "FIELD_ID" integer,
    "FIELD_NAME" character varying(255),
    "ID" integer NOT NULL,
    "VALUE" text,
    "VALUE_ORDER" integer
);


ALTER TABLE public."AO_54307E_VIEWPORTFIELDVALUE" OWNER TO jira;

--
-- Name: AO_54307E_VIEWPORTFIELDVALUE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_VIEWPORTFIELDVALUE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_VIEWPORTFIELDVALUE_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_VIEWPORTFIELDVALUE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_VIEWPORTFIELDVALUE_ID_seq" OWNED BY "AO_54307E_VIEWPORTFIELDVALUE"."ID";


--
-- Name: AO_54307E_VIEWPORTFIELD_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_VIEWPORTFIELD_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_VIEWPORTFIELD_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_VIEWPORTFIELD_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_VIEWPORTFIELD_ID_seq" OWNED BY "AO_54307E_VIEWPORTFIELD"."ID";


--
-- Name: AO_54307E_VIEWPORTFORM; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_54307E_VIEWPORTFORM" (
    "CALL_TO_ACTION" text,
    "DESCRIPTION" text,
    "FORM_ORDER" integer,
    "ICON" integer,
    "ICON_ID" bigint,
    "ID" integer NOT NULL,
    "INTRO" text,
    "ISSUE_TYPE_ID" bigint NOT NULL,
    "KEY" character varying(255) NOT NULL,
    "NAME" character varying(255) NOT NULL,
    "VIEWPORT_ID" integer
);


ALTER TABLE public."AO_54307E_VIEWPORTFORM" OWNER TO jira;

--
-- Name: AO_54307E_VIEWPORTFORM_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_VIEWPORTFORM_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_VIEWPORTFORM_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_VIEWPORTFORM_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_VIEWPORTFORM_ID_seq" OWNED BY "AO_54307E_VIEWPORTFORM"."ID";


--
-- Name: AO_54307E_VIEWPORT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_54307E_VIEWPORT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_54307E_VIEWPORT_ID_seq" OWNER TO jira;

--
-- Name: AO_54307E_VIEWPORT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_54307E_VIEWPORT_ID_seq" OWNED BY "AO_54307E_VIEWPORT"."ID";


--
-- Name: AO_550953_SHORTCUT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_550953_SHORTCUT" (
    "ID" integer NOT NULL,
    "NAME" character varying(255),
    "PROJECT_ID" bigint,
    "SHORTCUT_URL" text,
    "URL" character varying(255),
    "ICON" character varying(255)
);


ALTER TABLE public."AO_550953_SHORTCUT" OWNER TO jira;

--
-- Name: AO_550953_SHORTCUT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_550953_SHORTCUT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_550953_SHORTCUT_ID_seq" OWNER TO jira;

--
-- Name: AO_550953_SHORTCUT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_550953_SHORTCUT_ID_seq" OWNED BY "AO_550953_SHORTCUT"."ID";


--
-- Name: AO_563AEE_ACTIVITY_ENTITY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_563AEE_ACTIVITY_ENTITY" (
    "ACTIVITY_ID" bigint NOT NULL,
    "ACTOR_ID" integer,
    "CONTENT" text,
    "GENERATOR_DISPLAY_NAME" character varying(255),
    "GENERATOR_ID" character varying(450),
    "ICON_ID" integer,
    "ID" character varying(450),
    "ISSUE_KEY" character varying(255),
    "OBJECT_ID" integer,
    "POSTER" character varying(255),
    "PROJECT_KEY" character varying(255),
    "PUBLISHED" timestamp without time zone,
    "TARGET_ID" integer,
    "TITLE" character varying(255),
    "URL" character varying(450),
    "USERNAME" character varying(255),
    "VERB" character varying(450)
);


ALTER TABLE public."AO_563AEE_ACTIVITY_ENTITY" OWNER TO jira;

--
-- Name: AO_563AEE_ACTIVITY_ENTITY_ACTIVITY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_563AEE_ACTIVITY_ENTITY_ACTIVITY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_563AEE_ACTIVITY_ENTITY_ACTIVITY_ID_seq" OWNER TO jira;

--
-- Name: AO_563AEE_ACTIVITY_ENTITY_ACTIVITY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_563AEE_ACTIVITY_ENTITY_ACTIVITY_ID_seq" OWNED BY "AO_563AEE_ACTIVITY_ENTITY"."ACTIVITY_ID";


--
-- Name: AO_563AEE_ACTOR_ENTITY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_563AEE_ACTOR_ENTITY" (
    "FULL_NAME" character varying(255),
    "ID" integer NOT NULL,
    "PROFILE_PAGE_URI" character varying(450),
    "PROFILE_PICTURE_URI" character varying(450),
    "USERNAME" character varying(255)
);


ALTER TABLE public."AO_563AEE_ACTOR_ENTITY" OWNER TO jira;

--
-- Name: AO_563AEE_ACTOR_ENTITY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_563AEE_ACTOR_ENTITY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_563AEE_ACTOR_ENTITY_ID_seq" OWNER TO jira;

--
-- Name: AO_563AEE_ACTOR_ENTITY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_563AEE_ACTOR_ENTITY_ID_seq" OWNED BY "AO_563AEE_ACTOR_ENTITY"."ID";


--
-- Name: AO_563AEE_MEDIA_LINK_ENTITY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_563AEE_MEDIA_LINK_ENTITY" (
    "DURATION" integer,
    "HEIGHT" integer,
    "ID" integer NOT NULL,
    "URL" character varying(450),
    "WIDTH" integer
);


ALTER TABLE public."AO_563AEE_MEDIA_LINK_ENTITY" OWNER TO jira;

--
-- Name: AO_563AEE_MEDIA_LINK_ENTITY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_563AEE_MEDIA_LINK_ENTITY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_563AEE_MEDIA_LINK_ENTITY_ID_seq" OWNER TO jira;

--
-- Name: AO_563AEE_MEDIA_LINK_ENTITY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_563AEE_MEDIA_LINK_ENTITY_ID_seq" OWNED BY "AO_563AEE_MEDIA_LINK_ENTITY"."ID";


--
-- Name: AO_563AEE_OBJECT_ENTITY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_563AEE_OBJECT_ENTITY" (
    "CONTENT" character varying(255),
    "DISPLAY_NAME" character varying(255),
    "ID" integer NOT NULL,
    "IMAGE_ID" integer,
    "OBJECT_ID" character varying(450),
    "OBJECT_TYPE" character varying(450),
    "SUMMARY" character varying(255),
    "URL" character varying(450)
);


ALTER TABLE public."AO_563AEE_OBJECT_ENTITY" OWNER TO jira;

--
-- Name: AO_563AEE_OBJECT_ENTITY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_563AEE_OBJECT_ENTITY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_563AEE_OBJECT_ENTITY_ID_seq" OWNER TO jira;

--
-- Name: AO_563AEE_OBJECT_ENTITY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_563AEE_OBJECT_ENTITY_ID_seq" OWNED BY "AO_563AEE_OBJECT_ENTITY"."ID";


--
-- Name: AO_563AEE_TARGET_ENTITY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_563AEE_TARGET_ENTITY" (
    "CONTENT" character varying(255),
    "DISPLAY_NAME" character varying(255),
    "ID" integer NOT NULL,
    "IMAGE_ID" integer,
    "OBJECT_ID" character varying(450),
    "OBJECT_TYPE" character varying(450),
    "SUMMARY" character varying(255),
    "URL" character varying(450)
);


ALTER TABLE public."AO_563AEE_TARGET_ENTITY" OWNER TO jira;

--
-- Name: AO_563AEE_TARGET_ENTITY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_563AEE_TARGET_ENTITY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_563AEE_TARGET_ENTITY_ID_seq" OWNER TO jira;

--
-- Name: AO_563AEE_TARGET_ENTITY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_563AEE_TARGET_ENTITY_ID_seq" OWNED BY "AO_563AEE_TARGET_ENTITY"."ID";


--
-- Name: AO_56464C_APPROVAL; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_56464C_APPROVAL" (
    "APPROVE_CONDITION_TYPE" character varying(255) NOT NULL,
    "APPROVE_CONDITION_VALUE" character varying(255) NOT NULL,
    "COMPLETED_DATE" bigint,
    "CREATED_DATE" bigint NOT NULL,
    "DECISION" character varying(255),
    "ID" integer NOT NULL,
    "ISSUE_ID" bigint NOT NULL,
    "NAME" character varying(255) NOT NULL,
    "STATUS_ID" character varying(255),
    "SYSTEM_DECIDED" boolean
);


ALTER TABLE public."AO_56464C_APPROVAL" OWNER TO jira;

--
-- Name: AO_56464C_APPROVAL_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_56464C_APPROVAL_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_56464C_APPROVAL_ID_seq" OWNER TO jira;

--
-- Name: AO_56464C_APPROVAL_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_56464C_APPROVAL_ID_seq" OWNED BY "AO_56464C_APPROVAL"."ID";


--
-- Name: AO_56464C_APPROVER; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_56464C_APPROVER" (
    "APPROVAL_ID" integer,
    "ID" integer NOT NULL,
    "TYPE" character varying(255) NOT NULL,
    "VALUE" character varying(255) NOT NULL
);


ALTER TABLE public."AO_56464C_APPROVER" OWNER TO jira;

--
-- Name: AO_56464C_APPROVERDECISION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_56464C_APPROVERDECISION" (
    "APPROVAL_ID" integer,
    "DECISION" character varying(255) NOT NULL,
    "ID" integer NOT NULL,
    "SENT_DATE" bigint NOT NULL,
    "TO_STATUS_ID" character varying(255),
    "USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_56464C_APPROVERDECISION" OWNER TO jira;

--
-- Name: AO_56464C_APPROVERDECISION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_56464C_APPROVERDECISION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_56464C_APPROVERDECISION_ID_seq" OWNER TO jira;

--
-- Name: AO_56464C_APPROVERDECISION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_56464C_APPROVERDECISION_ID_seq" OWNED BY "AO_56464C_APPROVERDECISION"."ID";


--
-- Name: AO_56464C_APPROVER_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_56464C_APPROVER_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_56464C_APPROVER_ID_seq" OWNER TO jira;

--
-- Name: AO_56464C_APPROVER_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_56464C_APPROVER_ID_seq" OWNED BY "AO_56464C_APPROVER"."ID";


--
-- Name: AO_56464C_NOTIFICATIONRECORD; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_56464C_NOTIFICATIONRECORD" (
    "APPROVAL_ID" integer,
    "ID" integer NOT NULL,
    "SENT_DATE" bigint NOT NULL,
    "USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_56464C_NOTIFICATIONRECORD" OWNER TO jira;

--
-- Name: AO_56464C_NOTIFICATIONRECORD_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_56464C_NOTIFICATIONRECORD_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_56464C_NOTIFICATIONRECORD_ID_seq" OWNER TO jira;

--
-- Name: AO_56464C_NOTIFICATIONRECORD_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_56464C_NOTIFICATIONRECORD_ID_seq" OWNED BY "AO_56464C_NOTIFICATIONRECORD"."ID";


--
-- Name: AO_587B34_GLANCE_CONFIG; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_587B34_GLANCE_CONFIG" (
    "ROOM_ID" bigint DEFAULT 0 NOT NULL,
    "STATE" character varying(255),
    "SYNC_NEEDED" boolean,
    "JQL" character varying(255),
    "APPLICATION_USER_KEY" character varying(255),
    "NAME" character varying(255)
);


ALTER TABLE public."AO_587B34_GLANCE_CONFIG" OWNER TO jira;

--
-- Name: AO_587B34_PROJECT_CONFIG; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_587B34_PROJECT_CONFIG" (
    "CONFIGURATION_GROUP_ID" character varying(255) NOT NULL,
    "ID" integer NOT NULL,
    "NAME" character varying(255),
    "NAME_UNIQUE_CONSTRAINT" character varying(255) NOT NULL,
    "PROJECT_ID" bigint DEFAULT 0 NOT NULL,
    "ROOM_ID" bigint DEFAULT 0 NOT NULL,
    "VALUE" character varying(255)
);


ALTER TABLE public."AO_587B34_PROJECT_CONFIG" OWNER TO jira;

--
-- Name: AO_587B34_PROJECT_CONFIG_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_587B34_PROJECT_CONFIG_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_587B34_PROJECT_CONFIG_ID_seq" OWNER TO jira;

--
-- Name: AO_587B34_PROJECT_CONFIG_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_587B34_PROJECT_CONFIG_ID_seq" OWNED BY "AO_587B34_PROJECT_CONFIG"."ID";


--
-- Name: AO_5FB9D7_AOHIP_CHAT_LINK; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_5FB9D7_AOHIP_CHAT_LINK" (
    "ADDON_TOKEN_EXPIRY" timestamp without time zone,
    "API_URL" character varying(255),
    "CONNECT_DESCRIPTOR" text,
    "GROUP_ID" integer DEFAULT 0,
    "GROUP_NAME" character varying(255),
    "ID" integer NOT NULL,
    "OAUTH_ID" character varying(255),
    "SECRET_KEY" character varying(255),
    "SYSTEM_PASSWORD" character varying(255),
    "SYSTEM_TOKEN_EXPIRY" timestamp without time zone,
    "SYSTEM_USER" character varying(255),
    "SYSTEM_USER_TOKEN" character varying(255),
    "TOKEN" character varying(255)
);


ALTER TABLE public."AO_5FB9D7_AOHIP_CHAT_LINK" OWNER TO jira;

--
-- Name: AO_5FB9D7_AOHIP_CHAT_LINK_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_5FB9D7_AOHIP_CHAT_LINK_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_5FB9D7_AOHIP_CHAT_LINK_ID_seq" OWNER TO jira;

--
-- Name: AO_5FB9D7_AOHIP_CHAT_LINK_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_5FB9D7_AOHIP_CHAT_LINK_ID_seq" OWNED BY "AO_5FB9D7_AOHIP_CHAT_LINK"."ID";


--
-- Name: AO_5FB9D7_AOHIP_CHAT_USER; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_5FB9D7_AOHIP_CHAT_USER" (
    "HIP_CHAT_LINK_ID" integer,
    "HIP_CHAT_USER_ID" character varying(255),
    "HIP_CHAT_USER_NAME" character varying(255),
    "ID" integer NOT NULL,
    "REFRESH_CODE" character varying(255),
    "USER_KEY" character varying(255),
    "USER_SCOPES" character varying(255),
    "USER_TOKEN" character varying(255),
    "USER_TOKEN_EXPIRY" timestamp without time zone
);


ALTER TABLE public."AO_5FB9D7_AOHIP_CHAT_USER" OWNER TO jira;

--
-- Name: AO_5FB9D7_AOHIP_CHAT_USER_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_5FB9D7_AOHIP_CHAT_USER_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_5FB9D7_AOHIP_CHAT_USER_ID_seq" OWNER TO jira;

--
-- Name: AO_5FB9D7_AOHIP_CHAT_USER_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_5FB9D7_AOHIP_CHAT_USER_ID_seq" OWNED BY "AO_5FB9D7_AOHIP_CHAT_USER"."ID";


--
-- Name: AO_733371_EVENT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_733371_EVENT" (
    "ACTION" character varying(255),
    "ACTION_ID" character varying(255),
    "CREATED" timestamp without time zone NOT NULL,
    "EVENT_BUNDLE_ID" character varying(255),
    "EVENT_TYPE" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "USER_KEY" character varying(255)
);


ALTER TABLE public."AO_733371_EVENT" OWNER TO jira;

--
-- Name: AO_733371_EVENT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_733371_EVENT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_733371_EVENT_ID_seq" OWNER TO jira;

--
-- Name: AO_733371_EVENT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_733371_EVENT_ID_seq" OWNED BY "AO_733371_EVENT"."ID";


--
-- Name: AO_733371_EVENT_PARAMETER; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_733371_EVENT_PARAMETER" (
    "EVENT_ID" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "NAME" character varying(255) NOT NULL,
    "VALUE" text
);


ALTER TABLE public."AO_733371_EVENT_PARAMETER" OWNER TO jira;

--
-- Name: AO_733371_EVENT_PARAMETER_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_733371_EVENT_PARAMETER_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_733371_EVENT_PARAMETER_ID_seq" OWNER TO jira;

--
-- Name: AO_733371_EVENT_PARAMETER_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_733371_EVENT_PARAMETER_ID_seq" OWNED BY "AO_733371_EVENT_PARAMETER"."ID";


--
-- Name: AO_733371_EVENT_RECIPIENT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_733371_EVENT_RECIPIENT" (
    "CONSUMER_NAME" character varying(255) NOT NULL,
    "CREATED" timestamp without time zone NOT NULL,
    "EVENT_ID" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "SEND_DATE" timestamp without time zone,
    "STATUS" character varying(255) NOT NULL,
    "UPDATED" timestamp without time zone,
    "USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_733371_EVENT_RECIPIENT" OWNER TO jira;

--
-- Name: AO_733371_EVENT_RECIPIENT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_733371_EVENT_RECIPIENT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_733371_EVENT_RECIPIENT_ID_seq" OWNER TO jira;

--
-- Name: AO_733371_EVENT_RECIPIENT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_733371_EVENT_RECIPIENT_ID_seq" OWNED BY "AO_733371_EVENT_RECIPIENT"."ID";


--
-- Name: AO_7A2604_CALENDAR; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_7A2604_CALENDAR" (
    "CONTEXT" text NOT NULL,
    "DESCRIPTION" text,
    "ID" integer NOT NULL,
    "NAME" character varying(63) NOT NULL,
    "TIMEZONE" character varying(63) NOT NULL
);


ALTER TABLE public."AO_7A2604_CALENDAR" OWNER TO jira;

--
-- Name: AO_7A2604_CALENDAR_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_7A2604_CALENDAR_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_7A2604_CALENDAR_ID_seq" OWNER TO jira;

--
-- Name: AO_7A2604_CALENDAR_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_7A2604_CALENDAR_ID_seq" OWNED BY "AO_7A2604_CALENDAR"."ID";


--
-- Name: AO_7A2604_HOLIDAY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_7A2604_HOLIDAY" (
    "CALENDAR_ID" integer,
    "DATE_STRING" character varying(63) NOT NULL,
    "ID" integer NOT NULL,
    "NAME" character varying(63) NOT NULL,
    "RECURRING" boolean
);


ALTER TABLE public."AO_7A2604_HOLIDAY" OWNER TO jira;

--
-- Name: AO_7A2604_HOLIDAY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_7A2604_HOLIDAY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_7A2604_HOLIDAY_ID_seq" OWNER TO jira;

--
-- Name: AO_7A2604_HOLIDAY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_7A2604_HOLIDAY_ID_seq" OWNED BY "AO_7A2604_HOLIDAY"."ID";


--
-- Name: AO_7A2604_WORKINGTIME; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_7A2604_WORKINGTIME" (
    "CALENDAR_ID" integer,
    "DAY" character varying(63) NOT NULL,
    "DISABLED" boolean,
    "END_TIME" bigint NOT NULL,
    "ID" integer NOT NULL,
    "START_TIME" bigint NOT NULL
);


ALTER TABLE public."AO_7A2604_WORKINGTIME" OWNER TO jira;

--
-- Name: AO_7A2604_WORKINGTIME_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_7A2604_WORKINGTIME_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_7A2604_WORKINGTIME_ID_seq" OWNER TO jira;

--
-- Name: AO_7A2604_WORKINGTIME_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_7A2604_WORKINGTIME_ID_seq" OWNED BY "AO_7A2604_WORKINGTIME"."ID";


--
-- Name: AO_97EDAB_USERINVITATION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_97EDAB_USERINVITATION" (
    "APPLICATION_KEYS" character varying(255),
    "EMAIL_ADDRESS" character varying(255),
    "EXPIRY" timestamp without time zone,
    "ID" integer NOT NULL,
    "REDEEMED" boolean,
    "SENDER_USERNAME" character varying(255),
    "TOKEN" character varying(255)
);


ALTER TABLE public."AO_97EDAB_USERINVITATION" OWNER TO jira;

--
-- Name: AO_97EDAB_USERINVITATION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_97EDAB_USERINVITATION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_97EDAB_USERINVITATION_ID_seq" OWNER TO jira;

--
-- Name: AO_97EDAB_USERINVITATION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_97EDAB_USERINVITATION_ID_seq" OWNED BY "AO_97EDAB_USERINVITATION"."ID";


--
-- Name: AO_9B2E3B_EXEC_RULE_MSG_ITEM; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_EXEC_RULE_MSG_ITEM" (
    "ID" bigint NOT NULL,
    "RULE_EXECUTION_ID" bigint NOT NULL,
    "RULE_MESSAGE_KEY" character varying(127) NOT NULL,
    "RULE_MESSAGE_VALUE" text
);


ALTER TABLE public."AO_9B2E3B_EXEC_RULE_MSG_ITEM" OWNER TO jira;

--
-- Name: AO_9B2E3B_EXEC_RULE_MSG_ITEM_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_EXEC_RULE_MSG_ITEM_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_EXEC_RULE_MSG_ITEM_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_EXEC_RULE_MSG_ITEM_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_EXEC_RULE_MSG_ITEM_ID_seq" OWNED BY "AO_9B2E3B_EXEC_RULE_MSG_ITEM"."ID";


--
-- Name: AO_9B2E3B_IF_CONDITION_CONFIG; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_IF_CONDITION_CONFIG" (
    "ID" bigint NOT NULL,
    "IF_THEN_ID" bigint NOT NULL,
    "MODULE_KEY" character varying(450) NOT NULL,
    "ORDINAL" integer NOT NULL
);


ALTER TABLE public."AO_9B2E3B_IF_CONDITION_CONFIG" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_CONDITION_CONFIG_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_IF_CONDITION_CONFIG_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_IF_CONDITION_CONFIG_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_CONDITION_CONFIG_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_IF_CONDITION_CONFIG_ID_seq" OWNED BY "AO_9B2E3B_IF_CONDITION_CONFIG"."ID";


--
-- Name: AO_9B2E3B_IF_COND_CONF_DATA; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_IF_COND_CONF_DATA" (
    "CONFIG_DATA_KEY" character varying(127) NOT NULL,
    "CONFIG_DATA_VALUE" text,
    "ID" bigint NOT NULL,
    "IF_CONDITION_CONFIG_ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_IF_COND_CONF_DATA" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_COND_CONF_DATA_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_IF_COND_CONF_DATA_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_IF_COND_CONF_DATA_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_COND_CONF_DATA_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_IF_COND_CONF_DATA_ID_seq" OWNED BY "AO_9B2E3B_IF_COND_CONF_DATA"."ID";


--
-- Name: AO_9B2E3B_IF_COND_EXECUTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_IF_COND_EXECUTION" (
    "FINISH_TIME_MILLIS" bigint,
    "ID" bigint NOT NULL,
    "IF_CONDITION_CONFIG_ID" bigint NOT NULL,
    "IF_EXECUTION_ID" bigint NOT NULL,
    "MESSAGE" text,
    "OUTCOME" character varying(127) NOT NULL,
    "START_TIME_MILLIS" bigint
);


ALTER TABLE public."AO_9B2E3B_IF_COND_EXECUTION" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_COND_EXECUTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_IF_COND_EXECUTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_IF_COND_EXECUTION_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_COND_EXECUTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_IF_COND_EXECUTION_ID_seq" OWNED BY "AO_9B2E3B_IF_COND_EXECUTION"."ID";


--
-- Name: AO_9B2E3B_IF_EXECUTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_IF_EXECUTION" (
    "FINISH_TIME_MILLIS" bigint,
    "ID" bigint NOT NULL,
    "IF_THEN_EXECUTION_ID" bigint NOT NULL,
    "MESSAGE" text,
    "OUTCOME" character varying(127) NOT NULL,
    "START_TIME_MILLIS" bigint
);


ALTER TABLE public."AO_9B2E3B_IF_EXECUTION" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_EXECUTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_IF_EXECUTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_IF_EXECUTION_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_EXECUTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_IF_EXECUTION_ID_seq" OWNED BY "AO_9B2E3B_IF_EXECUTION"."ID";


--
-- Name: AO_9B2E3B_IF_THEN; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_IF_THEN" (
    "ID" bigint NOT NULL,
    "ORDINAL" integer NOT NULL,
    "RULE_ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_IF_THEN" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_THEN_EXECUTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_IF_THEN_EXECUTION" (
    "FINISH_TIME_MILLIS" bigint,
    "ID" bigint NOT NULL,
    "IF_THEN_ID" bigint NOT NULL,
    "MESSAGE" text,
    "OUTCOME" character varying(127) NOT NULL,
    "RULE_EXECUTION_ID" bigint NOT NULL,
    "START_TIME_MILLIS" bigint
);


ALTER TABLE public."AO_9B2E3B_IF_THEN_EXECUTION" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_THEN_EXECUTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_IF_THEN_EXECUTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_IF_THEN_EXECUTION_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_THEN_EXECUTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_IF_THEN_EXECUTION_ID_seq" OWNED BY "AO_9B2E3B_IF_THEN_EXECUTION"."ID";


--
-- Name: AO_9B2E3B_IF_THEN_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_IF_THEN_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_IF_THEN_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_IF_THEN_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_IF_THEN_ID_seq" OWNED BY "AO_9B2E3B_IF_THEN"."ID";


--
-- Name: AO_9B2E3B_PROJECT_USER_CONTEXT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_PROJECT_USER_CONTEXT" (
    "ID" bigint NOT NULL,
    "PROJECT_ID" bigint NOT NULL,
    "STRATEGY" character varying(127) NOT NULL,
    "USER_KEY" character varying(255)
);


ALTER TABLE public."AO_9B2E3B_PROJECT_USER_CONTEXT" OWNER TO jira;

--
-- Name: AO_9B2E3B_PROJECT_USER_CONTEXT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_PROJECT_USER_CONTEXT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_PROJECT_USER_CONTEXT_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_PROJECT_USER_CONTEXT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_PROJECT_USER_CONTEXT_ID_seq" OWNED BY "AO_9B2E3B_PROJECT_USER_CONTEXT"."ID";


--
-- Name: AO_9B2E3B_RSETREV_PROJ_CONTEXT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_RSETREV_PROJ_CONTEXT" (
    "ID" bigint NOT NULL,
    "PROJECT_ID" bigint NOT NULL,
    "RULESET_REVISION_ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_RSETREV_PROJ_CONTEXT" OWNER TO jira;

--
-- Name: AO_9B2E3B_RSETREV_PROJ_CONTEXT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_RSETREV_PROJ_CONTEXT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_RSETREV_PROJ_CONTEXT_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_RSETREV_PROJ_CONTEXT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_RSETREV_PROJ_CONTEXT_ID_seq" OWNED BY "AO_9B2E3B_RSETREV_PROJ_CONTEXT"."ID";


--
-- Name: AO_9B2E3B_RSETREV_USER_CONTEXT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_RSETREV_USER_CONTEXT" (
    "ID" bigint NOT NULL,
    "RULESET_REVISION_ID" bigint NOT NULL,
    "STRATEGY" character varying(127) NOT NULL,
    "USER_KEY" character varying(255)
);


ALTER TABLE public."AO_9B2E3B_RSETREV_USER_CONTEXT" OWNER TO jira;

--
-- Name: AO_9B2E3B_RSETREV_USER_CONTEXT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_RSETREV_USER_CONTEXT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_RSETREV_USER_CONTEXT_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_RSETREV_USER_CONTEXT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_RSETREV_USER_CONTEXT_ID_seq" OWNED BY "AO_9B2E3B_RSETREV_USER_CONTEXT"."ID";


--
-- Name: AO_9B2E3B_RULE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_RULE" (
    "ENABLED" boolean DEFAULT true NOT NULL,
    "ID" bigint NOT NULL,
    "ORDINAL" integer NOT NULL,
    "RULESET_REVISION_ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_RULE" OWNER TO jira;

--
-- Name: AO_9B2E3B_RULESET; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_RULESET" (
    "ACTIVE_REVISION_ID" bigint,
    "ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_RULESET" OWNER TO jira;

--
-- Name: AO_9B2E3B_RULESET_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_RULESET_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_RULESET_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_RULESET_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_RULESET_ID_seq" OWNED BY "AO_9B2E3B_RULESET"."ID";


--
-- Name: AO_9B2E3B_RULESET_REVISION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_RULESET_REVISION" (
    "CREATED_BY" character varying(127) NOT NULL,
    "CREATED_TIMESTAMP_MILLIS" bigint,
    "DESCRIPTION" character varying(450),
    "ID" bigint NOT NULL,
    "IS_SYSTEM_RULE_SET" boolean DEFAULT false NOT NULL,
    "NAME" character varying(127) NOT NULL,
    "RULE_SET_ID" bigint NOT NULL,
    "TRIGGER_FROM_OTHER_RULES" boolean
);


ALTER TABLE public."AO_9B2E3B_RULESET_REVISION" OWNER TO jira;

--
-- Name: AO_9B2E3B_RULESET_REVISION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_RULESET_REVISION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_RULESET_REVISION_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_RULESET_REVISION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_RULESET_REVISION_ID_seq" OWNED BY "AO_9B2E3B_RULESET_REVISION"."ID";


--
-- Name: AO_9B2E3B_RULE_EXECUTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_RULE_EXECUTION" (
    "EXECUTOR_USER_KEY" character varying(127),
    "FINISH_TIME_MILLIS" bigint,
    "ID" bigint NOT NULL,
    "MESSAGE" text,
    "OUTCOME" character varying(127) NOT NULL,
    "RULE_ID" bigint NOT NULL,
    "START_TIME_MILLIS" bigint
);


ALTER TABLE public."AO_9B2E3B_RULE_EXECUTION" OWNER TO jira;

--
-- Name: AO_9B2E3B_RULE_EXECUTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_RULE_EXECUTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_RULE_EXECUTION_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_RULE_EXECUTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_RULE_EXECUTION_ID_seq" OWNED BY "AO_9B2E3B_RULE_EXECUTION"."ID";


--
-- Name: AO_9B2E3B_RULE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_RULE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_RULE_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_RULE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_RULE_ID_seq" OWNED BY "AO_9B2E3B_RULE"."ID";


--
-- Name: AO_9B2E3B_THEN_ACTION_CONFIG; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_THEN_ACTION_CONFIG" (
    "ID" bigint NOT NULL,
    "IF_THEN_ID" bigint NOT NULL,
    "MODULE_KEY" character varying(450) NOT NULL,
    "ORDINAL" integer NOT NULL
);


ALTER TABLE public."AO_9B2E3B_THEN_ACTION_CONFIG" OWNER TO jira;

--
-- Name: AO_9B2E3B_THEN_ACTION_CONFIG_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_THEN_ACTION_CONFIG_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_THEN_ACTION_CONFIG_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_THEN_ACTION_CONFIG_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_THEN_ACTION_CONFIG_ID_seq" OWNED BY "AO_9B2E3B_THEN_ACTION_CONFIG"."ID";


--
-- Name: AO_9B2E3B_THEN_ACT_CONF_DATA; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_THEN_ACT_CONF_DATA" (
    "CONFIG_DATA_KEY" character varying(127) NOT NULL,
    "CONFIG_DATA_VALUE" text,
    "ID" bigint NOT NULL,
    "THEN_ACTION_CONFIG_ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_THEN_ACT_CONF_DATA" OWNER TO jira;

--
-- Name: AO_9B2E3B_THEN_ACT_CONF_DATA_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_THEN_ACT_CONF_DATA_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_THEN_ACT_CONF_DATA_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_THEN_ACT_CONF_DATA_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_THEN_ACT_CONF_DATA_ID_seq" OWNED BY "AO_9B2E3B_THEN_ACT_CONF_DATA"."ID";


--
-- Name: AO_9B2E3B_THEN_ACT_EXECUTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_THEN_ACT_EXECUTION" (
    "FINISH_TIME_MILLIS" bigint,
    "ID" bigint NOT NULL,
    "MESSAGE" text,
    "OUTCOME" character varying(127) NOT NULL,
    "START_TIME_MILLIS" bigint,
    "THEN_ACTION_CONFIG_ID" bigint NOT NULL,
    "THEN_EXECUTION_ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_THEN_ACT_EXECUTION" OWNER TO jira;

--
-- Name: AO_9B2E3B_THEN_ACT_EXECUTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_THEN_ACT_EXECUTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_THEN_ACT_EXECUTION_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_THEN_ACT_EXECUTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_THEN_ACT_EXECUTION_ID_seq" OWNED BY "AO_9B2E3B_THEN_ACT_EXECUTION"."ID";


--
-- Name: AO_9B2E3B_THEN_EXECUTION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_THEN_EXECUTION" (
    "FINISH_TIME_MILLIS" bigint,
    "ID" bigint NOT NULL,
    "IF_THEN_EXECUTION_ID" bigint NOT NULL,
    "MESSAGE" text,
    "OUTCOME" character varying(127) NOT NULL,
    "START_TIME_MILLIS" bigint
);


ALTER TABLE public."AO_9B2E3B_THEN_EXECUTION" OWNER TO jira;

--
-- Name: AO_9B2E3B_THEN_EXECUTION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_THEN_EXECUTION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_THEN_EXECUTION_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_THEN_EXECUTION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_THEN_EXECUTION_ID_seq" OWNED BY "AO_9B2E3B_THEN_EXECUTION"."ID";


--
-- Name: AO_9B2E3B_WHEN_HANDLER_CONFIG; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_WHEN_HANDLER_CONFIG" (
    "ID" bigint NOT NULL,
    "MODULE_KEY" character varying(450) NOT NULL,
    "ORDINAL" integer NOT NULL,
    "RULE_ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_WHEN_HANDLER_CONFIG" OWNER TO jira;

--
-- Name: AO_9B2E3B_WHEN_HANDLER_CONFIG_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_WHEN_HANDLER_CONFIG_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_WHEN_HANDLER_CONFIG_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_WHEN_HANDLER_CONFIG_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_WHEN_HANDLER_CONFIG_ID_seq" OWNED BY "AO_9B2E3B_WHEN_HANDLER_CONFIG"."ID";


--
-- Name: AO_9B2E3B_WHEN_HAND_CONF_DATA; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_9B2E3B_WHEN_HAND_CONF_DATA" (
    "CONFIG_DATA_KEY" character varying(127) NOT NULL,
    "CONFIG_DATA_VALUE" text,
    "ID" bigint NOT NULL,
    "WHEN_HANDLER_CONFIG_ID" bigint NOT NULL
);


ALTER TABLE public."AO_9B2E3B_WHEN_HAND_CONF_DATA" OWNER TO jira;

--
-- Name: AO_9B2E3B_WHEN_HAND_CONF_DATA_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_9B2E3B_WHEN_HAND_CONF_DATA_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_9B2E3B_WHEN_HAND_CONF_DATA_ID_seq" OWNER TO jira;

--
-- Name: AO_9B2E3B_WHEN_HAND_CONF_DATA_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_9B2E3B_WHEN_HAND_CONF_DATA_ID_seq" OWNED BY "AO_9B2E3B_WHEN_HAND_CONF_DATA"."ID";


--
-- Name: AO_A0B856_WEB_HOOK_LISTENER_AO; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_A0B856_WEB_HOOK_LISTENER_AO" (
    "DESCRIPTION" text,
    "ENABLED" boolean,
    "EVENTS" text,
    "EXCLUDE_BODY" boolean,
    "FILTERS" text,
    "ID" integer NOT NULL,
    "LAST_UPDATED" timestamp without time zone NOT NULL,
    "LAST_UPDATED_USER" character varying(255),
    "NAME" text NOT NULL,
    "PARAMETERS" text,
    "REGISTRATION_METHOD" character varying(255) NOT NULL,
    "URL" text NOT NULL
);


ALTER TABLE public."AO_A0B856_WEB_HOOK_LISTENER_AO" OWNER TO jira;

--
-- Name: AO_A0B856_WEB_HOOK_LISTENER_AO_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_A0B856_WEB_HOOK_LISTENER_AO_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_A0B856_WEB_HOOK_LISTENER_AO_ID_seq" OWNER TO jira;

--
-- Name: AO_A0B856_WEB_HOOK_LISTENER_AO_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_A0B856_WEB_HOOK_LISTENER_AO_ID_seq" OWNED BY "AO_A0B856_WEB_HOOK_LISTENER_AO"."ID";


--
-- Name: AO_B9A0F0_APPLIED_TEMPLATE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_B9A0F0_APPLIED_TEMPLATE" (
    "ID" integer NOT NULL,
    "PROJECT_ID" bigint DEFAULT 0,
    "PROJECT_TEMPLATE_MODULE_KEY" character varying(255),
    "PROJECT_TEMPLATE_WEB_ITEM_KEY" character varying(255)
);


ALTER TABLE public."AO_B9A0F0_APPLIED_TEMPLATE" OWNER TO jira;

--
-- Name: AO_B9A0F0_APPLIED_TEMPLATE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_B9A0F0_APPLIED_TEMPLATE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_B9A0F0_APPLIED_TEMPLATE_ID_seq" OWNER TO jira;

--
-- Name: AO_B9A0F0_APPLIED_TEMPLATE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_B9A0F0_APPLIED_TEMPLATE_ID_seq" OWNED BY "AO_B9A0F0_APPLIED_TEMPLATE"."ID";


--
-- Name: AO_C16815_ALERT_AO; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_C16815_ALERT_AO" (
    "CREATED_DATE" bigint DEFAULT 0,
    "DETAILS_JSON" text,
    "ID" bigint NOT NULL,
    "ISSUE_COMPONENT_ID" character varying(255),
    "ISSUE_ID" character varying(255),
    "ISSUE_SEVERITY" integer DEFAULT 0,
    "NODE_NAME" character varying(255),
    "TRIGGER_MODULE" character varying(255),
    "TRIGGER_PLUGIN_KEY" character varying(255),
    "TRIGGER_PLUGIN_KEY_VERSION" character varying(255),
    "TRIGGER_PLUGIN_VERSION" character varying(255)
);


ALTER TABLE public."AO_C16815_ALERT_AO" OWNER TO jira;

--
-- Name: AO_C16815_ALERT_AO_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_C16815_ALERT_AO_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_C16815_ALERT_AO_ID_seq" OWNER TO jira;

--
-- Name: AO_C16815_ALERT_AO_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_C16815_ALERT_AO_ID_seq" OWNED BY "AO_C16815_ALERT_AO"."ID";


--
-- Name: AO_C7F17E_LINGO; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_C7F17E_LINGO" (
    "CATEGORY" character varying(255),
    "CREATED_TIMESTAMP" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "LOGICAL_ID" character varying(255),
    "PROJECT_ID" bigint,
    "SYSTEM_I18N" character varying(255)
);


ALTER TABLE public."AO_C7F17E_LINGO" OWNER TO jira;

--
-- Name: AO_C7F17E_LINGO_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_C7F17E_LINGO_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_C7F17E_LINGO_ID_seq" OWNER TO jira;

--
-- Name: AO_C7F17E_LINGO_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_C7F17E_LINGO_ID_seq" OWNED BY "AO_C7F17E_LINGO"."ID";


--
-- Name: AO_C7F17E_LINGO_REVISION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_C7F17E_LINGO_REVISION" (
    "CREATED_TIMESTAMP" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "LINGO_ID" bigint
);


ALTER TABLE public."AO_C7F17E_LINGO_REVISION" OWNER TO jira;

--
-- Name: AO_C7F17E_LINGO_REVISION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_C7F17E_LINGO_REVISION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_C7F17E_LINGO_REVISION_ID_seq" OWNER TO jira;

--
-- Name: AO_C7F17E_LINGO_REVISION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_C7F17E_LINGO_REVISION_ID_seq" OWNED BY "AO_C7F17E_LINGO_REVISION"."ID";


--
-- Name: AO_C7F17E_LINGO_TRANSLATION; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_C7F17E_LINGO_TRANSLATION" (
    "CONTENT" text NOT NULL,
    "CREATED_TIMESTAMP" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "LANGUAGE" character varying(63) NOT NULL,
    "LINGO_REVISION_ID" bigint,
    "LOCALE" character varying(63) NOT NULL
);


ALTER TABLE public."AO_C7F17E_LINGO_TRANSLATION" OWNER TO jira;

--
-- Name: AO_C7F17E_LINGO_TRANSLATION_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_C7F17E_LINGO_TRANSLATION_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_C7F17E_LINGO_TRANSLATION_ID_seq" OWNER TO jira;

--
-- Name: AO_C7F17E_LINGO_TRANSLATION_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_C7F17E_LINGO_TRANSLATION_ID_seq" OWNED BY "AO_C7F17E_LINGO_TRANSLATION"."ID";


--
-- Name: AO_C7F17E_PROJECT_LANGUAGE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_C7F17E_PROJECT_LANGUAGE" (
    "ACTIVE" boolean,
    "ID" bigint NOT NULL,
    "LOCALE" character varying(63),
    "PROJECT_LANG_REV_ID" bigint
);


ALTER TABLE public."AO_C7F17E_PROJECT_LANGUAGE" OWNER TO jira;

--
-- Name: AO_C7F17E_PROJECT_LANGUAGE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_C7F17E_PROJECT_LANGUAGE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_C7F17E_PROJECT_LANGUAGE_ID_seq" OWNER TO jira;

--
-- Name: AO_C7F17E_PROJECT_LANGUAGE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_C7F17E_PROJECT_LANGUAGE_ID_seq" OWNED BY "AO_C7F17E_PROJECT_LANGUAGE"."ID";


--
-- Name: AO_C7F17E_PROJECT_LANG_CONFIG; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_C7F17E_PROJECT_LANG_CONFIG" (
    "CREATED_TIMESTAMP" bigint,
    "ID" bigint NOT NULL,
    "PROJECT_ID" bigint
);


ALTER TABLE public."AO_C7F17E_PROJECT_LANG_CONFIG" OWNER TO jira;

--
-- Name: AO_C7F17E_PROJECT_LANG_CONFIG_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_C7F17E_PROJECT_LANG_CONFIG_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_C7F17E_PROJECT_LANG_CONFIG_ID_seq" OWNER TO jira;

--
-- Name: AO_C7F17E_PROJECT_LANG_CONFIG_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_C7F17E_PROJECT_LANG_CONFIG_ID_seq" OWNED BY "AO_C7F17E_PROJECT_LANG_CONFIG"."ID";


--
-- Name: AO_C7F17E_PROJECT_LANG_REV; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_C7F17E_PROJECT_LANG_REV" (
    "AUTHOR_USER_KEY" character varying(255),
    "CREATED_TIMESTAMP" bigint,
    "DEFAULT_LANGUAGE" character varying(63),
    "ID" bigint NOT NULL,
    "PROJECT_LANG_CONFIG_ID" bigint
);


ALTER TABLE public."AO_C7F17E_PROJECT_LANG_REV" OWNER TO jira;

--
-- Name: AO_C7F17E_PROJECT_LANG_REV_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_C7F17E_PROJECT_LANG_REV_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_C7F17E_PROJECT_LANG_REV_ID_seq" OWNER TO jira;

--
-- Name: AO_C7F17E_PROJECT_LANG_REV_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_C7F17E_PROJECT_LANG_REV_ID_seq" OWNED BY "AO_C7F17E_PROJECT_LANG_REV"."ID";


--
-- Name: AO_D530BB_CANNEDRESPONSE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_D530BB_CANNEDRESPONSE" (
    "CREATED_TIME" bigint,
    "CREATED_USER_KEY" character varying(255) NOT NULL,
    "ENABLED" boolean DEFAULT true NOT NULL,
    "ID" bigint NOT NULL,
    "SERVICE_DESK_ID" bigint,
    "SOFT_DELETED" boolean DEFAULT false,
    "TEXT" text,
    "TITLE" character varying(255),
    "UPDATED_TIME" bigint,
    "UPDATED_USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_D530BB_CANNEDRESPONSE" OWNER TO jira;

--
-- Name: AO_D530BB_CANNEDRESPONSEAUDIT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_D530BB_CANNEDRESPONSEAUDIT" (
    "ACTION" character varying(255) NOT NULL,
    "CANNED_RESPONSE_ID" bigint NOT NULL,
    "EVENT_TIME" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_D530BB_CANNEDRESPONSEAUDIT" OWNER TO jira;

--
-- Name: AO_D530BB_CANNEDRESPONSEAUDIT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_D530BB_CANNEDRESPONSEAUDIT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_D530BB_CANNEDRESPONSEAUDIT_ID_seq" OWNER TO jira;

--
-- Name: AO_D530BB_CANNEDRESPONSEAUDIT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_D530BB_CANNEDRESPONSEAUDIT_ID_seq" OWNED BY "AO_D530BB_CANNEDRESPONSEAUDIT"."ID";


--
-- Name: AO_D530BB_CANNEDRESPONSEUSAGE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_D530BB_CANNEDRESPONSEUSAGE" (
    "CANNED_RESPONSE_ID" bigint NOT NULL,
    "ID" bigint NOT NULL,
    "USAGE_TIME" bigint NOT NULL,
    "USER_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_D530BB_CANNEDRESPONSEUSAGE" OWNER TO jira;

--
-- Name: AO_D530BB_CANNEDRESPONSEUSAGE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_D530BB_CANNEDRESPONSEUSAGE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_D530BB_CANNEDRESPONSEUSAGE_ID_seq" OWNER TO jira;

--
-- Name: AO_D530BB_CANNEDRESPONSEUSAGE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_D530BB_CANNEDRESPONSEUSAGE_ID_seq" OWNED BY "AO_D530BB_CANNEDRESPONSEUSAGE"."ID";


--
-- Name: AO_D530BB_CANNEDRESPONSE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_D530BB_CANNEDRESPONSE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_D530BB_CANNEDRESPONSE_ID_seq" OWNER TO jira;

--
-- Name: AO_D530BB_CANNEDRESPONSE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_D530BB_CANNEDRESPONSE_ID_seq" OWNED BY "AO_D530BB_CANNEDRESPONSE"."ID";


--
-- Name: AO_D530BB_CRAUDITACTIONDATA; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_D530BB_CRAUDITACTIONDATA" (
    "CANNED_RESPONSE_AUDIT_LOG_ID" bigint NOT NULL,
    "DATA_COLUMN_NAME" character varying(255) NOT NULL,
    "DATA_TYPE" integer NOT NULL,
    "FROM_BOOLEAN" boolean,
    "FROM_STRING" character varying(450),
    "FROM_TEXT" text,
    "ID" bigint NOT NULL,
    "TO_BOOLEAN" boolean,
    "TO_STRING" character varying(450),
    "TO_TEXT" text
);


ALTER TABLE public."AO_D530BB_CRAUDITACTIONDATA" OWNER TO jira;

--
-- Name: AO_D530BB_CRAUDITACTIONDATA_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_D530BB_CRAUDITACTIONDATA_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_D530BB_CRAUDITACTIONDATA_ID_seq" OWNER TO jira;

--
-- Name: AO_D530BB_CRAUDITACTIONDATA_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_D530BB_CRAUDITACTIONDATA_ID_seq" OWNED BY "AO_D530BB_CRAUDITACTIONDATA"."ID";


--
-- Name: AO_ED669C_SEEN_ASSERTIONS; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_ED669C_SEEN_ASSERTIONS" (
    "ASSERTION_ID" character varying(255) NOT NULL,
    "EXPIRY_TIMESTAMP" bigint DEFAULT 0 NOT NULL,
    "ID" integer NOT NULL
);


ALTER TABLE public."AO_ED669C_SEEN_ASSERTIONS" OWNER TO jira;

--
-- Name: AO_ED669C_SEEN_ASSERTIONS_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_ED669C_SEEN_ASSERTIONS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_ED669C_SEEN_ASSERTIONS_ID_seq" OWNER TO jira;

--
-- Name: AO_ED669C_SEEN_ASSERTIONS_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_ED669C_SEEN_ASSERTIONS_ID_seq" OWNED BY "AO_ED669C_SEEN_ASSERTIONS"."ID";


--
-- Name: AO_F1B27B_HISTORY_RECORD; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_F1B27B_HISTORY_RECORD" (
    "EVENT_TIME_MILLIS" bigint,
    "EVENT_TYPE" character varying(63) NOT NULL,
    "ID" integer NOT NULL,
    "MESSAGE" character varying(450) NOT NULL,
    "TARGET_TIME_MILLIS" bigint,
    "TIMED_PROMISE_HISTORY_KEY_HASH" character varying(255) NOT NULL
);


ALTER TABLE public."AO_F1B27B_HISTORY_RECORD" OWNER TO jira;

--
-- Name: AO_F1B27B_HISTORY_RECORD_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_F1B27B_HISTORY_RECORD_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_F1B27B_HISTORY_RECORD_ID_seq" OWNER TO jira;

--
-- Name: AO_F1B27B_HISTORY_RECORD_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_F1B27B_HISTORY_RECORD_ID_seq" OWNED BY "AO_F1B27B_HISTORY_RECORD"."ID";


--
-- Name: AO_F1B27B_KEY_COMPONENT; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_F1B27B_KEY_COMPONENT" (
    "ID" integer NOT NULL,
    "KEY" character varying(450) NOT NULL,
    "TIMED_PROMISE_ID" integer NOT NULL,
    "VALUE" character varying(450) NOT NULL
);


ALTER TABLE public."AO_F1B27B_KEY_COMPONENT" OWNER TO jira;

--
-- Name: AO_F1B27B_KEY_COMPONENT_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_F1B27B_KEY_COMPONENT_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_F1B27B_KEY_COMPONENT_ID_seq" OWNER TO jira;

--
-- Name: AO_F1B27B_KEY_COMPONENT_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_F1B27B_KEY_COMPONENT_ID_seq" OWNED BY "AO_F1B27B_KEY_COMPONENT"."ID";


--
-- Name: AO_F1B27B_KEY_COMP_HISTORY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_F1B27B_KEY_COMP_HISTORY" (
    "ID" integer NOT NULL,
    "KEY" character varying(450) NOT NULL,
    "TIMED_PROMISE_ID" integer NOT NULL,
    "VALUE" character varying(450) NOT NULL
);


ALTER TABLE public."AO_F1B27B_KEY_COMP_HISTORY" OWNER TO jira;

--
-- Name: AO_F1B27B_KEY_COMP_HISTORY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_F1B27B_KEY_COMP_HISTORY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_F1B27B_KEY_COMP_HISTORY_ID_seq" OWNER TO jira;

--
-- Name: AO_F1B27B_KEY_COMP_HISTORY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_F1B27B_KEY_COMP_HISTORY_ID_seq" OWNED BY "AO_F1B27B_KEY_COMP_HISTORY"."ID";


--
-- Name: AO_F1B27B_PROMISE; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_F1B27B_PROMISE" (
    "CLASSIFICATION" character varying(127) NOT NULL,
    "CONTENT" text,
    "CREATED_TIME_MILLIS" bigint,
    "ID" integer NOT NULL,
    "KEY_HASH" character varying(255) NOT NULL,
    "MIME_TYPE" character varying(127),
    "STATUS" character varying(63) NOT NULL,
    "TARGET_TIME_MILLIS" bigint,
    "TASK_KEY" character varying(255) NOT NULL,
    "UPDATED_TIME_MILLIS" bigint
);


ALTER TABLE public."AO_F1B27B_PROMISE" OWNER TO jira;

--
-- Name: AO_F1B27B_PROMISE_HISTORY; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE "AO_F1B27B_PROMISE_HISTORY" (
    "CLASSIFICATION" character varying(127) NOT NULL,
    "ID" integer NOT NULL,
    "KEY_HASH" character varying(255) NOT NULL,
    "TASK_KEY" character varying(255) NOT NULL
);


ALTER TABLE public."AO_F1B27B_PROMISE_HISTORY" OWNER TO jira;

--
-- Name: AO_F1B27B_PROMISE_HISTORY_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_F1B27B_PROMISE_HISTORY_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_F1B27B_PROMISE_HISTORY_ID_seq" OWNER TO jira;

--
-- Name: AO_F1B27B_PROMISE_HISTORY_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_F1B27B_PROMISE_HISTORY_ID_seq" OWNED BY "AO_F1B27B_PROMISE_HISTORY"."ID";


--
-- Name: AO_F1B27B_PROMISE_ID_seq; Type: SEQUENCE; Schema: public; Owner: jira
--

CREATE SEQUENCE "AO_F1B27B_PROMISE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AO_F1B27B_PROMISE_ID_seq" OWNER TO jira;

--
-- Name: AO_F1B27B_PROMISE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jira
--

ALTER SEQUENCE "AO_F1B27B_PROMISE_ID_seq" OWNED BY "AO_F1B27B_PROMISE"."ID";


--
-- Name: app_user; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE app_user (
    id numeric(18,0) NOT NULL,
    user_key character varying(255),
    lower_user_name character varying(255)
);


ALTER TABLE public.app_user OWNER TO jira;

--
-- Name: audit_changed_value; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE audit_changed_value (
    id numeric(18,0) NOT NULL,
    log_id numeric(18,0),
    name character varying(255),
    delta_from text,
    delta_to text
);


ALTER TABLE public.audit_changed_value OWNER TO jira;

--
-- Name: audit_item; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE audit_item (
    id numeric(18,0) NOT NULL,
    log_id numeric(18,0),
    object_type character varying(60),
    object_id character varying(255),
    object_name character varying(255),
    object_parent_id character varying(255),
    object_parent_name character varying(255)
);


ALTER TABLE public.audit_item OWNER TO jira;

--
-- Name: audit_log; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE audit_log (
    id numeric(18,0) NOT NULL,
    remote_address character varying(60),
    created timestamp with time zone,
    author_key character varying(255),
    summary character varying(255),
    category character varying(255),
    object_type character varying(60),
    object_id character varying(255),
    object_name character varying(255),
    object_parent_id character varying(255),
    object_parent_name character varying(255),
    author_type numeric(9,0),
    event_source_name character varying(255),
    description character varying(255),
    long_description text,
    search_field text
);


ALTER TABLE public.audit_log OWNER TO jira;

--
-- Name: avatar; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE avatar (
    id numeric(18,0) NOT NULL,
    filename character varying(255),
    contenttype character varying(255),
    avatartype character varying(60),
    owner character varying(255),
    systemavatar numeric(9,0)
);


ALTER TABLE public.avatar OWNER TO jira;

--
-- Name: board; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE board (
    id numeric(18,0) NOT NULL,
    jql text
);


ALTER TABLE public.board OWNER TO jira;

--
-- Name: boardproject; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE boardproject (
    board_id numeric(18,0) NOT NULL,
    project_id numeric(18,0) NOT NULL
);


ALTER TABLE public.boardproject OWNER TO jira;

--
-- Name: changegroup; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE changegroup (
    id numeric(18,0) NOT NULL,
    issueid numeric(18,0),
    author character varying(255),
    created timestamp with time zone
);


ALTER TABLE public.changegroup OWNER TO jira;

--
-- Name: changeitem; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE changeitem (
    id numeric(18,0) NOT NULL,
    groupid numeric(18,0),
    fieldtype character varying(255),
    field character varying(255),
    oldvalue text,
    oldstring text,
    newvalue text,
    newstring text
);


ALTER TABLE public.changeitem OWNER TO jira;

--
-- Name: clusteredjob; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE clusteredjob (
    id numeric(18,0) NOT NULL,
    job_id character varying(255),
    job_runner_key character varying(255),
    sched_type character(1),
    interval_millis numeric(18,0),
    first_run numeric(18,0),
    cron_expression character varying(255),
    time_zone character varying(60),
    next_run numeric(18,0),
    version numeric(18,0),
    parameters bytea
);


ALTER TABLE public.clusteredjob OWNER TO jira;

--
-- Name: clusterlockstatus; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE clusterlockstatus (
    id numeric(18,0) NOT NULL,
    lock_name character varying(255),
    locked_by_node character varying(60),
    update_time numeric(18,0)
);


ALTER TABLE public.clusterlockstatus OWNER TO jira;

--
-- Name: clustermessage; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE clustermessage (
    id numeric(18,0) NOT NULL,
    source_node character varying(60),
    destination_node character varying(60),
    claimed_by_node character varying(60),
    message character varying(255),
    message_time timestamp with time zone
);


ALTER TABLE public.clustermessage OWNER TO jira;

--
-- Name: clusternode; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE clusternode (
    node_id character varying(60) NOT NULL,
    node_state character varying(60),
    "timestamp" numeric(18,0),
    ip character varying(60),
    cache_listener_port numeric(18,0),
    node_build_number numeric(18,0),
    node_version character varying(60)
);


ALTER TABLE public.clusternode OWNER TO jira;

--
-- Name: clusternodeheartbeat; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE clusternodeheartbeat (
    node_id character varying(60) NOT NULL,
    heartbeat_time numeric(18,0),
    database_time numeric(18,0)
);


ALTER TABLE public.clusternodeheartbeat OWNER TO jira;

--
-- Name: clusterupgradestate; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE clusterupgradestate (
    id numeric(18,0) NOT NULL,
    database_time numeric(18,0),
    cluster_build_number numeric(18,0),
    cluster_version character varying(60),
    state character varying(60),
    order_number numeric(18,0)
);


ALTER TABLE public.clusterupgradestate OWNER TO jira;

--
-- Name: columnlayout; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE columnlayout (
    id numeric(18,0) NOT NULL,
    username character varying(255),
    searchrequest numeric(18,0)
);


ALTER TABLE public.columnlayout OWNER TO jira;

--
-- Name: columnlayoutitem; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE columnlayoutitem (
    id numeric(18,0) NOT NULL,
    columnlayout numeric(18,0),
    fieldidentifier character varying(255),
    horizontalposition numeric(18,0)
);


ALTER TABLE public.columnlayoutitem OWNER TO jira;

--
-- Name: component; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE component (
    id numeric(18,0) NOT NULL,
    project numeric(18,0),
    cname character varying(255),
    description text,
    url character varying(255),
    lead character varying(255),
    assigneetype numeric(18,0),
    archived character varying(10)
);


ALTER TABLE public.component OWNER TO jira;

--
-- Name: configurationcontext; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE configurationcontext (
    id numeric(18,0) NOT NULL,
    projectcategory numeric(18,0),
    project numeric(18,0),
    customfield character varying(255),
    fieldconfigscheme numeric(18,0)
);


ALTER TABLE public.configurationcontext OWNER TO jira;

--
-- Name: customfield; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE customfield (
    id numeric(18,0) NOT NULL,
    cfkey character varying(255),
    customfieldtypekey character varying(255),
    customfieldsearcherkey character varying(255),
    cfname character varying(255),
    description text,
    defaultvalue character varying(255),
    fieldtype numeric(18,0),
    project numeric(18,0),
    issuetype character varying(255)
);


ALTER TABLE public.customfield OWNER TO jira;

--
-- Name: customfieldoption; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE customfieldoption (
    id numeric(18,0) NOT NULL,
    customfield numeric(18,0),
    customfieldconfig numeric(18,0),
    parentoptionid numeric(18,0),
    sequence numeric(18,0),
    customvalue character varying(255),
    optiontype character varying(60),
    disabled character varying(60)
);


ALTER TABLE public.customfieldoption OWNER TO jira;

--
-- Name: customfieldvalue; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE customfieldvalue (
    id numeric(18,0) NOT NULL,
    issue numeric(18,0),
    customfield numeric(18,0),
    updated numeric(18,0),
    parentkey character varying(255),
    stringvalue character varying(255),
    numbervalue double precision,
    textvalue text,
    datevalue timestamp with time zone,
    valuetype character varying(255)
);


ALTER TABLE public.customfieldvalue OWNER TO jira;

--
-- Name: cwd_application; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_application (
    id numeric(18,0) NOT NULL,
    application_name character varying(255),
    lower_application_name character varying(255),
    created_date timestamp with time zone,
    updated_date timestamp with time zone,
    active numeric(9,0),
    description character varying(255),
    application_type character varying(255),
    credential character varying(255)
);


ALTER TABLE public.cwd_application OWNER TO jira;

--
-- Name: cwd_application_address; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_application_address (
    application_id numeric(18,0) NOT NULL,
    remote_address character varying(255) NOT NULL,
    encoded_address_binary character varying(255),
    remote_address_mask numeric(9,0)
);


ALTER TABLE public.cwd_application_address OWNER TO jira;

--
-- Name: cwd_directory; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_directory (
    id numeric(18,0) NOT NULL,
    directory_name character varying(255),
    lower_directory_name character varying(255),
    created_date timestamp with time zone,
    updated_date timestamp with time zone,
    active numeric(9,0),
    description character varying(255),
    impl_class character varying(255),
    lower_impl_class character varying(255),
    directory_type character varying(60),
    directory_position numeric(18,0)
);


ALTER TABLE public.cwd_directory OWNER TO jira;

--
-- Name: cwd_directory_attribute; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_directory_attribute (
    directory_id numeric(18,0) NOT NULL,
    attribute_name character varying(255) NOT NULL,
    attribute_value character varying(4000)
);


ALTER TABLE public.cwd_directory_attribute OWNER TO jira;

--
-- Name: cwd_directory_operation; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_directory_operation (
    directory_id numeric(18,0) NOT NULL,
    operation_type character varying(60) NOT NULL
);


ALTER TABLE public.cwd_directory_operation OWNER TO jira;

--
-- Name: cwd_group; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_group (
    id numeric(18,0) NOT NULL,
    group_name character varying(255),
    lower_group_name character varying(255),
    active numeric(9,0),
    local numeric(9,0),
    created_date timestamp with time zone,
    updated_date timestamp with time zone,
    description character varying(255),
    lower_description character varying(255),
    group_type character varying(60),
    directory_id numeric(18,0)
);


ALTER TABLE public.cwd_group OWNER TO jira;

--
-- Name: cwd_group_attributes; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_group_attributes (
    id numeric(18,0) NOT NULL,
    group_id numeric(18,0),
    directory_id numeric(18,0),
    attribute_name character varying(255),
    attribute_value character varying(255),
    lower_attribute_value character varying(255)
);


ALTER TABLE public.cwd_group_attributes OWNER TO jira;

--
-- Name: cwd_membership; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_membership (
    id numeric(18,0) NOT NULL,
    parent_id numeric(18,0),
    child_id numeric(18,0),
    membership_type character varying(60),
    group_type character varying(60),
    parent_name character varying(255),
    lower_parent_name character varying(255),
    child_name character varying(255),
    lower_child_name character varying(255),
    directory_id numeric(18,0)
);


ALTER TABLE public.cwd_membership OWNER TO jira;

--
-- Name: cwd_user; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_user (
    id numeric(18,0) NOT NULL,
    directory_id numeric(18,0),
    user_name character varying(255),
    lower_user_name character varying(255),
    active numeric(9,0),
    created_date timestamp with time zone,
    updated_date timestamp with time zone,
    first_name character varying(255),
    lower_first_name character varying(255),
    last_name character varying(255),
    lower_last_name character varying(255),
    display_name character varying(255),
    lower_display_name character varying(255),
    email_address character varying(255),
    lower_email_address character varying(255),
    credential character varying(255),
    deleted_externally numeric(9,0),
    external_id character varying(255)
);


ALTER TABLE public.cwd_user OWNER TO jira;

--
-- Name: cwd_user_attributes; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE cwd_user_attributes (
    id numeric(18,0) NOT NULL,
    user_id numeric(18,0),
    directory_id numeric(18,0),
    attribute_name character varying(255),
    attribute_value character varying(255),
    lower_attribute_value character varying(255)
);


ALTER TABLE public.cwd_user_attributes OWNER TO jira;

--
-- Name: deadletter; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE deadletter (
    id numeric(18,0) NOT NULL,
    message_id character varying(255),
    last_seen numeric(18,0),
    mail_server_id numeric(18,0),
    folder_name character varying(255)
);


ALTER TABLE public.deadletter OWNER TO jira;

--
-- Name: draftworkflowscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE draftworkflowscheme (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text,
    workflow_scheme_id numeric(18,0),
    last_modified_date timestamp with time zone,
    last_modified_user character varying(255)
);


ALTER TABLE public.draftworkflowscheme OWNER TO jira;

--
-- Name: draftworkflowschemeentity; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE draftworkflowschemeentity (
    id numeric(18,0) NOT NULL,
    scheme numeric(18,0),
    workflow character varying(255),
    issuetype character varying(255)
);


ALTER TABLE public.draftworkflowschemeentity OWNER TO jira;

--
-- Name: entity_property; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE entity_property (
    id numeric(18,0) NOT NULL,
    entity_name character varying(255),
    entity_id numeric(18,0),
    property_key character varying(255),
    created timestamp with time zone,
    updated timestamp with time zone,
    json_value text
);


ALTER TABLE public.entity_property OWNER TO jira;

--
-- Name: entity_property_index_document; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE entity_property_index_document (
    id numeric(18,0) NOT NULL,
    plugin_key character varying(255),
    module_key character varying(255),
    entity_key character varying(255),
    updated timestamp with time zone,
    document text
);


ALTER TABLE public.entity_property_index_document OWNER TO jira;

--
-- Name: entity_translation; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE entity_translation (
    id numeric(18,0) NOT NULL,
    entity_name character varying(255),
    entity_id numeric(18,0),
    locale character varying(60),
    trans_name character varying(255),
    trans_desc character varying(255)
);


ALTER TABLE public.entity_translation OWNER TO jira;

--
-- Name: external_entities; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE external_entities (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    entitytype character varying(255)
);


ALTER TABLE public.external_entities OWNER TO jira;

--
-- Name: externalgadget; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE externalgadget (
    id numeric(18,0) NOT NULL,
    gadget_xml text
);


ALTER TABLE public.externalgadget OWNER TO jira;

--
-- Name: favouriteassociations; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE favouriteassociations (
    id numeric(18,0) NOT NULL,
    username character varying(255),
    entitytype character varying(60),
    entityid numeric(18,0),
    sequence numeric(18,0)
);


ALTER TABLE public.favouriteassociations OWNER TO jira;

--
-- Name: feature; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE feature (
    id numeric(18,0) NOT NULL,
    feature_name character varying(255),
    feature_type character varying(10),
    user_key character varying(255)
);


ALTER TABLE public.feature OWNER TO jira;

--
-- Name: fieldconfigscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldconfigscheme (
    id numeric(18,0) NOT NULL,
    configname character varying(255),
    description text,
    fieldid character varying(60),
    customfield numeric(18,0)
);


ALTER TABLE public.fieldconfigscheme OWNER TO jira;

--
-- Name: fieldconfigschemeissuetype; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldconfigschemeissuetype (
    id numeric(18,0) NOT NULL,
    issuetype character varying(255),
    fieldconfigscheme numeric(18,0),
    fieldconfiguration numeric(18,0)
);


ALTER TABLE public.fieldconfigschemeissuetype OWNER TO jira;

--
-- Name: fieldconfiguration; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldconfiguration (
    id numeric(18,0) NOT NULL,
    configname character varying(255),
    description text,
    fieldid character varying(60),
    customfield numeric(18,0)
);


ALTER TABLE public.fieldconfiguration OWNER TO jira;

--
-- Name: fieldlayout; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldlayout (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description character varying(255),
    layout_type character varying(255),
    layoutscheme numeric(18,0)
);


ALTER TABLE public.fieldlayout OWNER TO jira;

--
-- Name: fieldlayoutitem; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldlayoutitem (
    id numeric(18,0) NOT NULL,
    fieldlayout numeric(18,0),
    fieldidentifier character varying(255),
    description text,
    verticalposition numeric(18,0),
    ishidden character varying(60),
    isrequired character varying(60),
    renderertype character varying(255)
);


ALTER TABLE public.fieldlayoutitem OWNER TO jira;

--
-- Name: fieldlayoutscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldlayoutscheme (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text
);


ALTER TABLE public.fieldlayoutscheme OWNER TO jira;

--
-- Name: fieldlayoutschemeassociation; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldlayoutschemeassociation (
    id numeric(18,0) NOT NULL,
    issuetype character varying(255),
    project numeric(18,0),
    fieldlayoutscheme numeric(18,0)
);


ALTER TABLE public.fieldlayoutschemeassociation OWNER TO jira;

--
-- Name: fieldlayoutschemeentity; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldlayoutschemeentity (
    id numeric(18,0) NOT NULL,
    scheme numeric(18,0),
    issuetype character varying(255),
    fieldlayout numeric(18,0)
);


ALTER TABLE public.fieldlayoutschemeentity OWNER TO jira;

--
-- Name: fieldscreen; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldscreen (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description character varying(255)
);


ALTER TABLE public.fieldscreen OWNER TO jira;

--
-- Name: fieldscreenlayoutitem; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldscreenlayoutitem (
    id numeric(18,0) NOT NULL,
    fieldidentifier character varying(255),
    sequence numeric(18,0),
    fieldscreentab numeric(18,0)
);


ALTER TABLE public.fieldscreenlayoutitem OWNER TO jira;

--
-- Name: fieldscreenscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldscreenscheme (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description character varying(255)
);


ALTER TABLE public.fieldscreenscheme OWNER TO jira;

--
-- Name: fieldscreenschemeitem; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldscreenschemeitem (
    id numeric(18,0) NOT NULL,
    operation numeric(18,0),
    fieldscreen numeric(18,0),
    fieldscreenscheme numeric(18,0)
);


ALTER TABLE public.fieldscreenschemeitem OWNER TO jira;

--
-- Name: fieldscreentab; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fieldscreentab (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description character varying(255),
    sequence numeric(18,0),
    fieldscreen numeric(18,0)
);


ALTER TABLE public.fieldscreentab OWNER TO jira;

--
-- Name: fileattachment; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE fileattachment (
    id numeric(18,0) NOT NULL,
    issueid numeric(18,0),
    mimetype character varying(255),
    filename character varying(255),
    created timestamp with time zone,
    filesize numeric(18,0),
    author character varying(255),
    zip numeric(9,0),
    thumbnailable numeric(9,0)
);


ALTER TABLE public.fileattachment OWNER TO jira;

--
-- Name: filtersubscription; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE filtersubscription (
    id numeric(18,0) NOT NULL,
    filter_i_d numeric(18,0),
    username character varying(60),
    groupname character varying(60),
    last_run timestamp with time zone,
    email_on_empty character varying(10)
);


ALTER TABLE public.filtersubscription OWNER TO jira;

--
-- Name: gadgetuserpreference; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE gadgetuserpreference (
    id numeric(18,0) NOT NULL,
    portletconfiguration numeric(18,0),
    userprefkey character varying(255),
    userprefvalue text
);


ALTER TABLE public.gadgetuserpreference OWNER TO jira;

--
-- Name: genericconfiguration; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE genericconfiguration (
    id numeric(18,0) NOT NULL,
    datatype character varying(60),
    datakey character varying(60),
    xmlvalue text
);


ALTER TABLE public.genericconfiguration OWNER TO jira;

--
-- Name: globalpermissionentry; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE globalpermissionentry (
    id numeric(18,0) NOT NULL,
    permission character varying(255),
    group_id character varying(255)
);


ALTER TABLE public.globalpermissionentry OWNER TO jira;

--
-- Name: groupbase; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE groupbase (
    id numeric(18,0) NOT NULL,
    groupname character varying(255)
);


ALTER TABLE public.groupbase OWNER TO jira;

--
-- Name: issue_field_option; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issue_field_option (
    id numeric(18,0) NOT NULL,
    option_id numeric(18,0),
    field_key character varying(255),
    option_value character varying(255),
    properties text
);


ALTER TABLE public.issue_field_option OWNER TO jira;

--
-- Name: issue_field_option_scope; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issue_field_option_scope (
    id numeric(18,0) NOT NULL,
    option_id numeric(18,0),
    entity_id character varying(255),
    scope_type character varying(255)
);


ALTER TABLE public.issue_field_option_scope OWNER TO jira;

--
-- Name: issuelink; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issuelink (
    id numeric(18,0) NOT NULL,
    linktype numeric(18,0),
    source numeric(18,0),
    destination numeric(18,0),
    sequence numeric(18,0)
);


ALTER TABLE public.issuelink OWNER TO jira;

--
-- Name: issuelinktype; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issuelinktype (
    id numeric(18,0) NOT NULL,
    linkname character varying(255),
    inward character varying(255),
    outward character varying(255),
    pstyle character varying(60)
);


ALTER TABLE public.issuelinktype OWNER TO jira;

--
-- Name: issuesecurityscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issuesecurityscheme (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text,
    defaultlevel numeric(18,0)
);


ALTER TABLE public.issuesecurityscheme OWNER TO jira;

--
-- Name: issuestatus; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issuestatus (
    id character varying(60) NOT NULL,
    sequence numeric(18,0),
    pname character varying(60),
    description text,
    iconurl character varying(255),
    statuscategory numeric(18,0)
);


ALTER TABLE public.issuestatus OWNER TO jira;

--
-- Name: issuetype; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issuetype (
    id character varying(60) NOT NULL,
    sequence numeric(18,0),
    pname character varying(60),
    pstyle character varying(60),
    description text,
    iconurl character varying(255),
    avatar numeric(18,0)
);


ALTER TABLE public.issuetype OWNER TO jira;

--
-- Name: issuetypescreenscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issuetypescreenscheme (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description character varying(255)
);


ALTER TABLE public.issuetypescreenscheme OWNER TO jira;

--
-- Name: issuetypescreenschemeentity; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE issuetypescreenschemeentity (
    id numeric(18,0) NOT NULL,
    issuetype character varying(255),
    scheme numeric(18,0),
    fieldscreenscheme numeric(18,0)
);


ALTER TABLE public.issuetypescreenschemeentity OWNER TO jira;

--
-- Name: jiraaction; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jiraaction (
    id numeric(18,0) NOT NULL,
    issueid numeric(18,0),
    author character varying(255),
    actiontype character varying(255),
    actionlevel character varying(255),
    rolelevel numeric(18,0),
    actionbody text,
    created timestamp with time zone,
    updateauthor character varying(255),
    updated timestamp with time zone,
    actionnum numeric(18,0)
);


ALTER TABLE public.jiraaction OWNER TO jira;

--
-- Name: jiradraftworkflows; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jiradraftworkflows (
    id numeric(18,0) NOT NULL,
    parentname character varying(255),
    descriptor text
);


ALTER TABLE public.jiradraftworkflows OWNER TO jira;

--
-- Name: jiraeventtype; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jiraeventtype (
    id numeric(18,0) NOT NULL,
    template_id numeric(18,0),
    name character varying(255),
    description text,
    event_type character varying(60)
);


ALTER TABLE public.jiraeventtype OWNER TO jira;

--
-- Name: jiraissue; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jiraissue (
    id numeric(18,0) NOT NULL,
    pkey character varying(255),
    issuenum numeric(18,0),
    project numeric(18,0),
    reporter character varying(255),
    assignee character varying(255),
    creator character varying(255),
    issuetype character varying(255),
    summary character varying(255),
    description text,
    environment text,
    priority character varying(255),
    resolution character varying(255),
    issuestatus character varying(255),
    created timestamp with time zone,
    updated timestamp with time zone,
    duedate timestamp with time zone,
    resolutiondate timestamp with time zone,
    votes numeric(18,0),
    watches numeric(18,0),
    timeoriginalestimate numeric(18,0),
    timeestimate numeric(18,0),
    timespent numeric(18,0),
    workflow_id numeric(18,0),
    security numeric(18,0),
    fixfor numeric(18,0),
    component numeric(18,0),
    archived character(1),
    archivedby character varying(255),
    archiveddate timestamp with time zone
);


ALTER TABLE public.jiraissue OWNER TO jira;

--
-- Name: jiraperms; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jiraperms (
    id numeric(18,0) NOT NULL,
    permtype numeric(18,0),
    projectid numeric(18,0),
    groupname character varying(255)
);


ALTER TABLE public.jiraperms OWNER TO jira;

--
-- Name: jiraworkflows; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jiraworkflows (
    id numeric(18,0) NOT NULL,
    workflowname character varying(255),
    creatorname character varying(255),
    descriptor text,
    islocked character varying(60)
);


ALTER TABLE public.jiraworkflows OWNER TO jira;

--
-- Name: jiraworkflowstatuses; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jiraworkflowstatuses (
    id numeric(18,0) NOT NULL,
    status character varying(255),
    parentname character varying(255)
);


ALTER TABLE public.jiraworkflowstatuses OWNER TO jira;

--
-- Name: jquartz_blob_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_blob_triggers (
    sched_name character varying(120),
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    blob_data bytea
);


ALTER TABLE public.jquartz_blob_triggers OWNER TO jira;

--
-- Name: jquartz_calendars; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_calendars (
    sched_name character varying(120),
    calendar_name character varying(200) NOT NULL,
    calendar bytea
);


ALTER TABLE public.jquartz_calendars OWNER TO jira;

--
-- Name: jquartz_cron_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_cron_triggers (
    sched_name character varying(120),
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    cron_expression character varying(120),
    time_zone_id character varying(80)
);


ALTER TABLE public.jquartz_cron_triggers OWNER TO jira;

--
-- Name: jquartz_fired_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_fired_triggers (
    sched_name character varying(120),
    entry_id character varying(95) NOT NULL,
    trigger_name character varying(200),
    trigger_group character varying(200),
    is_volatile boolean,
    instance_name character varying(200),
    fired_time numeric(18,0),
    sched_time numeric(18,0),
    priority numeric(9,0),
    state character varying(16),
    job_name character varying(200),
    job_group character varying(200),
    is_stateful boolean,
    is_nonconcurrent boolean,
    is_update_data boolean,
    requests_recovery boolean
);


ALTER TABLE public.jquartz_fired_triggers OWNER TO jira;

--
-- Name: jquartz_job_details; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_job_details (
    sched_name character varying(120),
    job_name character varying(200) NOT NULL,
    job_group character varying(200) NOT NULL,
    description character varying(250),
    job_class_name character varying(250),
    is_durable boolean,
    is_volatile boolean,
    is_stateful boolean,
    is_nonconcurrent boolean,
    is_update_data boolean,
    requests_recovery boolean,
    job_data bytea
);


ALTER TABLE public.jquartz_job_details OWNER TO jira;

--
-- Name: jquartz_job_listeners; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_job_listeners (
    job_name character varying(200) NOT NULL,
    job_group character varying(200) NOT NULL,
    job_listener character varying(200) NOT NULL
);


ALTER TABLE public.jquartz_job_listeners OWNER TO jira;

--
-- Name: jquartz_locks; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_locks (
    sched_name character varying(120),
    lock_name character varying(40) NOT NULL
);


ALTER TABLE public.jquartz_locks OWNER TO jira;

--
-- Name: jquartz_paused_trigger_grps; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_paused_trigger_grps (
    sched_name character varying(120),
    trigger_group character varying(200) NOT NULL
);


ALTER TABLE public.jquartz_paused_trigger_grps OWNER TO jira;

--
-- Name: jquartz_scheduler_state; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_scheduler_state (
    sched_name character varying(120),
    instance_name character varying(200) NOT NULL,
    last_checkin_time numeric(18,0),
    checkin_interval numeric(18,0)
);


ALTER TABLE public.jquartz_scheduler_state OWNER TO jira;

--
-- Name: jquartz_simple_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_simple_triggers (
    sched_name character varying(120),
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    repeat_count numeric(18,0),
    repeat_interval numeric(18,0),
    times_triggered numeric(18,0)
);


ALTER TABLE public.jquartz_simple_triggers OWNER TO jira;

--
-- Name: jquartz_simprop_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_simprop_triggers (
    sched_name character varying(120),
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    str_prop_1 character varying(512),
    str_prop_2 character varying(512),
    str_prop_3 character varying(512),
    int_prop_1 numeric(9,0),
    int_prop_2 numeric(9,0),
    long_prop_1 numeric(18,0),
    long_prop_2 numeric(18,0),
    dec_prop_1 numeric(13,4),
    dec_prop_2 numeric(13,4),
    bool_prop_1 boolean,
    bool_prop_2 boolean
);


ALTER TABLE public.jquartz_simprop_triggers OWNER TO jira;

--
-- Name: jquartz_trigger_listeners; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_trigger_listeners (
    trigger_name character varying(200),
    trigger_group character varying(200) NOT NULL,
    trigger_listener character varying(200) NOT NULL
);


ALTER TABLE public.jquartz_trigger_listeners OWNER TO jira;

--
-- Name: jquartz_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE jquartz_triggers (
    sched_name character varying(120),
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    job_name character varying(200),
    job_group character varying(200),
    is_volatile boolean,
    description character varying(250),
    next_fire_time numeric(18,0),
    prev_fire_time numeric(18,0),
    priority numeric(9,0),
    trigger_state character varying(16),
    trigger_type character varying(8),
    start_time numeric(18,0),
    end_time numeric(18,0),
    calendar_name character varying(200),
    misfire_instr numeric(4,0),
    job_data bytea
);


ALTER TABLE public.jquartz_triggers OWNER TO jira;

--
-- Name: label; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE label (
    id numeric(18,0) NOT NULL,
    fieldid numeric(18,0),
    issue numeric(18,0),
    label character varying(255)
);


ALTER TABLE public.label OWNER TO jira;

--
-- Name: licenserolesdefault; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE licenserolesdefault (
    id numeric(18,0) NOT NULL,
    license_role_name character varying(255)
);


ALTER TABLE public.licenserolesdefault OWNER TO jira;

--
-- Name: licenserolesgroup; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE licenserolesgroup (
    id numeric(18,0) NOT NULL,
    license_role_name character varying(255),
    group_id character varying(255),
    primary_group character(1)
);


ALTER TABLE public.licenserolesgroup OWNER TO jira;

--
-- Name: listenerconfig; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE listenerconfig (
    id numeric(18,0) NOT NULL,
    clazz character varying(255),
    listenername character varying(255)
);


ALTER TABLE public.listenerconfig OWNER TO jira;

--
-- Name: mailserver; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE mailserver (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text,
    mailfrom character varying(255),
    prefix character varying(60),
    smtp_port character varying(60),
    protocol character varying(60),
    server_type character varying(60),
    servername character varying(255),
    jndilocation character varying(255),
    mailusername character varying(255),
    mailpassword character varying(255),
    istlsrequired character varying(60),
    timeout numeric(18,0),
    socks_port character varying(60),
    socks_host character varying(60)
);


ALTER TABLE public.mailserver OWNER TO jira;

--
-- Name: managedconfigurationitem; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE managedconfigurationitem (
    id numeric(18,0) NOT NULL,
    item_id character varying(255),
    item_type character varying(255),
    managed character varying(10),
    access_level character varying(255),
    source character varying(255),
    description_key character varying(255)
);


ALTER TABLE public.managedconfigurationitem OWNER TO jira;

--
-- Name: membershipbase; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE membershipbase (
    id numeric(18,0) NOT NULL,
    user_name character varying(255),
    group_name character varying(255)
);


ALTER TABLE public.membershipbase OWNER TO jira;

--
-- Name: moved_issue_key; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE moved_issue_key (
    id numeric(18,0) NOT NULL,
    old_issue_key character varying(255),
    issue_id numeric(18,0)
);


ALTER TABLE public.moved_issue_key OWNER TO jira;

--
-- Name: nodeassociation; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE nodeassociation (
    source_node_id numeric(18,0) NOT NULL,
    source_node_entity character varying(60) NOT NULL,
    sink_node_id numeric(18,0) NOT NULL,
    sink_node_entity character varying(60) NOT NULL,
    association_type character varying(60) NOT NULL,
    sequence numeric(9,0)
);


ALTER TABLE public.nodeassociation OWNER TO jira;

--
-- Name: nodeindexcounter; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE nodeindexcounter (
    id numeric(18,0) NOT NULL,
    node_id character varying(60),
    sending_node_id character varying(60),
    index_operation_id numeric(18,0)
);


ALTER TABLE public.nodeindexcounter OWNER TO jira;

--
-- Name: notification; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE notification (
    id numeric(18,0) NOT NULL,
    scheme numeric(18,0),
    event character varying(60),
    event_type_id numeric(18,0),
    template_id numeric(18,0),
    notif_type character varying(60),
    notif_parameter character varying(60)
);


ALTER TABLE public.notification OWNER TO jira;

--
-- Name: notificationinstance; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE notificationinstance (
    id numeric(18,0) NOT NULL,
    notificationtype character varying(60),
    source numeric(18,0),
    emailaddress character varying(255),
    messageid character varying(255)
);


ALTER TABLE public.notificationinstance OWNER TO jira;

--
-- Name: notificationscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE notificationscheme (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text
);


ALTER TABLE public.notificationscheme OWNER TO jira;

--
-- Name: oauthconsumer; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE oauthconsumer (
    id numeric(18,0) NOT NULL,
    created timestamp with time zone,
    consumername character varying(255),
    consumer_key character varying(255),
    consumerservice character varying(255),
    public_key text,
    private_key text,
    description text,
    callback text,
    signature_method character varying(60),
    shared_secret text
);


ALTER TABLE public.oauthconsumer OWNER TO jira;

--
-- Name: oauthconsumertoken; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE oauthconsumertoken (
    id numeric(18,0) NOT NULL,
    created timestamp with time zone,
    token_key character varying(255),
    token character varying(255),
    token_secret character varying(255),
    token_type character varying(60),
    consumer_key character varying(255)
);


ALTER TABLE public.oauthconsumertoken OWNER TO jira;

--
-- Name: oauthspconsumer; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE oauthspconsumer (
    id numeric(18,0) NOT NULL,
    created timestamp with time zone,
    consumer_key character varying(255),
    consumername character varying(255),
    public_key text,
    description text,
    callback text,
    two_l_o_allowed character varying(60),
    executing_two_l_o_user character varying(255),
    two_l_o_impersonation_allowed character varying(60),
    three_l_o_allowed character varying(60)
);


ALTER TABLE public.oauthspconsumer OWNER TO jira;

--
-- Name: oauthsptoken; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE oauthsptoken (
    id numeric(18,0) NOT NULL,
    created timestamp with time zone,
    token character varying(255),
    token_secret character varying(255),
    token_type character varying(60),
    consumer_key character varying(255),
    username character varying(255),
    ttl numeric(18,0),
    spauth character varying(60),
    callback text,
    spverifier character varying(255),
    spversion character varying(60),
    session_handle character varying(255),
    session_creation_time timestamp with time zone,
    session_last_renewal_time timestamp with time zone,
    session_time_to_live timestamp with time zone
);


ALTER TABLE public.oauthsptoken OWNER TO jira;

--
-- Name: optionconfiguration; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE optionconfiguration (
    id numeric(18,0) NOT NULL,
    fieldid character varying(60),
    optionid character varying(60),
    fieldconfig numeric(18,0),
    sequence numeric(18,0)
);


ALTER TABLE public.optionconfiguration OWNER TO jira;

--
-- Name: os_currentstep; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE os_currentstep (
    id numeric(18,0) NOT NULL,
    entry_id numeric(18,0),
    step_id numeric(9,0),
    action_id numeric(9,0),
    owner character varying(60),
    start_date timestamp with time zone,
    due_date timestamp with time zone,
    finish_date timestamp with time zone,
    status character varying(60),
    caller character varying(60)
);


ALTER TABLE public.os_currentstep OWNER TO jira;

--
-- Name: os_currentstep_prev; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE os_currentstep_prev (
    id numeric(18,0) NOT NULL,
    previous_id numeric(18,0) NOT NULL
);


ALTER TABLE public.os_currentstep_prev OWNER TO jira;

--
-- Name: os_historystep; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE os_historystep (
    id numeric(18,0) NOT NULL,
    entry_id numeric(18,0),
    step_id numeric(9,0),
    action_id numeric(9,0),
    owner character varying(60),
    start_date timestamp with time zone,
    due_date timestamp with time zone,
    finish_date timestamp with time zone,
    status character varying(60),
    caller character varying(60)
);


ALTER TABLE public.os_historystep OWNER TO jira;

--
-- Name: os_historystep_prev; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE os_historystep_prev (
    id numeric(18,0) NOT NULL,
    previous_id numeric(18,0) NOT NULL
);


ALTER TABLE public.os_historystep_prev OWNER TO jira;

--
-- Name: os_wfentry; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE os_wfentry (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    initialized numeric(9,0),
    state numeric(9,0)
);


ALTER TABLE public.os_wfentry OWNER TO jira;

--
-- Name: permissionscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE permissionscheme (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text
);


ALTER TABLE public.permissionscheme OWNER TO jira;

--
-- Name: permissionschemeattribute; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE permissionschemeattribute (
    id numeric(18,0) NOT NULL,
    scheme numeric(18,0),
    attribute_key character varying(255),
    attribute_value text
);


ALTER TABLE public.permissionschemeattribute OWNER TO jira;

--
-- Name: pluginstate; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE pluginstate (
    pluginkey character varying(255) NOT NULL,
    pluginenabled character varying(60)
);


ALTER TABLE public.pluginstate OWNER TO jira;

--
-- Name: pluginversion; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE pluginversion (
    id numeric(18,0) NOT NULL,
    pluginname character varying(255),
    pluginkey character varying(255),
    pluginversion character varying(255),
    created timestamp with time zone
);


ALTER TABLE public.pluginversion OWNER TO jira;

--
-- Name: portalpage; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE portalpage (
    id numeric(18,0) NOT NULL,
    username character varying(255),
    pagename character varying(255),
    description character varying(255),
    sequence numeric(18,0),
    fav_count numeric(18,0),
    layout character varying(255),
    ppversion numeric(18,0)
);


ALTER TABLE public.portalpage OWNER TO jira;

--
-- Name: portletconfiguration; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE portletconfiguration (
    id numeric(18,0) NOT NULL,
    portalpage numeric(18,0),
    portlet_id character varying(255),
    column_number numeric(9,0),
    positionseq numeric(9,0),
    gadget_xml text,
    color character varying(255),
    dashboard_module_complete_key text
);


ALTER TABLE public.portletconfiguration OWNER TO jira;

--
-- Name: priority; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE priority (
    id character varying(60) NOT NULL,
    sequence numeric(18,0),
    pname character varying(60),
    description text,
    iconurl character varying(255),
    status_color character varying(60)
);


ALTER TABLE public.priority OWNER TO jira;

--
-- Name: productlicense; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE productlicense (
    id numeric(18,0) NOT NULL,
    license text
);


ALTER TABLE public.productlicense OWNER TO jira;

--
-- Name: project; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE project (
    id numeric(18,0) NOT NULL,
    pname character varying(255),
    url character varying(255),
    lead character varying(255),
    description text,
    pkey character varying(255),
    pcounter numeric(18,0),
    assigneetype numeric(18,0),
    avatar numeric(18,0),
    originalkey character varying(255),
    projecttype character varying(255)
);


ALTER TABLE public.project OWNER TO jira;

--
-- Name: project_key; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE project_key (
    id numeric(18,0) NOT NULL,
    project_id numeric(18,0),
    project_key character varying(255)
);


ALTER TABLE public.project_key OWNER TO jira;

--
-- Name: projectcategory; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE projectcategory (
    id numeric(18,0) NOT NULL,
    cname character varying(255),
    description text
);


ALTER TABLE public.projectcategory OWNER TO jira;

--
-- Name: projectchangedtime; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE projectchangedtime (
    project_id numeric(18,0) NOT NULL,
    issue_changed_time timestamp with time zone
);


ALTER TABLE public.projectchangedtime OWNER TO jira;

--
-- Name: projectrole; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE projectrole (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text
);


ALTER TABLE public.projectrole OWNER TO jira;

--
-- Name: projectroleactor; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE projectroleactor (
    id numeric(18,0) NOT NULL,
    pid numeric(18,0),
    projectroleid numeric(18,0),
    roletype character varying(255),
    roletypeparameter character varying(255)
);


ALTER TABLE public.projectroleactor OWNER TO jira;

--
-- Name: projectversion; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE projectversion (
    id numeric(18,0) NOT NULL,
    project numeric(18,0),
    vname character varying(255),
    description text,
    sequence numeric(18,0),
    released character varying(10),
    archived character varying(10),
    url character varying(255),
    startdate timestamp with time zone,
    releasedate timestamp with time zone
);


ALTER TABLE public.projectversion OWNER TO jira;

--
-- Name: propertydata; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE propertydata (
    id numeric(18,0) NOT NULL,
    propertyvalue oid
);


ALTER TABLE public.propertydata OWNER TO jira;

--
-- Name: propertydate; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE propertydate (
    id numeric(18,0) NOT NULL,
    propertyvalue timestamp with time zone
);


ALTER TABLE public.propertydate OWNER TO jira;

--
-- Name: propertydecimal; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE propertydecimal (
    id numeric(18,0) NOT NULL,
    propertyvalue double precision
);


ALTER TABLE public.propertydecimal OWNER TO jira;

--
-- Name: propertyentry; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE propertyentry (
    id numeric(18,0) NOT NULL,
    entity_name character varying(255),
    entity_id numeric(18,0),
    property_key character varying(255),
    propertytype numeric(9,0)
);


ALTER TABLE public.propertyentry OWNER TO jira;

--
-- Name: propertynumber; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE propertynumber (
    id numeric(18,0) NOT NULL,
    propertyvalue numeric(18,0)
);


ALTER TABLE public.propertynumber OWNER TO jira;

--
-- Name: propertystring; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE propertystring (
    id numeric(18,0) NOT NULL,
    propertyvalue text
);


ALTER TABLE public.propertystring OWNER TO jira;

--
-- Name: propertytext; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE propertytext (
    id numeric(18,0) NOT NULL,
    propertyvalue text
);


ALTER TABLE public.propertytext OWNER TO jira;

--
-- Name: qrtz_calendars; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE qrtz_calendars (
    id numeric(18,0),
    calendar_name character varying(255) NOT NULL,
    calendar text
);


ALTER TABLE public.qrtz_calendars OWNER TO jira;

--
-- Name: qrtz_cron_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE qrtz_cron_triggers (
    id numeric(18,0) NOT NULL,
    trigger_id numeric(18,0),
    cronexperssion character varying(255)
);


ALTER TABLE public.qrtz_cron_triggers OWNER TO jira;

--
-- Name: qrtz_fired_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE qrtz_fired_triggers (
    id numeric(18,0),
    entry_id character varying(255) NOT NULL,
    trigger_id numeric(18,0),
    trigger_listener character varying(255),
    fired_time timestamp with time zone,
    trigger_state character varying(255)
);


ALTER TABLE public.qrtz_fired_triggers OWNER TO jira;

--
-- Name: qrtz_job_details; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE qrtz_job_details (
    id numeric(18,0) NOT NULL,
    job_name character varying(255),
    job_group character varying(255),
    class_name character varying(255),
    is_durable character varying(60),
    is_stateful character varying(60),
    requests_recovery character varying(60),
    job_data character varying(255)
);


ALTER TABLE public.qrtz_job_details OWNER TO jira;

--
-- Name: qrtz_job_listeners; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE qrtz_job_listeners (
    id numeric(18,0) NOT NULL,
    job numeric(18,0),
    job_listener character varying(255)
);


ALTER TABLE public.qrtz_job_listeners OWNER TO jira;

--
-- Name: qrtz_simple_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE qrtz_simple_triggers (
    id numeric(18,0) NOT NULL,
    trigger_id numeric(18,0),
    repeat_count numeric(9,0),
    repeat_interval numeric(18,0),
    times_triggered numeric(9,0)
);


ALTER TABLE public.qrtz_simple_triggers OWNER TO jira;

--
-- Name: qrtz_trigger_listeners; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE qrtz_trigger_listeners (
    id numeric(18,0) NOT NULL,
    trigger_id numeric(18,0),
    trigger_listener character varying(255)
);


ALTER TABLE public.qrtz_trigger_listeners OWNER TO jira;

--
-- Name: qrtz_triggers; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE qrtz_triggers (
    id numeric(18,0) NOT NULL,
    trigger_name character varying(255),
    trigger_group character varying(255),
    job numeric(18,0),
    next_fire timestamp with time zone,
    trigger_state character varying(255),
    trigger_type character varying(60),
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    calendar_name character varying(255),
    misfire_instr numeric(9,0)
);


ALTER TABLE public.qrtz_triggers OWNER TO jira;

--
-- Name: reindex_component; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE reindex_component (
    id numeric(18,0) NOT NULL,
    request_id numeric(18,0),
    affected_index character varying(60),
    entity_type character varying(60)
);


ALTER TABLE public.reindex_component OWNER TO jira;

--
-- Name: reindex_request; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE reindex_request (
    id numeric(18,0) NOT NULL,
    type character varying(60),
    request_time timestamp with time zone,
    start_time timestamp with time zone,
    completion_time timestamp with time zone,
    status character varying(60),
    execution_node_id character varying(60),
    query text
);


ALTER TABLE public.reindex_request OWNER TO jira;

--
-- Name: remembermetoken; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE remembermetoken (
    id numeric(18,0) NOT NULL,
    created timestamp with time zone,
    token character varying(255),
    username character varying(255)
);


ALTER TABLE public.remembermetoken OWNER TO jira;

--
-- Name: remotelink; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE remotelink (
    id numeric(18,0) NOT NULL,
    issueid numeric(18,0),
    globalid character varying(255),
    title character varying(255),
    summary text,
    url text,
    iconurl text,
    icontitle text,
    relationship character varying(255),
    resolved character(1),
    statusname character varying(255),
    statusdescription text,
    statusiconurl text,
    statusicontitle text,
    statusiconlink text,
    statuscategorykey character varying(255),
    statuscategorycolorname character varying(255),
    applicationtype character varying(255),
    applicationname character varying(255)
);


ALTER TABLE public.remotelink OWNER TO jira;

--
-- Name: replicatedindexoperation; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE replicatedindexoperation (
    id numeric(18,0) NOT NULL,
    index_time timestamp with time zone,
    node_id character varying(60),
    affected_index character varying(60),
    entity_type character varying(60),
    affected_ids text,
    operation character varying(60),
    filename character varying(255)
);


ALTER TABLE public.replicatedindexoperation OWNER TO jira;

--
-- Name: resolution; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE resolution (
    id character varying(60) NOT NULL,
    sequence numeric(18,0),
    pname character varying(60),
    description text,
    iconurl character varying(255)
);


ALTER TABLE public.resolution OWNER TO jira;

--
-- Name: rundetails; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE rundetails (
    id numeric(18,0) NOT NULL,
    job_id character varying(255),
    start_time timestamp with time zone,
    run_duration numeric(18,0),
    run_outcome character(1),
    info_message character varying(255)
);


ALTER TABLE public.rundetails OWNER TO jira;

--
-- Name: schemeissuesecurities; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE schemeissuesecurities (
    id numeric(18,0) NOT NULL,
    scheme numeric(18,0),
    security numeric(18,0),
    sec_type character varying(255),
    sec_parameter character varying(255)
);


ALTER TABLE public.schemeissuesecurities OWNER TO jira;

--
-- Name: schemeissuesecuritylevels; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE schemeissuesecuritylevels (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text,
    scheme numeric(18,0)
);


ALTER TABLE public.schemeissuesecuritylevels OWNER TO jira;

--
-- Name: schemepermissions; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE schemepermissions (
    id numeric(18,0) NOT NULL,
    scheme numeric(18,0),
    permission numeric(18,0),
    perm_type character varying(255),
    perm_parameter character varying(255),
    permission_key character varying(255)
);


ALTER TABLE public.schemepermissions OWNER TO jira;

--
-- Name: searchrequest; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE searchrequest (
    id numeric(18,0) NOT NULL,
    filtername character varying(255),
    authorname character varying(255),
    description text,
    username character varying(255),
    groupname character varying(255),
    projectid numeric(18,0),
    reqcontent text,
    fav_count numeric(18,0),
    filtername_lower character varying(255)
);


ALTER TABLE public.searchrequest OWNER TO jira;

--
-- Name: sequence_value_item; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE sequence_value_item (
    seq_name character varying(60) NOT NULL,
    seq_id numeric(18,0)
);


ALTER TABLE public.sequence_value_item OWNER TO jira;

--
-- Name: serviceconfig; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE serviceconfig (
    id numeric(18,0) NOT NULL,
    delaytime numeric(18,0),
    clazz character varying(255),
    servicename character varying(255),
    cron_expression character varying(255)
);


ALTER TABLE public.serviceconfig OWNER TO jira;

--
-- Name: sharepermissions; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE sharepermissions (
    id numeric(18,0) NOT NULL,
    entityid numeric(18,0),
    entitytype character varying(60),
    sharetype character varying(10),
    param1 character varying(255),
    param2 character varying(60),
    rights numeric(9,0)
);


ALTER TABLE public.sharepermissions OWNER TO jira;

--
-- Name: tempattachmentsmonitor; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE tempattachmentsmonitor (
    temporary_attachment_id character varying(255) NOT NULL,
    form_token character varying(255),
    file_name character varying(255),
    content_type character varying(255),
    file_size numeric(18,0),
    created_time numeric(18,0)
);


ALTER TABLE public.tempattachmentsmonitor OWNER TO jira;

--
-- Name: trackback_ping; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE trackback_ping (
    id numeric(18,0) NOT NULL,
    issue numeric(18,0),
    url character varying(255),
    title character varying(255),
    blogname character varying(255),
    excerpt character varying(255),
    created timestamp with time zone
);


ALTER TABLE public.trackback_ping OWNER TO jira;

--
-- Name: trustedapp; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE trustedapp (
    id numeric(18,0) NOT NULL,
    application_id character varying(255),
    name character varying(255),
    public_key text,
    ip_match text,
    url_match text,
    timeout numeric(18,0),
    created timestamp with time zone,
    created_by character varying(255),
    updated timestamp with time zone,
    updated_by character varying(255)
);


ALTER TABLE public.trustedapp OWNER TO jira;

--
-- Name: upgradehistory; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE upgradehistory (
    id numeric(18,0),
    upgradeclass character varying(255) NOT NULL,
    targetbuild character varying(255),
    status character varying(255),
    downgradetaskrequired character(1)
);


ALTER TABLE public.upgradehistory OWNER TO jira;

--
-- Name: upgradetaskhistory; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE upgradetaskhistory (
    id numeric(18,0) NOT NULL,
    upgrade_task_factory_key character varying(255),
    build_number numeric(9,0),
    status character varying(60),
    upgrade_type character varying(10)
);


ALTER TABLE public.upgradetaskhistory OWNER TO jira;

--
-- Name: upgradetaskhistoryauditlog; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE upgradetaskhistoryauditlog (
    id numeric(18,0) NOT NULL,
    upgrade_task_factory_key character varying(255),
    build_number numeric(9,0),
    status character varying(60),
    upgrade_type character varying(10),
    timeperformed timestamp with time zone,
    action character varying(10)
);


ALTER TABLE public.upgradetaskhistoryauditlog OWNER TO jira;

--
-- Name: upgradeversionhistory; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE upgradeversionhistory (
    id numeric(18,0),
    timeperformed timestamp with time zone,
    targetbuild character varying(255) NOT NULL,
    targetversion character varying(255)
);


ALTER TABLE public.upgradeversionhistory OWNER TO jira;

--
-- Name: userassociation; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE userassociation (
    source_name character varying(60) NOT NULL,
    sink_node_id numeric(18,0) NOT NULL,
    sink_node_entity character varying(60) NOT NULL,
    association_type character varying(60) NOT NULL,
    sequence numeric(9,0),
    created timestamp with time zone
);


ALTER TABLE public.userassociation OWNER TO jira;

--
-- Name: userbase; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE userbase (
    id numeric(18,0) NOT NULL,
    username character varying(255),
    password_hash character varying(255)
);


ALTER TABLE public.userbase OWNER TO jira;

--
-- Name: userhistoryitem; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE userhistoryitem (
    id numeric(18,0) NOT NULL,
    entitytype character varying(10),
    entityid character varying(60),
    username character varying(255),
    lastviewed numeric(18,0),
    data text
);


ALTER TABLE public.userhistoryitem OWNER TO jira;

--
-- Name: userpickerfilter; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE userpickerfilter (
    id numeric(18,0) NOT NULL,
    customfield numeric(18,0),
    customfieldconfig numeric(18,0),
    enabled character varying(60)
);


ALTER TABLE public.userpickerfilter OWNER TO jira;

--
-- Name: userpickerfiltergroup; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE userpickerfiltergroup (
    id numeric(18,0) NOT NULL,
    userpickerfilter numeric(18,0),
    groupname character varying(255)
);


ALTER TABLE public.userpickerfiltergroup OWNER TO jira;

--
-- Name: userpickerfilterrole; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE userpickerfilterrole (
    id numeric(18,0) NOT NULL,
    userpickerfilter numeric(18,0),
    projectroleid numeric(18,0)
);


ALTER TABLE public.userpickerfilterrole OWNER TO jira;

--
-- Name: versioncontrol; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE versioncontrol (
    id numeric(18,0) NOT NULL,
    vcsname character varying(255),
    vcsdescription character varying(255),
    vcstype character varying(255)
);


ALTER TABLE public.versioncontrol OWNER TO jira;

--
-- Name: votehistory; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE votehistory (
    id numeric(18,0) NOT NULL,
    issueid numeric(18,0),
    votes numeric(18,0),
    "timestamp" timestamp with time zone
);


ALTER TABLE public.votehistory OWNER TO jira;

--
-- Name: workflowscheme; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE workflowscheme (
    id numeric(18,0) NOT NULL,
    name character varying(255),
    description text
);


ALTER TABLE public.workflowscheme OWNER TO jira;

--
-- Name: workflowschemeentity; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE workflowschemeentity (
    id numeric(18,0) NOT NULL,
    scheme numeric(18,0),
    workflow character varying(255),
    issuetype character varying(255)
);


ALTER TABLE public.workflowschemeentity OWNER TO jira;

--
-- Name: worklog; Type: TABLE; Schema: public; Owner: jira; Tablespace: 
--

CREATE TABLE worklog (
    id numeric(18,0) NOT NULL,
    issueid numeric(18,0),
    author character varying(255),
    grouplevel character varying(255),
    rolelevel numeric(18,0),
    worklogbody text,
    created timestamp with time zone,
    updateauthor character varying(255),
    updated timestamp with time zone,
    startdate timestamp with time zone,
    timeworked numeric(18,0)
);


ALTER TABLE public.worklog OWNER TO jira;

--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_0201F0_KB_HELPFUL_AGGR" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_0201F0_KB_HELPFUL_AGGR_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_0201F0_KB_VIEW_AGGR" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_0201F0_KB_VIEW_AGGR_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_0201F0_STATS_EVENT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_0201F0_STATS_EVENT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_0201F0_STATS_EVENT_PARAM" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_0201F0_STATS_EVENT_PARAM_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_21D670_WHITELIST_RULES" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_21D670_WHITELIST_RULES_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_21F425_MESSAGE_MAPPING_AO" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_21F425_MESSAGE_MAPPING_AO_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_21F425_USER_PROPERTY_AO" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_21F425_USER_PROPERTY_AO_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILCHANNEL" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_2C4E5C_MAILCHANNEL_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILCONNECTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_2C4E5C_MAILCONNECTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILGLOBALHANDLER" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_2C4E5C_MAILGLOBALHANDLER_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILHANDLER" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_2C4E5C_MAILHANDLER_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILITEM" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_2C4E5C_MAILITEM_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILITEMAUDIT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_2C4E5C_MAILITEMAUDIT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILITEMCHUNK" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_2C4E5C_MAILITEMCHUNK_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILRUNAUDIT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_2C4E5C_MAILRUNAUDIT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_319474_MESSAGE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_319474_MESSAGE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_319474_MESSAGE_PROPERTY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_319474_MESSAGE_PROPERTY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_319474_QUEUE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_319474_QUEUE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_319474_QUEUE_PROPERTY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_319474_QUEUE_PROPERTY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_38321B_CUSTOM_CONTENT_LINK" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_38321B_CUSTOM_CONTENT_LINK_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_3B1893_LOOP_DETECTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_3B1893_LOOP_DETECTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_4789DD_HEALTH_CHECK_STATUS" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_4789DD_HEALTH_CHECK_STATUS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_4789DD_PROPERTIES" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_4789DD_PROPERTIES_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_4789DD_READ_NOTIFICATIONS" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_4789DD_READ_NOTIFICATIONS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_4789DD_TASK_MONITOR" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_4789DD_TASK_MONITOR_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_4AEACD_WEBHOOK_DAO" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_4AEACD_WEBHOOK_DAO_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_4E8AE6_NOTIF_BATCH_QUEUE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_4E8AE6_NOTIF_BATCH_QUEUE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_4E8AE6_OUT_EMAIL_SETTINGS" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_4E8AE6_OUT_EMAIL_SETTINGS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_ASYNCUPGRADERECORD" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_ASYNCUPGRADERECORD_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CAPABILITY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_CAPABILITY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKB" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_CONFLUENCEKB_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBENABLED" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_CONFLUENCEKBENABLED_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBLABELS" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_CONFLUENCEKBLABELS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CUSTOMGLOBALTHEME" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_CUSTOMGLOBALTHEME_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CUSTOMTHEME" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_CUSTOMTHEME_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_EMAILCHANNELSETTING" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_EMAILCHANNELSETTING_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_EMAILSETTINGS" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_EMAILSETTINGS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_GOAL" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_GOAL_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_GROUP" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_GROUP_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_GROUPTOREQUESTTYPE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_GROUPTOREQUESTTYPE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_IMAGES" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_IMAGES_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_METRICCONDITION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_METRICCONDITION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_ORGANIZATION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION_MEMBER" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_ORGANIZATION_MEMBER_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION_PROJECT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_ORGANIZATION_PROJECT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_OUT_EMAIL_SETTINGS" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_OUT_EMAIL_SETTINGS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_PARTICIPANTSETTINGS" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_PARTICIPANTSETTINGS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_QUEUE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_QUEUE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_QUEUECOLUMN" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_QUEUECOLUMN_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_REPORT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_REPORT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_SERIES" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_SERIES_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_SERVICEDESK" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_SERVICEDESK_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_SLAAUDITLOG" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_SLAAUDITLOG_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_SLAAUDITLOGDATA" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_SLAAUDITLOGDATA_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_STATUSMAPPING" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_STATUSMAPPING_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_SUBSCRIPTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_SUBSCRIPTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_SYNCUPGRADERECORD" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_SYNCUPGRADERECORD_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_THRESHOLD" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_THRESHOLD_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_TIMEMETRIC" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_TIMEMETRIC_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_VIEWPORT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_VIEWPORT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFIELD" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_VIEWPORTFIELD_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFIELDVALUE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_VIEWPORTFIELDVALUE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFORM" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_54307E_VIEWPORTFORM_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_550953_SHORTCUT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_550953_SHORTCUT_ID_seq"'::regclass);


--
-- Name: ACTIVITY_ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_ACTIVITY_ENTITY" ALTER COLUMN "ACTIVITY_ID" SET DEFAULT nextval('"AO_563AEE_ACTIVITY_ENTITY_ACTIVITY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_ACTOR_ENTITY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_563AEE_ACTOR_ENTITY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_MEDIA_LINK_ENTITY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_563AEE_MEDIA_LINK_ENTITY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_OBJECT_ENTITY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_563AEE_OBJECT_ENTITY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_TARGET_ENTITY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_563AEE_TARGET_ENTITY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_56464C_APPROVAL" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_56464C_APPROVAL_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_56464C_APPROVER" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_56464C_APPROVER_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_56464C_APPROVERDECISION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_56464C_APPROVERDECISION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_56464C_NOTIFICATIONRECORD" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_56464C_NOTIFICATIONRECORD_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_587B34_PROJECT_CONFIG" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_587B34_PROJECT_CONFIG_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_5FB9D7_AOHIP_CHAT_LINK" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_5FB9D7_AOHIP_CHAT_LINK_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_5FB9D7_AOHIP_CHAT_USER" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_5FB9D7_AOHIP_CHAT_USER_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_733371_EVENT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_733371_EVENT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_733371_EVENT_PARAMETER" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_733371_EVENT_PARAMETER_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_733371_EVENT_RECIPIENT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_733371_EVENT_RECIPIENT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_7A2604_CALENDAR" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_7A2604_CALENDAR_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_7A2604_HOLIDAY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_7A2604_HOLIDAY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_7A2604_WORKINGTIME" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_7A2604_WORKINGTIME_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_97EDAB_USERINVITATION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_97EDAB_USERINVITATION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_EXEC_RULE_MSG_ITEM" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_EXEC_RULE_MSG_ITEM_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_CONDITION_CONFIG" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_IF_CONDITION_CONFIG_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_COND_CONF_DATA" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_IF_COND_CONF_DATA_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_COND_EXECUTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_IF_COND_EXECUTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_EXECUTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_IF_EXECUTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_THEN" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_IF_THEN_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_THEN_EXECUTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_IF_THEN_EXECUTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_PROJECT_USER_CONTEXT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_PROJECT_USER_CONTEXT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RSETREV_PROJ_CONTEXT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_RSETREV_PROJ_CONTEXT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RSETREV_USER_CONTEXT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_RSETREV_USER_CONTEXT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RULE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_RULE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RULESET" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_RULESET_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RULESET_REVISION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_RULESET_REVISION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RULE_EXECUTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_RULE_EXECUTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACTION_CONFIG" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_THEN_ACTION_CONFIG_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACT_CONF_DATA" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_THEN_ACT_CONF_DATA_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACT_EXECUTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_THEN_ACT_EXECUTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_EXECUTION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_THEN_EXECUTION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_WHEN_HANDLER_CONFIG" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_WHEN_HANDLER_CONFIG_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_WHEN_HAND_CONF_DATA" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_9B2E3B_WHEN_HAND_CONF_DATA_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_A0B856_WEB_HOOK_LISTENER_AO" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_A0B856_WEB_HOOK_LISTENER_AO_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_B9A0F0_APPLIED_TEMPLATE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_B9A0F0_APPLIED_TEMPLATE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C16815_ALERT_AO" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_C16815_ALERT_AO_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_LINGO" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_C7F17E_LINGO_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_LINGO_REVISION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_C7F17E_LINGO_REVISION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_LINGO_TRANSLATION" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_C7F17E_LINGO_TRANSLATION_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_PROJECT_LANGUAGE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_C7F17E_PROJECT_LANGUAGE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_PROJECT_LANG_CONFIG" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_C7F17E_PROJECT_LANG_CONFIG_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_PROJECT_LANG_REV" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_C7F17E_PROJECT_LANG_REV_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_D530BB_CANNEDRESPONSE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_D530BB_CANNEDRESPONSE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_D530BB_CANNEDRESPONSEAUDIT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_D530BB_CANNEDRESPONSEAUDIT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_D530BB_CANNEDRESPONSEUSAGE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_D530BB_CANNEDRESPONSEUSAGE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_D530BB_CRAUDITACTIONDATA" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_D530BB_CRAUDITACTIONDATA_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_ED669C_SEEN_ASSERTIONS" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_ED669C_SEEN_ASSERTIONS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_F1B27B_HISTORY_RECORD" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_F1B27B_HISTORY_RECORD_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_F1B27B_KEY_COMPONENT" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_F1B27B_KEY_COMPONENT_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_F1B27B_KEY_COMP_HISTORY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_F1B27B_KEY_COMP_HISTORY_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_F1B27B_PROMISE" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_F1B27B_PROMISE_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_F1B27B_PROMISE_HISTORY" ALTER COLUMN "ID" SET DEFAULT nextval('"AO_F1B27B_PROMISE_HISTORY_ID_seq"'::regclass);


--
-- Data for Name: AO_0201F0_KB_HELPFUL_AGGR; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_0201F0_KB_HELPFUL_AGGR" ("COUNT", "ID", "SERVICE_DESK_ID", "START_TIME") FROM stdin;
\.


--
-- Name: AO_0201F0_KB_HELPFUL_AGGR_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_0201F0_KB_HELPFUL_AGGR_ID_seq"', 1, false);


--
-- Data for Name: AO_0201F0_KB_VIEW_AGGR; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_0201F0_KB_VIEW_AGGR" ("COUNT", "ID", "SERVICE_DESK_ID", "START_TIME") FROM stdin;
\.


--
-- Name: AO_0201F0_KB_VIEW_AGGR_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_0201F0_KB_VIEW_AGGR_ID_seq"', 1, false);


--
-- Data for Name: AO_0201F0_STATS_EVENT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_0201F0_STATS_EVENT" ("EVENT_KEY", "EVENT_TIME", "ID") FROM stdin;
\.


--
-- Name: AO_0201F0_STATS_EVENT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_0201F0_STATS_EVENT_ID_seq"', 1, false);


--
-- Data for Name: AO_0201F0_STATS_EVENT_PARAM; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_0201F0_STATS_EVENT_PARAM" ("ID", "PARAM_NAME", "PARAM_VALUE", "STATS_EVENT_ID") FROM stdin;
\.


--
-- Name: AO_0201F0_STATS_EVENT_PARAM_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_0201F0_STATS_EVENT_PARAM_ID_seq"', 1, false);


--
-- Data for Name: AO_0AC321_RECOMMENDATION_AO; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_0AC321_RECOMMENDATION_AO" ("CATEGORY", "CUSTOM_FIELD_ID", "ID", "NAME", "PERFORMANCE_IMPACT", "PROJECT_IDS", "RESOLVED", "TYPE") FROM stdin;
\.


--
-- Data for Name: AO_21D670_WHITELIST_RULES; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_21D670_WHITELIST_RULES" ("ALLOWINBOUND", "EXPRESSION", "ID", "TYPE") FROM stdin;
f	http://www.atlassian.com/*	1	WILDCARD_EXPRESSION
f	http://www.atlassian.com/*	2	WILDCARD_EXPRESSION
\.


--
-- Name: AO_21D670_WHITELIST_RULES_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_21D670_WHITELIST_RULES_ID_seq"', 2, true);


--
-- Data for Name: AO_21F425_MESSAGE_AO; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_21F425_MESSAGE_AO" ("CONTENT", "ID") FROM stdin;
\.


--
-- Data for Name: AO_21F425_MESSAGE_MAPPING_AO; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_21F425_MESSAGE_MAPPING_AO" ("ID", "MESSAGE_ID", "USER_HASH") FROM stdin;
\.


--
-- Name: AO_21F425_MESSAGE_MAPPING_AO_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_21F425_MESSAGE_MAPPING_AO_ID_seq"', 1, false);


--
-- Data for Name: AO_21F425_USER_PROPERTY_AO; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_21F425_USER_PROPERTY_AO" ("ID", "KEY", "USER", "VALUE") FROM stdin;
\.


--
-- Name: AO_21F425_USER_PROPERTY_AO_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_21F425_USER_PROPERTY_AO_ID_seq"', 1, false);


--
-- Data for Name: AO_2C4E5C_MAILCHANNEL; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_2C4E5C_MAILCHANNEL" ("CREATED_BY", "CREATED_TIMESTAMP", "ENABLED", "ID", "MAIL_CHANNEL_KEY", "MAIL_CONNECTION_ID", "MAX_RETRY_ON_FAILURE", "MODIFIED_BY", "PROJECT_ID", "UPDATED_TIMESTAMP") FROM stdin;
\.


--
-- Name: AO_2C4E5C_MAILCHANNEL_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_2C4E5C_MAILCHANNEL_ID_seq"', 1, false);


--
-- Data for Name: AO_2C4E5C_MAILCONNECTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_2C4E5C_MAILCONNECTION" ("CREATED_TIMESTAMP", "EMAIL_ADDRESS", "FOLDER", "HOST_NAME", "ID", "PASSWORD", "PORT", "PROTOCOL", "PULL_FROM_DATE", "TIMEOUT", "TLS", "UPDATED_TIMESTAMP", "USER_NAME") FROM stdin;
\.


--
-- Name: AO_2C4E5C_MAILCONNECTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_2C4E5C_MAILCONNECTION_ID_seq"', 1, false);


--
-- Data for Name: AO_2C4E5C_MAILGLOBALHANDLER; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_2C4E5C_MAILGLOBALHANDLER" ("CREATED_TIMESTAMP", "HANDLER_TYPE", "ID", "MODULE_COMPLETE_KEY", "UPDATED_TIMESTAMP") FROM stdin;
\.


--
-- Name: AO_2C4E5C_MAILGLOBALHANDLER_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_2C4E5C_MAILGLOBALHANDLER_ID_seq"', 1, false);


--
-- Data for Name: AO_2C4E5C_MAILHANDLER; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_2C4E5C_MAILHANDLER" ("CREATED_TIMESTAMP", "HANDLER_TYPE", "ID", "MAIL_CHANNEL_ID", "MODULE_COMPLETE_KEY", "UPDATED_TIMESTAMP") FROM stdin;
\.


--
-- Name: AO_2C4E5C_MAILHANDLER_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_2C4E5C_MAILHANDLER_ID_seq"', 1, false);


--
-- Data for Name: AO_2C4E5C_MAILITEM; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_2C4E5C_MAILITEM" ("CREATED_TIMESTAMP", "FROM_ADDRESS", "ID", "MAIL_CONNECTION_ID", "STATUS", "SUBJECT", "UPDATED_TIMESTAMP") FROM stdin;
\.


--
-- Data for Name: AO_2C4E5C_MAILITEMAUDIT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_2C4E5C_MAILITEMAUDIT" ("CREATED_TIMESTAMP", "FROM_ADDRESS", "HANDLER_NAME_KEY", "ID", "ISSUE_KEY", "MAIL_CHANNEL_ID", "MAIL_CHANNEL_NAME", "MAIL_ITEM_ID", "MESSAGE", "NO_OF_RETRY", "RESULT_STATUS", "SUBJECT", "UPDATED_TIMESTAMP") FROM stdin;
\.


--
-- Name: AO_2C4E5C_MAILITEMAUDIT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_2C4E5C_MAILITEMAUDIT_ID_seq"', 1, false);


--
-- Data for Name: AO_2C4E5C_MAILITEMCHUNK; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_2C4E5C_MAILITEMCHUNK" ("ID", "MAIL_ITEM_ID", "MIME_MSG_CHUNK", "MIME_MSG_CHUNK_IDX") FROM stdin;
\.


--
-- Name: AO_2C4E5C_MAILITEMCHUNK_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_2C4E5C_MAILITEMCHUNK_ID_seq"', 1, false);


--
-- Name: AO_2C4E5C_MAILITEM_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_2C4E5C_MAILITEM_ID_seq"', 1, false);


--
-- Data for Name: AO_2C4E5C_MAILRUNAUDIT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_2C4E5C_MAILRUNAUDIT" ("CREATED_TIMESTAMP", "FAILURE_MESSAGE", "ID", "MAIL_CHANNEL_NAME", "MAIL_CONNECTION_ID", "NO_OF_RETRY", "RUN_OUTCOME", "UPDATED_TIMESTAMP") FROM stdin;
\.


--
-- Name: AO_2C4E5C_MAILRUNAUDIT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_2C4E5C_MAILRUNAUDIT_ID_seq"', 1, false);


--
-- Data for Name: AO_319474_MESSAGE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_319474_MESSAGE" ("CLAIMANT", "CLAIMANT_TIME", "CLAIM_COUNT", "CONTENT_TYPE", "CREATED_TIME", "EXPIRY_TIME", "ID", "MSG_DATA", "MSG_ID", "MSG_LENGTH", "PRIORITY", "QUEUE_ID", "VERSION") FROM stdin;
\.


--
-- Name: AO_319474_MESSAGE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_319474_MESSAGE_ID_seq"', 1, false);


--
-- Data for Name: AO_319474_MESSAGE_PROPERTY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_319474_MESSAGE_PROPERTY" ("ID", "LONG_VALUE", "MESSAGE_ID", "NAME", "PROPERTY_TYPE", "STRING_VALUE") FROM stdin;
\.


--
-- Name: AO_319474_MESSAGE_PROPERTY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_319474_MESSAGE_PROPERTY_ID_seq"', 1, false);


--
-- Data for Name: AO_319474_QUEUE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_319474_QUEUE" ("CLAIMANT", "CLAIMANT_TIME", "CREATED_TIME", "ID", "MESSAGE_COUNT", "MODIFIED_TIME", "NAME", "PURPOSE", "TOPIC") FROM stdin;
\N	\N	1560741082972	1	0	1560741082972	com.atlassian.servicedesk.plugins.automation.execution.asyncthen.queue	A queue used by Service Desk Automation to store AsyncThen execution details, for execution off thread.	\N
\.


--
-- Name: AO_319474_QUEUE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_319474_QUEUE_ID_seq"', 1, true);


--
-- Data for Name: AO_319474_QUEUE_PROPERTY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_319474_QUEUE_PROPERTY" ("ID", "LONG_VALUE", "NAME", "PROPERTY_TYPE", "QUEUE_ID", "STRING_VALUE") FROM stdin;
\.


--
-- Name: AO_319474_QUEUE_PROPERTY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_319474_QUEUE_PROPERTY_ID_seq"', 1, false);


--
-- Data for Name: AO_38321B_CUSTOM_CONTENT_LINK; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_38321B_CUSTOM_CONTENT_LINK" ("CONTENT_KEY", "ID", "LINK_LABEL", "LINK_URL", "SEQUENCE") FROM stdin;
\.


--
-- Name: AO_38321B_CUSTOM_CONTENT_LINK_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_38321B_CUSTOM_CONTENT_LINK_ID_seq"', 1, false);


--
-- Data for Name: AO_3B1893_LOOP_DETECTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_3B1893_LOOP_DETECTION" ("COUNTER", "EXPIRES_AT", "ID", "SENDER_EMAIL") FROM stdin;
\.


--
-- Name: AO_3B1893_LOOP_DETECTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_3B1893_LOOP_DETECTION_ID_seq"', 1, false);


--
-- Data for Name: AO_4789DD_HEALTH_CHECK_STATUS; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_4789DD_HEALTH_CHECK_STATUS" ("APPLICATION_NAME", "COMPLETE_KEY", "DESCRIPTION", "FAILED_DATE", "FAILURE_REASON", "ID", "IS_HEALTHY", "IS_RESOLVED", "RESOLVED_DATE", "SEVERITY", "STATUS_NAME") FROM stdin;
JIRA	com.atlassian.troubleshooting.plugin-jira:gadgetFeedUrlHealthCheck	Checks if Jira is able to access itself through the gadgets feed URL to ensure that dashboard gadgets will work.	2019-06-17 03:12:31.797	Jira is not able to access itself through the Gadget feed URL. This is necessary so that dashboard gadgets can be generated successfully. Please verify the current Base URL and if necessary, review your network configurations to resolve the problem.	1	t	t	2019-06-17 03:29:38.129	WARNING	Gadget feed URL
\.


--
-- Name: AO_4789DD_HEALTH_CHECK_STATUS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_4789DD_HEALTH_CHECK_STATUS_ID_seq"', 1, true);


--
-- Data for Name: AO_4789DD_PROPERTIES; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_4789DD_PROPERTIES" ("ID", "PROPERTY_NAME", "PROPERTY_VALUE") FROM stdin;
1	last-run	1560742178226
\.


--
-- Name: AO_4789DD_PROPERTIES_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_4789DD_PROPERTIES_ID_seq"', 1, true);


--
-- Data for Name: AO_4789DD_READ_NOTIFICATIONS; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_4789DD_READ_NOTIFICATIONS" ("ID", "IS_SNOOZED", "NOTIFICATION_ID", "SNOOZE_COUNT", "SNOOZE_DATE", "USER_KEY") FROM stdin;
1	t	1	1	2019-06-17 03:29:51.241	admin
\.


--
-- Name: AO_4789DD_READ_NOTIFICATIONS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_4789DD_READ_NOTIFICATIONS_ID_seq"', 1, true);


--
-- Data for Name: AO_4789DD_TASK_MONITOR; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_4789DD_TASK_MONITOR" ("CLUSTERED_TASK_ID", "CREATED_TIMESTAMP", "ID", "NODE_ID", "PROGRESS_MESSAGE", "PROGRESS_PERCENTAGE", "SERIALIZED_ERRORS", "SERIALIZED_WARNINGS", "TASK_ID", "TASK_MONITOR_KIND", "TASK_STATUS") FROM stdin;
\.


--
-- Name: AO_4789DD_TASK_MONITOR_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_4789DD_TASK_MONITOR_ID_seq"', 1, false);


--
-- Data for Name: AO_4AEACD_WEBHOOK_DAO; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_4AEACD_WEBHOOK_DAO" ("ENABLED", "ENCODED_EVENTS", "FILTER", "ID", "JQL", "LAST_UPDATED", "LAST_UPDATED_USER", "NAME", "REGISTRATION_METHOD", "URL", "PARAMETERS", "EXCLUDE_ISSUE_DETAILS") FROM stdin;
\.


--
-- Name: AO_4AEACD_WEBHOOK_DAO_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_4AEACD_WEBHOOK_DAO_ID_seq"', 1, false);


--
-- Data for Name: AO_4E8AE6_NOTIF_BATCH_QUEUE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_4E8AE6_NOTIF_BATCH_QUEUE" ("AUTHOR_ID", "CONTEXT", "EVENT_TIME", "HTML_CONTENT", "ID", "ISSUE_ID", "PROJECT_ID", "RECIPIENT_ID", "SENT_TIME", "TEXT_CONTENT") FROM stdin;
\.


--
-- Name: AO_4E8AE6_NOTIF_BATCH_QUEUE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_4E8AE6_NOTIF_BATCH_QUEUE_ID_seq"', 1, false);


--
-- Data for Name: AO_4E8AE6_OUT_EMAIL_SETTINGS; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_4E8AE6_OUT_EMAIL_SETTINGS" ("CSS", "HTML_LINGO_ID", "ID", "PLAINTEXT_LINGO_ID", "PROJECT_ID", "SUBJECT_LINGO_ID") FROM stdin;
\.


--
-- Name: AO_4E8AE6_OUT_EMAIL_SETTINGS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_4E8AE6_OUT_EMAIL_SETTINGS_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_ASYNCUPGRADERECORD; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_ASYNCUPGRADERECORD" ("ACTION", "CREATED_DATE", "EXCEPTION", "ID", "MESSAGE", "SERVICE_DESK_VERSION", "UPGRADE_TASK_NAME") FROM stdin;
STARTED	2019-06-17 03:09:57.183		1		4.2.2-REL-0002	AsyncUpgradeTaskAddParticipantFieldToScreens
COMPLETED	2019-06-17 03:09:57.92		2		4.2.2-REL-0002	AsyncUpgradeTaskAddParticipantFieldToScreens
STARTED	2019-06-17 03:09:57.944		3		4.2.2-REL-0002	AsyncUpgradeTaskMigrateCommentAutotransition
COMPLETED	2019-06-17 03:09:58.361		4		4.2.2-REL-0002	AsyncUpgradeTaskMigrateCommentAutotransition
STARTED	2019-06-17 03:09:58.398		5		4.2.2-REL-0002	AsyncUpgradeTaskLockVpOriginField
COMPLETED	2019-06-17 03:09:58.531		6	Do not need to run as VpOrigin custom field is locked already	4.2.2-REL-0002	AsyncUpgradeTaskLockVpOriginField
STARTED	2019-06-17 03:09:58.541		7		4.2.2-REL-0002	AsyncUpgradeTaskAddOrganisationFieldToScreens
COMPLETED	2019-06-17 03:09:58.638		8	Successfully added Organisations custom field to the screens of every existing SD projects	4.2.2-REL-0002	AsyncUpgradeTaskAddOrganisationFieldToScreens
STARTED	2019-06-17 03:09:58.651		9		4.2.2-REL-0002	AUT_0008_TurnOnSlaRendering
COMPLETED	2019-06-17 03:09:58.714		10	SD SLA Rendering feature flag turned on successfully.	4.2.2-REL-0002	AUT_0008_TurnOnSlaRendering
STARTED	2019-06-17 03:09:58.725		11		4.2.2-REL-0002	AUT_0009_TurnOnSlaValueUpdateDate
COMPLETED	2019-06-17 03:09:58.756		12	SD SLA Value with Updated Date feature flag turned on successfully.	4.2.2-REL-0002	AUT_0009_TurnOnSlaValueUpdateDate
STARTED	2019-06-17 03:09:58.768		13		4.2.2-REL-0002	AUT_0010_TurnOnCannedResponses
COMPLETED	2019-06-17 03:09:58.792		14	Canned Responses feature flag turned on successfully.	4.2.2-REL-0002	AUT_0010_TurnOnCannedResponses
STARTED	2019-06-17 03:09:58.798		15		4.2.2-REL-0002	AUT_0011_TurnOnOffThreadServiceDeskOnCompletionEvents
COMPLETED	2019-06-17 03:09:58.805		16	Off Thread Service Desk On Completion Events feature flag turned on successfully.	4.2.2-REL-0002	AUT_0011_TurnOnOffThreadServiceDeskOnCompletionEvents
STARTED	2019-06-17 03:09:58.821		17		4.2.2-REL-0002	AUT_0012_WorkingtimeAddDisabled
COMPLETED	2019-06-17 03:09:58.826		18		4.2.2-REL-0002	AUT_0012_WorkingtimeAddDisabled
\.


--
-- Name: AO_54307E_ASYNCUPGRADERECORD_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_ASYNCUPGRADERECORD_ID_seq"', 18, true);


--
-- Data for Name: AO_54307E_CAPABILITY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_CAPABILITY" ("CAPABILITY_NAME", "CAPABILITY_VALUE", "ID", "SERVICE_DESK_ID", "USER_KEY") FROM stdin;
SHOW_WELCOME_SCREEN	true	1	1	\N
SERVICE_FEEDBACK_REQUEST	true	2	1	\N
SD_PRIORITY_SCHEME_IN_USE_10000	dismissed	3	1	\N
\.


--
-- Name: AO_54307E_CAPABILITY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_CAPABILITY_ID_seq"', 3, true);


--
-- Data for Name: AO_54307E_CONFLUENCEKB; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_CONFLUENCEKB" ("APPLINKS_APPLICATION_ID", "APPLINK_NAME", "APPLINK_URL", "ID", "SERVICE_DESK_ID", "SPACE_KEY", "SPACE_NAME", "SPACE_URL") FROM stdin;
\.


--
-- Data for Name: AO_54307E_CONFLUENCEKBENABLED; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_CONFLUENCEKBENABLED" ("CONFLUENCE_KBID", "ENABLED", "FORM_ID", "ID", "SERVICE_DESK_ID") FROM stdin;
\.


--
-- Name: AO_54307E_CONFLUENCEKBENABLED_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_CONFLUENCEKBENABLED_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_CONFLUENCEKBLABELS; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_CONFLUENCEKBLABELS" ("CONFLUENCE_KBID", "FORM_ID", "ID", "LABEL", "SERVICE_DESK_ID") FROM stdin;
\.


--
-- Name: AO_54307E_CONFLUENCEKBLABELS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_CONFLUENCEKBLABELS_ID_seq"', 1, false);


--
-- Name: AO_54307E_CONFLUENCEKB_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_CONFLUENCEKB_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_CUSTOMGLOBALTHEME; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_CUSTOMGLOBALTHEME" ("CONTENT_LINK_COLOR", "CONTENT_TEXT_COLOR", "CUSTOM_CSS", "HEADER_BADGE_BGCOLOR", "HEADER_BGCOLOR", "HEADER_LINK_COLOR", "HEADER_LINK_HOVER_BGCOLOR", "HEADER_LINK_HOVER_COLOR", "HELP_CENTER_TITLE", "ID", "LOGO_ID") FROM stdin;
\.


--
-- Name: AO_54307E_CUSTOMGLOBALTHEME_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_CUSTOMGLOBALTHEME_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_CUSTOMTHEME; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_CUSTOMTHEME" ("HEADER_BGCOLOR", "HEADER_LINK_COLOR", "HEADER_LINK_HOVER_BGCOLOR", "HEADER_LINK_HOVER_COLOR", "ID") FROM stdin;
\.


--
-- Name: AO_54307E_CUSTOMTHEME_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_CUSTOMTHEME_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_EMAILCHANNELSETTING; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_EMAILCHANNELSETTING" ("EMAIL_ADDRESS", "ID", "LAST_PROCEEDED_TIME", "MAIL_CHANNEL_KEY", "ON_DEMAND", "REQUEST_TYPE_ID", "SERVICE_DESK_ID") FROM stdin;
\.


--
-- Name: AO_54307E_EMAILCHANNELSETTING_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_EMAILCHANNELSETTING_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_EMAILSETTINGS; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_EMAILSETTINGS" ("EMAIL_ADDRESS", "ENABLED", "ID", "JIRA_MAIL_SERVER_ID", "LAST_PROCEEDED_TIME", "ON_DEMAND", "REQUEST_TYPE_ID", "SERVICE_DESK_ID") FROM stdin;
\.


--
-- Name: AO_54307E_EMAILSETTINGS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_EMAILSETTINGS_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_GOAL; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_GOAL" ("CALENDAR_ID", "DEFAULT_GOAL", "ID", "JQL_QUERY", "POS", "TARGET_DURATION", "TIME_METRIC_ID", "TIME_UPDATED_DATE", "TIME_UPDATED_MS_EPOCH") FROM stdin;
1	f	1	priority = Blocker	0	14400000	1	2019-06-17 03:30:27.556	1560742227556
1	t	2		1	57600000	1	2019-06-17 03:30:27.556	1560742227556
1	f	3	priority = Blocker	0	7200000	2	2019-06-17 03:30:27.769	1560742227769
1	t	4		1	28800000	2	2019-06-17 03:30:27.769	1560742227769
\.


--
-- Name: AO_54307E_GOAL_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_GOAL_ID_seq"', 4, true);


--
-- Data for Name: AO_54307E_GROUP; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_GROUP" ("DELETED_TIME", "GROUP_NAME", "ID", "ORDER", "VIEWPORT_ID") FROM stdin;
\N	General	1	0	1
\N	Need approval	2	1	1
\.


--
-- Data for Name: AO_54307E_GROUPTOREQUESTTYPE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_GROUPTOREQUESTTYPE" ("GROUP_ID", "ID", "ORDER", "REQUEST_TYPE_ID") FROM stdin;
1	1	\N	1
1	2	\N	2
1	3	\N	3
1	4	\N	4
1	5	\N	5
2	6	\N	6
2	7	\N	7
\.


--
-- Name: AO_54307E_GROUPTOREQUESTTYPE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_GROUPTOREQUESTTYPE_ID_seq"', 7, true);


--
-- Name: AO_54307E_GROUP_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_GROUP_ID_seq"', 2, true);


--
-- Data for Name: AO_54307E_IMAGES; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_IMAGES" ("CONTENTS", "ID") FROM stdin;
\.


--
-- Name: AO_54307E_IMAGES_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_IMAGES_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_METRICCONDITION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_METRICCONDITION" ("CONDITION_ID", "FACTORY_KEY", "ID", "PLUGIN_KEY", "TIME_METRIC_ID", "TYPE_NAME") FROM stdin;
issue-created-hit-condition	issue-created-sla-condition-factory	1	com.atlassian.servicedesk	1	Start
resolution-cleared-hit-condition	resolution-sla-condition-factory	2	com.atlassian.servicedesk	1	Start
resolution-set-hit-condition	resolution-sla-condition-factory	3	com.atlassian.servicedesk	1	Stop
issue-created-hit-condition	issue-created-sla-condition-factory	4	com.atlassian.servicedesk	2	Start
resolution-set-hit-condition	resolution-sla-condition-factory	5	com.atlassian.servicedesk	2	Stop
comment-for-reporter-hit-condition	comment-sla-condition-factory	6	com.atlassian.servicedesk	2	Stop
10002	status-sla-condition-factory	7	com.atlassian.servicedesk	2	Stop
\.


--
-- Name: AO_54307E_METRICCONDITION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_METRICCONDITION_ID_seq"', 7, true);


--
-- Data for Name: AO_54307E_ORGANIZATION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_ORGANIZATION" ("ID", "LOWER_NAME", "NAME", "SEARCH_NAME") FROM stdin;
\.


--
-- Name: AO_54307E_ORGANIZATION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_ORGANIZATION_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_ORGANIZATION_MEMBER; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_ORGANIZATION_MEMBER" ("DEFAULT_ORGANIZATION", "ID", "ORGANIZATION_ID", "USER_KEY") FROM stdin;
\.


--
-- Name: AO_54307E_ORGANIZATION_MEMBER_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_ORGANIZATION_MEMBER_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_ORGANIZATION_PROJECT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_ORGANIZATION_PROJECT" ("ID", "ORGANIZATION_ID", "PROJECT_ID") FROM stdin;
\.


--
-- Name: AO_54307E_ORGANIZATION_PROJECT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_ORGANIZATION_PROJECT_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_OUT_EMAIL_SETTINGS; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_OUT_EMAIL_SETTINGS" ("EMAIL_SUBJECT_PREFIX_ENABLED", "ID", "SERVICE_DESK_ID") FROM stdin;
\.


--
-- Name: AO_54307E_OUT_EMAIL_SETTINGS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_OUT_EMAIL_SETTINGS_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_PARTICIPANTSETTINGS; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_PARTICIPANTSETTINGS" ("AUTOCOMPLETE_ENABLED", "ID", "MANAGE_ENABLED", "SERVICE_DESK_ID") FROM stdin;
\.


--
-- Name: AO_54307E_PARTICIPANTSETTINGS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_PARTICIPANTSETTINGS_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_QUEUE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_QUEUE" ("ID", "JQL", "NAME", "PROJECT_ID", "PROJECT_KEY", "QUEUE_ORDER") FROM stdin;
1	resolution = Unresolved ORDER BY "Time to resolution" ASC	All open	10000	 	0
2	assignee is EMPTY AND resolution = Unresolved ORDER BY "Time to resolution" ASC	Unassigned issues	10000	 	1
3	assignee = currentUser() AND resolution = Unresolved ORDER BY "Time to resolution" ASC	Assigned to me	10000	 	2
4	assignee = currentUser() AND resolution = Unresolved AND status in ("Waiting for support", "In progress", Escalated) ORDER BY "Time to resolution" ASC	↳ Waiting on me	10000	 	3
5	assignee = currentUser() AND resolution = Unresolved AND status = "Waiting for customer" ORDER BY "Time to resolution" ASC	↳ Waiting on customer	10000	 	4
6	resolution = Unresolved AND issuetype = "IT Help" ORDER BY "Time to resolution" ASC	IT issues	10000	 	5
7	resolution = Unresolved AND issuetype in ("Service Request", "Service Request with Approvals") ORDER BY "Time to resolution" ASC	Service requests	10000	 	6
8	resolved >= -1w ORDER BY resolved DESC	Recently resolved	10000	 	7
\.


--
-- Data for Name: AO_54307E_QUEUECOLUMN; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_QUEUECOLUMN" ("COLUMN_ID", "COLUMN_ORDER", "ID", "QUEUE_ID") FROM stdin;
customfield_10201	0	1	1
issuetype	1	2	1
issuekey	2	3	1
status	3	4	1
summary	4	5	1
created	5	6	1
reporter	6	7	1
duedate	7	8	1
customfield_10201	0	9	2
issuetype	1	10	2
issuekey	2	11	2
status	3	12	2
summary	4	13	2
created	5	14	2
reporter	6	15	2
duedate	7	16	2
customfield_10201	0	17	3
issuetype	1	18	3
issuekey	2	19	3
status	3	20	3
summary	4	21	3
created	5	22	3
reporter	6	23	3
duedate	7	24	3
customfield_10201	0	25	4
issuetype	1	26	4
issuekey	2	27	4
status	3	28	4
summary	4	29	4
created	5	30	4
reporter	6	31	4
duedate	7	32	4
customfield_10201	0	33	5
issuetype	1	34	5
issuekey	2	35	5
status	3	36	5
summary	4	37	5
created	5	38	5
reporter	6	39	5
duedate	7	40	5
customfield_10201	0	41	6
issuetype	1	42	6
issuekey	2	43	6
status	3	44	6
summary	4	45	6
created	5	46	6
reporter	6	47	6
duedate	7	48	6
customfield_10201	0	49	7
issuetype	1	50	7
issuekey	2	51	7
status	3	52	7
summary	4	53	7
created	5	54	7
reporter	6	55	7
duedate	7	56	7
customfield_10201	0	57	8
issuetype	1	58	8
issuekey	2	59	8
status	3	60	8
summary	4	61	8
created	5	62	8
reporter	6	63	8
customfield_10101	7	64	8
\.


--
-- Name: AO_54307E_QUEUECOLUMN_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_QUEUECOLUMN_ID_seq"', 64, true);


--
-- Name: AO_54307E_QUEUE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_QUEUE_ID_seq"', 8, true);


--
-- Data for Name: AO_54307E_REPORT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_REPORT" ("CREATED_DATE", "ID", "NAME", "REPORT_ORDER", "REPORT_TYPE", "SERVICE_DESK_ID", "TARGET", "UPDATED_DATE") FROM stdin;
2019-06-17 03:30:28.074	1	Created vs Resolved	1	sd.report.type.line.graph.count	1	0	2019-06-17 03:30:28.074
2019-06-17 03:30:28.121	2	Time to resolution	2	sd.report.type.line.graph.count	1	0	2019-06-17 03:30:28.121
2019-06-17 03:30:28.172	3	SLA met vs breached	3	sd.report.type.line.graph.count	1	0	2019-06-17 03:30:28.172
2019-06-17 03:30:28.268	4	Resolution by component	4	sd.report.type.line.graph.count	1	0	2019-06-17 03:30:28.268
2019-06-17 03:30:28.401	5	SLA success rate	2	sd.report.type.line.graph.count	1	0	2019-06-17 03:30:28.401
\.


--
-- Name: AO_54307E_REPORT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_REPORT_ID_seq"', 5, true);


--
-- Data for Name: AO_54307E_SERIES; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_SERIES" ("COLOR", "CREATED_DATE", "GOAL_ID", "ID", "JQL", "REPORT_ID", "SERIES_DATA_TYPE", "SERIES_LABEL", "TIME_METRIC_ID", "UPDATED_DATE") FROM stdin;
#f15c75	2019-06-17 03:30:28.094	\N	1		1	sd.series.type.created.count	Created	\N	2019-06-17 03:30:28.094
#7bc1a1	2019-06-17 03:30:28.105	\N	2		1	sd.series.type.resolved.count	Resolved	\N	2019-06-17 03:30:28.105
#8eb021	2019-06-17 03:30:28.147	\N	3		2	sd.series.type.time.metric.elapsed.time	All issues	1	2019-06-17 03:30:28.147
#ea632b	2019-06-17 03:30:28.154	\N	4	priority = Blocker	2	sd.series.type.time.metric.elapsed.time	Priority: Blocker	1	2019-06-17 03:30:28.154
#14892c	2019-06-17 03:30:28.213	\N	5		3	sd.series.type.sla.succeeded	Met	1	2019-06-17 03:30:28.213
#d04437	2019-06-17 03:30:28.243	\N	6		3	sd.series.type.sla.breached	Breached	1	2019-06-17 03:30:28.243
#8eb021	2019-06-17 03:30:28.296	\N	7	component = "Public website"	4	sd.series.type.time.metric.elapsed.time	Public website	1	2019-06-17 03:30:28.296
#d39c3f	2019-06-17 03:30:28.323	\N	8	component = Intranet	4	sd.series.type.time.metric.elapsed.time	Intranet	1	2019-06-17 03:30:28.323
#ac707a	2019-06-17 03:30:28.371	\N	9	component = Jira	4	sd.series.type.time.metric.elapsed.time	Jira	1	2019-06-17 03:30:28.371
#14892c	2019-06-17 03:30:28.426	\N	10		5	sd.series.type.percentage.met	Time to first response	2	2019-06-17 03:30:28.426
#59afe1	2019-06-17 03:30:28.431	\N	11		5	sd.series.type.percentage.met	Time to resolution	1	2019-06-17 03:30:28.431
\.


--
-- Name: AO_54307E_SERIES_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_SERIES_ID_seq"', 11, true);


--
-- Data for Name: AO_54307E_SERVICEDESK; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_SERVICEDESK" ("CREATED_BY_USER_KEY", "CREATED_DATE", "CREATED_WITH_EMPTY_PROJECT", "DISABLED", "ID", "LEGACY_TRANSITION_DISABLED", "OPEN_CUSTOMER_ACCESS", "PROJECT_ID", "PROJECT_KEY", "PUBLIC_SIGNUP", "VERSION_CREATED_AT") FROM stdin;
admin	2019-06-17 03:30:22.463	t	\N	1	t	1	10000	_UNUSED	\N	4.2.2-REL-0002
\.


--
-- Name: AO_54307E_SERVICEDESK_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_SERVICEDESK_ID_seq"', 1, true);


--
-- Data for Name: AO_54307E_SLAAUDITLOG; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_SLAAUDITLOG" ("EVENT_TIME", "ID", "ISSUE_ID", "REASON", "SLA_ID") FROM stdin;
1560742231175	1	10000	DEFAULT_ISSUE_CREATION	2
1560742231175	2	10000	DEFAULT_ISSUE_CREATION	1
1560742231864	3	10000	SCHEDULING_TIMED_PROMISE	\N
\.


--
-- Data for Name: AO_54307E_SLAAUDITLOGDATA; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_SLAAUDITLOGDATA" ("ID", "KEY", "SLA_AUDIT_LOG_ID", "VALUE") FROM stdin;
1	issueKey	1	TEST-1
2	slaOngoingDataPaused	1	false
3	slaThresholdRemainingTime	1	7200000
4	issueKey	2	TEST-1
5	slaOngoingDataPaused	2	false
6	slaThresholdRemainingTime	2	14400000
7	issueKey	3	TEST-1
8	nextTimedPromise	3	1560765600000
\.


--
-- Name: AO_54307E_SLAAUDITLOGDATA_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_SLAAUDITLOGDATA_ID_seq"', 8, true);


--
-- Name: AO_54307E_SLAAUDITLOG_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_SLAAUDITLOG_ID_seq"', 3, true);


--
-- Data for Name: AO_54307E_STATUSMAPPING; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_STATUSMAPPING" ("FORM_ID", "ID", "STATUS_ID", "STATUS_NAME") FROM stdin;
1	1	10001	\N
1	2	10002	\N
1	3	10003	\N
1	4	5	\N
1	5	3	\N
1	6	10004	\N
1	7	10005	\N
1	8	6	\N
2	9	10001	\N
2	10	10002	\N
2	11	10003	\N
2	12	5	\N
2	13	3	\N
2	14	10004	\N
2	15	10005	\N
2	16	6	\N
3	17	10001	\N
3	18	10002	\N
3	19	10003	\N
3	20	5	\N
3	21	3	\N
3	22	10004	\N
3	23	10005	\N
3	24	6	\N
4	25	10001	\N
4	26	10002	\N
4	27	10003	\N
4	28	5	\N
4	29	3	\N
4	30	10004	\N
4	31	10005	\N
4	32	6	\N
5	33	10001	\N
5	34	10002	\N
5	35	10003	\N
5	36	5	\N
5	37	3	\N
5	38	10004	\N
5	39	10005	\N
5	40	6	\N
6	41	10001	\N
6	42	10002	\N
6	43	10003	\N
6	44	5	\N
6	45	3	\N
6	46	10006	\N
6	47	10004	\N
6	48	10005	\N
6	49	6	\N
7	50	10001	\N
7	51	10002	\N
7	52	10003	\N
7	53	5	\N
7	54	3	\N
7	55	10006	\N
7	56	10004	\N
7	57	10005	\N
7	58	6	\N
\.


--
-- Name: AO_54307E_STATUSMAPPING_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_STATUSMAPPING_ID_seq"', 58, true);


--
-- Data for Name: AO_54307E_SUBSCRIPTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_SUBSCRIPTION" ("ID", "ISSUE_ID", "SUBSCRIBED", "USER_KEY") FROM stdin;
\.


--
-- Name: AO_54307E_SUBSCRIPTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_SUBSCRIPTION_ID_seq"', 1, false);


--
-- Data for Name: AO_54307E_SYNCUPGRADERECORD; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_SYNCUPGRADERECORD" ("ACTION", "CREATED_DATE", "EXCEPTION", "ID", "MESSAGE", "SERVICE_DESK_VERSION", "UPGRADE_TASK_NAME") FROM stdin;
STARTED	2019-06-17 03:11:32.387		1		4.2.2-REL-0002	SyncUpgradeTaskMigrateAgentPermission
COMPLETED	2019-06-17 03:11:32.401		2		4.2.2-REL-0002	SyncUpgradeTaskMigrateAgentPermission
STARTED	2019-06-17 03:11:32.414		3		4.2.2-REL-0002	SyncUpgradeTaskMarkReporterCommentsAsPublic
COMPLETED	2019-06-17 03:11:32.55		4		4.2.2-REL-0002	SyncUpgradeTaskMarkReporterCommentsAsPublic
STARTED	2019-06-17 03:11:32.556		5		4.2.2-REL-0002	SyncUpgradeTaskUpdateCollaboratorRoleDescription
COMPLETED	2019-06-17 03:11:32.594		6		4.2.2-REL-0002	SyncUpgradeTaskUpdateCollaboratorRoleDescription
STARTED	2019-06-17 03:11:32.6		7		4.2.2-REL-0002	SyncUpgradeTaskFixOldStyleRequestChannelIssueProperties
COMPLETED	2019-06-17 03:11:32.619		8		4.2.2-REL-0002	SyncUpgradeTaskFixOldStyleRequestChannelIssueProperties
STARTED	2019-06-17 03:11:32.623		9		4.2.2-REL-0002	SyncUpgradeTaskMigrateTimeMetricsForSlaThresholds
COMPLETED	2019-06-17 03:11:32.64		10		4.2.2-REL-0002	SyncUpgradeTaskMigrateTimeMetricsForSlaThresholds
STARTED	2019-06-17 03:11:32.644		11		4.2.2-REL-0002	SyncUpgradeTaskMigrateDateColumnsForTimeMetrics
COMPLETED	2019-06-17 03:11:32.68		12		4.2.2-REL-0002	SyncUpgradeTaskMigrateDateColumnsForTimeMetrics
STARTED	2019-06-17 03:11:32.684		13		4.2.2-REL-0002	SyncUpgradeTaskMigrateRequestTypeIconIDs
COMPLETED	2019-06-17 03:11:32.826		14		4.2.2-REL-0002	SyncUpgradeTaskMigrateRequestTypeIconIDs
\.


--
-- Name: AO_54307E_SYNCUPGRADERECORD_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_SYNCUPGRADERECORD_ID_seq"', 14, true);


--
-- Data for Name: AO_54307E_THRESHOLD; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_THRESHOLD" ("ID", "REMAINING_TIME", "TIME_METRIC_ID") FROM stdin;
1	3600000	1
2	1800000	1
3	0	1
4	3600000	2
5	1800000	2
6	0	2
\.


--
-- Name: AO_54307E_THRESHOLD_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_THRESHOLD_ID_seq"', 6, true);


--
-- Data for Name: AO_54307E_TIMEMETRIC; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_TIMEMETRIC" ("CREATED_DATE", "CUSTOM_FIELD_ID", "DEFINITION_CHANGE_DATE", "DEFINITION_CHANGE_MS_EPOCH", "GOALS_CHANGE_DATE", "GOALS_CHANGE_MS_EPOCH", "ID", "NAME", "SERVICE_DESK_ID", "THRESHOLDS_CHANGE_MS_EPOCH", "THRESHOLDS_CONFIG_CHANGE_DATE") FROM stdin;
1560742227382	10201	1970-01-01 00:00:00	0	2019-06-17 03:30:27.592	1560742227592	1	Time to resolution	1	1560742227382	2019-06-17 03:30:27.382
1560742227647	10202	1970-01-01 00:00:00	0	2019-06-17 03:30:27.778	1560742227778	2	Time to first response	1	1560742227647	2019-06-17 03:30:27.647
\.


--
-- Name: AO_54307E_TIMEMETRIC_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_TIMEMETRIC_ID_seq"', 2, true);


--
-- Data for Name: AO_54307E_VIEWPORT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_VIEWPORT" ("DESCRIPTION", "ID", "KEY", "LOGO_ID", "NAME", "PROJECT_ID", "SEND_EMAIL_NOTIFICATIONS", "THEME_ID") FROM stdin;
Welcome! You can raise a Test request from the options provided.	1	test	\N	Test	10000	t	\N
\.


--
-- Data for Name: AO_54307E_VIEWPORTFIELD; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_VIEWPORTFIELD" ("DESCRIPTION", "DISPLAYED", "FIELD_ID", "FIELD_ORDER", "FIELD_TYPE", "FORM_ID", "ID", "LABEL", "REQUIRED") FROM stdin;
Tell us what you're looking for. For example, 'put me on the mailing list'.	t	summary	0	text	1	1	What do you need?	t
	t	description	1	textarea	1	2	Why do you need this?	t
Tell us what's wrong. For example, my laptop's screen sometimes blinks.	t	summary	0	text	2	3	Summary	t
Give us more details, like if you're on Mac or Windows. Or tell us more about your problem, such as when the screen blinks.	t	description	1	textarea	2	4	Details	t
	t	attachment	2	attachment	2	5	Attachment	f
	t	summary	0	text	3	6	What do you need?	t
	t	description	1	textarea	3	7	Description	t
	t	attachment	2	attachment	3	8	Attachment	f
	t	summary	0	text	4	9	Employee name	t
If you are unsure of the date, give us a tentative one.	t	duedate	1	duedate	4	10	Last working day	t
	t	description	2	textarea	4	11	Description	f
	t	attachment	3	attachment	4	12	Attachment	f
Enter their full name, first and last.	t	summary	0	text	5	13	New employee name	t
If you are unsure of the date, give us a tentative one.	t	duedate	1	duedate	5	14	Employee start date	f
	t	description	2	textarea	5	15	Description	f
	t	attachment	3	attachment	5	16	Attachment	f
Tell us where you're going and for how long. For example, '3 day trip to New York'.	t	summary	0	text	6	17	Summary	t
Give us some details of your plans, for example why you're going, the dates you're gone, and a rough budget.	t	description	1	textarea	6	18	Description	t
	t	customfield_10200	2	multiuserpicker	6	19	Approvers	f
	t	attachment	3	attachment	6	20	Attachment	f
\N	f	priority	0	select	6	21	Priority	f
	t	summary	0	text	7	22	Summary	t
	t	description	1	textarea	7	23	Description	t
	t	customfield_10200	2	multiuserpicker	7	24	Approvers	f
	t	attachment	3	attachment	7	25	Attachment	f
\.


--
-- Data for Name: AO_54307E_VIEWPORTFIELDVALUE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_VIEWPORTFIELDVALUE" ("FIELD_ID", "FIELD_NAME", "ID", "VALUE", "VALUE_ORDER") FROM stdin;
21	priority	1	2	0
\.


--
-- Name: AO_54307E_VIEWPORTFIELDVALUE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_VIEWPORTFIELDVALUE_ID_seq"', 1, true);


--
-- Name: AO_54307E_VIEWPORTFIELD_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_VIEWPORTFIELD_ID_seq"', 25, true);


--
-- Data for Name: AO_54307E_VIEWPORTFORM; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_54307E_VIEWPORTFORM" ("CALL_TO_ACTION", "DESCRIPTION", "FORM_ORDER", "ICON", "ICON_ID", "ID", "INTRO", "ISSUE_TYPE_ID", "KEY", "NAME", "VIEWPORT_ID") FROM stdin;
\N		0	\N	10627	1	Get general tech support, like help with the Wi-Fi or printing.	10000	getithelp	IT help	1
\N		1	\N	10610	2	If you have problems with your laptop, let us know here.	10000	desktoplaptopsupport	Computer support	1
\N		2	\N	10612	3	Order something small, like a keyboard. If it's under $100, you don't need approval.	10000	purchaseunder100	Purchase under $100	1
\N		3	\N	10626	4	Moving on to better things? Start your transition here.	10001	employeeexit	Employee exit	1
\N		4	\N	10631	5	Onboard a new hire.	10001	newemployee	New employee	1
\N		5	\N	10615	6	Got travel plans? Start the process here.	10002	travelrequests	Travel request	1
\N		6	\N	10636	7	Order something big, like a new phone. Purchases over $100 require approval.	10002	purchaseover100	Purchase over $100	1
\.


--
-- Name: AO_54307E_VIEWPORTFORM_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_VIEWPORTFORM_ID_seq"', 7, true);


--
-- Name: AO_54307E_VIEWPORT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_54307E_VIEWPORT_ID_seq"', 1, true);


--
-- Data for Name: AO_550953_SHORTCUT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_550953_SHORTCUT" ("ID", "NAME", "PROJECT_ID", "SHORTCUT_URL", "URL", "ICON") FROM stdin;
\.


--
-- Name: AO_550953_SHORTCUT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_550953_SHORTCUT_ID_seq"', 1, false);


--
-- Data for Name: AO_563AEE_ACTIVITY_ENTITY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_563AEE_ACTIVITY_ENTITY" ("ACTIVITY_ID", "ACTOR_ID", "CONTENT", "GENERATOR_DISPLAY_NAME", "GENERATOR_ID", "ICON_ID", "ID", "ISSUE_KEY", "OBJECT_ID", "POSTER", "PROJECT_KEY", "PUBLISHED", "TARGET_ID", "TITLE", "URL", "USERNAME", "VERB") FROM stdin;
\.


--
-- Name: AO_563AEE_ACTIVITY_ENTITY_ACTIVITY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_563AEE_ACTIVITY_ENTITY_ACTIVITY_ID_seq"', 1, false);


--
-- Data for Name: AO_563AEE_ACTOR_ENTITY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_563AEE_ACTOR_ENTITY" ("FULL_NAME", "ID", "PROFILE_PAGE_URI", "PROFILE_PICTURE_URI", "USERNAME") FROM stdin;
\.


--
-- Name: AO_563AEE_ACTOR_ENTITY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_563AEE_ACTOR_ENTITY_ID_seq"', 1, false);


--
-- Data for Name: AO_563AEE_MEDIA_LINK_ENTITY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_563AEE_MEDIA_LINK_ENTITY" ("DURATION", "HEIGHT", "ID", "URL", "WIDTH") FROM stdin;
\.


--
-- Name: AO_563AEE_MEDIA_LINK_ENTITY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_563AEE_MEDIA_LINK_ENTITY_ID_seq"', 1, false);


--
-- Data for Name: AO_563AEE_OBJECT_ENTITY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_563AEE_OBJECT_ENTITY" ("CONTENT", "DISPLAY_NAME", "ID", "IMAGE_ID", "OBJECT_ID", "OBJECT_TYPE", "SUMMARY", "URL") FROM stdin;
\.


--
-- Name: AO_563AEE_OBJECT_ENTITY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_563AEE_OBJECT_ENTITY_ID_seq"', 1, false);


--
-- Data for Name: AO_563AEE_TARGET_ENTITY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_563AEE_TARGET_ENTITY" ("CONTENT", "DISPLAY_NAME", "ID", "IMAGE_ID", "OBJECT_ID", "OBJECT_TYPE", "SUMMARY", "URL") FROM stdin;
\.


--
-- Name: AO_563AEE_TARGET_ENTITY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_563AEE_TARGET_ENTITY_ID_seq"', 1, false);


--
-- Data for Name: AO_56464C_APPROVAL; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_56464C_APPROVAL" ("APPROVE_CONDITION_TYPE", "APPROVE_CONDITION_VALUE", "COMPLETED_DATE", "CREATED_DATE", "DECISION", "ID", "ISSUE_ID", "NAME", "STATUS_ID", "SYSTEM_DECIDED") FROM stdin;
\.


--
-- Name: AO_56464C_APPROVAL_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_56464C_APPROVAL_ID_seq"', 1, false);


--
-- Data for Name: AO_56464C_APPROVER; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_56464C_APPROVER" ("APPROVAL_ID", "ID", "TYPE", "VALUE") FROM stdin;
\.


--
-- Data for Name: AO_56464C_APPROVERDECISION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_56464C_APPROVERDECISION" ("APPROVAL_ID", "DECISION", "ID", "SENT_DATE", "TO_STATUS_ID", "USER_KEY") FROM stdin;
\.


--
-- Name: AO_56464C_APPROVERDECISION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_56464C_APPROVERDECISION_ID_seq"', 1, false);


--
-- Name: AO_56464C_APPROVER_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_56464C_APPROVER_ID_seq"', 1, false);


--
-- Data for Name: AO_56464C_NOTIFICATIONRECORD; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_56464C_NOTIFICATIONRECORD" ("APPROVAL_ID", "ID", "SENT_DATE", "USER_KEY") FROM stdin;
\.


--
-- Name: AO_56464C_NOTIFICATIONRECORD_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_56464C_NOTIFICATIONRECORD_ID_seq"', 1, false);


--
-- Data for Name: AO_587B34_GLANCE_CONFIG; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_587B34_GLANCE_CONFIG" ("ROOM_ID", "STATE", "SYNC_NEEDED", "JQL", "APPLICATION_USER_KEY", "NAME") FROM stdin;
\.


--
-- Data for Name: AO_587B34_PROJECT_CONFIG; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_587B34_PROJECT_CONFIG" ("CONFIGURATION_GROUP_ID", "ID", "NAME", "NAME_UNIQUE_CONSTRAINT", "PROJECT_ID", "ROOM_ID", "VALUE") FROM stdin;
\.


--
-- Name: AO_587B34_PROJECT_CONFIG_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_587B34_PROJECT_CONFIG_ID_seq"', 1, false);


--
-- Data for Name: AO_5FB9D7_AOHIP_CHAT_LINK; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_5FB9D7_AOHIP_CHAT_LINK" ("ADDON_TOKEN_EXPIRY", "API_URL", "CONNECT_DESCRIPTOR", "GROUP_ID", "GROUP_NAME", "ID", "OAUTH_ID", "SECRET_KEY", "SYSTEM_PASSWORD", "SYSTEM_TOKEN_EXPIRY", "SYSTEM_USER", "SYSTEM_USER_TOKEN", "TOKEN") FROM stdin;
\.


--
-- Name: AO_5FB9D7_AOHIP_CHAT_LINK_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_5FB9D7_AOHIP_CHAT_LINK_ID_seq"', 1, false);


--
-- Data for Name: AO_5FB9D7_AOHIP_CHAT_USER; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_5FB9D7_AOHIP_CHAT_USER" ("HIP_CHAT_LINK_ID", "HIP_CHAT_USER_ID", "HIP_CHAT_USER_NAME", "ID", "REFRESH_CODE", "USER_KEY", "USER_SCOPES", "USER_TOKEN", "USER_TOKEN_EXPIRY") FROM stdin;
\.


--
-- Name: AO_5FB9D7_AOHIP_CHAT_USER_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_5FB9D7_AOHIP_CHAT_USER_ID_seq"', 1, false);


--
-- Data for Name: AO_733371_EVENT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_733371_EVENT" ("ACTION", "ACTION_ID", "CREATED", "EVENT_BUNDLE_ID", "EVENT_TYPE", "ID", "USER_KEY") FROM stdin;
\.


--
-- Name: AO_733371_EVENT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_733371_EVENT_ID_seq"', 1, false);


--
-- Data for Name: AO_733371_EVENT_PARAMETER; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_733371_EVENT_PARAMETER" ("EVENT_ID", "ID", "NAME", "VALUE") FROM stdin;
\.


--
-- Name: AO_733371_EVENT_PARAMETER_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_733371_EVENT_PARAMETER_ID_seq"', 1, false);


--
-- Data for Name: AO_733371_EVENT_RECIPIENT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_733371_EVENT_RECIPIENT" ("CONSUMER_NAME", "CREATED", "EVENT_ID", "ID", "SEND_DATE", "STATUS", "UPDATED", "USER_KEY") FROM stdin;
\.


--
-- Name: AO_733371_EVENT_RECIPIENT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_733371_EVENT_RECIPIENT_ID_seq"', 1, false);


--
-- Data for Name: AO_7A2604_CALENDAR; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_7A2604_CALENDAR" ("CONTEXT", "DESCRIPTION", "ID", "NAME", "TIMEZONE") FROM stdin;
{"owner":"com.atlassian.servicedesk","serviceDeskId":"1"}	\N	1	Sample 9-5 Calendar	UTC
\.


--
-- Name: AO_7A2604_CALENDAR_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_7A2604_CALENDAR_ID_seq"', 1, true);


--
-- Data for Name: AO_7A2604_HOLIDAY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_7A2604_HOLIDAY" ("CALENDAR_ID", "DATE_STRING", "ID", "NAME", "RECURRING") FROM stdin;
\.


--
-- Name: AO_7A2604_HOLIDAY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_7A2604_HOLIDAY_ID_seq"', 1, false);


--
-- Data for Name: AO_7A2604_WORKINGTIME; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_7A2604_WORKINGTIME" ("CALENDAR_ID", "DAY", "DISABLED", "END_TIME", "ID", "START_TIME") FROM stdin;
1	monday	f	61200000	1	32400000
1	tuesday	f	61200000	2	32400000
1	wednesday	f	61200000	3	32400000
1	thursday	f	61200000	4	32400000
1	friday	f	61200000	5	32400000
\.


--
-- Name: AO_7A2604_WORKINGTIME_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_7A2604_WORKINGTIME_ID_seq"', 5, true);


--
-- Data for Name: AO_97EDAB_USERINVITATION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_97EDAB_USERINVITATION" ("APPLICATION_KEYS", "EMAIL_ADDRESS", "EXPIRY", "ID", "REDEEMED", "SENDER_USERNAME", "TOKEN") FROM stdin;
\.


--
-- Name: AO_97EDAB_USERINVITATION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_97EDAB_USERINVITATION_ID_seq"', 1, false);


--
-- Data for Name: AO_9B2E3B_EXEC_RULE_MSG_ITEM; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_EXEC_RULE_MSG_ITEM" ("ID", "RULE_EXECUTION_ID", "RULE_MESSAGE_KEY", "RULE_MESSAGE_VALUE") FROM stdin;
\.


--
-- Name: AO_9B2E3B_EXEC_RULE_MSG_ITEM_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_EXEC_RULE_MSG_ITEM_ID_seq"', 1, false);


--
-- Data for Name: AO_9B2E3B_IF_CONDITION_CONFIG; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_IF_CONDITION_CONFIG" ("ID", "IF_THEN_ID", "MODULE_KEY", "ORDINAL") FROM stdin;
1	1	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:jql-if-condition	0
2	1	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:sd-comment-visibility-if-condition	1
3	1	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:sd-user-role-if-condition	2
4	1	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:comment-primary-action-if-condition	3
5	2	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:jql-if-condition	0
6	2	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:sd-comment-visibility-if-condition	1
7	2	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:sd-user-role-if-condition	2
8	2	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:comment-primary-action-if-condition	3
9	3	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:linked-issue-jql-if-condition	0
10	3	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:link-type-if-condition	1
11	5	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:sd-comment-visibility-if-condition	0
12	6	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:sd-comment-visibility-if-condition	0
13	7	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:issue-resolution-changed-if-condition	0
14	8	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:issue-resolution-changed-if-condition	0
15	12	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:customer-visible-status-change-if-condition	0
\.


--
-- Name: AO_9B2E3B_IF_CONDITION_CONFIG_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_IF_CONDITION_CONFIG_ID_seq"', 15, true);


--
-- Data for Name: AO_9B2E3B_IF_COND_CONF_DATA; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_IF_COND_CONF_DATA" ("CONFIG_DATA_KEY", "CONFIG_DATA_VALUE", "ID", "IF_CONDITION_CONFIG_ID") FROM stdin;
jql	Status = "Waiting for support" and "sd.origin.customfield.default.name" is not empty	1	1
commentType	public	2	2
userRoleType	non_customer	3	3
jql	Status = "Waiting for customer" and "sd.origin.customfield.default.name" is not empty	4	5
commentType	public	5	6
userRoleType	customer	6	7
jql	Resolution is not empty	7	9
linkTypeName	is caused by	8	10
linkTypeId	10200	9	10
linkTypeDirection	inward	10	10
commentType	public	11	11
commentType	public	12	12
fieldChangeType	SET	13	13
fieldChangeType	CLEAR	14	14
\.


--
-- Name: AO_9B2E3B_IF_COND_CONF_DATA_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_IF_COND_CONF_DATA_ID_seq"', 14, true);


--
-- Data for Name: AO_9B2E3B_IF_COND_EXECUTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_IF_COND_EXECUTION" ("FINISH_TIME_MILLIS", "ID", "IF_CONDITION_CONFIG_ID", "IF_EXECUTION_ID", "MESSAGE", "OUTCOME", "START_TIME_MILLIS") FROM stdin;
\.


--
-- Name: AO_9B2E3B_IF_COND_EXECUTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_IF_COND_EXECUTION_ID_seq"', 1, false);


--
-- Data for Name: AO_9B2E3B_IF_EXECUTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_IF_EXECUTION" ("FINISH_TIME_MILLIS", "ID", "IF_THEN_EXECUTION_ID", "MESSAGE", "OUTCOME", "START_TIME_MILLIS") FROM stdin;
\.


--
-- Name: AO_9B2E3B_IF_EXECUTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_IF_EXECUTION_ID_seq"', 1, false);


--
-- Data for Name: AO_9B2E3B_IF_THEN; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_IF_THEN" ("ID", "ORDINAL", "RULE_ID") FROM stdin;
1	0	1
2	1	1
3	0	2
4	0	3
5	0	4
6	0	5
7	0	6
8	0	7
9	0	8
10	0	9
11	0	10
12	0	11
\.


--
-- Data for Name: AO_9B2E3B_IF_THEN_EXECUTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_IF_THEN_EXECUTION" ("FINISH_TIME_MILLIS", "ID", "IF_THEN_ID", "MESSAGE", "OUTCOME", "RULE_EXECUTION_ID", "START_TIME_MILLIS") FROM stdin;
\.


--
-- Name: AO_9B2E3B_IF_THEN_EXECUTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_IF_THEN_EXECUTION_ID_seq"', 1, false);


--
-- Name: AO_9B2E3B_IF_THEN_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_IF_THEN_ID_seq"', 12, true);


--
-- Data for Name: AO_9B2E3B_PROJECT_USER_CONTEXT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_PROJECT_USER_CONTEXT" ("ID", "PROJECT_ID", "STRATEGY", "USER_KEY") FROM stdin;
1	10000	specificUser	admin
\.


--
-- Name: AO_9B2E3B_PROJECT_USER_CONTEXT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_PROJECT_USER_CONTEXT_ID_seq"', 1, true);


--
-- Data for Name: AO_9B2E3B_RSETREV_PROJ_CONTEXT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_RSETREV_PROJ_CONTEXT" ("ID", "PROJECT_ID", "RULESET_REVISION_ID") FROM stdin;
1	10000	1
2	10000	2
3	10000	3
4	10000	4
5	10000	5
6	10000	6
7	10000	7
8	10000	8
9	10000	9
10	10000	10
11	10000	11
\.


--
-- Name: AO_9B2E3B_RSETREV_PROJ_CONTEXT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_RSETREV_PROJ_CONTEXT_ID_seq"', 11, true);


--
-- Data for Name: AO_9B2E3B_RSETREV_USER_CONTEXT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_RSETREV_USER_CONTEXT" ("ID", "RULESET_REVISION_ID", "STRATEGY", "USER_KEY") FROM stdin;
1	3	currentUser	\N
2	4	currentUser	\N
3	5	currentUser	\N
4	6	currentUser	\N
5	7	currentUser	\N
6	8	currentUser	\N
7	9	currentUser	\N
8	10	currentUser	\N
9	11	currentUser	\N
\.


--
-- Name: AO_9B2E3B_RSETREV_USER_CONTEXT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_RSETREV_USER_CONTEXT_ID_seq"', 9, true);


--
-- Data for Name: AO_9B2E3B_RULE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_RULE" ("ENABLED", "ID", "ORDINAL", "RULESET_REVISION_ID") FROM stdin;
t	1	0	1
t	2	0	2
t	3	0	3
t	4	0	4
t	5	0	5
t	6	0	6
t	7	0	7
t	8	0	8
t	9	0	9
t	10	0	10
t	11	0	11
\.


--
-- Data for Name: AO_9B2E3B_RULESET; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_RULESET" ("ACTIVE_REVISION_ID", "ID") FROM stdin;
1	1
2	2
3	3
4	4
5	5
6	6
7	7
8	8
9	9
10	10
11	11
\.


--
-- Name: AO_9B2E3B_RULESET_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_RULESET_ID_seq"', 11, true);


--
-- Data for Name: AO_9B2E3B_RULESET_REVISION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_RULESET_REVISION" ("CREATED_BY", "CREATED_TIMESTAMP_MILLIS", "DESCRIPTION", "ID", "IS_SYSTEM_RULE_SET", "NAME", "RULE_SET_ID", "TRIGGER_FROM_OTHER_RULES") FROM stdin;
admin	1560742233065	When a comment is added to an issue, this rule automatically transitions the issue so it's clear who it's waiting on.	1	f	Transition on comment	1	t
admin	1560742233292	When the status of an issue changes, this rule will add a comment to its related issues. You can customize this to resolve related issues, change which issues are updated, and more.	2	f	Update when a linked issue changes	2	t
admin	1560742234071	\N	3	t	sd.notification.default.rule.issue.created.name	3	t
admin	1560742234180	\N	4	t	sd.notification.default.rule.comment.added.name	4	t
admin	1560742234290	\N	5	t	sd.notification.default.rule.comment.edited.name	5	t
admin	1560742234386	\N	6	t	sd.notification.default.rule.issue.resolved.name	6	t
admin	1560742234546	\N	7	t	sd.notification.default.rule.issue.reopened.name	7	t
admin	1560742234652	\N	8	t	sd.notification.default.rule.request.participant.added.name	8	t
admin	1560742234756	\N	9	t	sd.notification.default.rule.request.organisation.added.name	9	t
admin	1560742234803	\N	10	t	sd.notification.default.rule.approval.required.name	10	t
admin	1560742234900	\N	11	t	sd.notification.default.rule.customer.visible.status.change.name	11	t
\.


--
-- Name: AO_9B2E3B_RULESET_REVISION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_RULESET_REVISION_ID_seq"', 11, true);


--
-- Data for Name: AO_9B2E3B_RULE_EXECUTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_RULE_EXECUTION" ("EXECUTOR_USER_KEY", "FINISH_TIME_MILLIS", "ID", "MESSAGE", "OUTCOME", "RULE_ID", "START_TIME_MILLIS") FROM stdin;
\.


--
-- Name: AO_9B2E3B_RULE_EXECUTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_RULE_EXECUTION_ID_seq"', 1, false);


--
-- Name: AO_9B2E3B_RULE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_RULE_ID_seq"', 11, true);


--
-- Data for Name: AO_9B2E3B_THEN_ACTION_CONFIG; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_THEN_ACTION_CONFIG" ("ID", "IF_THEN_ID", "MODULE_KEY", "ORDINAL") FROM stdin;
1	1	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:workflow-transition-action	0
2	2	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:workflow-transition-action	0
3	3	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:servicedesk-comment-action	0
4	4	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
5	5	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
6	6	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
7	7	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
8	8	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
9	9	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
10	10	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
11	11	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
12	12	com.atlassian.servicedesk.servicedesk-notifications-plugin:servicedesk-send-notification-action	0
\.


--
-- Name: AO_9B2E3B_THEN_ACTION_CONFIG_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_THEN_ACTION_CONFIG_ID_seq"', 12, true);


--
-- Data for Name: AO_9B2E3B_THEN_ACT_CONF_DATA; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_THEN_ACT_CONF_DATA" ("CONFIG_DATA_KEY", "CONFIG_DATA_VALUE", "ID", "THEN_ACTION_CONFIG_ID") FROM stdin;
workflowAction	Respond to customer	1	1
workflowAction	Respond to support	2	2
includeLinkedIssueDetails	true	3	3
commentType	internal	4	3
comment	Please check this issue for more details	5	3
recipients	[{"type":"all_customers"}]	6	4
lingo	1	7	4
projectid	10000	8	4
content	{"zh-CN":"只是确认我们已收到您的请求。我们正在处理。","es-ES":"Simplemente te confirmamos que hemos recibido tu solicitud. Estamos en ello.","sv-SE":"Tack för ditt ärende. Vi återkommer så snart som möjligt.","ja-JP":"今、確かに貴方からの依頼を受け取りました。それにとりかかっています。","ko-KR":"귀하의 요청을 받았다는 확인입니다. 저희가 처리를 맡습니다.","en-UK":"Just confirming that we got your request. We're on it.","nl-NL":"Slechts een bevestiging dat wij uw aanvraag hebben ontvangen. We zijn ermee bezig.","ru-RU":"Просто подтверждаем, что получили ваш запрос. Мы работаем над ним.","hu-HU":"Csak megerősítjük, hogy megkaptuk a kérését. Már dolgozunk is rajta.","pt-BR":"Apenas confirmando que recebemos o seu pedido. Estamos atentos.","de-DE":"Wir wollten nur bestätigen, dass wir Ihre Anfrage erhalten haben. Wir arbeiten dran. ","is-IS":"Þetta er staðfesting á því að við höfum móttekið beiðnina þína. Við erum að vinna í henni.","cs-CZ":"Potvrzujeme, že jsme vaši žádost přijali. Pracujeme na tom.","da-DK":"Vi bekræfter bare, at vi fik din anmodning. Vi arbejder på det.","fi-FI":"Olemme vastaanottaneet tukipyyntösi ja otamme sen työn alle.","et-EE":"Kinnitame, et saime teie taotluse kätte. Me tegeleme sellega.","pl-PL":"Potwierdzamy przyjęcie zgłoszenia. Zajmiemy się nim już wkrótce. ","sk-SK":"Len potvrdzujeme, že máme vašu požiadavku. Ideme na to.","it-IT":"Gentile utente,\\r\\n\\r\\nIl ticket di supporto è stato inviato correttamente. Cerchiamo di rispondere a tutte le richieste che riceviamo il prima possibile, solitamente in 24/48 ore.\\r\\n\\r\\nVerrà inviata una email di notifica quando il nostro staff risponderà al ticket.\\r\\n\\r\\n\\r\\n*NON RISPONDERE A QUESTA E-MAIL*\\r\\nQuesto è un messaggio email automatico inviato dal sistema di supporto. Non rispondere a questa email poiché non è una casella presidiata!\\r\\n","fr-FR":"Nous sommes en train de confirmer que nous avons reçu votre demande.","en-US":"Just confirming that we got your request. We're on it.","default":"Just confirming that we got your request. We're on it.","ro-RO":"Confirmăm că am primit solicitarea dumneavoastră. Ne ocupăm de aceasta.","no-NO":"Bare bekrefter at vi har fått forespørselen din. Vi jobber med saken."}	9	4
isdefaultrule	true	10	4
excludeinitiator	false	11	4
recipients	[{"type":"all_customers"}]	12	5
lingo	2	13	5
projectid	10000	14	5
content	{"zh-CN":"${event.user.name} commented:\\n\\n${comment}","es-ES":"${event.user.name} comentó:\\n\\n${comment}","sv-SE":"${event.user.name} kommenterade:\\r\\n\\r\\n${comment}","ja-JP":"\\n${event.user.name}が${comment}とコメントした","ko-KR":"${event.user.name}님이 코멘트를 하셨습니다:\\n\\n${comment}","en-UK":"${event.user.name} commented:\\n\\n${comment}","nl-NL":"${event.user.name} gaf commentaar:\\n\\n${comment}","ru-RU":"${event.user.name} прокомментировал:\\n\\n${comment}","hu-HU":"${event.user.name} commented:\\n\\n${comment}","pt-BR":"${event.user.name} comentou:\\n\\n${comment}","de-DE":"${event.user.name} hat kommentiert:\\n\\n${comment}","is-IS":"${event.user.name} commented:\\n\\n${comment}","cs-CZ":"${event.user.name} commented:\\n\\n${comment}","da-DK":"${event.user.name} commented:\\n\\n${comment}","fi-FI":"${event.user.name} kommentoi:\\r\\n\\r\\n${comment}","et-EE":"${event.user.name} commented:\\n\\n${comment}","pl-PL":"Użytkownik ${event.user.name} skomentował:\\n\\n${comment}","sk-SK":"${event.user.name} to komentoval:\\r\\n\\r\\n${comment}","it-IT":"${event.user.name} ha commentato:\\r\\n\\r\\n${comment}","fr-FR":"${event.user.name} a commenté :\\n\\n${comment}","en-US":"${event.user.name} commented:\\n\\n${comment}","default":"${event.user.name} commented:\\n\\n${comment}","ro-RO":"${event.user.name} a adaugat un comentariu:\\r\\n\\r\\n${comment}","no-NO":"${event.user.name} commented:\\n\\n${comment}"}	15	5
isdefaultrule	true	16	5
excludeinitiator	true	17	5
recipients	[{"type":"all_customers"}]	18	6
lingo	3	19	6
projectid	10000	20	6
content	{"zh-CN":"${event.user.name} updated a comment:\\n\\n${comment}","es-ES":"${event.user.name} ha actualizado un comentario:\\n\\n${comment}","sv-SE":"${event.user.name} uppdaterade en kommentar:\\r\\n\\r\\n${comment}","ja-JP":"${event.user.name} が次のようにコメントを更新しました:\\n\\n${comment}","ko-KR":"${event.user.name}님이 코멘트를 갱신하셨습니다:\\n\\n${comment}","en-UK":"${event.user.name} updated a comment:\\n\\n${comment}","nl-NL":"${event.user.name} heeft een opmerking geüpdatet:\\n\\n${comment}","ru-RU":"${event.user.name} обновил комментарий:\\n\\n${comment}","hu-HU":"${event.user.name} updated a comment:\\n\\n${comment}","pt-BR":"${event.user.name} atualizou um comentário:\\n\\n${comment}","de-DE":"${event.user.name} hat einen Kommentar aktualisiert:\\n\\n${comment}","is-IS":"${event.user.name} updated a comment:\\n\\n${comment}","cs-CZ":"${event.user.name} updated a comment:\\n\\n${comment}","da-DK":"${event.user.name} updated a comment:\\n\\n${comment}","fi-FI":"${event.user.name} updated a comment:\\n\\n${comment}","et-EE":"${event.user.name} updated a comment:\\n\\n${comment}","pl-PL":"Użytkownik ${event.user.name} zaktualizował komentarz:\\n\\n${comment}","sk-SK":"${event.user.name} aktualizoval komentár:\\r\\n\\r\\n${comment}","it-IT":"${event.user.name} ha aggiornato un commento:\\r\\n\\r\\n${comment}","fr-FR":"${event.user.name} a mis à jour un commentaire :\\n\\n${comment}","en-US":"${event.user.name} updated a comment:\\n\\n${comment}","default":"${event.user.name} updated a comment:\\n\\n${comment}","ro-RO":"${event.user.name} updated a comment:\\n\\n${comment}","no-NO":"${event.user.name} updated a comment:\\n\\n${comment}"}	21	6
isdefaultrule	true	22	6
excludeinitiator	true	23	6
recipients	[{"type":"all_customers"},{"type":"reporter"}]	24	7
lingo	4	25	7
projectid	10000	26	7
content	{"zh-CN":"${event.user.name} resolved this as ${issue.resolution}.","es-ES":"${event.user.name} ha resuelto esto como ${issue.resolution}.","sv-SE":"${event.user.name} löste detta med ${issue.resolution}.","ja-JP":"${event.user.name}が ${issue.resolution}のように解決しました。","ko-KR":"${event.user.name}님이 이것을 ${issue.resolution}(으)로서 해결하셨습니다.","en-UK":"${event.user.name} resolved this as ${issue.resolution}.","nl-NL":"${event.user.name} loste dit op als ${issue.resolution}.","ru-RU":"${event.user.name} ответил на это ${issue.resolution}.","hu-HU":"${event.user.name} resolved this as ${issue.resolution}.","pt-BR":"${event.user.name} resolveu isso como ${issue.resolution}.","de-DE":"${event.user.name} löste dies zu ${issue.resolution} auf.","is-IS":"${event.user.name} resolved this as ${issue.resolution}.","cs-CZ":"${event.user.name} resolved this as ${issue.resolution}.","da-DK":"${event.user.name} resolved this as ${issue.resolution}.","fi-FI":"${event.user.name} resolved this as ${issue.resolution}.","et-EE":"${event.user.name} resolved this as ${issue.resolution}.","pl-PL":"${event.user.name} rozwiązał zgłoszenie jako ${issue.resolution}.","sk-SK":"${event.user.name} to vyriešil s príznakom ${issue.resolution}.","it-IT":"${event.user.name} ha risolto  ${issue.resolution}.","fr-FR":"${event.user.name} a résolu cela comme ${issue.resolution}.\\n","en-US":"${event.user.name} resolved this as ${issue.resolution}.","default":"${event.user.name} resolved this as ${issue.resolution}.","ro-RO":"${event.user.name} a inchis sezizarea cu starea:  ${issue.resolution}.","no-NO":"${event.user.name} resolved this as ${issue.resolution}."}	27	7
isdefaultrule	true	28	7
excludeinitiator	false	29	7
recipients	[{"type":"all_customers"}]	30	8
lingo	5	31	8
projectid	10000	32	8
content	{"zh-CN":"${event.user.name} reopened this request.","es-ES":"${event.user.name} ha reabierto esta solicitud.","sv-SE":"${event.user.name} öppnade begäran på nytt.","ja-JP":"${event.user.name} がこのリクエストに応答しました。","ko-KR":"${event.user.name}님이 이 요청을 다시 여셨습니다.","en-UK":"${event.user.name} reopened this request.","nl-NL":"${event.user.name} heeft dit verzoek opnieuw geopend.","ru-RU":"${event.user.name} открыл этот запрос повторно.","hu-HU":"${event.user.name} reopened this request.","pt-BR":"${event.user.name} reabriu esse pedido.","de-DE":"${event.user.name} hat diese Anfrage neu aufgemacht. ","is-IS":"${event.user.name} reopened this request.","cs-CZ":"${event.user.name} reopened this request.","da-DK":"${event.user.name} reopened this request.","fi-FI":"${event.user.name} reopened this request.","et-EE":"${event.user.name} reopened this request.","pl-PL":"Użytkownik ${event.user.name} otworzył ponownie to żądanie.","sk-SK":"${event.user.name} reopened this request.","it-IT":"${event.user.name} ha riaperto questa richiesta.","fr-FR":"${event.user.name} a rouvert cette requête.","en-US":"${event.user.name} reopened this request.","default":"${event.user.name} reopened this request.","ro-RO":"${event.user.name} reopened this request.","no-NO":"${event.user.name} reopened this request."}	33	8
isdefaultrule	true	34	8
excludeinitiator	false	35	8
recipients	[{"type":"new_participants"}]	36	9
lingo	6	37	9
projectid	10000	38	9
content	{"zh-CN":"${event.user.name} added you as a participant.","es-ES":"${event.user.name} te ha añadido como participante.","sv-SE":"${event.user.name} lade till dig som deltagare.","ja-JP":"${event.user.name} が貴方を参加者に追加しました。","ko-KR":"${event.user.name}님이 당신을 참가자로 추가하셨습니다.","en-UK":"${event.user.name} added you as a participant.","nl-NL":"${event.user.name} heeft u toegevoegd als een deelnemer.","ru-RU":"${event.user.name} добавил вас, как участника","hu-HU":"${event.user.name} added you as a participant.","pt-BR":"${event.user.name} te adicionou como um participante.","de-DE":"${event.user.name} hat Sie als Teilnehmer hinzugefügt. ","is-IS":"${event.user.name} added you as a participant.","cs-CZ":"${event.user.name} added you as a participant.","da-DK":"${event.user.name} added you as a participant.","fi-FI":"${event.user.name} added you as a participant.","et-EE":"${event.user.name}  lisas sind osalejaks.","pl-PL":"${event.user.name} dodał(a) Cię jako uczestnika.","sk-SK":"${event.user.name} vás pridal ako účastníka.","it-IT":"${event.user.name} ti ha aggiunto come partecipante.","fr-FR":"${event.user.name} vous a ajouté en tant que participant.","en-US":"${event.user.name} added you as a participant.","default":"${event.user.name} added you as a participant.","ro-RO":"${event.user.name} added you as a participant.","no-NO":"${event.user.name} added you as a participant."}	39	9
isdefaultrule	true	40	9
excludeinitiator	true	41	9
recipients	[{"type":"new_organizations"}]	42	10
lingo	7	43	10
projectid	10000	44	10
content	{"zh-CN":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","es-ES":"${event.user.name} ha compartido esto con tu organización.\\n\\nVisualice la solicitud y seleccione *Recibir notificaciones* para seguir.","sv-SE":"${event.user.name} delade detta med din organisation.\\r\\n\\r\\nVisa begäran och välj *Få meddelanden* för att följa.","ja-JP":"${event.user.name} がこれをあなたの組織と共有しました。\\n\\nリクエストを確認し、*通知を受け取る*を選択してフォローしましょう。","ko-KR":"${event.user.name} 가 귀하의 구조에 해당 내용을 공유하였습니다.\\n\\n요청 내용을 확인하고 안내와 같이 *공지 받기*를 선택하세요.","en-UK":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","nl-NL":"${event.user.name} heeft dit gedeeld met je organisatie.\\n\\nBekijk de aanvraag en selecteer *Meldingen ontvangen* om te volgen.","ru-RU":"${event.user.name} поделился этим с вашей организацией.\\n\\nПросмотрите запрос и выберите «Получать уведомления», чтобы оставаться в курсе.","hu-HU":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","pt-BR":"${event.user.name} compartilhou isso com sua organização.\\n\\nVeja a solicitação e selecione *Receber notificações* para acompanhar.","de-DE":"${event.user.name} hat dies mit Ihrer Organisation geteilt.\\n\\nBetrachten Sie die Anfrage und wählen Sie die Einstellung *Benachrichtigungen erhalten*.","is-IS":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","cs-CZ":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","da-DK":"${event.user.name} del denne med din organisation.\\r\\n\\r\\nSe sagen og vælg: *Få notifikationer* for at følge med.","fi-FI":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","et-EE":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","pl-PL":"Użytkownik ${event.user.name} udostępnił to Twojej organizacji.\\n\\nPrzejrzyj żądanie i wybierz opcję *Otrzymuj powiadomienia*, aby śledzić zmiany.","sk-SK":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","it-IT":"${event.user.name} ha condiviso questo elemento con la tua organizzazione.\\n\\nVisualizza la richiesta e seleziona *Ricevi notifiche* per seguirne i progressi.","fr-FR":"${event.user.name} a partagé ceci avec votre organisation.\\n\\nAffichez la requête et sélectionnez *Recevoir des notifications* pour être tenu au courant.","en-US":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","default":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","ro-RO":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along.","no-NO":"${event.user.name} shared this with your organization.\\n\\nView the request and select *Get notifications* to follow along."}	45	10
isdefaultrule	true	46	10
excludeinitiator	true	47	10
recipients	[{"type":"new_approvers"}]	48	11
lingo	8	49	11
projectid	10000	50	11
content	{"zh-CN":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","es-ES":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","sv-SE":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","ja-JP":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","ko-KR":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","en-UK":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","nl-NL":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","ru-RU":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","hu-HU":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","pt-BR":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","de-DE":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","is-IS":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","cs-CZ":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","da-DK":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","fi-FI":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","et-EE":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","pl-PL":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","sk-SK":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","it-IT":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","fr-FR":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","en-US":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","default":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","ro-RO":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}","no-NO":"This request created by ${issue.reporter.name} is awaiting approval.\\n${request.details}\\n${approval.buttons}"}	51	11
isdefaultrule	true	52	11
excludeinitiator	false	53	11
recipients	[{"type":"all_customers"}]	54	12
lingo	9	55	12
projectid	10000	56	12
content	{"zh-CN":"${event.user.name} changed the status to ${request.status}.","es-ES":"${event.user.name} ha cambiado el estado a ${request.status}.","sv-SE":"${event.user.name} ändrade statusen till ${request.status}.","ja-JP":"${event.user.name}がステータスを${request.status}に変更した。","ko-KR":"${event.user.name}님이 상태를 ${request.status}(으)로 변경했습니다.","en-UK":"${event.user.name} changed the status to ${request.status}.","nl-NL":"${event.user.name} veranderde deze status naar ${request.status}.","ru-RU":"${event.user.name} изменил статус на ${request.status}.","hu-HU":"${event.user.name} changed the status to ${request.status}.","pt-BR":"${event.user.name} mudou o status para ${request.status}.","de-DE":"${event.user.name} hat den Status auf ${request.status} geändert.","is-IS":"${event.user.name} changed the status to ${request.status}.","cs-CZ":"${event.user.name} changed the status to ${request.status}.","da-DK":"${event.user.name} changed the status to ${request.status}.","fi-FI":"${event.user.name} changed the status to ${request.status}.","et-EE":"${event.user.name} changed the status to ${request.status}.","pl-PL":"${event.user.name} zmienił status na ${request.status}.","sk-SK":"${event.user.name} zmenil(a) stav na ${request.status}.","it-IT":"${event.user.name} ha cambiato lo stato in ${request.status}.","fr-FR":"${event.user.name} a changé le statut à ${request.status}.","en-US":"${event.user.name} changed the status to ${request.status}.","default":"${event.user.name} changed the status to ${request.status}.","ro-RO":"${event.user.name} a schimbat starea in  ${request.status}.","no-NO":"${event.user.name} changed the status to ${request.status}."}	57	12
isdefaultrule	true	58	12
excludeinitiator	false	59	12
\.


--
-- Name: AO_9B2E3B_THEN_ACT_CONF_DATA_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_THEN_ACT_CONF_DATA_ID_seq"', 59, true);


--
-- Data for Name: AO_9B2E3B_THEN_ACT_EXECUTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_THEN_ACT_EXECUTION" ("FINISH_TIME_MILLIS", "ID", "MESSAGE", "OUTCOME", "START_TIME_MILLIS", "THEN_ACTION_CONFIG_ID", "THEN_EXECUTION_ID") FROM stdin;
\.


--
-- Name: AO_9B2E3B_THEN_ACT_EXECUTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_THEN_ACT_EXECUTION_ID_seq"', 1, false);


--
-- Data for Name: AO_9B2E3B_THEN_EXECUTION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_THEN_EXECUTION" ("FINISH_TIME_MILLIS", "ID", "IF_THEN_EXECUTION_ID", "MESSAGE", "OUTCOME", "START_TIME_MILLIS") FROM stdin;
\.


--
-- Name: AO_9B2E3B_THEN_EXECUTION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_THEN_EXECUTION_ID_seq"', 1, false);


--
-- Data for Name: AO_9B2E3B_WHEN_HANDLER_CONFIG; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_WHEN_HANDLER_CONFIG" ("ID", "MODULE_KEY", "ORDINAL", "RULE_ID") FROM stdin;
1	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:servicedesk-commented-automation-rule-when-handler	0	1
2	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:linked-issue-when-handler	0	2
3	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:issue-created-automation-rule-when-handler	0	3
4	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:servicedesk-commented-automation-rule-when-handler	0	4
5	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:servicedesk-comment-edited-automation-rule-when-handler	0	5
6	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:issue-resolution-changed-automation-rule-when-handler	0	6
7	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:issue-resolution-changed-automation-rule-when-handler	0	7
8	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:new-participants-when-handler	0	8
9	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:organisation-added-to-issue-when-handler	0	9
10	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:approval-required-when-handler	0	10
11	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin:issue-status-transitioned-automation-rule-when-handler	0	11
\.


--
-- Name: AO_9B2E3B_WHEN_HANDLER_CONFIG_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_WHEN_HANDLER_CONFIG_ID_seq"', 11, true);


--
-- Data for Name: AO_9B2E3B_WHEN_HAND_CONF_DATA; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_9B2E3B_WHEN_HAND_CONF_DATA" ("CONFIG_DATA_KEY", "CONFIG_DATA_VALUE", "ID", "WHEN_HANDLER_CONFIG_ID") FROM stdin;
\.


--
-- Name: AO_9B2E3B_WHEN_HAND_CONF_DATA_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_9B2E3B_WHEN_HAND_CONF_DATA_ID_seq"', 1, false);


--
-- Data for Name: AO_A0B856_WEB_HOOK_LISTENER_AO; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_A0B856_WEB_HOOK_LISTENER_AO" ("DESCRIPTION", "ENABLED", "EVENTS", "EXCLUDE_BODY", "FILTERS", "ID", "LAST_UPDATED", "LAST_UPDATED_USER", "NAME", "PARAMETERS", "REGISTRATION_METHOD", "URL") FROM stdin;
\.


--
-- Name: AO_A0B856_WEB_HOOK_LISTENER_AO_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_A0B856_WEB_HOOK_LISTENER_AO_ID_seq"', 1, false);


--
-- Data for Name: AO_B9A0F0_APPLIED_TEMPLATE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_B9A0F0_APPLIED_TEMPLATE" ("ID", "PROJECT_ID", "PROJECT_TEMPLATE_MODULE_KEY", "PROJECT_TEMPLATE_WEB_ITEM_KEY") FROM stdin;
1	10000	com.atlassian.servicedesk:basic-service-desk-project	com.atlassian.servicedesk:basic-service-desk-project
\.


--
-- Name: AO_B9A0F0_APPLIED_TEMPLATE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_B9A0F0_APPLIED_TEMPLATE_ID_seq"', 1, true);


--
-- Data for Name: AO_C16815_ALERT_AO; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_C16815_ALERT_AO" ("CREATED_DATE", "DETAILS_JSON", "ID", "ISSUE_COMPONENT_ID", "ISSUE_ID", "ISSUE_SEVERITY", "NODE_NAME", "TRIGGER_MODULE", "TRIGGER_PLUGIN_KEY", "TRIGGER_PLUGIN_KEY_VERSION", "TRIGGER_PLUGIN_VERSION") FROM stdin;
\.


--
-- Name: AO_C16815_ALERT_AO_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_C16815_ALERT_AO_ID_seq"', 1, false);


--
-- Data for Name: AO_C7F17E_LINGO; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_C7F17E_LINGO" ("CATEGORY", "CREATED_TIMESTAMP", "ID", "LOGICAL_ID", "PROJECT_ID", "SYSTEM_I18N") FROM stdin;
sd.notifications.custom.email.content	1560742233670	1	\N	10000	sd.notification.default.rule.content.issue.created
sd.notifications.custom.email.content	1560742234120	2	\N	10000	sd.notification.default.rule.content.comment.added
sd.notifications.custom.email.content	1560742234227	3	\N	10000	sd.notification.default.rule.content.comment.edited
sd.notifications.custom.email.content	1560742234310	4	\N	10000	sd.notification.default.rule.content.issue.resolved
sd.notifications.custom.email.content	1560742234490	5	\N	10000	sd.notification.default.rule.content.issue.reopened
sd.notifications.custom.email.content	1560742234598	6	\N	10000	sd.notification.default.rule.content.request.participant.added
sd.notifications.custom.email.content	1560742234719	7	\N	10000	sd.notification.default.rule.content.request.organisation.added
sd.notifications.custom.email.content	1560742234767	8	\N	10000	sd.notification.default.rule.content.approval.required.v2
sd.notifications.custom.email.content	1560742234839	9	\N	10000	sd.notification.default.rule.content.customer.visible.status.change
\.


--
-- Name: AO_C7F17E_LINGO_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_C7F17E_LINGO_ID_seq"', 9, true);


--
-- Data for Name: AO_C7F17E_LINGO_REVISION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_C7F17E_LINGO_REVISION" ("CREATED_TIMESTAMP", "ID", "LINGO_ID") FROM stdin;
1560742233670	1	1
1560742234098	2	1
1560742234120	3	2
1560742234216	4	2
1560742234227	5	3
1560742234300	6	3
1560742234310	7	4
1560742234399	8	4
1560742234490	9	5
1560742234556	10	5
1560742234598	11	6
1560742234676	12	6
1560742234719	13	7
1560742234762	14	7
1560742234767	15	8
1560742234814	16	8
1560742234839	17	9
1560742234903	18	9
\.


--
-- Name: AO_C7F17E_LINGO_REVISION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_C7F17E_LINGO_REVISION_ID_seq"', 18, true);


--
-- Data for Name: AO_C7F17E_LINGO_TRANSLATION; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_C7F17E_LINGO_TRANSLATION" ("CONTENT", "CREATED_TIMESTAMP", "ID", "LANGUAGE", "LINGO_REVISION_ID", "LOCALE") FROM stdin;
\.


--
-- Name: AO_C7F17E_LINGO_TRANSLATION_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_C7F17E_LINGO_TRANSLATION_ID_seq"', 1, false);


--
-- Data for Name: AO_C7F17E_PROJECT_LANGUAGE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_C7F17E_PROJECT_LANGUAGE" ("ACTIVE", "ID", "LOCALE", "PROJECT_LANG_REV_ID") FROM stdin;
t	1	en-US	1
\.


--
-- Name: AO_C7F17E_PROJECT_LANGUAGE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_C7F17E_PROJECT_LANGUAGE_ID_seq"', 1, true);


--
-- Data for Name: AO_C7F17E_PROJECT_LANG_CONFIG; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_C7F17E_PROJECT_LANG_CONFIG" ("CREATED_TIMESTAMP", "ID", "PROJECT_ID") FROM stdin;
1560742234975	1	10000
\.


--
-- Name: AO_C7F17E_PROJECT_LANG_CONFIG_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_C7F17E_PROJECT_LANG_CONFIG_ID_seq"', 1, true);


--
-- Data for Name: AO_C7F17E_PROJECT_LANG_REV; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_C7F17E_PROJECT_LANG_REV" ("AUTHOR_USER_KEY", "CREATED_TIMESTAMP", "DEFAULT_LANGUAGE", "ID", "PROJECT_LANG_CONFIG_ID") FROM stdin;
admin	1560742234975	en-US	1	1
\.


--
-- Name: AO_C7F17E_PROJECT_LANG_REV_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_C7F17E_PROJECT_LANG_REV_ID_seq"', 1, true);


--
-- Data for Name: AO_D530BB_CANNEDRESPONSE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_D530BB_CANNEDRESPONSE" ("CREATED_TIME", "CREATED_USER_KEY", "ENABLED", "ID", "SERVICE_DESK_ID", "SOFT_DELETED", "TEXT", "TITLE", "UPDATED_TIME", "UPDATED_USER_KEY") FROM stdin;
\.


--
-- Data for Name: AO_D530BB_CANNEDRESPONSEAUDIT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_D530BB_CANNEDRESPONSEAUDIT" ("ACTION", "CANNED_RESPONSE_ID", "EVENT_TIME", "ID", "USER_KEY") FROM stdin;
\.


--
-- Name: AO_D530BB_CANNEDRESPONSEAUDIT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_D530BB_CANNEDRESPONSEAUDIT_ID_seq"', 1, false);


--
-- Data for Name: AO_D530BB_CANNEDRESPONSEUSAGE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_D530BB_CANNEDRESPONSEUSAGE" ("CANNED_RESPONSE_ID", "ID", "USAGE_TIME", "USER_KEY") FROM stdin;
\.


--
-- Name: AO_D530BB_CANNEDRESPONSEUSAGE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_D530BB_CANNEDRESPONSEUSAGE_ID_seq"', 1, false);


--
-- Name: AO_D530BB_CANNEDRESPONSE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_D530BB_CANNEDRESPONSE_ID_seq"', 1, false);


--
-- Data for Name: AO_D530BB_CRAUDITACTIONDATA; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_D530BB_CRAUDITACTIONDATA" ("CANNED_RESPONSE_AUDIT_LOG_ID", "DATA_COLUMN_NAME", "DATA_TYPE", "FROM_BOOLEAN", "FROM_STRING", "FROM_TEXT", "ID", "TO_BOOLEAN", "TO_STRING", "TO_TEXT") FROM stdin;
\.


--
-- Name: AO_D530BB_CRAUDITACTIONDATA_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_D530BB_CRAUDITACTIONDATA_ID_seq"', 1, false);


--
-- Data for Name: AO_ED669C_SEEN_ASSERTIONS; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_ED669C_SEEN_ASSERTIONS" ("ASSERTION_ID", "EXPIRY_TIMESTAMP", "ID") FROM stdin;
\.


--
-- Name: AO_ED669C_SEEN_ASSERTIONS_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_ED669C_SEEN_ASSERTIONS_ID_seq"', 1, false);


--
-- Data for Name: AO_F1B27B_HISTORY_RECORD; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_F1B27B_HISTORY_RECORD" ("EVENT_TIME_MILLIS", "EVENT_TYPE", "ID", "MESSAGE", "TARGET_TIME_MILLIS", "TIMED_PROMISE_HISTORY_KEY_HASH") FROM stdin;
1560742232152	SCHEDULED	1	SLA Threshold Events - scheduled timed promise for issue: TEST-1 (origin: consistency task)	1560765600000	0033bcb6706d7e5b19a921896b7c69e385d2a7ac
\.


--
-- Name: AO_F1B27B_HISTORY_RECORD_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_F1B27B_HISTORY_RECORD_ID_seq"', 1, true);


--
-- Data for Name: AO_F1B27B_KEY_COMPONENT; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_F1B27B_KEY_COMPONENT" ("ID", "KEY", "TIMED_PROMISE_ID", "VALUE") FROM stdin;
1	issue.id	1	10000
\.


--
-- Name: AO_F1B27B_KEY_COMPONENT_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_F1B27B_KEY_COMPONENT_ID_seq"', 1, true);


--
-- Data for Name: AO_F1B27B_KEY_COMP_HISTORY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_F1B27B_KEY_COMP_HISTORY" ("ID", "KEY", "TIMED_PROMISE_ID", "VALUE") FROM stdin;
1	issue.id	1	10000
\.


--
-- Name: AO_F1B27B_KEY_COMP_HISTORY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_F1B27B_KEY_COMP_HISTORY_ID_seq"', 1, true);


--
-- Data for Name: AO_F1B27B_PROMISE; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_F1B27B_PROMISE" ("CLASSIFICATION", "CONTENT", "CREATED_TIME_MILLIS", "ID", "KEY_HASH", "MIME_TYPE", "STATUS", "TARGET_TIME_MILLIS", "TASK_KEY", "UPDATED_TIME_MILLIS") FROM stdin;
servicedesk.sla.threshold.event	\N	1560742232135	1	0033bcb6706d7e5b19a921896b7c69e385d2a7ac	\N	SCHEDULED	1560765600000	com.atlassian.servicedesk.sla.threshold.event.task.runner	1560742232135
\.


--
-- Data for Name: AO_F1B27B_PROMISE_HISTORY; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY "AO_F1B27B_PROMISE_HISTORY" ("CLASSIFICATION", "ID", "KEY_HASH", "TASK_KEY") FROM stdin;
servicedesk.sla.threshold.event	1	0033bcb6706d7e5b19a921896b7c69e385d2a7ac	com.atlassian.servicedesk.sla.threshold.event.task.runner
\.


--
-- Name: AO_F1B27B_PROMISE_HISTORY_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_F1B27B_PROMISE_HISTORY_ID_seq"', 1, true);


--
-- Name: AO_F1B27B_PROMISE_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: jira
--

SELECT pg_catalog.setval('"AO_F1B27B_PROMISE_ID_seq"', 1, true);


--
-- Data for Name: app_user; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY app_user (id, user_key, lower_user_name) FROM stdin;
10000	admin	admin
\.


--
-- Data for Name: audit_changed_value; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY audit_changed_value (id, log_id, name, delta_from, delta_to) FROM stdin;
10000	10001	Permission		Browse Users
10001	10001	Group		jira-servicedesk-users
10002	10002	Permission		Bulk Change
10003	10002	Group		jira-servicedesk-users
10004	10003	Permission		Manage Group Filter Subscriptions
10005	10003	Group		jira-servicedesk-users
10006	10004	Permission		Create Shared Objects
10007	10004	Group		jira-servicedesk-users
10008	10005	Organisation	\N	justingriffin.com
10009	10005	Date Purchased	\N	14/Jun/19
10010	10005	License Type	\N	Jira Service Desk (Server): Evaluation
10011	10005	Server ID	\N	BUM1-QTHP-3IHA-N17H
10012	10005	Support Entitlement Number (SEN)	\N	SEN-L13772563
10013	10005	User Limit	\N	Unlimited
10014	10005	jira-servicedesk	\N	-1
10015	10006	Username	\N	admin
10016	10006	Full name	\N	Admin
10017	10006	Email	\N	justin@justingriffin.com
10018	10006	Active / Inactive	\N	Active
10019	10008	Permission		Create Shared Objects
10020	10008	Group		jira-administrators
10021	10009	Permission		Manage Group Filter Subscriptions
10022	10009	Group		jira-administrators
10023	10010	Permission		Bulk Change
10024	10010	Group		jira-administrators
10025	10011	Permission		Browse Users
10026	10011	Group		jira-administrators
10100	10100	Name	\N	Approvals
10101	10100	Description	\N	Provides search options for Jira Service Desk approvals information. This custom field is created programmatically and required by Service Desk.
10102	10100	Type	\N	Approvals
10103	10101	Name	\N	Satisfaction
10104	10101	Description	\N	Stores request feedback in Service Desk requests. This custom field is created programmatically and required by Service Desk.
10105	10101	Type	\N	Satisfaction
10106	10102	Name	\N	Satisfaction date
10107	10102	Description	\N	Stores request feedback date in Service Desk requests. This custom field is created programmatically and required by Service Desk.
10108	10102	Type	\N	Satisfaction date
10200	10200	Name	\N	Jira Service Desk Permission Scheme for Project TEST
10201	10200	Description	\N	This Jira Service Desk Permission Scheme was generated for Project TEST
10202	10201	Permission		Administer Projects
10203	10201	Type		Project Role
10204	10201	Value		Administrators
10205	10202	Permission		Service Desk Agent
10206	10202	Type		Project Role
10207	10202	Value		Administrators
10208	10203	Permission		Browse Projects
10209	10203	Type		Project Role
10210	10203	Value		Administrators
10211	10204	Type		Project Role
10212	10204	Value		Administrators
10213	10205	Permission		View Read-Only Workflow
10214	10205	Type		Project Role
10215	10205	Value		Administrators
10216	10206	Permission		Create Issues
10217	10206	Type		Project Role
10218	10206	Value		Administrators
10219	10207	Permission		Edit Issues
10220	10207	Type		Project Role
10221	10207	Value		Administrators
10222	10208	Permission		Schedule Issues
10223	10208	Type		Project Role
10224	10208	Value		Administrators
10225	10209	Permission		Move Issues
10226	10209	Type		Project Role
10227	10209	Value		Administrators
10228	10210	Permission		Assign Issues
10229	10210	Type		Project Role
10230	10210	Value		Administrators
10231	10211	Permission		Assignable User
10232	10211	Type		Project Role
10233	10211	Value		Administrators
10234	10212	Permission		Resolve Issues
10235	10212	Type		Project Role
10236	10212	Value		Administrators
10237	10213	Permission		Close Issues
10238	10213	Type		Project Role
10239	10213	Value		Administrators
10240	10214	Permission		Modify Reporter
10241	10214	Type		Project Role
10242	10214	Value		Administrators
10243	10215	Permission		Delete Issues
10244	10215	Type		Project Role
10245	10215	Value		Administrators
10246	10216	Permission		Link Issues
10247	10216	Type		Project Role
10248	10216	Value		Administrators
10249	10217	Permission		Transition Issues
10250	10217	Type		Project Role
10251	10217	Value		Administrators
10252	10218	Permission		Set Issue Security
10253	10218	Type		Project Role
10254	10218	Value		Administrators
10255	10219	Permission		View Voters and Watchers
10256	10219	Type		Project Role
10257	10219	Value		Administrators
10258	10220	Permission		Manage Watchers
10259	10220	Type		Project Role
10260	10220	Value		Administrators
10261	10221	Permission		Add Comments
10262	10221	Type		Project Role
10263	10221	Value		Administrators
10264	10222	Permission		Edit All Comments
10265	10222	Type		Project Role
10266	10222	Value		Administrators
10267	10223	Permission		Edit Own Comments
10268	10223	Type		Project Role
10269	10223	Value		Administrators
10270	10224	Permission		Delete Own Comments
10271	10224	Type		Project Role
10272	10224	Value		Administrators
10273	10225	Permission		Delete All Comments
10274	10225	Type		Project Role
10275	10225	Value		Administrators
10276	10226	Permission		Delete All Attachments
10277	10226	Type		Project Role
10278	10226	Value		Administrators
10279	10227	Permission		Create Attachments
10280	10227	Type		Project Role
10281	10227	Value		Administrators
10282	10228	Permission		Delete Own Attachments
10283	10228	Type		Project Role
10284	10228	Value		Administrators
10285	10229	Permission		Work On Issues
10286	10229	Type		Project Role
10287	10229	Value		Administrators
10288	10230	Permission		Edit Own Worklogs
10289	10230	Type		Project Role
10290	10230	Value		Administrators
10291	10231	Permission		Delete Own Worklogs
10292	10231	Type		Project Role
10293	10231	Value		Administrators
10294	10232	Permission		Edit All Worklogs
10295	10232	Type		Project Role
10296	10232	Value		Administrators
10297	10233	Permission		Delete All Worklogs
10298	10233	Type		Project Role
10299	10233	Value		Administrators
10300	10234	Permission		Browse Projects
10301	10234	Type		Project Role
10302	10234	Value		Service Desk Team
10303	10235	Permission		Service Desk Agent
10304	10235	Type		Project Role
10305	10235	Value		Service Desk Team
10306	10236	Permission		View Read-Only Workflow
10307	10236	Type		Project Role
10308	10236	Value		Service Desk Team
10309	10237	Permission		Create Issues
10310	10237	Type		Project Role
10311	10237	Value		Service Desk Team
10312	10238	Permission		Edit Issues
10313	10238	Type		Project Role
10314	10238	Value		Service Desk Team
10315	10239	Permission		Schedule Issues
10316	10239	Type		Project Role
10317	10239	Value		Service Desk Team
10318	10240	Permission		Move Issues
10319	10240	Type		Project Role
10320	10240	Value		Service Desk Team
10321	10241	Permission		Assign Issues
10322	10241	Type		Project Role
10323	10241	Value		Service Desk Team
10324	10242	Permission		Assignable User
10325	10242	Type		Project Role
10326	10242	Value		Service Desk Team
10327	10243	Permission		Resolve Issues
10328	10243	Type		Project Role
10329	10243	Value		Service Desk Team
10330	10244	Permission		Close Issues
10331	10244	Type		Project Role
10332	10244	Value		Service Desk Team
10333	10245	Permission		Modify Reporter
10334	10245	Type		Project Role
10335	10245	Value		Service Desk Team
10336	10246	Permission		Delete Issues
10337	10246	Type		Project Role
10338	10246	Value		Service Desk Team
10339	10247	Permission		Link Issues
10340	10247	Type		Project Role
10341	10247	Value		Service Desk Team
10342	10248	Permission		Transition Issues
10343	10248	Type		Project Role
10344	10248	Value		Service Desk Team
10345	10249	Permission		Set Issue Security
10346	10249	Type		Project Role
10347	10249	Value		Service Desk Team
10348	10250	Permission		View Voters and Watchers
10349	10250	Type		Project Role
10350	10250	Value		Service Desk Team
10351	10251	Permission		Manage Watchers
10352	10251	Type		Project Role
10353	10251	Value		Service Desk Team
10354	10252	Permission		Add Comments
10355	10252	Type		Project Role
10356	10252	Value		Service Desk Team
10357	10253	Permission		Edit All Comments
10358	10253	Type		Project Role
10359	10253	Value		Service Desk Team
10360	10254	Permission		Edit Own Comments
10361	10254	Type		Project Role
10362	10254	Value		Service Desk Team
10363	10255	Permission		Delete Own Comments
10364	10255	Type		Project Role
10365	10255	Value		Service Desk Team
10366	10256	Permission		Delete All Comments
10367	10256	Type		Project Role
10368	10256	Value		Service Desk Team
10369	10257	Permission		Delete All Attachments
10370	10257	Type		Project Role
10371	10257	Value		Service Desk Team
10372	10258	Permission		Create Attachments
10373	10258	Type		Project Role
10374	10258	Value		Service Desk Team
10375	10259	Permission		Delete Own Attachments
10376	10259	Type		Project Role
10377	10259	Value		Service Desk Team
10378	10260	Permission		Work On Issues
10379	10260	Type		Project Role
10380	10260	Value		Service Desk Team
10381	10261	Permission		Edit Own Worklogs
10382	10261	Type		Project Role
10383	10261	Value		Service Desk Team
10384	10262	Permission		Delete Own Worklogs
10385	10262	Type		Project Role
10386	10262	Value		Service Desk Team
10387	10263	Permission		Browse Projects
10388	10263	Type		Service Desk Customer - Portal Access
10389	10264	Permission		Create Issues
10390	10264	Type		Service Desk Customer - Portal Access
10391	10265	Permission		Edit Issues
10392	10265	Type		Service Desk Customer - Portal Access
10393	10266	Permission		Schedule Issues
10394	10266	Type		Service Desk Customer - Portal Access
10395	10267	Permission		Move Issues
10396	10267	Type		Service Desk Customer - Portal Access
10397	10268	Permission		Assign Issues
10398	10268	Type		Service Desk Customer - Portal Access
10399	10269	Permission		Resolve Issues
10400	10269	Type		Service Desk Customer - Portal Access
10401	10270	Permission		Close Issues
10402	10270	Type		Service Desk Customer - Portal Access
10403	10271	Permission		Modify Reporter
10404	10271	Type		Service Desk Customer - Portal Access
10405	10272	Permission		Delete Issues
10406	10272	Type		Service Desk Customer - Portal Access
10407	10273	Permission		Link Issues
10408	10273	Type		Service Desk Customer - Portal Access
10409	10274	Permission		Transition Issues
10410	10274	Type		Service Desk Customer - Portal Access
10411	10275	Permission		Set Issue Security
10412	10275	Type		Service Desk Customer - Portal Access
10413	10276	Permission		View Voters and Watchers
10414	10276	Type		Service Desk Customer - Portal Access
10415	10277	Permission		Manage Watchers
10416	10277	Type		Service Desk Customer - Portal Access
10417	10278	Permission		Add Comments
10418	10278	Type		Service Desk Customer - Portal Access
10419	10279	Permission		Edit Own Comments
10420	10279	Type		Service Desk Customer - Portal Access
10421	10280	Permission		Delete Own Comments
10422	10280	Type		Service Desk Customer - Portal Access
10423	10281	Permission		Create Attachments
10424	10281	Type		Service Desk Customer - Portal Access
10425	10282	Permission		Delete Own Attachments
10426	10282	Type		Service Desk Customer - Portal Access
10427	10285	Name	\N	Jira Service Desk IT Support Workflow Scheme generated for Project TEST
10428	10285	Description	\N	This Jira Service Desk IT Support Workflow Scheme was generated for Project TEST
10429	10286	Name	\N	TEST: Service Request Fulfilment workflow for Jira Service Desk
10430	10286	Description	\N	
10431	10286	Status	\N	Waiting for support
10432	10286	Status	\N	Waiting for customer
10433	10286	Status	\N	Pending
10434	10286	Status	\N	Resolved
10435	10286	Status	\N	In progress
10436	10286	Status	\N	Canceled
10437	10286	Status	\N	Escalated
10438	10286	Status	\N	Closed
10439	10286	Transition	\N	Create issue (Waiting for support)
10440	10286	Transition	\N	Resolve this issue ([Waiting for support, Waiting for customer, Pending, In progress] -> Resolved)
10441	10286	Transition	\N	Respond to support (Waiting for customer -> Waiting for support)
10442	10286	Transition	\N	Respond to customer ([Waiting for support, Pending] -> Waiting for customer)
10443	10286	Transition	\N	Pending ([Waiting for support, In progress] -> Pending)
10444	10286	Transition	\N	In progress ([Waiting for support, Pending, Escalated] -> In progress)
10445	10286	Transition	\N	Cancel request ([Waiting for support, Waiting for customer, Pending, In progress] -> Canceled)
10446	10286	Transition	\N	Escalate this issue ([Waiting for customer] -> Escalated)
10447	10286	Transition	\N	Escalate (Waiting for support -> Escalated)
10448	10286	Transition	\N	Back to in progress (Resolved -> In progress)
10449	10286	Transition	\N	Close ([Resolved, Canceled] -> Closed)
10450	10287	Name	\N	Approvers
10451	10287	Description	\N	Contains users needed for approval. This custom field was created by Jira Service Desk.
10452	10287	Type	\N	User Picker (multiple users)
10453	10289	Description		This Jira Service Desk Service Request Fulfilment workflow was generated for Project TEST
10454	10291	Name	\N	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk
10455	10291	Description	\N	
10456	10291	Status	\N	Waiting for support
10457	10291	Status	\N	Waiting for customer
10458	10291	Status	\N	Pending
10459	10291	Status	\N	Resolved
10460	10291	Status	\N	In progress
10461	10291	Status	\N	Waiting for approval
10462	10291	Status	\N	Canceled
10463	10291	Status	\N	Escalated
10464	10291	Status	\N	Closed
10465	10291	Transition	\N	Create issue (Waiting for approval)
10466	10291	Transition	\N	Resolve this issue ([Waiting for support, Waiting for customer, Pending, In progress] -> Resolved)
10467	10291	Transition	\N	Respond to support (Waiting for customer -> Waiting for support)
10468	10291	Transition	\N	Respond to customer ([Waiting for support, Pending] -> Waiting for customer)
10469	10291	Transition	\N	Pending ([Waiting for support, In progress] -> Pending)
10470	10291	Transition	\N	In progress ([Waiting for support, Pending, Escalated] -> In progress)
10471	10291	Transition	\N	Approved (Waiting for approval -> Waiting for support)
10472	10291	Transition	\N	Declined (Waiting for approval -> Resolved)
10473	10291	Transition	\N	Cancel request ([Waiting for support, Waiting for customer, Pending, In progress, Waiting for approval] -> Canceled)
10474	10291	Transition	\N	Escalate (Waiting for support -> Escalated)
10475	10291	Transition	\N	Escalate this issue (Waiting for customer -> Escalated)
10476	10291	Transition	\N	Back to in progress (Resolved -> In progress)
10477	10291	Transition	\N	Close ([Resolved, Canceled] -> Closed)
10478	10293	Description		This Jira Service Desk Service Request Fulfilment with Approvals workflow was generated for Project TEST
10479	10295	Name	\N	TEST: Jira Service Desk default workflow
10480	10295	Description	\N	
10481	10295	Status	\N	Open
10482	10295	Status	\N	Pending
10483	10295	Status	\N	Work in progress
10484	10295	Status	\N	Reopened
10485	10295	Status	\N	Done
10486	10295	Transition	\N	Create (Open)
10487	10295	Transition	\N	Start progress ([Open, Pending, Reopened] -> Work in progress)
10488	10295	Transition	\N	Pending ([Open, Work in progress, Reopened] -> Pending)
10489	10295	Transition	\N	Back to open ([Pending, Work in progress] -> Open)
10490	10295	Transition	\N	Mark as Done ([Open, Work in progress, Reopened] -> Done)
10491	10295	Transition	\N	Reopen issue (Done -> Reopened)
10492	10297	Description		This Jira Service Desk default workflow was generated for Project TEST
10493	10299	Name	\N	Jira Service Desk Field Configuration Scheme for Project TEST
10494	10299	Description	\N	This Jira Service Desk Field Configuration Scheme was generated for Project TEST
10495	10300	Issue Type	Default	Default
10496	10300	Field Configuration	Default Field Configuration	Jira Service Desk Field Configuration for Project TEST
10497	10301	Issue Type	Default	Default
10498	10304	Name	\N	Jira
10499	10304	Description	\N	Issues related to Jira. Created by Jira Service Desk.
10500	10304	Component Lead	\N	admin
10501	10304	Default Assignee	\N	Unassigned
10502	10305	Name	\N	Intranet
10503	10305	Description	\N	Issues related to the intranet. Created by Jira Service Desk.
10504	10305	Component Lead	\N	admin
10505	10305	Default Assignee	\N	Unassigned
10506	10306	Name	\N	Public website
10507	10306	Description	\N	Issues related to the public website. Created by Jira Service Desk.
10508	10306	Component Lead	\N	admin
10509	10306	Default Assignee	\N	Unassigned
10510	10307	Name	\N	Time to resolution
10511	10307	Description	\N	This custom field was created by Jira Service Desk.
10512	10307	Type	\N	SLA CustomField Type
10513	10308	Name	\N	Time to first response
10514	10308	Description	\N	This custom field was created by Jira Service Desk.
10515	10308	Type	\N	SLA CustomField Type
10516	10309	Name	\N	Test
10517	10309	Key	\N	TEST
10518	10309	Description	\N	
10519	10309	URL	\N	
10520	10309	Project Lead	\N	admin
10521	10309	Default Assignee	\N	Unassigned
\.


--
-- Data for Name: audit_item; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY audit_item (id, log_id, object_type, object_id, object_name, object_parent_id, object_parent_name) FROM stdin;
10000	10007	USER	admin	admin	1	Jira Internal Directory
10001	10012	USER	admin	admin	1	Jira Internal Directory
10100	10283	SCHEME	0	Default Permission Scheme	\N	\N
10101	10284	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N
10102	10290	SCHEME	10100	Jira Service Desk IT Support Workflow Scheme generated for Project TEST	\N	\N
10103	10294	SCHEME	10100	Jira Service Desk IT Support Workflow Scheme generated for Project TEST	\N	\N
10104	10298	SCHEME	10100	Jira Service Desk IT Support Workflow Scheme generated for Project TEST	\N	\N
10105	10303	SCHEME	10000	Jira Service Desk Field Configuration Scheme for Project TEST	\N	\N
10106	10304	PROJECT	10000	Test	\N	\N
10107	10304	USER	admin	admin	1	Jira Internal Directory
10108	10305	PROJECT	10000	Test	\N	\N
10109	10305	USER	admin	admin	1	Jira Internal Directory
10110	10306	PROJECT	10000	Test	\N	\N
10111	10306	USER	admin	admin	1	Jira Internal Directory
10112	10309	USER	admin	admin	1	Jira Internal Directory
\.


--
-- Data for Name: audit_log; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY audit_log (id, remote_address, created, author_key, summary, category, object_type, object_id, object_name, object_parent_id, object_parent_name, author_type, event_source_name, description, long_description, search_field) FROM stdin;
10000	192.168.33.1	2019-06-17 03:06:27.379+00	\N	Group created	group management	GROUP	\N	jira-servicedesk-users	1	Jira Internal Directory	0		\N		192.168.33.1 group created management jira-servicedesk-users jira internal directory
10001	192.168.33.1	2019-06-17 03:06:27.529+00	\N	Global permission added	permissions	PERMISSIONS	\N	Global Permissions	\N	\N	0		\N		192.168.33.1 global permission added permissions browse users jira-servicedesk-users
10002	192.168.33.1	2019-06-17 03:06:27.554+00	\N	Global permission added	permissions	PERMISSIONS	\N	Global Permissions	\N	\N	0		\N		192.168.33.1 global permission added permissions bulk change jira-servicedesk-users
10003	192.168.33.1	2019-06-17 03:06:27.557+00	\N	Global permission added	permissions	PERMISSIONS	\N	Global Permissions	\N	\N	0		\N		192.168.33.1 global permission added permissions manage group filter subscriptions jira-servicedesk-users
10004	192.168.33.1	2019-06-17 03:06:27.567+00	\N	Global permission added	permissions	PERMISSIONS	\N	Global Permissions	\N	\N	0		\N		192.168.33.1 global permission added permissions create shared objects jira-servicedesk-users
10005	192.168.33.1	2019-06-17 03:06:27.662+00	\N	New license added	system	LICENSE	0	SEN-L13772563	0	License SEN	0		\N		192.168.33.1 new license added system sen-l13772563 sen justingriffin.com 14/jun/19 jira service desk (server): evaluation bum1-qthp-3iha-n17h unlimited -1
10006	192.168.33.1	2019-06-17 03:10:43.865+00	\N	User created	user management	USER	admin	admin	1	Jira Internal Directory	0		\N		192.168.33.1 user created management admin jira internal directory justin@justingriffin.com active
10007	192.168.33.1	2019-06-17 03:10:44.046+00	\N	User added to group	group management	GROUP	\N	jira-administrators	1	Jira Internal Directory	0		\N		192.168.33.1 user added to group management jira-administrators jira internal directory admin
10008	192.168.33.1	2019-06-17 03:10:44.157+00	\N	Global permission added	permissions	PERMISSIONS	\N	Global Permissions	\N	\N	0		\N		192.168.33.1 global permission added permissions create shared objects jira-administrators
10009	192.168.33.1	2019-06-17 03:10:44.164+00	\N	Global permission added	permissions	PERMISSIONS	\N	Global Permissions	\N	\N	0		\N		192.168.33.1 global permission added permissions manage group filter subscriptions jira-administrators
10010	192.168.33.1	2019-06-17 03:10:44.171+00	\N	Global permission added	permissions	PERMISSIONS	\N	Global Permissions	\N	\N	0		\N		192.168.33.1 global permission added permissions bulk change jira-administrators
10011	192.168.33.1	2019-06-17 03:10:44.175+00	\N	Global permission added	permissions	PERMISSIONS	\N	Global Permissions	\N	\N	0		\N		192.168.33.1 global permission added permissions browse users jira-administrators
10012	192.168.33.1	2019-06-17 03:10:44.196+00	\N	User added to group	group management	GROUP	\N	jira-servicedesk-users	1	Jira Internal Directory	0		\N		192.168.33.1 user added to group management jira-servicedesk-users jira internal directory admin
10100	192.168.33.1	2019-06-17 03:11:22.45+00	\N	Custom field created	fields	CUSTOM_FIELD	customfield_10100	Approvals	\N	\N	0		\N		192.168.33.1 custom field created fields approvals provides search options for jira service desk information. this is programmatically and required by desk.
10101	192.168.33.1	2019-06-17 03:11:32.944+00	\N	Custom field created	fields	CUSTOM_FIELD	customfield_10101	Satisfaction	\N	\N	0		\N		192.168.33.1 custom field created fields satisfaction stores request feedback in service desk requests. this is programmatically and required by desk.
10102	192.168.33.1	2019-06-17 03:11:33.259+00	\N	Custom field created	fields	CUSTOM_FIELD	customfield_10102	Satisfaction date	\N	\N	0		\N		192.168.33.1 custom field created fields satisfaction date stores request feedback in service desk requests. this is programmatically and required by desk.
10200	192.168.33.1	2019-06-17 03:30:19.837+00	admin	Permission scheme created	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme created permissions jira service desk for project test this was generated
10201	192.168.33.1	2019-06-17 03:30:19.938+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test administer projects role administrators
10202	192.168.33.1	2019-06-17 03:30:19.955+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test agent role administrators
10203	192.168.33.1	2019-06-17 03:30:19.992+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test browse projects role administrators
10204	192.168.33.1	2019-06-17 03:30:20.011+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test role administrators
10205	192.168.33.1	2019-06-17 03:30:20.028+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test view read-only workflow role administrators
10206	192.168.33.1	2019-06-17 03:30:20.037+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test create issues role administrators
10207	192.168.33.1	2019-06-17 03:30:20.054+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit issues role administrators
10208	192.168.33.1	2019-06-17 03:30:20.073+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test schedule issues role administrators
10209	192.168.33.1	2019-06-17 03:30:20.096+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test move issues role administrators
10210	192.168.33.1	2019-06-17 03:30:20.169+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test assign issues role administrators
10211	192.168.33.1	2019-06-17 03:30:20.272+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test assignable user role administrators
10212	192.168.33.1	2019-06-17 03:30:20.284+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test resolve issues role administrators
10213	192.168.33.1	2019-06-17 03:30:20.298+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test close issues role administrators
10214	192.168.33.1	2019-06-17 03:30:20.351+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test modify reporter role administrators
10215	192.168.33.1	2019-06-17 03:30:20.417+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete issues role administrators
10216	192.168.33.1	2019-06-17 03:30:20.426+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test link issues role administrators
10217	192.168.33.1	2019-06-17 03:30:20.437+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test transition issues role administrators
10218	192.168.33.1	2019-06-17 03:30:20.446+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test set issue security role administrators
10219	192.168.33.1	2019-06-17 03:30:20.459+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test view voters and watchers role administrators
10220	192.168.33.1	2019-06-17 03:30:20.494+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test manage watchers role administrators
10221	192.168.33.1	2019-06-17 03:30:20.515+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test add comments role administrators
10222	192.168.33.1	2019-06-17 03:30:20.525+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit all comments role administrators
10223	192.168.33.1	2019-06-17 03:30:20.534+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit own comments role administrators
10224	192.168.33.1	2019-06-17 03:30:20.552+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete own comments role administrators
10225	192.168.33.1	2019-06-17 03:30:20.61+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete all comments role administrators
10226	192.168.33.1	2019-06-17 03:30:20.68+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete all attachments role administrators
10227	192.168.33.1	2019-06-17 03:30:20.743+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test create attachments role administrators
10228	192.168.33.1	2019-06-17 03:30:20.805+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete own attachments role administrators
10229	192.168.33.1	2019-06-17 03:30:20.833+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test work on issues role administrators
10230	192.168.33.1	2019-06-17 03:30:20.845+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit own worklogs role administrators
10231	192.168.33.1	2019-06-17 03:30:20.862+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete own worklogs role administrators
10232	192.168.33.1	2019-06-17 03:30:20.871+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit all worklogs role administrators
10233	192.168.33.1	2019-06-17 03:30:20.88+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete all worklogs role administrators
10234	192.168.33.1	2019-06-17 03:30:20.886+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test browse projects role team
10235	192.168.33.1	2019-06-17 03:30:20.895+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test agent role team
10236	192.168.33.1	2019-06-17 03:30:20.902+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test view read-only workflow role team
10237	192.168.33.1	2019-06-17 03:30:20.908+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test create issues role team
10238	192.168.33.1	2019-06-17 03:30:20.919+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit issues role team
10239	192.168.33.1	2019-06-17 03:30:20.928+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test schedule issues role team
10240	192.168.33.1	2019-06-17 03:30:20.935+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test move issues role team
10241	192.168.33.1	2019-06-17 03:30:20.942+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test assign issues role team
10242	192.168.33.1	2019-06-17 03:30:20.948+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test assignable user role team
10243	192.168.33.1	2019-06-17 03:30:20.955+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test resolve issues role team
10244	192.168.33.1	2019-06-17 03:30:20.961+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test close issues role team
10245	192.168.33.1	2019-06-17 03:30:20.965+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test modify reporter role team
10246	192.168.33.1	2019-06-17 03:30:20.974+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete issues role team
10247	192.168.33.1	2019-06-17 03:30:20.981+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test link issues role team
10248	192.168.33.1	2019-06-17 03:30:20.987+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test transition issues role team
10249	192.168.33.1	2019-06-17 03:30:20.997+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test set issue security role team
10250	192.168.33.1	2019-06-17 03:30:21.003+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test view voters and watchers role team
10251	192.168.33.1	2019-06-17 03:30:21.013+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test manage watchers role team
10252	192.168.33.1	2019-06-17 03:30:21.019+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test add comments role team
10253	192.168.33.1	2019-06-17 03:30:21.025+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit all comments role team
10254	192.168.33.1	2019-06-17 03:30:21.101+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit own comments role team
10255	192.168.33.1	2019-06-17 03:30:21.159+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete own comments role team
10256	192.168.33.1	2019-06-17 03:30:21.236+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete all comments role team
10257	192.168.33.1	2019-06-17 03:30:21.283+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete all attachments role team
10258	192.168.33.1	2019-06-17 03:30:21.32+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test create attachments role team
10259	192.168.33.1	2019-06-17 03:30:21.381+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete own attachments role team
10260	192.168.33.1	2019-06-17 03:30:21.387+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test work on issues role team
10261	192.168.33.1	2019-06-17 03:30:21.431+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit own worklogs role team
10262	192.168.33.1	2019-06-17 03:30:21.483+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete own worklogs role team
10263	192.168.33.1	2019-06-17 03:30:21.585+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test browse projects customer - portal access
10264	192.168.33.1	2019-06-17 03:30:21.642+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test create issues customer - portal access
10265	192.168.33.1	2019-06-17 03:30:21.697+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit issues customer - portal access
10266	192.168.33.1	2019-06-17 03:30:21.732+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test schedule issues customer - portal access
10267	192.168.33.1	2019-06-17 03:30:21.737+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test move issues customer - portal access
10268	192.168.33.1	2019-06-17 03:30:21.745+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test assign issues customer - portal access
10269	192.168.33.1	2019-06-17 03:30:21.759+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test resolve issues customer - portal access
10270	192.168.33.1	2019-06-17 03:30:21.772+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test close issues customer - portal access
10271	192.168.33.1	2019-06-17 03:30:21.781+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test modify reporter customer - portal access
10272	192.168.33.1	2019-06-17 03:30:21.786+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete issues customer - portal access
10273	192.168.33.1	2019-06-17 03:30:21.793+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test link issues customer - portal access
10274	192.168.33.1	2019-06-17 03:30:21.798+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test transition issues customer - portal access
10275	192.168.33.1	2019-06-17 03:30:21.803+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test set issue security customer - portal access
10276	192.168.33.1	2019-06-17 03:30:21.82+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test view voters and watchers customer - portal access
10277	192.168.33.1	2019-06-17 03:30:21.835+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test manage watchers customer - portal access
10278	192.168.33.1	2019-06-17 03:30:21.84+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test add comments customer - portal access
10279	192.168.33.1	2019-06-17 03:30:21.877+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test edit own comments customer - portal access
10280	192.168.33.1	2019-06-17 03:30:21.893+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete own comments customer - portal access
10281	192.168.33.1	2019-06-17 03:30:21.898+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test create attachments customer - portal access
10282	192.168.33.1	2019-06-17 03:30:21.904+00	admin	Permission scheme updated	permissions	SCHEME	10000	Jira Service Desk Permission Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 permission scheme updated permissions jira service desk for project test delete own attachments customer - portal access
10283	192.168.33.1	2019-06-17 03:30:21.925+00	admin	Permission scheme removed from project	permissions	PROJECT	10000	Test	\N	\N	1		\N		admin 192.168.33.1 permission scheme removed from project permissions test default
10284	192.168.33.1	2019-06-17 03:30:22.347+00	admin	Permission scheme added to project	permissions	PROJECT	10000	Test	\N	\N	1		\N		admin 192.168.33.1 permission scheme added to project permissions test jira service desk for
10285	192.168.33.1	2019-06-17 03:30:22.643+00	admin	Workflow scheme created	workflows	SCHEME	10100	Jira Service Desk IT Support Workflow Scheme generated for Project TEST	\N	\N	1		\N		admin 192.168.33.1 workflow scheme created workflows jira service desk it support generated for project test this was
10286	192.168.33.1	2019-06-17 03:30:23.467+00	admin	Workflow created	workflows	WORKFLOW	TEST: Service Request Fulfilment workflow for Jira Service Desk	TEST: Service Request Fulfilment workflow for Jira Service Desk	\N	\N	1		\N		admin 192.168.33.1 workflow created workflows test: service request fulfilment for jira desk waiting support customer pending resolved in progress canceled escalated closed create issue (waiting support) resolve this ([waiting support, customer, pending, progress] -> resolved) respond to pending] customer) pending) escalated] progress) cancel canceled) escalate customer] escalated) back (resolved close ([resolved, canceled] closed)
10287	192.168.33.1	2019-06-17 03:30:23.619+00	admin	Custom field created	fields	CUSTOM_FIELD	customfield_10200	Approvers	\N	\N	1		\N		admin 192.168.33.1 custom field created fields approvers contains users needed for approval. this was by jira service desk. user picker (multiple users)
10288	192.168.33.1	2019-06-17 03:30:23.875+00	admin	Workflow updated	workflows	WORKFLOW	TEST: Service Request Fulfilment workflow for Jira Service Desk	TEST: Service Request Fulfilment workflow for Jira Service Desk	\N	\N	1		\N		admin 192.168.33.1 workflow updated workflows test: service request fulfilment for jira desk
10289	192.168.33.1	2019-06-17 03:30:23.973+00	admin	Workflow updated	workflows	WORKFLOW	TEST: Service Request Fulfilment workflow for Jira Service Desk	TEST: Service Request Fulfilment workflow for Jira Service Desk	\N	\N	1		\N		admin 192.168.33.1 workflow updated workflows test: service request fulfilment for jira desk this was generated project test
10290	192.168.33.1	2019-06-17 03:30:24.096+00	admin	Workflow scheme added to project	workflows	PROJECT	10000	Test	\N	\N	1		\N		admin 192.168.33.1 workflow scheme added to project workflows test jira service desk it support generated for
10291	192.168.33.1	2019-06-17 03:30:24.263+00	admin	Workflow created	workflows	WORKFLOW	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk	\N	\N	1		\N		admin 192.168.33.1 workflow created workflows test: service request fulfilment with approvals for jira desk waiting support customer pending resolved in progress approval canceled escalated closed create issue (waiting approval) resolve this ([waiting support, customer, pending, progress] -> resolved) respond to support) pending] customer) pending) escalated] progress) approved declined cancel progress, approval] canceled) escalate escalated) back (resolved close ([resolved, canceled] closed)
10292	192.168.33.1	2019-06-17 03:30:24.383+00	admin	Workflow updated	workflows	WORKFLOW	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk	\N	\N	1		\N		admin 192.168.33.1 workflow updated workflows test: service request fulfilment with approvals for jira desk
10293	192.168.33.1	2019-06-17 03:30:24.446+00	admin	Workflow updated	workflows	WORKFLOW	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk	\N	\N	1		\N		admin 192.168.33.1 workflow updated workflows test: service request fulfilment with approvals for jira desk this was generated project test
10294	192.168.33.1	2019-06-17 03:30:24.465+00	admin	Workflow scheme added to project	workflows	PROJECT	10000	Test	\N	\N	1		\N		admin 192.168.33.1 workflow scheme added to project workflows test jira service desk it support generated for
10295	192.168.33.1	2019-06-17 03:30:24.689+00	admin	Workflow created	workflows	WORKFLOW	TEST: Jira Service Desk default workflow	TEST: Jira Service Desk default workflow	\N	\N	1		\N		admin 192.168.33.1 workflow created workflows test: jira service desk default open pending work in progress reopened done create (open) start ([open, pending, reopened] -> progress) progress, pending) back to ([pending, progress] open) mark as done) reopen issue (done reopened)
10296	192.168.33.1	2019-06-17 03:30:24.762+00	admin	Workflow updated	workflows	WORKFLOW	TEST: Jira Service Desk default workflow	TEST: Jira Service Desk default workflow	\N	\N	1		\N		admin 192.168.33.1 workflow updated workflows test: jira service desk default
10297	192.168.33.1	2019-06-17 03:30:24.835+00	admin	Workflow updated	workflows	WORKFLOW	TEST: Jira Service Desk default workflow	TEST: Jira Service Desk default workflow	\N	\N	1		\N		admin 192.168.33.1 workflow updated workflows test: jira service desk default this was generated for project test
10298	192.168.33.1	2019-06-17 03:30:24.901+00	admin	Workflow scheme added to project	workflows	PROJECT	10000	Test	\N	\N	1		\N		admin 192.168.33.1 workflow scheme added to project workflows test jira service desk it support generated for
10299	192.168.33.1	2019-06-17 03:30:25.265+00	admin	Field Configuration scheme created	fields	SCHEME	10000	Jira Service Desk Field Configuration Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 field configuration scheme created fields jira service desk for project test this was generated
10300	192.168.33.1	2019-06-17 03:30:25.294+00	admin	Field Configuration scheme updated	fields	SCHEME	10000	Jira Service Desk Field Configuration Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 field configuration scheme updated fields jira service desk for project test default
10301	192.168.33.1	2019-06-17 03:30:25.311+00	admin	Field Configuration scheme updated	fields	SCHEME	10000	Jira Service Desk Field Configuration Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 field configuration scheme updated fields jira service desk for project test default
10302	192.168.33.1	2019-06-17 03:30:25.315+00	admin	Field Configuration scheme updated	fields	SCHEME	10000	Jira Service Desk Field Configuration Scheme for Project TEST	\N	\N	1		\N		admin 192.168.33.1 field configuration scheme updated fields jira service desk for project test
10303	192.168.33.1	2019-06-17 03:30:25.327+00	admin	Field Configuration scheme added to project	fields	PROJECT	10000	Test	\N	\N	1		\N		admin 192.168.33.1 field configuration scheme added to project fields test jira service desk for
10304	192.168.33.1	2019-06-17 03:30:26.841+00	admin	Project component created	projects	PROJECT_COMPONENT	10000	Jira	\N	\N	1		\N		admin 192.168.33.1 project component created projects jira test internal directory issues related to jira. by service desk. unassigned
10305	192.168.33.1	2019-06-17 03:30:26.865+00	admin	Project component created	projects	PROJECT_COMPONENT	10001	Intranet	\N	\N	1		\N		admin 192.168.33.1 project component created projects intranet test jira internal directory issues related to the intranet. by service desk. unassigned
10306	192.168.33.1	2019-06-17 03:30:26.88+00	admin	Project component created	projects	PROJECT_COMPONENT	10002	Public website	\N	\N	1		\N		admin 192.168.33.1 project component created projects public website test jira internal directory issues related to the website. by service desk. unassigned
10307	192.168.33.1	2019-06-17 03:30:27.299+00	admin	Custom field created	fields	CUSTOM_FIELD	customfield_10201	Time to resolution	\N	\N	1		\N		admin 192.168.33.1 custom field created fields time to resolution this was by jira service desk. sla customfield type
10308	192.168.33.1	2019-06-17 03:30:27.63+00	admin	Custom field created	fields	CUSTOM_FIELD	customfield_10202	Time to first response	\N	\N	1		\N		admin 192.168.33.1 custom field created fields time to first response this was by jira service desk. sla customfield type
10309	192.168.33.1	2019-06-17 03:30:35.128+00	admin	Project created	projects	PROJECT	10000	Test	\N	\N	1		\N		admin 192.168.33.1 project created projects test jira internal directory unassigned
\.


--
-- Data for Name: avatar; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY avatar (id, filename, contenttype, avatartype, owner, systemavatar) FROM stdin;
10321	development_task.svg	image/svg+xml	issuetype	\N	1
10322	feedback.svg	image/svg+xml	issuetype	\N	1
10000	codegeist.png	image/png	project	\N	1
10001	bird.svg	image/svg+xml	project	\N	1
10002	jm_black.png	image/png	project	\N	1
10323	request_access.svg	image/svg+xml	issuetype	\N	1
10003	jm_brown.png	image/png	project	\N	1
10324	default.svg	image/svg+xml	project	\N	1
10325	code.svg	image/svg+xml	project	\N	1
10004	jm_orange.png	image/png	project	\N	1
10326	coffee.svg	image/svg+xml	project	\N	1
10327	design.svg	image/svg+xml	project	\N	1
10328	drill.svg	image/svg+xml	project	\N	1
10329	food.svg	image/svg+xml	project	\N	1
10330	notes.svg	image/svg+xml	project	\N	1
10005	jm_red.png	image/png	project	\N	1
10331	red-flag.svg	image/svg+xml	project	\N	1
10006	jm_white.png	image/png	project	\N	1
10007	jm_yellow.png	image/png	project	\N	1
10332	science.svg	image/svg+xml	project	\N	1
10008	monster.png	image/png	project	\N	1
10009	nature.svg	image/svg+xml	project	\N	1
10333	support.svg	image/svg+xml	project	\N	1
10010	koala.svg	image/svg+xml	project	\N	1
10334	bull.svg	image/svg+xml	user	\N	1
10011	rocket.svg	image/svg+xml	project	\N	1
10335	cat.svg	image/svg+xml	user	\N	1
10100	Avatar-1.png	image/png	user	\N	1
10336	dog.svg	image/svg+xml	user	\N	1
10337	female_1.svg	image/svg+xml	user	\N	1
10101	Avatar-2.png	image/png	user	\N	1
10338	female_2.svg	image/svg+xml	user	\N	1
10102	Avatar-3.png	image/png	user	\N	1
10339	female_3.svg	image/svg+xml	user	\N	1
10340	female_4.svg	image/svg+xml	user	\N	1
10103	Avatar-4.png	image/png	user	\N	1
10341	ghost.svg	image/svg+xml	user	\N	1
10104	Avatar-5.png	image/png	user	\N	1
10342	male_1.svg	image/svg+xml	user	\N	1
10105	Avatar-6.png	image/png	user	\N	1
10106	Avatar-7.png	image/png	user	\N	1
10107	Avatar-8.png	image/png	user	\N	1
10108	Avatar-9.png	image/png	user	\N	1
10343	male_2.svg	image/svg+xml	user	\N	1
10109	Avatar-10.png	image/png	user	\N	1
10110	Avatar-11.png	image/png	user	\N	1
10344	male_3.svg	image/svg+xml	user	\N	1
10345	male_4.svg	image/svg+xml	user	\N	1
10111	Avatar-12.png	image/png	user	\N	1
10346	male_5.svg	image/svg+xml	user	\N	1
10112	Avatar-13.png	image/png	user	\N	1
10347	male_6.svg	image/svg+xml	user	\N	1
10113	Avatar-14.png	image/png	user	\N	1
10348	male_8.svg	image/svg+xml	user	\N	1
10114	Avatar-15.png	image/png	user	\N	1
10349	owl.svg	image/svg+xml	user	\N	1
10115	Avatar-16.png	image/png	user	\N	1
10350	pirate.svg	image/svg+xml	user	\N	1
10116	Avatar-17.png	image/png	user	\N	1
10117	Avatar-18.png	image/png	user	\N	1
10351	robot.svg	image/svg+xml	user	\N	1
10118	Avatar-19.png	image/png	user	\N	1
10352	vampire.svg	image/svg+xml	user	\N	1
10119	Avatar-20.png	image/png	user	\N	1
10120	Avatar-21.png	image/png	user	\N	1
10121	Avatar-22.png	image/png	user	\N	1
10122	Avatar-default.svg	image/svg+xml	user	\N	1
10123	Avatar-unknown.png	image/png	user	\N	1
10200	cloud.svg	image/svg+xml	project	\N	1
10201	spanner.svg	image/svg+xml	project	\N	1
10202	cd.svg	image/svg+xml	project	\N	1
10203	money.svg	image/svg+xml	project	\N	1
10204	mouse-hand.svg	image/svg+xml	project	\N	1
10205	yeti.svg	image/svg+xml	project	\N	1
10206	power.svg	image/svg+xml	project	\N	1
10207	refresh.svg	image/svg+xml	project	\N	1
10208	phone.svg	image/svg+xml	project	\N	1
10209	settings.svg	image/svg+xml	project	\N	1
10210	storm.svg	image/svg+xml	project	\N	1
10211	plane.svg	image/svg+xml	project	\N	1
10300	genericissue.svg	image/svg+xml	issuetype	\N	1
10303	bug.svg	image/svg+xml	issuetype	\N	1
10304	defect.svg	image/svg+xml	issuetype	\N	1
10306	documentation.svg	image/svg+xml	issuetype	\N	1
10307	epic.svg	image/svg+xml	issuetype	\N	1
10308	exclamation.svg	image/svg+xml	issuetype	\N	1
10309	design_task.svg	image/svg+xml	issuetype	\N	1
10310	improvement.svg	image/svg+xml	issuetype	\N	1
10311	newfeature.svg	image/svg+xml	issuetype	\N	1
10312	remove_feature.svg	image/svg+xml	issuetype	\N	1
10313	requirement.svg	image/svg+xml	issuetype	\N	1
10314	sales.svg	image/svg+xml	issuetype	\N	1
10315	story.svg	image/svg+xml	issuetype	\N	1
10316	subtask.svg	image/svg+xml	issuetype	\N	1
10318	task.svg	image/svg+xml	issuetype	\N	1
10320	question.svg	image/svg+xml	issuetype	\N	1
10500	female_5.svg	image/svg+xml	user	\N	1
10501	female_6.svg	image/svg+xml	user	\N	1
10502	female_7.svg	image/svg+xml	user	\N	1
10503	female_8.svg	image/svg+xml	user	\N	1
10504	female_9.svg	image/svg+xml	user	\N	1
10505	male_7.svg	image/svg+xml	user	\N	1
10506	male_9.svg	image/svg+xml	user	\N	1
10507	pirate_female.svg	image/svg+xml	user	\N	1
10508	princess.svg	image/svg+xml	user	\N	1
10509	spectrum.svg	image/svg+xml	project	\N	1
10600	RT_ICONS_plus.svg	image/svg+xml	SD_REQTYPE	\N	1
10601	RT_ICONS_present.svg	image/svg+xml	SD_REQTYPE	\N	1
10602	RT_ICONS_calendar.svg	image/svg+xml	SD_REQTYPE	\N	1
10603	RT_ICONS_open_book.svg	image/svg+xml	SD_REQTYPE	\N	1
10604	RT_ICONS_two_people.svg	image/svg+xml	SD_REQTYPE	\N	1
10605	RT_ICONS_camera.svg	image/svg+xml	SD_REQTYPE	\N	1
10606	RT_ICONS_heart.svg	image/svg+xml	SD_REQTYPE	\N	1
10607	RT_ICONS_light_bulb.svg	image/svg+xml	SD_REQTYPE	\N	1
10608	RT_ICONS_house.svg	image/svg+xml	SD_REQTYPE	\N	1
10609	RT_ICONS_chair.svg	image/svg+xml	SD_REQTYPE	\N	1
10610	RT_ICONS_robot.svg	image/svg+xml	SD_REQTYPE	\N	1
10611	RT_ICONS_swipe_card.svg	image/svg+xml	SD_REQTYPE	\N	1
10612	RT_ICONS_dollar.svg	image/svg+xml	SD_REQTYPE	\N	1
10613	RT_ICONS_marker.svg	image/svg+xml	SD_REQTYPE	\N	1
10614	RT_ICONS_car.svg	image/svg+xml	SD_REQTYPE	\N	1
10615	RT_ICONS_aeroplane.svg	image/svg+xml	SD_REQTYPE	\N	1
10616	RT_ICONS_database.svg	image/svg+xml	SD_REQTYPE	\N	1
10617	RT_ICONS_volume.svg	image/svg+xml	SD_REQTYPE	\N	1
10618	RT_ICONS_smartphone.svg	image/svg+xml	SD_REQTYPE	\N	1
10619	RT_ICONS_keyboard.svg	image/svg+xml	SD_REQTYPE	\N	1
10620	RT_ICONS_code.svg	image/svg+xml	SD_REQTYPE	\N	1
10621	RT_ICONS_workflow.svg	image/svg+xml	SD_REQTYPE	\N	1
10622	RT_ICONS_up_cloud.svg	image/svg+xml	SD_REQTYPE	\N	1
10623	RT_ICONS_power_button.svg	image/svg+xml	SD_REQTYPE	\N	1
10624	RT_ICONS_headset.svg	image/svg+xml	SD_REQTYPE	\N	1
10625	RT_ICONS_gears.svg	image/svg+xml	SD_REQTYPE	\N	1
10626	RT_ICONS_loop.svg	image/svg+xml	SD_REQTYPE	\N	1
10627	RT_ICONS_question.svg	image/svg+xml	SD_REQTYPE	\N	1
10628	RT_ICONS_signal_tower.svg	image/svg+xml	SD_REQTYPE	\N	1
10629	RT_ICONS_down_cloud.svg	image/svg+xml	SD_REQTYPE	\N	1
10630	RT_ICONS_wrench.svg	image/svg+xml	SD_REQTYPE	\N	1
10631	RT_ICONS_add_person.svg	image/svg+xml	SD_REQTYPE	\N	1
10632	RT_ICONS_monitor.svg	image/svg+xml	SD_REQTYPE	\N	1
10633	RT_ICONS_envelope.svg	image/svg+xml	SD_REQTYPE	\N	1
10634	RT_ICONS_padlock.svg	image/svg+xml	SD_REQTYPE	\N	1
10635	RT_ICONS_printer.svg	image/svg+xml	SD_REQTYPE	\N	1
10636	RT_ICONS_share.svg	image/svg+xml	SD_REQTYPE	\N	1
10637	RT_ICONS_cd.svg	image/svg+xml	SD_REQTYPE	\N	1
10638	RT_ICONS_warning.svg	image/svg+xml	SD_REQTYPE	\N	1
10639	RT_ICONS_thunder.svg	image/svg+xml	SD_REQTYPE	\N	1
10700	359ad3fc-0c8d-4fc4-8991-aff7e8a94fb4.png	image/png	issuetype	10000	0
10701	494e4eeb-a0ba-4519-b6b8-e4ed4497e56c.png	image/png	issuetype	10001	0
10702	c299d4df-0dea-4c12-b0f3-662e96ceef8a.png	image/png	issuetype	10002	0
10703	65a0278b-f6f8-4f8d-8ad7-412061cc3d18.png	image/png	issuetype	10003	0
10704	f0bf227b-0f9d-4f08-a4d9-2c67fefe7e07.png	image/png	issuetype	10004	0
\.


--
-- Data for Name: board; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY board (id, jql) FROM stdin;
\.


--
-- Data for Name: boardproject; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY boardproject (board_id, project_id) FROM stdin;
\.


--
-- Data for Name: changegroup; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY changegroup (id, issueid, author, created) FROM stdin;
\.


--
-- Data for Name: changeitem; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY changeitem (id, groupid, fieldtype, field, oldvalue, oldstring, newvalue, newstring) FROM stdin;
\.


--
-- Data for Name: clusteredjob; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY clusteredjob (id, job_id, job_runner_key, sched_type, interval_millis, first_run, cron_expression, time_zone, next_run, version, parameters) FROM stdin;
10400	com.atlassian.scheduler.timedpromise.internal.TimedPromiseExecutor.job	com.atlassian.scheduler.timedpromise.internal.TimedPromiseExecutor	I	86400000	1560765600000	\N	\N	1560765600000	1	\N
10001	com.atlassian.jira.service.JiraService:10002	com.atlassian.jira.service.DefaultServiceManager	C	\N	\N	0 0 0 * * ?	\N	1560816000000	1	\\xaced000573720037636f6d2e676f6f676c652e636f6d6d6f6e2e636f6c6c6563742e496d6d757461626c6542694d61702453657269616c697a6564466f726d000000000000000002000078720035636f6d2e676f6f676c652e636f6d6d6f6e2e636f6c6c6563742e496d6d757461626c654d61702453657269616c697a6564466f726d00000000000000000200025b00046b6579737400135b4c6a6176612f6c616e672f4f626a6563743b5b000676616c75657371007e00027870757200135b4c6a6176612e6c616e672e4f626a6563743b90ce589f1073296c020000787000000001740033636f6d2e61746c61737369616e2e6a6972612e736572766963652e536572766963654d616e616765723a7365727669636549647571007e0004000000017372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b02000078700000000000002712
10105	com.atlassian.jira.service.JiraService:10001	com.atlassian.jira.service.DefaultServiceManager	C	\N	\N	0 5 3/12 * * ?	\N	1560783900000	1	\\xaced000573720037636f6d2e676f6f676c652e636f6d6d6f6e2e636f6c6c6563742e496d6d757461626c6542694d61702453657269616c697a6564466f726d000000000000000002000078720035636f6d2e676f6f676c652e636f6d6d6f6e2e636f6c6c6563742e496d6d757461626c654d61702453657269616c697a6564466f726d00000000000000000200025b00046b6579737400135b4c6a6176612f6c616e672f4f626a6563743b5b000676616c75657371007e00027870757200135b4c6a6176612e6c616e672e4f626a6563743b90ce589f1073296c020000787000000001740033636f6d2e61746c61737369616e2e6a6972612e736572766963652e536572766963654d616e616765723a7365727669636549647571007e0004000000017372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b02000078700000000000002711
10309	com.atlassian.jira.plugins.inform.batching.cron.BatchNotificationJobSchedulerImpl	com.atlassian.jira.plugins.inform.batching.cron.BatchNotificationJobSchedulerImpl	I	300000	1560742410784	\N	\N	1560742710797	2	\\xaced000573720035636f6d2e676f6f676c652e636f6d6d6f6e2e636f6c6c6563742e496d6d757461626c654d61702453657269616c697a6564466f726d00000000000000000200025b00046b6579737400135b4c6a6176612f6c616e672f4f626a6563743b5b000676616c75657371007e00017870757200135b4c6a6176612e6c616e672e4f626a6563743b90ce589f1073296c0200007870000000027400116576656e74436f6e73756d65724e616d6574000d636c75737465724c6f636b49647571007e0003000000027400116d61696c4576656e74436f6e73756d6572740059636f6d2e61746c61737369616e2e6a6972612e706c7567696e732e696e666f726d2e6261746368696e672e63726f6e2e4f6e6365506572436c75737465724a6f6252756e6e65722e6d61696c4576656e74436f6e73756d6572
10300	analytics-collection	com.atlassian.plugins.authentication.impl.analytics.StatisticsCollectionService	C	\N	\N	0 0 23 * * ?	\N	1560812400000	1	\N
10301	assertionId-cleanup	com.atlassian.plugins.authentication.impl.web.saml.AssertionValidationService	I	3600000	1560745646319	\N	\N	1560745646319	1	\N
10303	com.atlassian.jira.web.action.issue.DefaultTemporaryWebAttachmentsMonitor	com.atlassian.jira.web.action.issue.DefaultTemporaryWebAttachmentsMonitor	I	3600000	1560745705658	\N	\N	1560745705658	1	\N
10316	CompatibilityPluginScheduler.JobId.hipchatUpdateGlancesDataJob	CompatibilityPluginScheduler.JobRunnerKey.com.atlassian.jira.plugins.hipchat.service.connect.UpdateGlancesDataJobHandler	I	60000	1560742131915	\N	\N	1560742513006	7	\N
10306	com.atlassian.jira.internal.mail.services.MailProcessorJobRunner	com.atlassian.jira.internal.mail.services.MailProcessorJobRunner	I	60000	1560742170746	\N	\N	1560742530806	7	\N
10305	com.atlassian.jira.internal.mail.services.MailPullerJobRunner	com.atlassian.jira.internal.mail.services.MailPullerJobRunner	I	60000	1560742170746	\N	\N	1560742530807	7	\N
10310	com.atlassian.jira.plugins.inform.batching.cron.BatchNotificationJobSchedulerImpl.mentions	com.atlassian.jira.plugins.inform.batching.cron.BatchNotificationJobSchedulerImpl	I	60000	1560742170813	\N	\N	1560742532968	7	\\xaced000573720035636f6d2e676f6f676c652e636f6d6d6f6e2e636f6c6c6563742e496d6d757461626c654d61702453657269616c697a6564466f726d00000000000000000200025b00046b6579737400135b4c6a6176612f6c616e672f4f626a6563743b5b000676616c75657371007e00017870757200135b4c6a6176612e6c616e672e4f626a6563743b90ce589f1073296c0200007870000000027400116576656e74436f6e73756d65724e616d6574000d636c75737465724c6f636b49647571007e0003000000027400186d61696c4d656e74696f6e4576656e74436f6e73756d6572740060636f6d2e61746c61737369616e2e6a6972612e706c7567696e732e696e666f726d2e6261746368696e672e63726f6e2e4f6e6365506572436c75737465724a6f6252756e6e65722e6d61696c4d656e74696f6e4576656e74436f6e73756d6572
10308	com.atlassian.jira.analytics.scheduler.AnalyticsScheduler	com.atlassian.jira.analytics.scheduler.AnalyticsScheduler	I	604800000	1560828510775	\N	\N	1560828510775	1	\N
10314	com.atlassian.servicedesk.internal.sla.audit.SlaAuditLogCleanupJobScheduler	com.atlassian.servicedesk.internal.sla.audit.SlaAuditLogCleanupJobScheduler	C	\N	\N	0 0 3 * * ?	\N	1560826800000	1	\N
10315	sd.email.channel.initializer.ensure.enabled.key	sd.email.channel.initializer.ensure.enabled.key	I	1200000	1560743313480	\N	\N	1560743313480	1	\N
10319	com.atlassian.jira.plugins.inform.events.cleanup.CleanupJobScheduler	com.atlassian.jira.plugins.inform.events.cleanup.CleanupJobScheduler	C	\N	\N	0 0 0 1/1 * ? *	\N	1560816000000	1	\N
10321	com.atlassian.servicedesk.plugins.reports.internal.bootstrap.database.StatsEventDatabaseShredder	com.atlassian.servicedesk.plugins.reports.internal.bootstrap.database.StatsEventDatabaseShredder	I	86400000	1560828513835	\N	\N	1560828513835	1	\N
10311	com.atlassian.scheduler.timedpromise.internal.TimedPromisePruner	com.atlassian.scheduler.timedpromise.internal.TimedPromisePruner	I	86400000	\N	\N	\N	1560828519940	2	\N
10312	com.atlassian.scheduler.timedpromise.TimedPromiseHistoryPruner	com.atlassian.scheduler.timedpromise.TimedPromiseHistoryPruner	I	86400000	\N	\N	\N	1560828520019	2	\N
10320	applink-status-analytics-job	com.atlassian.applinks.analytics.ApplinkStatusJob	I	86400000	\N	\N	\N	1560828520052	2	\N
10317	CompatibilityPluginScheduler.JobId.hipchatRefreshConnectionStatusJob	CompatibilityPluginScheduler.JobRunnerKey.com.atlassian.jira.plugins.hipchat.service.ping.RefreshConnectionStatusJobHandler	I	3600000	1560742135270	\N	\N	1560745735270	2	\N
10318	CompatibilityPluginScheduler.JobId.hipchatInstallGlancesJob	CompatibilityPluginScheduler.JobRunnerKey.com.atlassian.jira.plugins.hipchat.service.connect.InstallGlancesJobHandler	I	3600000	1560742138647	\N	\N	1560745738657	2	\N
10307	com.atlassian.jira.internal.mail.services.MailCleanerJobRunner	com.atlassian.jira.internal.mail.services.MailCleanerJobRunner	I	86400000	1560742170746	\N	\N	1560828570748	2	\N
10225	LocalPluginLicenseNotificationJob-job	LocalPluginLicenseNotificationJob-runner	I	86400000	1560741093884	\N	\N	1560827493888	2	\N
10226	PluginRequestCheckJob-job	PluginRequestCheckJob-runner	I	3600000	1560741093892	\N	\N	1560744693896	2	\N
10227	PluginUpdateCheckJob-job	PluginUpdateCheckJob-runner	I	86400000	1560820404841	\N	\N	1560820404841	1	\N
10228	InstanceTopologyJob-job	InstanceTopologyJob-runner	I	86400000	1560811551859	\N	\N	1560811551859	1	\N
10302	com.atlassian.diagnostics.internal.analytics.DailyAlertAnalyticsJob	DailyAlertAnalyticsJob	C	\N	\N	0 19 * * * ?	\N	1560745140000	1	\N
10324	sd.custom.notification.batch.clean.up	sd.custom.notification.clean.up.job.runner	C	\N	\N	0 0 1 ? * TUE	\N	1560819600000	1	\N
10325	com.atlassian.psmq.internal.scheduled.QueueCleanupScheduledJob	com.atlassian.psmq.internal.scheduled.QueueCleanupScheduledJob	C	\N	\N	0 15 0 * * ?	\N	1560816900000	1	\N
10326	Service Provider Session Remover	com.atlassian.oauth.serviceprovider.internal.ExpiredSessionRemover	I	28800000	1560770915112	\N	\N	1560770915112	1	\N
10327	com.atlassian.whisper.plugin.fetch.FetchJob	com.atlassian.whisper.plugin.fetch.FetchJob	I	21600000	1560745715180	\N	\N	1560745715180	1	\N
10328	com.atlassian.servicedesk.plugins.automation.internal.execution.history.ExecutionHistoryCleaner.job	com.atlassian.servicedesk.plugins.automation.internal.execution.history.ExecutionHistoryCleaner	C	\N	\N	0 0 2 * * ?	\N	1560823200000	1	\N
10329	TruncateAlertsJobRunner	com.atlassian.diagnostics.internal.DefaultMonitoringService$TruncateAlertsJobRunner	I	86400000	1560828515936	\N	\N	1560828515936	1	\N
\.


--
-- Data for Name: clusterlockstatus; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY clusterlockstatus (id, lock_name, locked_by_node, update_time) FROM stdin;
\.


--
-- Data for Name: clustermessage; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY clustermessage (id, source_node, destination_node, claimed_by_node, message, message_time) FROM stdin;
\.


--
-- Data for Name: clusternode; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY clusternode (node_id, node_state, "timestamp", ip, cache_listener_port, node_build_number, node_version) FROM stdin;
\.


--
-- Data for Name: clusternodeheartbeat; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY clusternodeheartbeat (node_id, heartbeat_time, database_time) FROM stdin;
\.


--
-- Data for Name: clusterupgradestate; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY clusterupgradestate (id, database_time, cluster_build_number, cluster_version, state, order_number) FROM stdin;
\.


--
-- Data for Name: columnlayout; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY columnlayout (id, username, searchrequest) FROM stdin;
\.


--
-- Data for Name: columnlayoutitem; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY columnlayoutitem (id, columnlayout, fieldidentifier, horizontalposition) FROM stdin;
\.


--
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY component (id, project, cname, description, url, lead, assigneetype, archived) FROM stdin;
10000	10000	Jira	Issues related to Jira. Created by Jira Service Desk.	\N	admin	3	N
10001	10000	Intranet	Issues related to the intranet. Created by Jira Service Desk.	\N	admin	3	N
10002	10000	Public website	Issues related to the public website. Created by Jira Service Desk.	\N	admin	3	N
\.


--
-- Data for Name: configurationcontext; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY configurationcontext (id, projectcategory, project, customfield, fieldconfigscheme) FROM stdin;
10000	\N	\N	issuetype	10000
10100	\N	\N	priority	10100
10200	\N	\N	customfield_10000	10200
10201	\N	\N	customfield_10001	10201
10202	\N	\N	customfield_10002	10202
10300	\N	\N	customfield_10100	10300
10301	\N	\N	customfield_10101	10301
10302	\N	\N	customfield_10102	10302
10400	\N	10000	issuetype	10400
10401	\N	\N	customfield_10200	10401
10402	\N	10000	priority	10402
10403	\N	\N	customfield_10201	10403
10404	\N	\N	customfield_10202	10404
\.


--
-- Data for Name: customfield; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY customfield (id, cfkey, customfieldtypekey, customfieldsearcherkey, cfname, description, defaultvalue, fieldtype, project, issuetype) FROM stdin;
10000	\N	com.atlassian.servicedesk:sd-request-participants	com.atlassian.servicedesk:sd-request-participants-searcher	sd.request.participants.field.name	sd.request.participants.desc	\N	\N	\N	\N
10001	\N	com.atlassian.servicedesk:vp-origin	com.atlassian.servicedesk:vp-origin-searcher	sd.origin.customfield.default.name	sd.origin.customfield.desc	\N	\N	\N	\N
10002	\N	com.atlassian.servicedesk:sd-customer-organizations	com.atlassian.servicedesk:sd-customer-organizations-searcher	sd.customer.organisations.field.name	sd.customer.organisations.desc	\N	\N	\N	\N
10100	\N	com.atlassian.servicedesk.approvals-plugin:sd-approvals	com.atlassian.servicedesk.approvals-plugin:sd-approvals-searcher	Approvals	Provides search options for Jira Service Desk approvals information. This custom field is created programmatically and required by Service Desk.	\N	\N	\N	\N
10101	\N	com.atlassian.servicedesk:sd-request-feedback	com.atlassian.servicedesk:sd-request-feedback-searcher	Satisfaction	Stores request feedback in Service Desk requests. This custom field is created programmatically and required by Service Desk.	\N	\N	\N	\N
10102	\N	com.atlassian.servicedesk:sd-request-feedback-date	com.atlassian.servicedesk:sd-request-feedback-date-searcher	Satisfaction date	Stores request feedback date in Service Desk requests. This custom field is created programmatically and required by Service Desk.	\N	\N	\N	\N
10200	\N	com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker	\N	Approvers	Contains users needed for approval. This custom field was created by Jira Service Desk.	\N	\N	\N	\N
10201	\N	com.atlassian.servicedesk:sd-sla-field	com.atlassian.servicedesk:sd-sla-field-searcher	Time to resolution	This custom field was created by Jira Service Desk.	\N	\N	\N	\N
10202	\N	com.atlassian.servicedesk:sd-sla-field	com.atlassian.servicedesk:sd-sla-field-searcher	Time to first response	This custom field was created by Jira Service Desk.	\N	\N	\N	\N
\.


--
-- Data for Name: customfieldoption; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY customfieldoption (id, customfield, customfieldconfig, parentoptionid, sequence, customvalue, optiontype, disabled) FROM stdin;
\.


--
-- Data for Name: customfieldvalue; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY customfieldvalue (id, issue, customfield, updated, parentkey, stringvalue, numbervalue, textvalue, datevalue, valuetype) FROM stdin;
10000	10000	10001	1560742229366	\N	test/getithelp	\N	\N	\N	\N
10001	10000	10202	1560742231824	\N	\N	\N	{"timeline":{"events":[{"date":1560742228871,"types":["START"]}]},"ongoingSLAData":{"goalId":3,"startTime":1560742228871,"paused":false,"thresholdData":{"calculatedAt":1560742231175,"remainingTime":7200000,"thresholdsConfigChangeDate":1560742227647,"thresholdsConfigChangeMsEpoch":1560742227647}},"completeSLAData":[],"metricId":2,"definitionChangeDate":0,"definitionChangeMsEpoch":0,"goalsChangeDate":1560742227778,"goalsChangeMsEpoch":1560742227778,"goalTimeUpdatedDate":1560742227769,"goalTimeUpdatedMsEpoch":1560742227769,"metricCreatedDate":1560742227647,"updatedDate":1560742231695}	\N	\N
10002	10000	10201	1560742231859	\N	\N	\N	{"timeline":{"events":[{"date":1560742228871,"types":["START"]}]},"ongoingSLAData":{"goalId":1,"startTime":1560742228871,"paused":false,"thresholdData":{"calculatedAt":1560742231175,"remainingTime":14400000,"thresholdsConfigChangeDate":1560742227382,"thresholdsConfigChangeMsEpoch":1560742227382}},"completeSLAData":[],"metricId":1,"definitionChangeDate":0,"definitionChangeMsEpoch":0,"goalsChangeDate":1560742227592,"goalsChangeMsEpoch":1560742227592,"goalTimeUpdatedDate":1560742227556,"goalTimeUpdatedMsEpoch":1560742227556,"metricCreatedDate":1560742227382,"updatedDate":1560742231855}	\N	\N
\.


--
-- Data for Name: cwd_application; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) FROM stdin;
1	crowd-embedded	crowd-embedded	2013-02-28 11:57:51.302+00	2013-02-28 11:57:51.302+00	1		CROWD	X
\.


--
-- Data for Name: cwd_application_address; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_application_address (application_id, remote_address, encoded_address_binary, remote_address_mask) FROM stdin;
\.


--
-- Data for Name: cwd_directory; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_directory (id, directory_name, lower_directory_name, created_date, updated_date, active, description, impl_class, lower_impl_class, directory_type, directory_position) FROM stdin;
1	Jira Internal Directory	jira internal directory	2013-02-28 11:57:51.308+00	2013-02-28 11:57:51.308+00	1	Jira default internal directory	com.atlassian.crowd.directory.InternalDirectory	com.atlassian.crowd.directory.internaldirectory	INTERNAL	0
\.


--
-- Data for Name: cwd_directory_attribute; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_directory_attribute (directory_id, attribute_name, attribute_value) FROM stdin;
1	user_encryption_method	atlassian-security
\.


--
-- Data for Name: cwd_directory_operation; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_directory_operation (directory_id, operation_type) FROM stdin;
1	CREATE_GROUP
1	CREATE_ROLE
1	CREATE_USER
1	DELETE_GROUP
1	DELETE_ROLE
1	DELETE_USER
1	UPDATE_GROUP
1	UPDATE_GROUP_ATTRIBUTE
1	UPDATE_ROLE
1	UPDATE_ROLE_ATTRIBUTE
1	UPDATE_USER
1	UPDATE_USER_ATTRIBUTE
\.


--
-- Data for Name: cwd_group; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_group (id, group_name, lower_group_name, active, local, created_date, updated_date, description, lower_description, group_type, directory_id) FROM stdin;
10000	jira-administrators	jira-administrators	1	0	2013-02-28 11:57:51.326+00	2013-02-28 11:57:51.326+00		\N	GROUP	1
10010	jira-servicedesk-users	jira-servicedesk-users	1	0	2019-06-17 03:06:27.299+00	2019-06-17 03:06:27.299+00	\N	\N	GROUP	1
\.


--
-- Data for Name: cwd_group_attributes; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_group_attributes (id, group_id, directory_id, attribute_name, attribute_value, lower_attribute_value) FROM stdin;
\.


--
-- Data for Name: cwd_membership; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) FROM stdin;
10000	10000	10000	GROUP_USER	\N	jira-administrators	jira-administrators	admin	admin	1
10001	10010	10000	GROUP_USER	\N	jira-servicedesk-users	jira-servicedesk-users	admin	admin	1
\.


--
-- Data for Name: cwd_user; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_user (id, directory_id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, credential, deleted_externally, external_id) FROM stdin;
10000	1	admin	admin	1	2019-06-17 03:10:42.299+00	2019-06-17 03:10:42.299+00			Admin	admin	Admin	admin	justin@justingriffin.com	justin@justingriffin.com	{PKCS5S2}eGhd3IqdWGos8obvtzoBfQ7TSdQZWVoKlaAOpj7y4QFYjrEe6gXx1CZrqwhR2Yg1	\N	ea3427aa-bb08-42d2-81d4-06f81b291392
\.


--
-- Data for Name: cwd_user_attributes; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY cwd_user_attributes (id, user_id, directory_id, attribute_name, attribute_value, lower_attribute_value) FROM stdin;
10000	10000	1	requiresPasswordChange	false	false
10001	10000	1	invalidPasswordAttempts	0	0
10002	10000	1	passwordLastChanged	1560741043361	1560741043361
10003	10000	1	password.reset.request.expiry	1560827443945	1560827443945
10004	10000	1	password.reset.request.token	e63adbf3eecb00c065cf03166a5725dfc370e6e4	e63adbf3eecb00c065cf03166a5725dfc370e6e4
10100	10000	1	login.currentFailedCount	0	0
10200	10000	1	lastAuthenticated	1560742385663	1560742385663
10101	10000	1	login.lastLoginMillis	1560742385940	1560742385940
10201	10000	1	login.previousLoginMillis	1560742157853	1560742157853
10102	10000	1	login.count	3	3
\.


--
-- Data for Name: deadletter; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY deadletter (id, message_id, last_seen, mail_server_id, folder_name) FROM stdin;
\.


--
-- Data for Name: draftworkflowscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY draftworkflowscheme (id, name, description, workflow_scheme_id, last_modified_date, last_modified_user) FROM stdin;
\.


--
-- Data for Name: draftworkflowschemeentity; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY draftworkflowschemeentity (id, scheme, workflow, issuetype) FROM stdin;
\.


--
-- Data for Name: entity_property; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY entity_property (id, entity_name, entity_id, property_key, created, updated, json_value) FROM stdin;
10000	IssueProperty	10000	request.channel.type	2019-06-17 03:30:31.023+00	2019-06-17 03:30:31.023+00	{"value":"portal"}
10001	ProjectProperty	10000	searchRequests	2019-06-17 03:30:35.087+00	2019-06-17 03:30:35.087+00	{"ids":[]}
\.


--
-- Data for Name: entity_property_index_document; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY entity_property_index_document (id, plugin_key, module_key, entity_key, updated, document) FROM stdin;
10000	com.atlassian.servicedesk	sd.kb.viewed	IssueProperty	2019-06-17 03:05:27.295+00	<?xml version="1.0" encoding="UTF-8"?>\n<index-document-configuration entity-key="IssueProperty"><key property-key="sd_kb_viewed"><extract path="value" type="STRING"/></key></index-document-configuration>
10001	com.atlassian.servicedesk	request.public-activity.lastupdated.index	IssueProperty	2019-06-17 03:05:27.338+00	<?xml version="1.0" encoding="UTF-8"?>\n<index-document-configuration entity-key="IssueProperty"><key property-key="request.public-activity.lastupdated"><extract path="" type="DATE" alias="request-last-activity-time"/></key></index-document-configuration>
10002	com.atlassian.servicedesk	request.channel.type.index	IssueProperty	2019-06-17 03:05:27.342+00	<?xml version="1.0" encoding="UTF-8"?>\n<index-document-configuration entity-key="IssueProperty"><key property-key="request.channel.type"><extract path="value" type="STRING" alias="request-channel-type"/></key></index-document-configuration>
10003	com.atlassian.servicedesk.servicedesk-knowledge-base-plugin	sd.kb.shared.index	IssueProperty	2019-06-17 03:05:27.351+00	<?xml version="1.0" encoding="UTF-8"?>\n<index-document-configuration entity-key="IssueProperty"><key property-key="sd.kb.shared"><extract path="value" type="STRING" alias="sd-kb-shared"/></key></index-document-configuration>
\.


--
-- Data for Name: entity_translation; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY entity_translation (id, entity_name, entity_id, locale, trans_name, trans_desc) FROM stdin;
\.


--
-- Data for Name: external_entities; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY external_entities (id, name, entitytype) FROM stdin;
\.


--
-- Data for Name: externalgadget; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY externalgadget (id, gadget_xml) FROM stdin;
\.


--
-- Data for Name: favouriteassociations; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY favouriteassociations (id, username, entitytype, entityid, sequence) FROM stdin;
\.


--
-- Data for Name: feature; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY feature (id, feature_name, feature_type, user_key) FROM stdin;
10001	com.atlassian.jira.projects.issuenavigator	site	
10000	com.atlassian.jira.projects.ProjectCentricNavigation.Switch	site	
10100	com.atlassian.jira.agile.darkfeature.kanplan.enabled	site	
10101	com.atlassian.jira.agile.darkfeature.kanplan.epics.and.versions.enabled	site	
10102	com.atlassian.jira.agile.darkfeature.sprint.goal.enabled	site	
10103	com.atlassian.jira.agile.darkfeature.edit.closed.sprint.enabled	site	
10104	com.atlassian.jira.agile.darkfeature.splitissue	site	
10200	sd.sla.improved.rendering.enabled	site	\N
10201	sd.slavalue.record.updated.date.enabled	site	\N
10202	sd.canned.responses.enabled	site	\N
10203	sd.internal.base.off.thread.on.completion.events.enabled	site	\N
\.


--
-- Data for Name: fieldconfigscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldconfigscheme (id, configname, description, fieldid, customfield) FROM stdin;
10000	Default Issue Type Scheme	Default issue type scheme is the list of global issue types. All newly created issue types will automatically be added to this scheme.	issuetype	\N
10100	Default priority scheme	This is default priority scheme used by all projects without any other scheme assigned	priority	\N
10200	Default Configuration Scheme for sd.request.participants.field.name	Default configuration scheme generated by Jira	customfield_10000	\N
10201	Default Configuration Scheme for sd.origin.customfield.default.name	Default configuration scheme generated by Jira	customfield_10001	\N
10202	Default Configuration Scheme for sd.customer.organisations.field.name	Default configuration scheme generated by Jira	customfield_10002	\N
10300	Default Configuration Scheme for Approvals	Default configuration scheme generated by Jira	customfield_10100	\N
10301	Default Configuration Scheme for Satisfaction	Default configuration scheme generated by Jira	customfield_10101	\N
10302	Default Configuration Scheme for Satisfaction date	Default configuration scheme generated by Jira	customfield_10102	\N
10400	TEST: Jira Service Desk Issue Type Scheme	This Jira Service Desk Issue Type Scheme was generated automatically	issuetype	\N
10401	Default Configuration Scheme for Approvers	Default configuration scheme generated by Jira	customfield_10200	\N
10402	TEST - Jira Service Desk Priority Scheme	\N	priority	\N
10403	Default Configuration Scheme for Time to resolution	Default configuration scheme generated by Jira	customfield_10201	\N
10404	Default Configuration Scheme for Time to first response	Default configuration scheme generated by Jira	customfield_10202	\N
\.


--
-- Data for Name: fieldconfigschemeissuetype; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldconfigschemeissuetype (id, issuetype, fieldconfigscheme, fieldconfiguration) FROM stdin;
10100	\N	10000	10000
10200	\N	10100	10100
10300	\N	10200	10200
10301	\N	10201	10201
10302	\N	10202	10202
10400	\N	10300	10300
10401	\N	10301	10301
10402	\N	10302	10302
10501	\N	10400	10400
10502	\N	10401	10401
10504	\N	10402	10402
10505	\N	10403	10403
10506	\N	10404	10404
\.


--
-- Data for Name: fieldconfiguration; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldconfiguration (id, configname, description, fieldid, customfield) FROM stdin;
10000	Default Configuration for Issue Type	Default configuration generated by Jira	issuetype	\N
10100	Default configuration for priority	Default configuration generated by Jira	priority	\N
10200	Default Configuration for sd.request.participants.field.name	Default configuration generated by JIRA	customfield_10000	\N
10201	Default Configuration for sd.origin.customfield.default.name	Default configuration generated by JIRA	customfield_10001	\N
10202	Default Configuration for sd.customer.organisations.field.name	Default configuration generated by JIRA	customfield_10002	\N
10300	Default Configuration for Approvals	Default configuration generated by JIRA	customfield_10100	\N
10301	Default Configuration for Satisfaction	Default configuration generated by JIRA	customfield_10101	\N
10302	Default Configuration for Satisfaction date	Default configuration generated by JIRA	customfield_10102	\N
10400	Default Configuration for Issue Type	Default configuration generated by JIRA	issuetype	\N
10401	Default Configuration for Approvers	Default configuration generated by JIRA	customfield_10200	\N
10402	Default Configuration for Priority	Default configuration generated by JIRA	priority	\N
10403	Default Configuration for Time to resolution	Default configuration generated by JIRA	customfield_10201	\N
10404	Default Configuration for Time to first response	Default configuration generated by JIRA	customfield_10202	\N
\.


--
-- Data for Name: fieldlayout; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldlayout (id, name, description, layout_type, layoutscheme) FROM stdin;
10000	Default Field Configuration	The default field configuration	default	\N
10100	Jira Service Desk Field Configuration for Project TEST	This Jira Service Desk Field Configuration was generated for Project TEST	\N	\N
\.


--
-- Data for Name: fieldlayoutitem; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldlayoutitem (id, fieldlayout, fieldidentifier, description, verticalposition, ishidden, isrequired, renderertype) FROM stdin;
10100	10000	issuelinks	\N	\N	false	false	jira-text-renderer
10101	10000	worklog	Allows work to be logged whilst creating, editing or transitioning issues.	\N	false	false	atlassian-wiki-renderer
10102	10000	labels	\N	\N	false	false	jira-text-renderer
10103	10000	comment	\N	\N	false	false	atlassian-wiki-renderer
10104	10000	attachment	\N	\N	false	false	jira-text-renderer
10105	10000	resolution	\N	\N	false	false	jira-text-renderer
10106	10000	timetracking	An estimate of how much work remains until this issue will be resolved.<br>The format of this is ' *w *d *h *m ' (representing weeks, days, hours and minutes - where * can be any number)<br>Examples: 4d, 5h 30m, 60m and 3w.<br>	\N	false	false	jira-text-renderer
10107	10000	description	\N	\N	false	false	atlassian-wiki-renderer
10108	10000	environment	For example operating system, software platform and/or hardware specifications (include as appropriate for the issue).	\N	false	false	atlassian-wiki-renderer
10109	10000	reporter	\N	\N	false	true	jira-text-renderer
10110	10000	assignee	\N	\N	false	false	jira-text-renderer
10111	10000	fixVersions	\N	\N	false	false	frother-control-renderer
10112	10000	versions	\N	\N	false	false	frother-control-renderer
10113	10000	components	\N	\N	false	false	frother-control-renderer
10114	10000	duedate	\N	\N	false	false	jira-text-renderer
10115	10000	priority	\N	\N	false	false	jira-text-renderer
10116	10000	security	\N	\N	false	false	jira-text-renderer
10117	10000	issuetype	\N	\N	false	true	jira-text-renderer
10118	10000	summary	\N	\N	false	true	jira-text-renderer
10119	10000	customfield_10000	sd.request.participants.desc	\N	false	false	jira-text-renderer
10120	10000	customfield_10001	sd.origin.customfield.desc	\N	false	false	jira-text-renderer
10121	10000	customfield_10002	sd.customer.organisations.desc	\N	false	false	jira-text-renderer
10200	10100	issuelinks	\N	\N	false	false	jira-text-renderer
10201	10100	worklog	Allows work to be logged whilst creating, editing or transitioning issues.	\N	false	false	atlassian-wiki-renderer
10202	10100	labels	\N	\N	false	false	jira-text-renderer
10203	10100	comment	\N	\N	false	false	atlassian-wiki-renderer
10204	10100	attachment	\N	\N	false	false	jira-text-renderer
10205	10100	resolution	\N	\N	false	false	jira-text-renderer
10206	10100	timetracking	An estimate of how much work remains until this issue will be resolved.<br>The format of this is ' *w *d *h *m ' (representing weeks, days, hours and minutes - where * can be any number)<br>Examples: 4d, 5h 30m, 60m and 3w.<br>	\N	false	false	jira-text-renderer
10207	10100	description	\N	\N	false	false	atlassian-wiki-renderer
10208	10100	environment	For example operating system, software platform and/or hardware specifications (include as appropriate for the issue).	\N	false	false	atlassian-wiki-renderer
10209	10100	reporter	\N	\N	false	false	jira-text-renderer
10210	10100	assignee	\N	\N	false	false	jira-text-renderer
10211	10100	fixVersions	\N	\N	false	false	frother-control-renderer
10212	10100	versions	\N	\N	false	false	frother-control-renderer
10213	10100	components	\N	\N	false	false	frother-control-renderer
10214	10100	duedate	\N	\N	false	false	jira-text-renderer
10215	10100	priority	\N	\N	false	false	jira-text-renderer
10216	10100	security	\N	\N	false	false	jira-text-renderer
10217	10100	issuetype	\N	\N	false	false	jira-text-renderer
10218	10100	summary	\N	\N	false	true	jira-text-renderer
10219	10100	customfield_10000	sd.request.participants.desc	\N	false	false	jira-text-renderer
10220	10100	customfield_10001	sd.origin.customfield.desc	\N	false	false	jira-text-renderer
10221	10100	customfield_10002	sd.customer.organisations.desc	\N	false	false	jira-text-renderer
10222	10100	customfield_10100	Provides search options for Jira Service Desk approvals information. This custom field is created programmatically and required by Service Desk.	\N	false	false	jira-text-renderer
10223	10100	customfield_10200	Contains users needed for approval. This custom field was created by Jira Service Desk.	\N	false	false	jira-text-renderer
10224	10100	customfield_10101	Stores request feedback in Service Desk requests. This custom field is created programmatically and required by Service Desk.	\N	false	false	jira-text-renderer
10225	10100	customfield_10102	Stores request feedback date in Service Desk requests. This custom field is created programmatically and required by Service Desk.	\N	false	false	jira-text-renderer
\.


--
-- Data for Name: fieldlayoutscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldlayoutscheme (id, name, description) FROM stdin;
10000	Jira Service Desk Field Configuration Scheme for Project TEST	This Jira Service Desk Field Configuration Scheme was generated for Project TEST
\.


--
-- Data for Name: fieldlayoutschemeassociation; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldlayoutschemeassociation (id, issuetype, project, fieldlayoutscheme) FROM stdin;
\.


--
-- Data for Name: fieldlayoutschemeentity; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldlayoutschemeentity (id, scheme, issuetype, fieldlayout) FROM stdin;
10000	10000	\N	10100
\.


--
-- Data for Name: fieldscreen; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldscreen (id, name, description) FROM stdin;
1	Default Screen	Allows to update all system fields.
2	Workflow Screen	This screen is used in the workflow and enables you to assign issues
3	Resolve Issue Screen	Allows to set resolution, change fix versions and assign an issue.
10000	TEST: Jira Service Desk Screen	This Jira Service Desk Screen was generated automatically
10001	JIRA Service Desk Resolve Issue Screen	Allows to set resolution and linked issues
10002	Workflow Screen	This screen is used in the workflow and enables you to assign issues
10003	JIRA Service Desk Pending Reason screen	Screen to specify the pending reason when transitioning to the Pending status
10004	JIRA Service Desk Resolve Issue Screen	Allows to set resolution and linked issues
10005	JIRA Service Desk Pending Reason screen	Screen to specify the pending reason when transitioning to the Pending status
10006	Workflow Screen	This screen is used in the workflow and enables you to assign issues
10007	Workflow Screen	This screen is used in the workflow and enables you to assign issues
10008	JIRA Service Desk Pending Reason screen	Screen to specify the pending reason when transitioning to the Pending status
10009	JIRA Service Desk Resolve Issue Screen	Allows to set resolution and linked issues
\.


--
-- Data for Name: fieldscreenlayoutitem; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldscreenlayoutitem (id, fieldidentifier, sequence, fieldscreentab) FROM stdin;
10000	summary	0	10000
10001	issuetype	1	10000
10002	security	2	10000
10003	priority	3	10000
10004	duedate	4	10000
10005	components	5	10000
10006	versions	6	10000
10007	fixVersions	7	10000
10008	assignee	8	10000
10009	reporter	9	10000
10010	environment	10	10000
10011	description	11	10000
10012	timetracking	12	10000
10013	attachment	13	10000
10014	assignee	0	10001
10015	resolution	0	10002
10016	fixVersions	1	10002
10017	assignee	2	10002
10018	worklog	3	10002
10100	labels	14	10000
10200	summary	0	10100
10201	issuetype	1	10100
10202	reporter	2	10100
10203	components	3	10100
10204	attachment	4	10100
10205	duedate	5	10100
10206	description	6	10100
10207	issuelinks	7	10100
10208	assignee	8	10100
10209	priority	9	10100
10210	labels	10	10100
10211	resolution	0	10101
10212	issuelinks	1	10101
10213	assignee	0	10102
10214	issuelinks	0	10103
10215	resolution	0	10104
10216	issuelinks	1	10104
10217	issuelinks	0	10105
10218	assignee	0	10106
10219	assignee	0	10107
10220	issuelinks	0	10108
10221	resolution	0	10109
10222	issuelinks	1	10109
10223	customfield_10000	11	10100
10224	customfield_10200	12	10100
10225	customfield_10002	13	10100
\.


--
-- Data for Name: fieldscreenscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldscreenscheme (id, name, description) FROM stdin;
1	Default Screen Scheme	Default Screen Scheme
10000	TEST: Jira Service Desk Screen Scheme	This Jira Service Desk Screen Scheme was generated automatically
\.


--
-- Data for Name: fieldscreenschemeitem; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldscreenschemeitem (id, operation, fieldscreen, fieldscreenscheme) FROM stdin;
10000	\N	1	1
10100	\N	10000	10000
\.


--
-- Data for Name: fieldscreentab; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fieldscreentab (id, name, description, sequence, fieldscreen) FROM stdin;
10000	Field Tab	\N	0	1
10001	Field Tab	\N	0	2
10002	Field Tab	\N	0	3
10100	Default	\N	0	10000
10101	Field Tab	\N	0	10001
10102	Field Tab	\N	0	10002
10103	Field Tab	\N	0	10003
10104	Field Tab	\N	0	10004
10105	Field Tab	\N	0	10005
10106	Field Tab	\N	0	10006
10107	Field Tab	\N	0	10007
10108	Field Tab	\N	0	10008
10109	Field Tab	\N	0	10009
\.


--
-- Data for Name: fileattachment; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY fileattachment (id, issueid, mimetype, filename, created, filesize, author, zip, thumbnailable) FROM stdin;
\.


--
-- Data for Name: filtersubscription; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY filtersubscription (id, filter_i_d, username, groupname, last_run, email_on_empty) FROM stdin;
\.


--
-- Data for Name: gadgetuserpreference; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY gadgetuserpreference (id, portletconfiguration, userprefkey, userprefvalue) FROM stdin;
10000	10002	isConfigured	true
10001	10003	keys	__all_projects__
10002	10003	isConfigured	true
10003	10003	title	Your Company Jira
10004	10003	numofentries	5
\.


--
-- Data for Name: genericconfiguration; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY genericconfiguration (id, datatype, datakey, xmlvalue) FROM stdin;
10000	DefaultValue	10000	<string>1</string>
\.


--
-- Data for Name: globalpermissionentry; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY globalpermissionentry (id, permission, group_id) FROM stdin;
10000	ADMINISTER	jira-administrators
10006	SYSTEM_ADMIN	jira-administrators
10100	USER_PICKER	jira-servicedesk-users
10101	BULK_CHANGE	jira-servicedesk-users
10102	MANAGE_GROUP_FILTER_SUBSCRIPTIONS	jira-servicedesk-users
10103	CREATE_SHARED_OBJECTS	jira-servicedesk-users
10104	CREATE_SHARED_OBJECTS	jira-administrators
10105	MANAGE_GROUP_FILTER_SUBSCRIPTIONS	jira-administrators
10106	BULK_CHANGE	jira-administrators
10107	USER_PICKER	jira-administrators
\.


--
-- Data for Name: groupbase; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY groupbase (id, groupname) FROM stdin;
\.


--
-- Data for Name: issue_field_option; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issue_field_option (id, option_id, field_key, option_value, properties) FROM stdin;
\.


--
-- Data for Name: issue_field_option_scope; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issue_field_option_scope (id, option_id, entity_id, scope_type) FROM stdin;
\.


--
-- Data for Name: issuelink; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issuelink (id, linktype, source, destination, sequence) FROM stdin;
\.


--
-- Data for Name: issuelinktype; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issuelinktype (id, linkname, inward, outward, pstyle) FROM stdin;
10100	jira_subtask_link	jira_subtask_inward	jira_subtask_outward	jira_subtask
10000	Blocks	is blocked by	blocks	\N
10001	Cloners	is cloned by	clones	\N
10002	Duplicate	is duplicated by	duplicates	\N
10003	Relates	relates to	relates to	\N
10200	Problem/Incident	is caused by	causes	servicedesk_automation_default_linktype
\.


--
-- Data for Name: issuesecurityscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issuesecurityscheme (id, name, description, defaultlevel) FROM stdin;
\.


--
-- Data for Name: issuestatus; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issuestatus (id, sequence, pname, description, iconurl, statuscategory) FROM stdin;
1	1	Open	The issue is open and ready for the assignee to start work on it.	/images/icons/statuses/open.png	2
3	3	In Progress	This issue is being actively worked on at the moment by the assignee.	/images/icons/statuses/inprogress.png	4
4	4	Reopened	This issue was once resolved, but the resolution was deemed incorrect. From here issues are either marked assigned or resolved.	/images/icons/statuses/reopened.png	2
5	5	Resolved	A resolution has been taken, and it is awaiting verification by reporter. From here issues are either reopened, or are closed.	/images/icons/statuses/resolved.png	3
6	6	Closed	The issue is considered finished, the resolution is correct. Issues which are closed can be reopened.	/images/icons/statuses/closed.png	3
10000	7	Declined	This was auto-generated by Jira Service Desk during workflow import	/images/icons/statuses/generic.png	3
10001	8	Waiting for support	This was auto-generated by Jira Service Desk during workflow import	/images/icons/status_generic.gif	1
10002	9	Waiting for customer	This was auto-generated by Jira Service Desk during workflow import	/images/icons/status_generic.gif	1
10003	10	Pending	This was auto-generated by Jira Service Desk during workflow import	/images/icons/status_generic.gif	4
10004	11	Canceled	This was auto-generated by Jira Service Desk during workflow import	/images/icons/status_generic.gif	3
10005	12	Escalated	This was auto-generated by Jira Service Desk during workflow import	/images/icons/status_generic.gif	4
10006	13	Waiting for approval	This was auto-generated by Jira Service Desk during workflow import	/images/icons/status_generic.gif	2
10007	14	Work in progress	This was auto-generated by Jira Service Desk during workflow import	/images/icons/status_generic.gif	4
10008	15	Done	This was auto-generated by Jira Service Desk during workflow import	/images/icons/status_generic.gif	3
\.


--
-- Data for Name: issuetype; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issuetype (id, sequence, pname, pstyle, description, iconurl, avatar) FROM stdin;
10000	\N	IT Help		For general IT problems and questions. Created by Jira Service Desk.	\N	10700
10001	\N	Service Request		Created by Jira Service Desk.	\N	10701
10002	\N	Service Request with Approvals		For requests that require approval. Created by Jira Service Desk	\N	10702
10003	\N	Task		A task that needs to be done.	\N	10703
10004	\N	Sub-task	jira_subtask	The sub-task of the issue	\N	10704
\.


--
-- Data for Name: issuetypescreenscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issuetypescreenscheme (id, name, description) FROM stdin;
1	Default Issue Type Screen Scheme	The default issue type screen scheme
10000	TEST: Jira Service Desk Issue Type Screen Scheme	This Jira Service Desk Issue Type Screen Scheme was generated automatically
\.


--
-- Data for Name: issuetypescreenschemeentity; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY issuetypescreenschemeentity (id, issuetype, scheme, fieldscreenscheme) FROM stdin;
10000	\N	1	1
10100	\N	10000	10000
\.


--
-- Data for Name: jiraaction; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jiraaction (id, issueid, author, actiontype, actionlevel, rolelevel, actionbody, created, updateauthor, updated, actionnum) FROM stdin;
\.


--
-- Data for Name: jiradraftworkflows; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jiradraftworkflows (id, parentname, descriptor) FROM stdin;
\.


--
-- Data for Name: jiraeventtype; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jiraeventtype (id, template_id, name, description, event_type) FROM stdin;
1	\N	Issue Created	This is the 'issue created' event.	jira.system.event.type
2	\N	Issue Updated	This is the 'issue updated' event.	jira.system.event.type
3	\N	Issue Assigned	This is the 'issue assigned' event.	jira.system.event.type
4	\N	Issue Resolved	This is the 'issue resolved' event.	jira.system.event.type
5	\N	Issue Closed	This is the 'issue closed' event.	jira.system.event.type
6	\N	Issue Commented	This is the 'issue commented' event.	jira.system.event.type
7	\N	Issue Reopened	This is the 'issue reopened' event.	jira.system.event.type
8	\N	Issue Deleted	This is the 'issue deleted' event.	jira.system.event.type
9	\N	Issue Moved	This is the 'issue moved' event.	jira.system.event.type
10	\N	Work Logged On Issue	This is the 'work logged on issue' event.	jira.system.event.type
11	\N	Work Started On Issue	This is the 'work started on issue' event.	jira.system.event.type
12	\N	Work Stopped On Issue	This is the 'work stopped on issue' event.	jira.system.event.type
13	\N	Generic Event	This is the 'generic event' event.	jira.system.event.type
14	\N	Issue Comment Edited	This is the 'issue comment edited' event.	jira.system.event.type
15	\N	Issue Worklog Updated	This is the 'issue worklog updated' event.	jira.system.event.type
16	\N	Issue Worklog Deleted	This is the 'issue worklog deleted' event.	jira.system.event.type
17	\N	Issue Comment Deleted	This is the 'issue comment deleted' event.	jira.system.event.type
18	\N	Issue Archived	This is the 'issue archived' event	jira.system.event.type
19	\N	Issue Restored	This is the 'issue restored' event	jira.system.event.type
\.


--
-- Data for Name: jiraissue; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jiraissue (id, pkey, issuenum, project, reporter, assignee, creator, issuetype, summary, description, environment, priority, resolution, issuestatus, created, updated, duedate, resolutiondate, votes, watches, timeoriginalestimate, timeestimate, timespent, workflow_id, security, fixfor, component, archived, archivedby, archiveddate) FROM stdin;
10000	\N	1	10000	admin	\N	admin	10000	What am I looking at?	* You are now looking at an issue in one of your preset queues. This is where your agents work on your end users' requests.\n* On your left hand side are the queues where you can easily see all requests coming from your end users. \n* You can also see what this issue looks like in the Customer Portal by selecting the *View customer request* link to the right.\n\n----\nGot it? Now click *Resolve this issue* and add a comment to complete this request.\n----	\N	10000	\N	10001	2019-06-17 03:30:28.871+00	2019-06-17 03:30:28.87+00	\N	\N	0	0	\N	\N	\N	10000	\N	\N	\N	N	\N	\N
\.


--
-- Data for Name: jiraperms; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jiraperms (id, permtype, projectid, groupname) FROM stdin;
\.


--
-- Data for Name: jiraworkflows; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jiraworkflows (id, workflowname, creatorname, descriptor, islocked) FROM stdin;
10000	classic default workflow	\N	<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE workflow PUBLIC "-//OpenSymphony Group//DTD OSWorkflow 2.8//EN" "http://www.opensymphony.com/osworkflow/workflow_2_8.dtd">\n<workflow>\n  <meta name="jira.description">The classic Jira default workflow</meta>\n  <initial-actions>\n    <action id="1" name="Create Issue">\n      <meta name="opsbar-sequence">0</meta>\n      <meta name="jira.i18n.title">common.forms.create</meta>\n      <validators>\n        <validator name="" type="class">\n          <arg name="class.name">com.atlassian.jira.workflow.validator.PermissionValidator</arg>\n          <arg name="permission">Create Issue</arg>\n        </validator>\n      </validators>\n      <results>\n        <unconditional-result old-status="Finished" status="Open" step="1">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueCreateFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n              <arg name="eventTypeId">1</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n  </initial-actions>\n  <common-actions>\n    <action id="2" name="Close Issue" view="resolveissue">\n      <meta name="opsbar-sequence">60</meta>\n      <meta name="jira.i18n.submit">closeissue.close</meta>\n      <meta name="jira.i18n.description">closeissue.desc</meta>\n      <meta name="jira.i18n.title">closeissue.title</meta>\n      <restrict-to>\n        <conditions type="AND">\n          <condition type="class">\n            <arg name="class.name">com.atlassian.jira.workflow.condition.PermissionCondition</arg>\n            <arg name="permission">Resolve Issue</arg>\n          </condition>\n          <condition type="class">\n            <arg name="class.name">com.atlassian.jira.workflow.condition.PermissionCondition</arg>\n            <arg name="permission">Close Issue</arg>\n          </condition>\n        </conditions>\n      </restrict-to>\n      <results>\n        <unconditional-result old-status="Finished" status="Closed" step="6">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n              <arg name="eventTypeId">5</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="3" name="Reopen Issue" view="commentassign">\n      <meta name="opsbar-sequence">80</meta>\n      <meta name="jira.i18n.submit">issue.operations.reopen.issue</meta>\n      <meta name="jira.i18n.description">issue.operations.reopen.description</meta>\n      <meta name="jira.i18n.title">issue.operations.reopen.issue</meta>\n      <restrict-to>\n        <conditions>\n          <condition type="class">\n            <arg name="class.name">com.atlassian.jira.workflow.condition.PermissionCondition</arg>\n            <arg name="permission">Resolve Issue</arg>\n          </condition>\n        </conditions>\n      </restrict-to>\n      <results>\n        <unconditional-result old-status="Finished" status="Reopened" step="5">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction</arg>\n              <arg name="field.value"></arg>\n              <arg name="field.name">resolution</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n              <arg name="eventTypeId">7</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="4" name="Start Progress">\n      <meta name="opsbar-sequence">20</meta>\n      <meta name="jira.i18n.title">startprogress.title</meta>\n      <restrict-to>\n        <conditions>\n          <condition type="class">\n            <arg name="class.name">com.atlassian.jira.workflow.condition.AllowOnlyAssignee</arg>\n          </condition>\n        </conditions>\n      </restrict-to>\n      <results>\n        <unconditional-result old-status="Finished" status="Underway" step="3">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction</arg>\n              <arg name="field.value"></arg>\n              <arg name="field.name">resolution</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n              <arg name="eventTypeId">11</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="5" name="Resolve Issue" view="resolveissue">\n      <meta name="opsbar-sequence">40</meta>\n      <meta name="jira.i18n.submit">resolveissue.resolve</meta>\n      <meta name="jira.i18n.description">resolveissue.desc.line1</meta>\n      <meta name="jira.i18n.title">resolveissue.title</meta>\n      <restrict-to>\n        <conditions>\n          <condition type="class">\n            <arg name="class.name">com.atlassian.jira.workflow.condition.PermissionCondition</arg>\n            <arg name="permission">Resolve Issue</arg>\n          </condition>\n        </conditions>\n      </restrict-to>\n      <results>\n        <unconditional-result old-status="Finished" status="Resolved" step="4">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n              <arg name="eventTypeId">4</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n  </common-actions>\n  <steps>\n    <step id="1" name="Open">\n      <meta name="jira.status.id">1</meta>\n      <actions>\n<common-action id="4" />\n<common-action id="5" />\n<common-action id="2" />\n      </actions>\n    </step>\n    <step id="3" name="In Progress">\n      <meta name="jira.status.id">3</meta>\n      <actions>\n<common-action id="5" />\n<common-action id="2" />\n        <action id="301" name="Stop Progress">\n          <meta name="opsbar-sequence">20</meta>\n          <meta name="jira.i18n.title">stopprogress.title</meta>\n          <restrict-to>\n            <conditions>\n              <condition type="class">\n                <arg name="class.name">com.atlassian.jira.workflow.condition.AllowOnlyAssignee</arg>\n              </condition>\n            </conditions>\n          </restrict-to>\n          <results>\n            <unconditional-result old-status="Finished" status="Assigned" step="1">\n              <post-functions>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction</arg>\n                  <arg name="field.value"></arg>\n                  <arg name="field.name">resolution</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                  <arg name="eventTypeId">12</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="4" name="Resolved">\n      <meta name="jira.status.id">5</meta>\n      <actions>\n<common-action id="3" />\n        <action id="701" name="Close Issue" view="commentassign">\n          <meta name="opsbar-sequence">60</meta>\n          <meta name="jira.i18n.submit">closeissue.close</meta>\n          <meta name="jira.i18n.description">closeissue.desc</meta>\n          <meta name="jira.i18n.title">closeissue.title</meta>\n          <meta name="jira.description">Closing an issue indicates there is no more work to be done on it, and it has been verified as complete.</meta>\n          <restrict-to>\n            <conditions>\n              <condition type="class">\n                <arg name="class.name">com.atlassian.jira.workflow.condition.PermissionCondition</arg>\n                <arg name="permission">Close Issue</arg>\n              </condition>\n            </conditions>\n          </restrict-to>\n          <results>\n            <unconditional-result old-status="Finished" status="Closed" step="6">\n              <post-functions>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                  <arg name="eventTypeId">5</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="5" name="Reopened">\n      <meta name="jira.status.id">4</meta>\n      <actions>\n<common-action id="5" />\n<common-action id="2" />\n<common-action id="4" />\n      </actions>\n    </step>\n    <step id="6" name="Closed">\n      <meta name="jira.status.id">6</meta>\n      <meta name="jira.issue.editable">false</meta>\n      <actions>\n<common-action id="3" />\n      </actions>\n    </step>\n  </steps>\n</workflow>\n	\N
10100	TEST: Service Request Fulfilment workflow for Jira Service Desk	\N	<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE workflow PUBLIC "-//OpenSymphony Group//DTD OSWorkflow 2.8//EN" "http://www.opensymphony.com/osworkflow/workflow_2_8.dtd">\n<workflow>\n  <meta name="jira.description">This Jira Service Desk Service Request Fulfilment workflow was generated for Project TEST</meta>\n  <meta name="jira.i18n.description"></meta>\n  <meta name="jira.update.author.key">admin</meta>\n  <meta name="jira.updated.date">1560742223931</meta>\n  <meta name="sd.workflow.key">sdItSupport</meta>\n  <initial-actions>\n    <action id="1" name="Create issue">\n      <meta name="opsbar-sequence">0</meta>\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.i18n.title">common.forms.create</meta>\n      <meta name="jira.fieldscreen.id"></meta>\n      <validators>\n        <validator name="" type="class">\n          <arg name="permission">Create Issue</arg>\n          <arg name="class.name">com.atlassian.jira.workflow.validator.PermissionValidator</arg>\n        </validator>\n      </validators>\n      <results>\n        <unconditional-result old-status="Finished" status="Open" step="8">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueCreateFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowreindexissue-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">1</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n  </initial-actions>\n  <common-actions>\n    <action id="851" name="Respond to customer" view="fieldscreen">\n      <meta name="jira.i18n.submit">sd.workflow.itsupport.v2.transition.respond.to.customer.submit</meta>\n      <meta name="opsbar-sequence">5</meta>\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.i18n.title">sd.workflow.itsupport.v2.transition.respond.to.customer.title</meta>\n      <meta name="jira.fieldscreen.id">10002</meta>\n      <results>\n        <unconditional-result old-status="Not Done" status="Done" step="10">\n          <post-functions>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowcreatecomment-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowgeneratechangehistory-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowreindexissue-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowfireevent-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="901" name="Cancel request" view="fieldscreen">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="servicedesk.customer.transition.active">true</meta>\n      <meta name="jira.fieldscreen.id">10001</meta>\n      <meta name="servicedesk.customer.transition.resolution">10001</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="14">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="871" name="Pending" view="fieldscreen">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id">10003</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="11">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="761" name="Resolve this issue" view="fieldscreen">\n      <meta name="jira.i18n.submit">sd.workflow.itsupport.v2.transition.resolve.this.issue.submit</meta>\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description">sd.workflow.itsupport.v2.transition.resolve.this.issue.description</meta>\n      <meta name="jira.i18n.title">sd.workflow.itsupport.v2.transition.resolve.this.issue.title</meta>\n      <meta name="servicedesk.customer.transition.active">true</meta>\n      <meta name="jira.fieldscreen.id">10001</meta>\n      <meta name="sd.tour.resolve.step">true</meta>\n      <meta name="servicedesk.customer.transition.resolution">10000</meta>\n      <results>\n        <unconditional-result old-status="Not Done" status="Done" step="12">\n          <post-functions>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowcreatecomment-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowgeneratechangehistory-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowreindexissue-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowfireevent-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="891" name="In progress" view="fieldscreen">\n      <meta name="opsbar-sequence">10</meta>\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id">10002</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="13">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="941" name="Close">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id"></meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="16">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="911" name="Escalate this issue" view="fieldscreen">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="servicedesk.customer.transition.active">false</meta>\n      <meta name="jira.fieldscreen.id">10002</meta>\n      <meta name="servicedesk.customer.transition.resolution"></meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="15">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n  </common-actions>\n  <steps>\n    <step id="8" name="Waiting for support">\n      <meta name="jira.status.id">10001</meta>\n      <meta name="sd.step.key">sdWFSupport</meta>\n      <actions>\n<common-action id="851" />\n<common-action id="761" />\n<common-action id="891" />\n<common-action id="901" />\n<common-action id="871" />\n        <action id="921" name="Escalate" view="fieldscreen">\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description"></meta>\n          <meta name="servicedesk.customer.transition.active">true</meta>\n          <meta name="jira.fieldscreen.id">10002</meta>\n          <meta name="servicedesk.customer.transition.resolution"></meta>\n          <results>\n            <unconditional-result old-status="null" status="null" step="15">\n              <post-functions>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="10" name="Waiting for customer">\n      <meta name="jira.status.id">10002</meta>\n      <meta name="sd.step.key">sdWFCustomer</meta>\n      <actions>\n<common-action id="761" />\n<common-action id="901" />\n<common-action id="911" />\n        <action id="781" name="Respond to support">\n          <meta name="jira.i18n.submit">sd.workflow.itsupport.v2.transition.respond.to.support.submit</meta>\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description">sd.workflow.itsupport.v2.transition.respond.to.support.description</meta>\n          <meta name="jira.i18n.title">sd.workflow.itsupport.v2.transition.respond.to.support.title</meta>\n          <meta name="jira.fieldscreen.id"></meta>\n          <results>\n            <unconditional-result old-status="Not Done" status="Done" step="8">\n              <post-functions>\n                <function type="class">\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowcreatecomment-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowgeneratechangehistory-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowreindexissue-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowfireevent-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="11" name="Pending">\n      <meta name="jira.status.id">10003</meta>\n      <actions>\n<common-action id="851" />\n<common-action id="891" />\n<common-action id="761" />\n<common-action id="901" />\n      </actions>\n    </step>\n    <step id="12" name="Resolved">\n      <meta name="jira.status.id">5</meta>\n      <actions>\n<common-action id="941" />\n        <action id="931" name="Back to in progress" view="fieldscreen">\n          <meta name="sd.resolution.clear"></meta>\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description"></meta>\n          <meta name="jira.fieldscreen.id">10002</meta>\n          <results>\n            <unconditional-result old-status="null" status="null" step="13">\n              <post-functions>\n                <function type="class">\n                  <arg name="field.name">resolution</arg>\n                  <arg name="field.value"></arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="13" name="In progress">\n      <meta name="jira.status.id">3</meta>\n      <actions>\n<common-action id="761" />\n<common-action id="901" />\n<common-action id="871" />\n      </actions>\n    </step>\n    <step id="14" name="Canceled">\n      <meta name="jira.status.id">10004</meta>\n      <actions>\n<common-action id="941" />\n      </actions>\n    </step>\n    <step id="15" name="Escalated">\n      <meta name="jira.status.id">10005</meta>\n      <actions>\n<common-action id="891" />\n      </actions>\n    </step>\n    <step id="16" name="Closed">\n      <meta name="jira.status.id">6</meta>\n    </step>\n  </steps>\n</workflow>\n	\N
10101	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk	\N	<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE workflow PUBLIC "-//OpenSymphony Group//DTD OSWorkflow 2.8//EN" "http://www.opensymphony.com/osworkflow/workflow_2_8.dtd">\n<workflow>\n  <meta name="jira.description">This Jira Service Desk Service Request Fulfilment with Approvals workflow was generated for Project TEST</meta>\n  <meta name="jira.i18n.description"></meta>\n  <meta name="jira.update.author.key">admin</meta>\n  <meta name="jira.updated.date">1560742224427</meta>\n  <meta name="sd.workflow.key">sdItSupport</meta>\n  <initial-actions>\n    <action id="1" name="Create issue">\n      <meta name="opsbar-sequence">0</meta>\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.i18n.title">common.forms.create</meta>\n      <meta name="jira.fieldscreen.id"></meta>\n      <validators>\n        <validator name="" type="class">\n          <arg name="permission">Create Issue</arg>\n          <arg name="class.name">com.atlassian.jira.workflow.validator.PermissionValidator</arg>\n        </validator>\n      </validators>\n      <results>\n        <unconditional-result old-status="Finished" status="Open" step="14">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueCreateFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowreindexissue-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">1</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n  </initial-actions>\n  <common-actions>\n    <action id="961" name="Close">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id"></meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="17">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="851" name="Respond to customer">\n      <meta name="jira.i18n.submit">sd.workflow.itsupport.v2.transition.respond.to.customer.submit</meta>\n      <meta name="opsbar-sequence">5</meta>\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.i18n.title">sd.workflow.itsupport.v2.transition.respond.to.customer.title</meta>\n      <meta name="jira.fieldscreen.id"></meta>\n      <results>\n        <unconditional-result old-status="Not Done" status="Done" step="10">\n          <post-functions>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowcreatecomment-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowgeneratechangehistory-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowreindexissue-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowfireevent-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="871" name="Pending" view="fieldscreen">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id">10005</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="11">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="761" name="Resolve this issue" view="fieldscreen">\n      <meta name="jira.i18n.submit">sd.workflow.itsupport.v2.transition.resolve.this.issue.submit</meta>\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description">sd.workflow.itsupport.v2.transition.resolve.this.issue.description</meta>\n      <meta name="jira.i18n.title">sd.workflow.itsupport.v2.transition.resolve.this.issue.title</meta>\n      <meta name="jira.fieldscreen.id">10004</meta>\n      <meta name="sd.tour.resolve.step">true</meta>\n      <results>\n        <unconditional-result old-status="Not Done" status="Done" step="12">\n          <post-functions>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowcreatecomment-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowgeneratechangehistory-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowreindexissue-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowfireevent-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="921" name="Cancel request" view="fieldscreen">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="servicedesk.customer.transition.active">true</meta>\n      <meta name="jira.fieldscreen.id">10004</meta>\n      <meta name="servicedesk.customer.transition.resolution">10001</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="15">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="891" name="In progress" view="fieldscreen">\n      <meta name="opsbar-sequence">10</meta>\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id">10006</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="13">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n  </common-actions>\n  <steps>\n    <step id="8" name="Waiting for support">\n      <meta name="jira.status.id">10001</meta>\n      <meta name="sd.step.key">sdWFSupport</meta>\n      <actions>\n<common-action id="851" />\n<common-action id="761" />\n<common-action id="891" />\n<common-action id="921" />\n<common-action id="871" />\n        <action id="931" name="Escalate" view="fieldscreen">\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description"></meta>\n          <meta name="servicedesk.customer.transition.active">true</meta>\n          <meta name="jira.fieldscreen.id">10006</meta>\n          <meta name="servicedesk.customer.transition.resolution"></meta>\n          <results>\n            <unconditional-result old-status="null" status="null" step="16">\n              <post-functions>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="10" name="Waiting for customer">\n      <meta name="jira.status.id">10002</meta>\n      <meta name="sd.step.key">sdWFCustomer</meta>\n      <actions>\n<common-action id="761" />\n<common-action id="921" />\n        <action id="781" name="Respond to support">\n          <meta name="jira.i18n.submit">sd.workflow.itsupport.v2.transition.respond.to.support.submit</meta>\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description">sd.workflow.itsupport.v2.transition.respond.to.support.description</meta>\n          <meta name="jira.i18n.title">sd.workflow.itsupport.v2.transition.respond.to.support.title</meta>\n          <meta name="jira.fieldscreen.id"></meta>\n          <results>\n            <unconditional-result old-status="Not Done" status="Done" step="8">\n              <post-functions>\n                <function type="class">\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowcreatecomment-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowgeneratechangehistory-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowreindexissue-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowfireevent-function</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n        <action id="941" name="Escalate this issue" view="fieldscreen">\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description"></meta>\n          <meta name="jira.fieldscreen.id">10006</meta>\n          <results>\n            <unconditional-result old-status="null" status="null" step="16">\n              <post-functions>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="11" name="Pending">\n      <meta name="jira.status.id">10003</meta>\n      <actions>\n<common-action id="851" />\n<common-action id="891" />\n<common-action id="761" />\n<common-action id="921" />\n      </actions>\n    </step>\n    <step id="12" name="Resolved">\n      <meta name="jira.status.id">5</meta>\n      <actions>\n<common-action id="961" />\n        <action id="951" name="Back to in progress" view="fieldscreen">\n          <meta name="sd.resolution.clear"></meta>\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description"></meta>\n          <meta name="jira.fieldscreen.id">10006</meta>\n          <results>\n            <unconditional-result old-status="null" status="null" step="13">\n              <post-functions>\n                <function type="class">\n                  <arg name="field.name">resolution</arg>\n                  <arg name="field.value"></arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="13" name="In progress">\n      <meta name="jira.status.id">3</meta>\n      <actions>\n<common-action id="761" />\n<common-action id="921" />\n<common-action id="871" />\n      </actions>\n    </step>\n    <step id="14" name="Waiting for approval">\n      <meta name="approval.transition.approved">901</meta>\n      <meta name="jira.status.id">10006</meta>\n      <meta name="approval.transition.rejected">911</meta>\n      <meta name="approval.condition.type">number</meta>\n      <meta name="approval.condition.value">1</meta>\n      <meta name="approval.active">true</meta>\n      <meta name="approval.field.id">customfield_10200</meta>\n      <actions>\n<common-action id="921" />\n        <action id="901" name="Approved">\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description"></meta>\n          <meta name="jira.fieldscreen.id"></meta>\n          <restrict-to>\n            <conditions>\n              <condition type="class">\n                <arg name="class.name">com.atlassian.servicedesk.plugins.approvals.internal.workflow.BlockInProgressApprovalCondition</arg>\n              </condition>\n            </conditions>\n          </restrict-to>\n          <results>\n            <unconditional-result old-status="null" status="null" step="8">\n              <post-functions>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n        <action id="911" name="Declined" view="fieldscreen">\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description"></meta>\n          <meta name="jira.fieldscreen.id">10004</meta>\n          <restrict-to>\n            <conditions>\n              <condition type="class">\n                <arg name="class.name">com.atlassian.servicedesk.plugins.approvals.internal.workflow.BlockInProgressApprovalCondition</arg>\n              </condition>\n            </conditions>\n          </restrict-to>\n          <results>\n            <unconditional-result old-status="null" status="null" step="12">\n              <post-functions>\n                <function type="class">\n                  <arg name="field.name">resolution</arg>\n                  <arg name="field.value">10003</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n    <step id="15" name="Canceled">\n      <meta name="jira.status.id">10004</meta>\n      <actions>\n<common-action id="961" />\n      </actions>\n    </step>\n    <step id="16" name="Escalated">\n      <meta name="jira.status.id">10005</meta>\n      <actions>\n<common-action id="891" />\n      </actions>\n    </step>\n    <step id="17" name="Closed">\n      <meta name="jira.status.id">6</meta>\n    </step>\n  </steps>\n</workflow>\n	\N
10102	TEST: Jira Service Desk default workflow	\N	<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE workflow PUBLIC "-//OpenSymphony Group//DTD OSWorkflow 2.8//EN" "http://www.opensymphony.com/osworkflow/workflow_2_8.dtd">\n<workflow>\n  <meta name="jira.description">This Jira Service Desk default workflow was generated for Project TEST</meta>\n  <meta name="jira.i18n.description"></meta>\n  <meta name="jira.update.author.key">admin</meta>\n  <meta name="jira.updated.date">1560742224799</meta>\n  <initial-actions>\n    <action id="1" name="Create">\n      <validators>\n        <validator name="" type="class">\n          <arg name="permission">Create Issue</arg>\n          <arg name="class.name">com.atlassian.jira.workflow.validator.PermissionValidator</arg>\n        </validator>\n      </validators>\n      <results>\n        <unconditional-result old-status="null" status="open" step="1">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueCreateFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="full.module.key">com.atlassian.jira.plugin.system.workflowupdateissuestatus-function</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">1</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n  </initial-actions>\n  <common-actions>\n    <action id="51" name="Back to open">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id"></meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="1">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="41" name="Pending" view="fieldscreen">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id">10008</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="2">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="11" name="Start progress" view="fieldscreen">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id">10007</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="3">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n    <action id="61" name="Mark as Done" view="fieldscreen">\n      <meta name="jira.description"></meta>\n      <meta name="jira.i18n.description"></meta>\n      <meta name="jira.fieldscreen.id">10009</meta>\n      <results>\n        <unconditional-result old-status="null" status="null" step="7">\n          <post-functions>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n            </function>\n            <function type="class">\n              <arg name="eventTypeId">13</arg>\n              <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n            </function>\n          </post-functions>\n        </unconditional-result>\n      </results>\n    </action>\n  </common-actions>\n  <steps>\n    <step id="1" name="Open">\n      <meta name="jira.status.id">1</meta>\n      <actions>\n<common-action id="41" />\n<common-action id="11" />\n<common-action id="61" />\n      </actions>\n    </step>\n    <step id="2" name="Pending">\n      <meta name="jira.status.id">10003</meta>\n      <actions>\n<common-action id="51" />\n<common-action id="11" />\n      </actions>\n    </step>\n    <step id="3" name="Work in progress">\n      <meta name="jira.status.id">10007</meta>\n      <actions>\n<common-action id="41" />\n<common-action id="51" />\n<common-action id="61" />\n      </actions>\n    </step>\n    <step id="6" name="Reopened">\n      <meta name="jira.status.id">4</meta>\n      <actions>\n<common-action id="11" />\n<common-action id="41" />\n<common-action id="61" />\n      </actions>\n    </step>\n    <step id="7" name="Done">\n      <meta name="jira.status.id">10008</meta>\n      <actions>\n        <action id="71" name="Reopen issue">\n          <meta name="sd.resolution.clear"></meta>\n          <meta name="jira.description"></meta>\n          <meta name="jira.i18n.description"></meta>\n          <meta name="jira.fieldscreen.id"></meta>\n          <results>\n            <unconditional-result old-status="null" status="null" step="6">\n              <post-functions>\n                <function type="class">\n                  <arg name="field.name">resolution</arg>\n                  <arg name="field.value"></arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.misc.CreateCommentFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="class.name">com.atlassian.jira.workflow.function.issue.IssueReindexFunction</arg>\n                </function>\n                <function type="class">\n                  <arg name="eventTypeId">13</arg>\n                  <arg name="class.name">com.atlassian.jira.workflow.function.event.FireIssueEventFunction</arg>\n                </function>\n              </post-functions>\n            </unconditional-result>\n          </results>\n        </action>\n      </actions>\n    </step>\n  </steps>\n</workflow>\n	\N
\.


--
-- Data for Name: jiraworkflowstatuses; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jiraworkflowstatuses (id, status, parentname) FROM stdin;
\.


--
-- Data for Name: jquartz_blob_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_blob_triggers (sched_name, trigger_name, trigger_group, blob_data) FROM stdin;
\.


--
-- Data for Name: jquartz_calendars; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_calendars (sched_name, calendar_name, calendar) FROM stdin;
\.


--
-- Data for Name: jquartz_cron_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_cron_triggers (sched_name, trigger_name, trigger_group, cron_expression, time_zone_id) FROM stdin;
\.


--
-- Data for Name: jquartz_fired_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_fired_triggers (sched_name, entry_id, trigger_name, trigger_group, is_volatile, instance_name, fired_time, sched_time, priority, state, job_name, job_group, is_stateful, is_nonconcurrent, is_update_data, requests_recovery) FROM stdin;
\.


--
-- Data for Name: jquartz_job_details; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_job_details (sched_name, job_name, job_group, description, job_class_name, is_durable, is_volatile, is_stateful, is_nonconcurrent, is_update_data, requests_recovery, job_data) FROM stdin;
\.


--
-- Data for Name: jquartz_job_listeners; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_job_listeners (job_name, job_group, job_listener) FROM stdin;
\.


--
-- Data for Name: jquartz_locks; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_locks (sched_name, lock_name) FROM stdin;
\N	TRIGGER_ACCESS
\N	JOB_ACCESS
\N	CALENDAR_ACCESS
\N	STATE_ACCESS
\N	MISFIRE_ACCESS
\.


--
-- Data for Name: jquartz_paused_trigger_grps; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_paused_trigger_grps (sched_name, trigger_group) FROM stdin;
\.


--
-- Data for Name: jquartz_scheduler_state; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_scheduler_state (sched_name, instance_name, last_checkin_time, checkin_interval) FROM stdin;
\.


--
-- Data for Name: jquartz_simple_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_simple_triggers (sched_name, trigger_name, trigger_group, repeat_count, repeat_interval, times_triggered) FROM stdin;
\.


--
-- Data for Name: jquartz_simprop_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_simprop_triggers (sched_name, trigger_name, trigger_group, str_prop_1, str_prop_2, str_prop_3, int_prop_1, int_prop_2, long_prop_1, long_prop_2, dec_prop_1, dec_prop_2, bool_prop_1, bool_prop_2) FROM stdin;
\.


--
-- Data for Name: jquartz_trigger_listeners; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_trigger_listeners (trigger_name, trigger_group, trigger_listener) FROM stdin;
\.


--
-- Data for Name: jquartz_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY jquartz_triggers (sched_name, trigger_name, trigger_group, job_name, job_group, is_volatile, description, next_fire_time, prev_fire_time, priority, trigger_state, trigger_type, start_time, end_time, calendar_name, misfire_instr, job_data) FROM stdin;
\.


--
-- Data for Name: label; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY label (id, fieldid, issue, label) FROM stdin;
\.


--
-- Data for Name: licenserolesdefault; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY licenserolesdefault (id, license_role_name) FROM stdin;
10000	jira-servicedesk
\.


--
-- Data for Name: licenserolesgroup; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY licenserolesgroup (id, license_role_name, group_id, primary_group) FROM stdin;
10000	jira-servicedesk	jira-servicedesk-users	Y
10001	jira-servicedesk	jira-administrators	N
\.


--
-- Data for Name: listenerconfig; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY listenerconfig (id, clazz, listenername) FROM stdin;
10000	com.atlassian.jira.event.listeners.mail.MailListener	Mail Listener
10001	com.atlassian.jira.event.listeners.history.IssueAssignHistoryListener	Issue Assignment Listener
10200	com.atlassian.jira.event.listeners.search.IssueIndexListener	Issue Index Listener
\.


--
-- Data for Name: mailserver; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY mailserver (id, name, description, mailfrom, prefix, smtp_port, protocol, server_type, servername, jndilocation, mailusername, mailpassword, istlsrequired, timeout, socks_port, socks_host) FROM stdin;
10000	Default SMTP Server	\N	justin@justingriffin.com	[JIRA]	25	smtp	smtp	localhost	\N	admin	supersecret	false	10000	\N	\N
\.


--
-- Data for Name: managedconfigurationitem; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY managedconfigurationitem (id, item_id, item_type, managed, access_level, source, description_key) FROM stdin;
10000	customfield_10000	CUSTOM_FIELD	true	LOCKED	com.atlassian.servicedesk:field-locking-service	admin.managed.configuration.items.generic.description.locked
10001	customfield_10001	CUSTOM_FIELD	true	LOCKED	com.atlassian.servicedesk:field-locking-service	sd.origin.customfield.locked.desc
10002	customfield_10002	CUSTOM_FIELD	true	LOCKED	com.atlassian.servicedesk:field-locking-service	admin.managed.configuration.items.generic.description.locked
10100	customfield_10100	CUSTOM_FIELD	true	LOCKED	com.atlassian.servicedesk.approvals-plugin:field-locking-service	admin.managed.configuration.items.generic.description.locked
10101	customfield_10101	CUSTOM_FIELD	true	LOCKED	com.atlassian.servicedesk:field-locking-service	admin.managed.configuration.items.generic.description.locked
10102	customfield_10102	CUSTOM_FIELD	true	LOCKED	com.atlassian.servicedesk:field-locking-service	admin.managed.configuration.items.generic.description.locked
10200	customfield_10201	CUSTOM_FIELD	true	LOCKED	com.atlassian.servicedesk:field-locking-service	sd.customfield.type.sla.locked.desc
10201	customfield_10202	CUSTOM_FIELD	true	LOCKED	com.atlassian.servicedesk:field-locking-service	sd.customfield.type.sla.locked.desc
\.


--
-- Data for Name: membershipbase; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY membershipbase (id, user_name, group_name) FROM stdin;
\.


--
-- Data for Name: moved_issue_key; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY moved_issue_key (id, old_issue_key, issue_id) FROM stdin;
\.


--
-- Data for Name: nodeassociation; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY nodeassociation (source_node_id, source_node_entity, sink_node_id, sink_node_entity, association_type, sequence) FROM stdin;
10000	Project	10000	NotificationScheme	ProjectScheme	\N
10000	Project	10000	IssueTypeScreenScheme	ProjectScheme	\N
10000	Project	10000	PermissionScheme	ProjectScheme	\N
10000	Project	10100	WorkflowScheme	ProjectScheme	\N
10000	Project	10000	FieldLayoutScheme	ProjectScheme	\N
\.


--
-- Data for Name: nodeindexcounter; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY nodeindexcounter (id, node_id, sending_node_id, index_operation_id) FROM stdin;
\.


--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY notification (id, scheme, event, event_type_id, template_id, notif_type, notif_parameter) FROM stdin;
10000	10000	\N	1	\N	Current_Assignee	\N
10001	10000	\N	1	\N	Current_Reporter	\N
10002	10000	\N	1	\N	All_Watchers	\N
10003	10000	\N	2	\N	Current_Assignee	\N
10004	10000	\N	2	\N	Current_Reporter	\N
10005	10000	\N	2	\N	All_Watchers	\N
10006	10000	\N	3	\N	Current_Assignee	\N
10007	10000	\N	3	\N	Current_Reporter	\N
10008	10000	\N	3	\N	All_Watchers	\N
10009	10000	\N	4	\N	Current_Assignee	\N
10010	10000	\N	4	\N	Current_Reporter	\N
10011	10000	\N	4	\N	All_Watchers	\N
10012	10000	\N	5	\N	Current_Assignee	\N
10013	10000	\N	5	\N	Current_Reporter	\N
10014	10000	\N	5	\N	All_Watchers	\N
10015	10000	\N	6	\N	Current_Assignee	\N
10016	10000	\N	6	\N	Current_Reporter	\N
10017	10000	\N	6	\N	All_Watchers	\N
10018	10000	\N	7	\N	Current_Assignee	\N
10019	10000	\N	7	\N	Current_Reporter	\N
10020	10000	\N	7	\N	All_Watchers	\N
10021	10000	\N	8	\N	Current_Assignee	\N
10022	10000	\N	8	\N	Current_Reporter	\N
10023	10000	\N	8	\N	All_Watchers	\N
10024	10000	\N	9	\N	Current_Assignee	\N
10025	10000	\N	9	\N	Current_Reporter	\N
10026	10000	\N	9	\N	All_Watchers	\N
10027	10000	\N	10	\N	Current_Assignee	\N
10028	10000	\N	10	\N	Current_Reporter	\N
10029	10000	\N	10	\N	All_Watchers	\N
10030	10000	\N	11	\N	Current_Assignee	\N
10031	10000	\N	11	\N	Current_Reporter	\N
10032	10000	\N	11	\N	All_Watchers	\N
10033	10000	\N	12	\N	Current_Assignee	\N
10034	10000	\N	12	\N	Current_Reporter	\N
10035	10000	\N	12	\N	All_Watchers	\N
10036	10000	\N	13	\N	Current_Assignee	\N
10037	10000	\N	13	\N	Current_Reporter	\N
10038	10000	\N	13	\N	All_Watchers	\N
10100	10000	\N	14	\N	Current_Assignee	\N
10101	10000	\N	14	\N	Current_Reporter	\N
10102	10000	\N	14	\N	All_Watchers	\N
10103	10000	\N	15	\N	Current_Assignee	\N
10104	10000	\N	15	\N	Current_Reporter	\N
10105	10000	\N	15	\N	All_Watchers	\N
10106	10000	\N	16	\N	Current_Assignee	\N
10107	10000	\N	16	\N	Current_Reporter	\N
10108	10000	\N	16	\N	All_Watchers	\N
10200	10000	\N	18	\N	Current_Assignee	\N
10201	10000	\N	18	\N	Current_Reporter	\N
10202	10000	\N	18	\N	All_Watchers	\N
10203	10000	\N	19	\N	Current_Assignee	\N
10204	10000	\N	19	\N	Current_Reporter	\N
10205	10000	\N	19	\N	All_Watchers	\N
\.


--
-- Data for Name: notificationinstance; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY notificationinstance (id, notificationtype, source, emailaddress, messageid) FROM stdin;
\.


--
-- Data for Name: notificationscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY notificationscheme (id, name, description) FROM stdin;
10000	Default Notification Scheme	\N
\.


--
-- Data for Name: oauthconsumer; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY oauthconsumer (id, created, consumername, consumer_key, consumerservice, public_key, private_key, description, callback, signature_method, shared_secret) FROM stdin;
\.


--
-- Data for Name: oauthconsumertoken; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY oauthconsumertoken (id, created, token_key, token, token_secret, token_type, consumer_key) FROM stdin;
\.


--
-- Data for Name: oauthspconsumer; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY oauthspconsumer (id, created, consumer_key, consumername, public_key, description, callback, two_l_o_allowed, executing_two_l_o_user, two_l_o_impersonation_allowed, three_l_o_allowed) FROM stdin;
\.


--
-- Data for Name: oauthsptoken; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY oauthsptoken (id, created, token, token_secret, token_type, consumer_key, username, ttl, spauth, callback, spverifier, spversion, session_handle, session_creation_time, session_last_renewal_time, session_time_to_live) FROM stdin;
\.


--
-- Data for Name: optionconfiguration; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY optionconfiguration (id, fieldid, optionid, fieldconfig, sequence) FROM stdin;
10200	priority	1	10100	1
10201	priority	2	10100	2
10202	priority	3	10100	3
10203	priority	4	10100	4
10204	priority	5	10100	5
10300	issuetype	10000	10400	0
10301	issuetype	10003	10400	1
10302	issuetype	10001	10400	2
10303	issuetype	10004	10400	3
10304	issuetype	10002	10400	4
10305	priority	10000	10100	6
10306	priority	10001	10100	7
10307	priority	10000	10402	0
10308	priority	2	10402	1
10309	priority	3	10402	2
10310	priority	4	10402	3
10311	priority	10001	10402	4
\.


--
-- Data for Name: os_currentstep; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY os_currentstep (id, entry_id, step_id, action_id, owner, start_date, due_date, finish_date, status, caller) FROM stdin;
10000	10000	8	0		2019-06-17 03:30:29.283+00	\N	\N	Open	\N
\.


--
-- Data for Name: os_currentstep_prev; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY os_currentstep_prev (id, previous_id) FROM stdin;
\.


--
-- Data for Name: os_historystep; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY os_historystep (id, entry_id, step_id, action_id, owner, start_date, due_date, finish_date, status, caller) FROM stdin;
\.


--
-- Data for Name: os_historystep_prev; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY os_historystep_prev (id, previous_id) FROM stdin;
\.


--
-- Data for Name: os_wfentry; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY os_wfentry (id, name, initialized, state) FROM stdin;
10000	TEST: Service Request Fulfilment workflow for Jira Service Desk	\N	1
\.


--
-- Data for Name: permissionscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY permissionscheme (id, name, description) FROM stdin;
0	Default Permission Scheme	This is the default Permission Scheme. Any new projects that are created will be assigned this scheme.
10000	Jira Service Desk Permission Scheme for Project TEST	This Jira Service Desk Permission Scheme was generated for Project TEST
\.


--
-- Data for Name: permissionschemeattribute; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY permissionschemeattribute (id, scheme, attribute_key, attribute_value) FROM stdin;
\.


--
-- Data for Name: pluginstate; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY pluginstate (pluginkey, pluginenabled) FROM stdin;
\.


--
-- Data for Name: pluginversion; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY pluginversion (id, pluginname, pluginkey, pluginversion, created) FROM stdin;
10000	Jira Projects Plugin	com.atlassian.jira.jira-projects-plugin	5.1.9	2019-06-17 03:10:11.696+00
10001	Atlassian OAuth Consumer Plugin	com.atlassian.oauth.consumer	4.0.2	2019-06-17 03:10:11.728+00
10002	Atlassian Jira - Plugins - My Jira Home	com.atlassian.jira.jira-my-home-plugin	8.2.2	2019-06-17 03:10:11.729+00
10003	Project Creation Plugin SPI for JIRA	com.atlassian.plugins.jira-project-creation	4.0.0-74bdd7a6fa	2019-06-17 03:10:11.73+00
10004	Service Desk Automation Modules Plugin	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin	4.2.2-REL-0002	2019-06-17 03:10:11.732+00
10005	Atlassian Embedded Crowd - Administration Plugin	com.atlassian.crowd.embedded.admin	2.3.4-j11-2	2019-06-17 03:10:11.733+00
10006	Analytics Client Plugin	com.atlassian.analytics.analytics-client	5.7.2	2019-06-17 03:10:11.734+00
10007	Jira Help Tips plugin	com.atlassian.plugins.helptips.jira-help-tips	8.2.2	2019-06-17 03:10:11.736+00
10008	Atlassian Navigation Links Plugin	com.atlassian.plugins.atlassian-nav-links-plugin	5.0.0	2019-06-17 03:10:11.737+00
10009	JIRA Core Romanian (Romania) Language Pack	tac.jira core.languages.ro_RO	8.3.0.v20190530001017	2019-06-17 03:10:11.738+00
10010	Atlassian Jira - Plugins - Issue Web Link	com.atlassian.jira.jira-issue-link-web-plugin	8.2.2	2019-06-17 03:10:11.742+00
10011	JIRA Core Slovak (Slovakia) Language Pack	tac.jira core.languages.sk_SK	8.3.0.v20190530001017	2019-06-17 03:10:11.744+00
10012	Atlassian Hipchat Integration Plugin Core	com.atlassian.plugins.base-hipchat-integration-plugin-api	8.4.0	2019-06-17 03:10:11.745+00
10013	Whisper Messages Plugin	whisper.messages	1.0	2019-06-17 03:10:11.747+00
10014	Project Role Actors Plugin	com.atlassian.jira.plugin.system.projectroleactors	1.0	2019-06-17 03:10:11.749+00
10015	Atlassian Jira - Plugins - OAuth Service Provider SPI	com.atlassian.jira.oauth.serviceprovider	8.2.2	2019-06-17 03:10:11.752+00
10016	Keyboard Shortcuts Plugin	jira.keyboard.shortcuts	1.0	2019-06-17 03:10:11.766+00
10017	Bitbucket Importer Plugin for JIM	com.atlassian.jira.plugins.jira-importers-bitbucket-plugin	3.0.0	2019-06-17 03:10:11.773+00
10018	Service Desk Automation Plugin	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-plugin	4.0.12	2019-06-17 03:10:11.775+00
10019	Atlassian Remote Event Common Plugin	com.atlassian.plugins.atlassian-remote-event-common-plugin	6.1.0	2019-06-17 03:10:11.777+00
10020	Jira GitHub Issue Importer	com.atlassian.jira.plugins.jira-importers-github-plugin	3.0.5	2019-06-17 03:10:11.778+00
10021	Atlassian Chaperone	com.atlassian.plugins.atlassian-chaperone	4.1.5	2019-06-17 03:10:11.779+00
10022	JIRA Password Policy Plugin	com.atlassian.jira.plugins.jira-password-policy-plugin	2.1.0	2019-06-17 03:10:11.78+00
10023	ROME: RSS/Atom syndication and publishing tools	com.springsource.com.sun.syndication-0.9.0	0.9.0	2019-06-17 03:10:11.784+00
10024	JIRA iCalendar Plugin	com.atlassian.jira.extra.jira-ical-feed	1.5.0	2019-06-17 03:10:11.786+00
10025	Jira Drag and Drop Attachment Plugin	com.atlassian.jira.plugins.jira-dnd-attachment-plugin	5.0.1	2019-06-17 03:10:11.79+00
10026	Atlassian Jira - Plugins - Post setup announcements plugin	com.atlassian.jira.jira-postsetup-announcements-plugin	8.2.2	2019-06-17 03:10:11.798+00
10027	Streams Inline Actions Plugin	com.atlassian.streams.actions	8.1.3	2019-06-17 03:10:11.81+00
10028	JIRA Core Swedish (Sweden) Language Pack	tac.jira core.languages.sv_SE	8.3.0.v20190530001017	2019-06-17 03:10:11.816+00
10029	Comment Panel Plugin	com.atlassian.jira.plugin.system.comment-panel	1.0	2019-06-17 03:10:11.819+00
10030	Atlassian Remote Event Consumer Plugin	com.atlassian.plugins.atlassian-remote-event-consumer-plugin	6.1.0	2019-06-17 03:10:11.822+00
10031	scala-2.11-provider-plugin	com.atlassian.scala.plugins.scala-2.11-provider-plugin	0.13	2019-06-17 03:10:11.823+00
10032	JIRA Workflow Transition Tabs	com.atlassian.jira.plugin.system.workfloweditor.transition.tabs	1.0	2019-06-17 03:10:11.829+00
10033	Jira inform - batching plugin	com.atlassian.jira.plugins.inform.batching-plugin	1.1.10	2019-06-17 03:10:11.831+00
10034	JIRA Core Czech (Czech Republic) Language Pack	tac.jira core.languages.cs_CZ	8.3.0.v20190530001017	2019-06-17 03:10:11.833+00
10035	Renderer Plugin	com.atlassian.jira.plugin.system.jirarenderers	1.0	2019-06-17 03:10:11.838+00
10036	Atlassian Gadgets OAuth Service Provider Plugin	com.atlassian.gadgets.oauth.serviceprovider	4.3.9	2019-06-17 03:10:11.852+00
10037	querydsl-4.1.4-provider-plugin	com.atlassian.querydsl.plugins.querydsl-4.1.4-provider-plugin	1.0	2019-06-17 03:10:11.854+00
10038	Atlassian Pocketknife API Commons Plugin	com.atlassian.pocketknife.api.commons.plugin	1.0.7	2019-06-17 03:10:11.855+00
10039	JIRA Core Japanese (Japan) Language Pack	tac.jira core.languages.ja_JP	8.3.0.v20190530001017	2019-06-17 03:10:11.882+00
10040	JIRA Core Italian (Italy) Language Pack	tac.jira core.languages.it_IT	8.3.0.v20190530001017	2019-06-17 03:10:11.886+00
10041	JIRA Core Polish (Poland) Language Pack	tac.jira core.languages.pl_PL	8.3.0.v20190530001017	2019-06-17 03:10:11.889+00
10042	Atlassian Jira - Plugins - Admin Navigation Component	com.atlassian.jira.jira-admin-navigation-plugin	8.2.2	2019-06-17 03:10:11.89+00
10043	Service Desk Frontend Plugin	com.atlassian.servicedesk.frontend-webpack-plugin	4.2.2-REL-0002	2019-06-17 03:10:11.891+00
10044	JIRA Core German (Germany) Language Pack	tac.jira core.languages.de_DE	8.3.0.v20190530001017	2019-06-17 03:10:11.893+00
10045	jira-email-processor-plugin	com.atlassian.jira.jira-email-processor-plugin	4.2.0	2019-06-17 03:10:11.894+00
10046	Atlassian Plugins - Web Resources - Implementation Plugin	com.atlassian.plugins.atlassian-plugins-webresource-plugin	4.0.3	2019-06-17 03:10:11.895+00
10047	Preset Filters Sections	jira.webfragments.preset.filters	1.0	2019-06-17 03:10:11.897+00
10048	Atlassian Jira - Plugins - Project Config Plugin	com.atlassian.jira.jira-project-config-plugin	8.2.2	2019-06-17 03:10:11.908+00
10049	Crowd System Password Encoders	crowd.system.passwordencoders	1.0	2019-06-17 03:10:11.918+00
10050	Atlassian UI Plugin	com.atlassian.auiplugin	8.0.3	2019-06-17 03:10:11.92+00
10051	Atlassian Jira - Plugins - Share Content Component	com.atlassian.jira.jira-share-plugin	8.2.2	2019-06-17 03:10:11.921+00
10052	Atlassian Jira - Plugins - Statistics plugin	com.atlassian.jira.jira-statistics-plugin	8.2.2	2019-06-17 03:10:11.922+00
10053	Atlassian Jira - Plugins - Remote Jira Link	com.atlassian.jira.jira-issue-link-remote-jira-plugin	8.2.2	2019-06-17 03:10:11.93+00
10054	Atlassian Timed Promise Plugin	com.atlassian.plugin.timedpromise.atlassian-timed-promise-plugin	4.0.5	2019-06-17 03:10:11.932+00
10055	Functional Extensions Guava Inter-Ops	io.atlassian.fugue.guava-4.7.2	4.7.2	2019-06-17 03:10:11.934+00
10056	Remote Link Aggregator Plugin	com.atlassian.plugins.remote-link-aggregator-plugin	3.0.0	2019-06-17 03:10:11.936+00
10057	Atlassian HealthCheck Common Module	com.atlassian.healthcheck.atlassian-healthcheck	6.0.0	2019-06-17 03:10:11.937+00
10058	Workbox - Common Plugin	com.atlassian.mywork.mywork-common-plugin	7.0.1	2019-06-17 03:10:11.938+00
10059	Service Desk Search Plugin	com.atlassian.servicedesk.plugins.servicedesk-search-plugin	4.2.2-REL-0002	2019-06-17 03:10:11.94+00
10060	Jira Workflow Sharing Plugin	com.atlassian.jira.plugins.workflow.sharing.jira-workflow-sharing-plugin	2.2.3	2019-06-17 03:10:11.942+00
10061	Entity property conditions	system.entity.property.conditions	1.0	2019-06-17 03:10:11.944+00
10062	org.osgi:org.osgi.service.cm	org.osgi.service.cm-1.5.0.201505202024	1.5.0.201505202024	2019-06-17 03:10:11.947+00
10063	Atlassian Jira - Plugins - APDEX	com.atlassian.jira.jira-apdex-plugin	8.2.2	2019-06-17 03:10:11.949+00
10064	JQL Functions	jira.jql.function	1.0	2019-06-17 03:10:11.952+00
10065	Atlassian Soy - Plugin	com.atlassian.soy.soy-template-plugin	5.0.0	2019-06-17 03:10:11.953+00
10066	atlassian-failure-cache-plugin	com.atlassian.atlassian-failure-cache-plugin	2.0.0	2019-06-17 03:10:11.954+00
10067	Atlassian Jira - Plugins - View Issue Panels	com.atlassian.jira.jira-view-issue-plugin	8.2.2	2019-06-17 03:10:11.955+00
10068	JIRA Footer	jira.footer	1.0	2019-06-17 03:10:11.956+00
10069	Applinks - Plugin - Core	com.atlassian.applinks.applinks-plugin	6.0.4	2019-06-17 03:10:11.957+00
10070	Atlassian Whitelist API Plugin	com.atlassian.plugins.atlassian-whitelist-api-plugin-4.0.7	4.0.7	2019-06-17 03:10:11.958+00
10071	Jira inform - batchers	com.atlassian.jira.plugins.inform.batchers	1.1.10	2019-06-17 03:10:11.959+00
10072	jira-optimizer-plugin	com.atlassian.jira.plugins.jira-optimizer-plugin	2.0.10	2019-06-17 03:10:11.962+00
10073	Service Desk Internal Base Plugin	com.atlassian.servicedesk.internal-base-plugin	4.2.2-REL-0002	2019-06-17 03:10:11.964+00
10074	Analytics Whitelist Plugin	com.atlassian.analytics.analytics-whitelist	3.68	2019-06-17 03:10:11.965+00
10075	jira-assets-plugin	com.atlassian.jira.plugins.jira-assets-plugin	4.2.2-REL-0002	2019-06-17 03:10:11.971+00
10076	Atlassian Jira - Plugins - Invite User	com.atlassian.jira.jira-invite-user-plugin	2.2.6	2019-06-17 03:10:11.977+00
10077	Applinks - Plugin - Basic Authentication	com.atlassian.applinks.applinks-basicauth-plugin	6.0.4	2019-06-17 03:10:11.979+00
10078	Atlassian Jira - Plugins - Quick Search	com.atlassian.jira.plugins.jira-quicksearch-plugin	8.2.2	2019-06-17 03:10:11.992+00
10079	User Profile Links	jira.webfragments.user.profile.links	1.0	2019-06-17 03:10:12.004+00
10080	Service Desk Canned Responses Plugin	com.atlassian.servicedesk.servicedesk-canned-responses-plugin	4.2.2-REL-0002	2019-06-17 03:10:12.006+00
10081	Admin Menu Sections	jira.webfragments.admin	1.0	2019-06-17 03:10:12.009+00
10082	wiki-editor	com.atlassian.jira.plugins.jira-wiki-editor	4.0.20	2019-06-17 03:10:12.01+00
10083	Atlassian OAuth Service Provider SPI	com.atlassian.oauth.atlassian-oauth-service-provider-spi	4.0.2	2019-06-17 03:10:12.012+00
10084	Top Navigation Bar	jira.top.navigation.bar	1.0	2019-06-17 03:10:12.013+00
10085	Apache Apache HttpClient OSGi bundle	org.apache.httpcomponents.httpclient-4.5.5	4.5.5	2019-06-17 03:10:12.016+00
10086	Applinks - Plugin - Trusted Apps	com.atlassian.applinks.applinks-trustedapps-plugin	6.0.4	2019-06-17 03:10:12.018+00
10087	Wiki Renderer Macros Plugin	com.atlassian.jira.plugin.system.renderers.wiki.macros	1.0	2019-06-17 03:10:12.019+00
10088	JIRA Service Desk Application	com.atlassian.servicedesk.application	4.2.2	2019-06-17 03:10:12.021+00
10089	Issue Views Plugin	jira.issueviews	1.0	2019-06-17 03:10:12.025+00
10090	JIRA Core Icelandic (Iceland) Language Pack	tac.jira core.languages.is_IS	8.3.0.v20190530001017	2019-06-17 03:10:12.054+00
10091	Streams SPI	com.atlassian.streams.streams-spi	8.1.3	2019-06-17 03:10:12.056+00
10092	Atlassian JWT Plugin	com.atlassian.jwt.jwt-plugin	3.0.0	2019-06-17 03:10:12.058+00
10093	Jira inform - event plugin	com.atlassian.jira.plugins.inform.event-plugin	1.1.10	2019-06-17 03:10:12.061+00
10094	Applinks - Plugin - CORS	com.atlassian.applinks.applinks-cors-plugin	6.0.4	2019-06-17 03:10:12.062+00
10095	Service Desk Reports Plugin	com.atlassian.servicedesk.servicedesk-reports-plugin	4.2.2-REL-0002	2019-06-17 03:10:12.064+00
10096	Streams Third Party Provider Plugin	com.atlassian.streams.streams-thirdparty-plugin	8.1.3	2019-06-17 03:10:12.067+00
10097	Atlassian OAuth Service Provider Plugin	com.atlassian.oauth.serviceprovider	4.0.2	2019-06-17 03:10:12.069+00
10098	Atlassian Jira - Plugins - Common AppLinks Based Issue Link Plugin	com.atlassian.jira.jira-issue-link-applinks-common-plugin	8.2.2	2019-06-17 03:10:12.07+00
10099	Functional Extensions	io.atlassian.fugue-4.7.2	4.7.2	2019-06-17 03:10:12.071+00
10100	jira-webresources-plugin	jira.webresources	1.0	2019-06-17 03:10:12.072+00
10101	Embedded Gadgets Plugin	com.atlassian.gadgets.embedded	4.3.9	2019-06-17 03:10:12.077+00
10102	Atlassian Client Resource	com.atlassian.plugins.atlassian-client-resource	2.0.3	2019-06-17 03:10:12.078+00
10103	Atlassian Jira - Plugins - Core Reports	com.atlassian.jira.jira-core-reports-plugin	8.2.2	2019-06-17 03:10:12.08+00
10104	Streams Plugin	com.atlassian.streams	8.1.3	2019-06-17 03:10:12.081+00
10105	Browse Project Operations Sections	jira.webfragments.browse.project.links	1.0	2019-06-17 03:10:12.084+00
10106	JIRA Issue Collector Plugin	com.atlassian.jira.collector.plugin.jira-issue-collector-plugin	3.0.7	2019-06-17 03:10:12.085+00
10107	JIRA Working Hours Plugin	com.atlassian.jira.plugins.workinghours	4.2.2-REL-0002	2019-06-17 03:10:12.087+00
10108	Atlassian Whitelist Core Plugin	com.atlassian.plugins.atlassian-whitelist-core-plugin	4.0.7	2019-06-17 03:10:12.088+00
10109	JIRA Core Russian (Russia) Language Pack	tac.jira core.languages.ru_RU	8.3.0.v20190530001017	2019-06-17 03:10:12.089+00
10110	ICU4J	com.atlassian.bundles.icu4j-3.8.0.1	3.8.0.1	2019-06-17 03:10:12.091+00
10111	Streams Core Plugin	com.atlassian.streams.core	8.1.3	2019-06-17 03:10:12.093+00
10112	Atlassian Jira - Plugins - WebHooks Plugin	com.atlassian.jira.plugins.webhooks.jira-webhooks-plugin	8.2.2	2019-06-17 03:10:12.094+00
10113	Issue Operations Plugin	com.atlassian.jira.plugin.system.issueoperations	1.0	2019-06-17 03:10:12.096+00
10114	Gadget Directory Plugin	com.atlassian.gadgets.directory	4.3.9	2019-06-17 03:10:12.099+00
10115	Attach Image for JIRA	com.atlassian.plugins.jira-html5-attach-images	4.0.0	2019-06-17 03:10:12.1+00
10116	Jira Monitoring Plugin	com.atlassian.jira.jira-monitoring-plugin	05.7.3	2019-06-17 03:10:12.101+00
10117	Atlassian Hipchat Integration Plugin	com.atlassian.plugins.base-hipchat-integration-plugin	8.4.0	2019-06-17 03:10:12.102+00
10118	Atlassian Jira - Plugins - Project Centric Issue Navigator	com.atlassian.jira.jira-projects-issue-navigator	9.1.8	2019-06-17 03:10:12.106+00
10119	jira-importers-plugin	com.atlassian.jira.plugins.jira-importers-plugin	9.0.11	2019-06-17 03:10:12.108+00
10120	Atlassian Plugins - JavaScript libraries	com.atlassian.plugin.jslibs	1.2.4	2019-06-17 03:10:12.109+00
10121	Jira Time Zone Detection plugin	com.atlassian.jira.jira-tzdetect-plugin	3.0.3	2019-06-17 03:10:12.111+00
10122	Hipchat for Jira	com.atlassian.labs.hipchat.hipchat-for-jira-plugin	8.4.0	2019-06-17 03:10:12.112+00
10123	Atlassian Jira - Plugins - Diagnostics Plugin	com.atlassian.jira.diagnostics	8.2.2	2019-06-17 03:10:12.114+00
10124	JIRA Core Dutch (Netherlands) Language Pack	tac.jira core.languages.nl_NL	8.3.0.v20190530001017	2019-06-17 03:10:12.115+00
10125	JIRA Core Estonian (Estonia) Language Pack	tac.jira core.languages.et_EE	8.3.0.v20190530001017	2019-06-17 03:10:12.117+00
10126	JSON Library	com.atlassian.bundles.json-20070829.0.0.1	20070829.0.0.1	2019-06-17 03:10:12.12+00
10127	JDOM DOM Processor	com.springsource.org.jdom-1.0.0	1.0.0	2019-06-17 03:10:12.124+00
10128	JIRA Core Chinese (China) Language Pack	tac.jira core.languages.zh_CN	8.3.0.v20190530001017	2019-06-17 03:10:12.126+00
10129	Atlassian Jira - Plugins - Feedback Plugin	com.atlassian.feedback.jira-feedback-plugin	8.2.2	2019-06-17 03:10:12.128+00
10130	ActiveObjects Plugin - OSGi Bundle	com.atlassian.activeobjects.activeobjects-plugin	3.0.0	2019-06-17 03:10:12.13+00
10131	Atlassian Jira - Plugins - Header Plugin	com.atlassian.jira.jira-header-plugin	8.2.2	2019-06-17 03:10:12.132+00
10132	Issue Tab Panels Plugin	com.atlassian.jira.plugin.system.issuetabpanels	1.0	2019-06-17 03:10:12.135+00
10133	JIRA Feature Keys	jira.feature.keys	1.0	2019-06-17 03:10:12.137+00
10134	JIRA Streams Inline Actions Plugin	com.atlassian.streams.jira.inlineactions	8.1.3	2019-06-17 03:10:12.138+00
10135	Atlassian Jira - Plugins - Application Properties	com.atlassian.jira.jira-application-properties-plugin	8.2.2	2019-06-17 03:10:12.14+00
10136	Atlassian Jira - Plugins - Gadgets Plugin	com.atlassian.jira.gadgets	8.2.2	2019-06-17 03:10:12.142+00
10137	Atlassian Jira - Plugins - Analytics whitelist	com.atlassian.jira.jira-analytics-whitelist-plugin	8.2.2	2019-06-17 03:10:12.143+00
10138	jira-inline-issue-create-plugin	com.atlassian.jira.plugins.inline-create.jira-inline-issue-create-plugin	2.0.15	2019-06-17 03:10:12.146+00
10139	Functional Extensions Scala Inter-Ops	io.atlassian.fugue.scala-4.7.2	4.7.2	2019-06-17 03:10:12.151+00
10140	Workbox - JIRA Provider Plugin	com.atlassian.mywork.mywork-jira-provider-plugin	7.0.1	2019-06-17 03:10:12.152+00
10141	JIRA Core Korean (South Korea) Language Pack	tac.jira core.languages.ko_KR	8.3.0.v20190530001017	2019-06-17 03:10:12.155+00
10142	Content Link Resolvers Plugin	com.atlassian.jira.plugin.wiki.contentlinkresolvers	1.0	2019-06-17 03:10:12.157+00
10143	Atlassian Application Manager plugin	com.atlassian.upm.upm-application-plugin	4.0.4	2019-06-17 03:10:12.159+00
10144	JIRA Core Spanish (Spain) Language Pack	tac.jira core.languages.es_ES	8.3.0.v20190530001017	2019-06-17 03:10:12.162+00
10145	JIRA Service Desk Public REST API Plugin	com.atlassian.servicedesk.public-rest-api	4.2.2-REL-0002	2019-06-17 03:10:12.164+00
10146	Atlassian JIRA - Plugins - Credits Plugin	com.atlassian.jira.jira-credits-plugin	8.2.2	2019-06-17 03:10:12.167+00
10147	Jira Mobile	com.atlassian.jira.mobile	3.2.4	2019-06-17 03:10:12.17+00
10148	Atlassian Jira - Plugins - Onboarding	com.atlassian.jira.jira-onboarding-assets-plugin	8.2.2	2019-06-17 03:10:12.173+00
10149	Atlassian JIRA - Admin Helper Plugin	com.atlassian.jira.plugins.jira-admin-helper-plugin	5.0.1	2019-06-17 03:10:12.175+00
10150	Atlassian browser metrics plugin	com.atlassian.plugins.browser.metrics.browser-metrics-plugin	7.0.1	2019-06-17 03:10:12.177+00
10151	Atlassian Jira - Plugins - Global Issue Navigator	com.atlassian.jira.jira-issue-nav-plugin	9.1.8	2019-06-17 03:10:12.178+00
10152	User Navigation Bar Sections	jira.webfragments.user.navigation.bar	1.0	2019-06-17 03:10:12.18+00
10153	Atlassian Troubleshooting and Support Tools	com.atlassian.troubleshooting.plugin-jira	1.16.0	2019-06-17 03:10:12.182+00
10154	Neko HTML	com.atlassian.bundles.nekohtml-1.9.12.1	1.9.12.1	2019-06-17 03:10:12.185+00
10155	Service Desk Approvals Plugin	com.atlassian.servicedesk.approvals-plugin	4.2.2-REL-0002	2019-06-17 03:10:12.187+00
10156	JIRA Global Permissions	jira.system.global.permissions	1.0	2019-06-17 03:10:12.198+00
10157	Atlassian Template Renderer API	com.atlassian.templaterenderer.api	4.0.0	2019-06-17 03:10:12.2+00
10158	JIRA Core Danish (Denmark) Language Pack	tac.jira core.languages.da_DK	8.3.0.v20190530001017	2019-06-17 03:10:12.204+00
10159	Atlassian JIRA - Plugins - File viewer plugin	com.atlassian.jira.jira-fileviewer-plugin	8.0.0	2019-06-17 03:10:12.206+00
10160	Atlassian Pretty URLs Plugin	com.atlassian.prettyurls.atlassian-pretty-urls-plugin	3.0.0	2019-06-17 03:10:12.208+00
10161	JIRA Attachment Archive File Processors	jira.system.attachment.processors	1.0	2019-06-17 03:10:12.219+00
10162	Atlassian Jira - Plugins - Admin Upgrades	com.atlassian.jira.jira-admin-updates-plugin	8.2.2	2019-06-17 03:10:12.223+00
10163	jira-ui	com.atlassian.jira.plugins.jira-ui	0.2.0	2019-06-17 03:10:12.229+00
10164	Atlassian Universal Plugin Manager Plugin	com.atlassian.upm.atlassian-universal-plugin-manager-plugin	4.0.4	2019-06-17 03:10:12.231+00
10165	SAML for Atlassian Data Center	com.atlassian.plugins.authentication.atlassian-authentication-plugin	3.1.1	2019-06-17 03:10:12.233+00
10166	Atlassian Whitelist UI Plugin	com.atlassian.plugins.atlassian-whitelist-ui-plugin	4.0.7	2019-06-17 03:10:12.235+00
10167	JIRA Core Portuguese (Brazil) Language Pack	tac.jira core.languages.pt_BR	8.3.0.v20190530001017	2019-06-17 03:10:12.237+00
10168	Redmine Importers Plugin for JIM	com.atlassian.jira.plugins.jira-importers-redmine-plugin	2.1.9	2019-06-17 03:10:12.238+00
10169	Atlassian JIRA - Plugins - Quick Edit Plugin	com.atlassian.jira.jira-quick-edit-plugin	3.2.0	2019-06-17 03:10:12.239+00
10170	Soy Function Plugin	com.atlassian.jira.plugin.system.soyfunction	1.0	2019-06-17 03:10:12.239+00
10171	Universal Plugin Manager - Role-Based Licensing Implementation Plugin	com.atlassian.upm.role-based-licensing-plugin	4.0.4	2019-06-17 03:10:12.241+00
10172	Jira Core Project Templates Plugin	com.atlassian.jira-core-project-templates	7.0.3	2019-06-17 03:10:12.243+00
10173	Atlassian OAuth Admin Plugin	com.atlassian.oauth.admin	4.0.2	2019-06-17 03:10:12.244+00
10174	Atlassian REST - Module Types	com.atlassian.plugins.rest.atlassian-rest-module	6.0.0	2019-06-17 03:10:12.246+00
10175	Crowd REST API	crowd-rest-application-management	1.0	2019-06-17 03:10:12.248+00
10176	jira-issue-nav-components	com.atlassian.jira.jira-issue-nav-components	9.1.8	2019-06-17 03:10:12.249+00
10177	jquery	com.atlassian.plugins.jquery	2.2.4.6	2019-06-17 03:10:12.25+00
10178	Atlassian Jira - Plugins - Auditing Plugin [Audit Log]	com.atlassian.jira.plugins.jira-auditing-plugin	8.2.2	2019-06-17 03:10:12.252+00
10179	JIRA Remote Link Aggregator Plugin	com.atlassian.plugins.jira-remote-link-aggregator-plugin	3.0.0	2019-06-17 03:10:12.254+00
10180	JIRA Service Desk	com.atlassian.servicedesk	4.2.2-REL-0002	2019-06-17 03:10:12.255+00
10181	Streams API	com.atlassian.streams.streams-api	8.1.3	2019-06-17 03:10:12.256+00
10182	Atlassian Jira - Plugins - Atlassian Notifications Plugin	com.atlassian.jira.jira-whisper-plugin	8.2.2	2019-06-17 03:10:12.256+00
10183	Atlassian HTTP Client, Apache HTTP components impl	com.atlassian.httpclient.atlassian-httpclient-plugin	2.0.0	2019-06-17 03:10:12.257+00
10184	Asana Importers Plugin for JIM	com.atlassian.jira.plugins.jira-importers-asana-plugin	2.0.2	2019-06-17 03:10:12.259+00
10185	Atlassian Awareness Capability	com.atlassian.plugins.atlassian-awareness-capability	0.0.6	2019-06-17 03:10:12.262+00
10186	Atlassian Plugins - Web Resources REST	com.atlassian.plugins.atlassian-plugins-webresource-rest	4.0.3	2019-06-17 03:10:12.279+00
10187	Custom Field Types & Searchers	com.atlassian.jira.plugin.system.customfieldtypes	1.0	2019-06-17 03:10:12.292+00
10188	Project Creation Capability Product REST Plugin	com.atlassian.plugins.atlassian-project-creation-plugin	4.0.0-74bdd7a6fa	2019-06-17 03:10:12.295+00
10189	Atlassian Jira - Plugins - REST Plugin	com.atlassian.jira.rest	8.2.2	2019-06-17 03:10:12.298+00
10190	Atlassian Spring Scanner Runtime	com.atlassian.plugin.atlassian-spring-scanner-runtime	2.1.7	2019-06-17 03:10:12.3+00
10191	Opensocial Plugin	com.atlassian.gadgets.opensocial	4.3.9	2019-06-17 03:10:12.302+00
10192	Atlassian Jira - Plugins - Confluence Link	com.atlassian.jira.jira-issue-link-confluence-plugin	8.2.2	2019-06-17 03:10:12.304+00
10193	Service Desk Knowledge Base Plugin	com.atlassian.servicedesk.servicedesk-knowledge-base-plugin	4.2.2-REL-0002	2019-06-17 03:10:12.306+00
10194	ROME, RSS and atOM utilitiEs for Java	rome.rome-1.0	1.0	2019-06-17 03:10:12.309+00
10195	User Format	jira.user.format	1.0	2019-06-17 03:10:12.312+00
10196	JIRA Core Finnish (Finland) Language Pack	tac.jira core.languages.fi_FI	8.3.0.v20190530001017	2019-06-17 03:10:12.313+00
10197	View Project Operations Sections	jira.webfragments.view.project.operations	1.0	2019-06-17 03:10:12.314+00
10198	atlassian-servlet-plugin	com.atlassian.web.atlassian-servlet-plugin	5.2.0	2019-06-17 03:10:12.315+00
10199	Apache Apache HttpCore OSGi bundle	org.apache.httpcomponents.httpcore-4.4.9	4.4.9	2019-06-17 03:10:12.317+00
10200	Service Desk Lingo Plugin	com.atlassian.servicedesk.servicedesk-lingo-plugin	4.2.2-REL-0002	2019-06-17 03:10:12.318+00
10201	Crowd REST API	crowd-rest-plugin	1.0	2019-06-17 03:10:12.323+00
10202	JIRA Service Desk Project Plugin	com.atlassian.servicedesk.project-ui	4.2.2-REL-0002	2019-06-17 03:10:12.323+00
10203	Service Desk Core UI Plugin	com.atlassian.servicedesk.core-ui	4.2.2-REL-0002	2019-06-17 03:10:12.325+00
10204	Atlassian OAuth Consumer SPI	com.atlassian.oauth.atlassian-oauth-consumer-spi	4.0.2	2019-06-17 03:10:12.327+00
10205	Atlassian Jira - Plugins - Post-Upgrade Landing Page	com.atlassian.jira.plugins.jira-post-upgrade-landing-page-plugin	8.2.2	2019-06-17 03:10:12.33+00
10206	JIRA Core Hungarian (Hungary) Language Pack	tac.jira core.languages.hu_HU	8.3.0.v20190530001017	2019-06-17 03:10:12.331+00
10207	Atlassian Spring Scanner Annotations	com.atlassian.plugin.atlassian-spring-scanner-annotation	2.1.7	2019-06-17 03:10:12.332+00
10208	jackson-module-scala-2.10-provider	com.atlassian.scala.plugins.jackson-module-scala-2.10-provider-plugin	0.5	2019-06-17 03:10:12.333+00
10209	Service Desk Automation Then-Webhook Plugin	com.atlassian.servicedesk.plugins.automation.servicedesk-automation-then-webhook-plugin	4.2.2-REL-0002	2019-06-17 03:10:12.334+00
10210	Service Desk Notifications Plugin	com.atlassian.servicedesk.servicedesk-notifications-plugin	4.2.2-REL-0002	2019-06-17 03:10:12.336+00
10211	Rich Text Editor for JIRA	com.atlassian.jira.plugins.jira-editor-plugin	4.0.20	2019-06-17 03:10:12.337+00
10212	Web Resources Plugin Jira Core	jira.core	1.0	2019-06-17 03:10:12.338+00
10213	Workflow Plugin	com.atlassian.jira.plugin.system.workflow	1.0	2019-06-17 03:10:12.339+00
10214	jira-importers-trello-plugin	com.atlassian.jira.plugins.jira-importers-trello-plugin	3.0.1	2019-06-17 03:10:12.339+00
10215	Icon Types Plugin	jira.icontypes	1.0	2019-06-17 03:10:12.34+00
10216	psmq-plugin	com.atlassian.psmq	4.0.3	2019-06-17 03:10:12.341+00
10217	JIRA Core Norwegian (Norway) Language Pack	tac.jira core.languages.no_NO	8.3.0.v20190530001017	2019-06-17 03:10:12.342+00
10218	Atlassian WebHooks Plugin	com.atlassian.webhooks.atlassian-webhooks-plugin	3.3.0	2019-06-17 03:10:12.343+00
10219	Help Paths Plugin	jira.help.paths	1.0	2019-06-17 03:10:12.344+00
10220	Jira Base URL Plugin	com.atlassian.jira.jira-baseurl-plugin	3.2.0	2019-06-17 03:10:12.347+00
10221	Atlassian Jira - Plugins - Look And Feel Logo Upload Plugin	com.atlassian.jira.lookandfeel	8.2.2	2019-06-17 03:10:12.349+00
10222	Issue Status Plugin	com.atlassian.plugins.issue-status-plugin	2.0.2	2019-06-17 03:10:12.35+00
10223	Gadget Spec Publisher Plugin	com.atlassian.gadgets.publisher	4.3.9	2019-06-17 03:10:12.35+00
10224	Atlassian Jira - Plugins - OAuth Consumer SPI	com.atlassian.jira.oauth.consumer	8.2.2	2019-06-17 03:10:12.351+00
10225	JIRA Core French (France) Language Pack	tac.jira core.languages.fr_FR	8.3.0.v20190530001017	2019-06-17 03:10:12.352+00
10226	Renderer Component Factories Plugin	com.atlassian.jira.plugin.wiki.renderercomponentfactories	1.0	2019-06-17 03:10:12.353+00
10227	Atlassian LESS Transformer Plugin	com.atlassian.plugins.less-transformer-plugin	4.0.0	2019-06-17 03:10:12.353+00
10228	Pocketknife Feature Flags Plugin	com.atlassian.pocketknife.featureflags-plugin	0.5.4	2019-06-17 03:10:12.354+00
10229	Functional Optics Library	io.atlassian.fugue.optics-4.7.2	4.7.2	2019-06-17 03:10:12.354+00
10230	Functional Extensions Retry Inter-Ops	io.atlassian.fugue.retry-4.7.2	4.7.2	2019-06-17 03:10:12.355+00
10231	Atlassian Front-End Runtime Plugin	com.atlassian.frontend.atlassian-frontend-runtime-plugin	0.3.0	2019-06-17 03:10:12.356+00
10232	Atlassian Bot Session Killer	com.atlassian.labs.atlassian-bot-killer	1.7.9	2019-06-17 03:10:12.356+00
10233	User Profile Panels	jira.user.profile.panels	1.0	2019-06-17 03:10:12.357+00
10234	scala-2.10-provider-plugin	com.atlassian.scala.plugins.scala-2.10-provider-plugin	0.14	2019-06-17 03:10:12.357+00
10235	Atlassian Jira - Plugins - ActiveObjects SPI	com.atlassian.jira.jira-activeobjects-spi-plugin	8.2.2	2019-06-17 03:10:12.358+00
10236	JIRA Project Permissions	jira.system.project.permissions	1.0	2019-06-17 03:10:12.358+00
10237	Wallboard Plugin	com.atlassian.jirawallboard.atlassian-wallboard-plugin	3.0.2	2019-06-17 03:10:12.358+00
10238	Atlassian Jira - Plugins - User Profile Plugin	com.atlassian.jira.jira-user-profile-plugin	4.0.0	2019-06-17 03:10:12.359+00
10239	Project Templates Plugin	com.atlassian.jira.project-templates-plugin	7.0.3	2019-06-17 03:10:12.36+00
10240	Atlassian Notifications	com.atlassian.whisper.atlassian-whisper-plugin	2.0.3	2019-06-17 03:10:12.361+00
10241	Filter Deletion Warning Plugin	jira.filter.deletion.warning	1.0	2019-06-17 03:10:12.361+00
10242	English (United States) Language Pack	com.atlassian.jira.jira-languages.en_US	8.2.2	2019-06-17 03:10:12.361+00
10243	Atlassian Jira - Plugins - LESS integration	com.atlassian.jira.jira-less-integration	8.2.2	2019-06-17 03:10:12.362+00
10244	Atlassian Jira - Plugins - SAL Plugin	com.atlassian.sal.jira	8.2.2	2019-06-17 03:10:12.363+00
10245	Atlassian Template Renderer Velocity 1.6 Plugin	com.atlassian.templaterenderer.atlassian-template-renderer-velocity1.6-plugin	4.0.0	2019-06-17 03:10:12.364+00
10246	Atlassian Jira - Plugins - Mail Plugin	com.atlassian.jira.jira-mail-plugin	11.0.1	2019-06-17 03:10:12.364+00
10247	Service Desk Variable Substitution Plugin	com.atlassian.servicedesk.servicedesk-variable-substitution-plugin	4.2.2-REL-0002	2019-06-17 03:10:12.365+00
10248	Atlassian Jira - Plugins - Closure Template Renderer	com.atlassian.jira.jira-soy-plugin	8.2.2	2019-06-17 03:10:12.366+00
10249	Jira Workflow Designer Plugin	com.atlassian.jira.plugins.jira-workflow-designer	8.0.6	2019-06-17 03:10:12.368+00
10250	Gadget Dashboard Plugin	com.atlassian.gadgets.dashboard	4.3.9	2019-06-17 03:10:12.369+00
10251	Atlassian - Administration - Quick Search - JIRA	com.atlassian.administration.atlassian-admin-quicksearch-jira	2.3.3	2019-06-17 03:10:12.37+00
10252	Web Panel Plugin	jira.webpanels	1.0	2019-06-17 03:10:12.37+00
10253	Applinks - Plugin - OAuth	com.atlassian.applinks.applinks-oauth-plugin	6.0.4	2019-06-17 03:10:12.371+00
10254	JIRA browser metrics integration plugin	com.atlassian.jira.plugins.jira-browser-metrics	2.0.14	2019-06-17 03:10:12.372+00
10255	JIRA Activity Stream Plugin	com.atlassian.streams.streams-jira-plugin	8.1.3	2019-06-17 03:10:12.372+00
10256	English (United Kingdom) Language Pack	com.atlassian.jira.jira-languages.en_UK	8.2.2	2019-06-17 03:10:12.373+00
10257	JIRA Usage Hints	jira.usage.hints	1.0	2019-06-17 03:10:12.373+00
\.


--
-- Data for Name: portalpage; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY portalpage (id, username, pagename, description, sequence, fav_count, layout, ppversion) FROM stdin;
10000	\N	System Dashboard	\N	0	0	AA	0
\.


--
-- Data for Name: portletconfiguration; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY portletconfiguration (id, portalpage, portlet_id, column_number, positionseq, gadget_xml, color, dashboard_module_complete_key) FROM stdin;
10000	10000	\N	0	0	\N	\N	com.atlassian.jira.gadgets:introduction-dashboard-item
10002	10000	\N	1	0	rest/gadgets/1.0/g/com.atlassian.jira.gadgets:assigned-to-me-gadget/gadgets/assigned-to-me-gadget.xml	\N	\N
10003	10000	\N	1	1	rest/gadgets/1.0/g/com.atlassian.streams.streams-jira-plugin:activitystream-gadget/gadgets/activitystream-gadget.xml	\N	\N
\.


--
-- Data for Name: priority; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY priority (id, sequence, pname, description, iconurl, status_color) FROM stdin;
1	1	Highest	This problem will block progress.	/images/icons/priorities/highest.png	#ff7452
2	2	High	Serious problem that could block progress.	/images/icons/priorities/high.png	#ff8f73
3	3	Medium	Has the potential to affect progress.	/images/icons/priorities/medium.png	#ffab00
4	4	Low	Minor problem or easily worked around.	/images/icons/priorities/low.png	#0065ff
5	5	Lowest	Trivial problem with little or no impact on progress.	/images/icons/priorities/lowest.png	#2684ff
10000	6	Blocker	The problem will block progress.	/images/icons/priorities/blocker.png	#990000
10001	7	Minor	Trivial problem with little or no impact on progress.	/images/icons/priorities/trivial.png	#999999
\.


--
-- Data for Name: productlicense; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY productlicense (id, license) FROM stdin;
10000	AAABaA0ODAoPeNqNkVFPgzAUhd/5FSS+6ANkDDfiEhInYMYCbI7NJ18qu8xuUMhtu+i/t4BGl4XpU3t723u+c3r1iFSfS6Zbt/pwMBnZk8FY9/y1Kqw7bU+RmDVWW5kJsykMDnikGWyBH0ySCXoEV6CEP26mgqAAdHNScNASWb4CLvKNusJdw9K8igk1LCEluHvJBWX33bJDmueUmVlVakuJ2Rvh4BMBbkNnDMaGNdJUzySiIJxTwsw+vkgdMg7rjxpaGW8Rx8HKC6fRdyt4ryl+/JruNNO/0IKY0KKfLVWqgKHvPmxiy3haz5aGHc6mRmI5swuAF6Au53mWoMJjAhhhWY+PfghQD7FGyk+TUmai0E+DxIgs23GGo7Gtqco9PVngjjDKiaAVc89z8RDa1v//jMlyVRXgVZKJxljR0Twrn43EUPOBZ0jrVnCuctHT7rWuGgf9uvuJm5eJHhxJIVt17WfbWfwEJpMHazAsAhRPT0oD+ogMpWWDWgsSEU4WiSc/IwIUTKlcH4yh0bxOdmy2eM3isNHg/f4=X02hl
\.


--
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY project (id, pname, url, lead, description, pkey, pcounter, assigneetype, avatar, originalkey, projecttype) FROM stdin;
10000	Test		admin		TEST	1	3	10324	TEST	service_desk
\.


--
-- Data for Name: project_key; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY project_key (id, project_id, project_key) FROM stdin;
10000	10000	TEST
\.


--
-- Data for Name: projectcategory; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY projectcategory (id, cname, description) FROM stdin;
\.


--
-- Data for Name: projectchangedtime; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY projectchangedtime (project_id, issue_changed_time) FROM stdin;
\.


--
-- Data for Name: projectrole; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY projectrole (id, name, description) FROM stdin;
10002	Administrators	A project role that represents administrators in a project
10100	Service Desk Customers	This project role was created by Jira Service Desk for managing customers. Please do not delete this role or edit its name.
10101	Service Desk Team	This project role was created by Jira Service Desk for managing the team. Please do not delete this role or edit its name.
10200	Developers	\N
\.


--
-- Data for Name: projectroleactor; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY projectroleactor (id, pid, projectroleid, roletype, roletypeparameter) FROM stdin;
10002	\N	10002	atlassian-group-role-actor	jira-administrators
10101	10000	10002	atlassian-user-role-actor	admin
\.


--
-- Data for Name: projectversion; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY projectversion (id, project, vname, description, sequence, released, archived, url, startdate, releasedate) FROM stdin;
\.


--
-- Data for Name: propertydata; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY propertydata (id, propertyvalue) FROM stdin;
\.


--
-- Data for Name: propertydate; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY propertydate (id, propertyvalue) FROM stdin;
\.


--
-- Data for Name: propertydecimal; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY propertydecimal (id, propertyvalue) FROM stdin;
\.


--
-- Data for Name: propertyentry; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY propertyentry (id, entity_name, entity_id, property_key, propertytype) FROM stdin;
6	jira.properties	1	jira.avatar.user.anonymous.id	5
7	jira.properties	1	jira.scheme.default.issue.type	5
9	jira.properties	1	jira.whitelist.disabled	1
10	jira.properties	1	jira.whitelist.rules	6
11	jira.properties	1	jira.option.timetracking	1
12	jira.properties	1	jira.timetracking.estimates.legacy.behaviour	1
13	jira.properties	1	jira.version	5
14	jira.properties	1	jira.downgrade.minimum.version	5
15	jira.properties	1	jira.option.allowunassigned	1
16	jira.properties	1	jira.path.index.use.default.directory	1
21	com.atlassian.jira.plugins.jira-workflow-designer	1	jira.workflow.layout:8a6044147cf2c19c02d099279cfbfd47	6
22	jira.properties	1	jira.onboarding.app_user.id.threshold	5
23	jira.properties	1	post.migration.page.displayed	1
1	jira.properties	1	jira.version.patched	5
2	jira.properties	1	jira.avatar.issuetype.subtask.default.id	5
3	jira.properties	1	jira.avatar.default.id	5
4	jira.properties	1	jira.avatar.issuetype.default.id	5
5	jira.properties	1	jira.avatar.user.default.id	5
10100	jira.properties	1	AO_587B34_#	5
10101	jira.properties	1	jira.webresource.superbatch.flushcounter	5
10102	jira.properties	1	jira.i18n.language.index	5
10103	jira.properties	1	jira.sid.key	5
10104	jira.properties	1	jira.scheme.default.priority	5
10105	jira.properties	1	AO_550953_#	5
10108	com.atlassian.jira.user.flag.FlagDismissalServiceImpl	1	com.atlassian.jira.flag.resets	6
10200	jira.properties	1	jira.webresource.flushcounter	5
10201	jira.properties	1	jira.mail.send.disabled	1
10202	jira.properties	1	AO_4AEACD_#	5
10203	jira.properties	1	mailsetting.jira.mail.send.disabled.modifiedBy	5
10204	jira.properties	1	mailsetting.jira.mail.send.disabled.modifiedDate	5
10205	jira.properties	1	jvm.system.timezone	5
10206	jira.properties	1	jira.i18n.default.locale	5
10207	jira.properties	1	webwork.i18n.encoding	5
10208	jira.properties	1	jira.title	5
10209	jira.properties	1	jira.baseurl	5
10210	jira.properties	1	jira.mode	5
10211	jira.properties	1	jira.path.attachments	5
10212	jira.properties	1	jira.path.attachments.use.default.directory	1
10213	jira.properties	1	jira.option.allowattachments	1
10214	ServiceConfig	10001	USE_DEFAULT_DIRECTORY	5
10215	jira.properties	1	jira.path.backup	5
10216	vp.properties	1	com.atlassian.servicedesk:sd-request-participants	3
10217	vp.properties	1	com.atlassian.servicedesk:vp-origin	3
10218	vp.properties	1	com.atlassian.servicedesk:sd-customer-organizations	3
10222	jira.properties	1	com.atlassian.jira.util.index.IndexingCounterManagerImpl.counterValue	3
10223	jira.properties	1	jira.monitoring.jmx.enabled	1
10224	jira.properties	1	jira.setup	5
10225	jira.properties	1	jira.initial.build.number	5
10226	jira.properties	1	jira.option.user.externalmanagement	1
10227	jira.properties	1	jira.option.voting	1
10228	jira.properties	1	jira.option.watching	1
10229	jira.properties	1	jira.option.issuelinking	1
10230	jira.properties	1	jira.option.emailvisible	5
10300	jira.properties	1	jira.option.allowsubtasks	1
10301	jira.properties	1	com.atlassian.analytics.client.configuration.uuid	5
10302	jira.properties	1	com.atlassian.analytics.client.configuration.serverid	5
10303	jira.properties	1	com.atlassian.analytics.client.configuration..analytics_enabled	5
10304	sd.approvals.properties	1	com.atlassian.servicedesk.approvals-plugin:sd-approvals	3
10305	jira.properties	1	jira-header-plugin.studio-tab-migration-complete	5
10306	jira.properties	1	com.atlassian.servicedesk:plugins:service-desk-database-version	5
10307	jira.properties	1	com.atlassian.sal.jira:build	5
10308	jira.properties	1	com.atlassian.plugins.atlassian-whitelist-api-plugin:whitelist.enabled	5
10309	jira.properties	1	com.atlassian.jira.gadgets:build	5
10310	jira.properties	1	com.atlassian.jira.plugins.webhooks.jira-webhooks-plugin:build	5
10311	jira.properties	1	com.atlassian.crowd.embedded.admin:build	5
10312	jira.properties	1	com.atlassian.labs.hipchat.hipchat-for-jira-plugin:build	5
10313	jira.properties	1	com.atlassian.jirawallboard.atlassian-wallboard-plugin:build	5
10314	jira.properties	1	com.atlassian.upm.atlassian-universal-plugin-manager-plugin:build	5
10315	jira.properties	1	com.atlassian.plugins.atlassian-whitelist-core-plugin:build	5
10316	jira.properties	1	com.atlassian.jira.lookandfeel:isDefaultFavicon	5
10317	jira.properties	1	com.atlassian.jira.lookandfeel:usingCustomFavicon	5
10318	jira.properties	1	com.atlassian.jira.lookandfeel:customDefaultFaviconURL	5
10319	jira.properties	1	com.atlassian.jira.lookandfeel:customDefaultFaviconHiresURL	5
10320	jira.properties	1	com.atlassian.jira.lookandfeel:faviconWidth	5
10321	jira.properties	1	com.atlassian.jira.lookandfeel:faviconHeight	5
10322	jira.properties	1	com.atlassian.jira.lookandfeel:build	5
10323	jira.properties	1	com.atlassian.jira.project-templates-plugin:build	5
10324	jira.properties	1	com.atlassian.jira.jira-mail-plugin:build	5
10325	jira.properties	1	com.atlassian.plugins.custom_apps.hasCustomOrder	5
10326	jira.properties	1	com.atlassian.plugins.atlassian-nav-links-plugin:build	5
10327	jira.properties	1	com.atlassian.jira.plugins.jira-workflow-designer:build	5
10328	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	GLOBAL_PULLER_JOB	1
10329	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	GLOBAL_PROCESSOR_JOB	1
10330	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	GLOBAL_CLEANER_AGE_THRESHOLD	2
10331	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	GLOBAL_CLEANER_MAX_AGE_THRESHOLD	2
10332	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	GLOBAL_CLEANER_MIN_AGE_THRESHOLD	2
10333	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	GLOBAL_CLEANER_THRESHOLD_OPTIONS	6
10334	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	MAIL_SIZE_LIMIT	2
10335	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	MAX_MAIL_PER_PULL	2
10336	JIRA_MAIL_PROCESSOR_GLOBAL_SETTING	1	PROCESSOR_MAIL_NUM_LIMIT	2
10337	jira.properties	1	com.atlassian.upm.log.PluginSettingsAuditLogService:log:upm_audit_log_v3	5
10338	jira.properties	1	com.atlassian.streams.InlineActivityStream:loaded.from.jira.projects	5
10339	automation.modules	1	servicedesk-automation-special-link-type	3
10340	jira.properties	1	com.atlassian.analytics.client.configuration..policy_acknowledged	5
10341	vp.properties	1	TASK_FIRST_INITIALISED_TIME_AsyncUpgradeTaskMigrateDateColumnsForTimeMetrics	3
10342	vp.properties	1	com.atlassian.servicedesk:sd-request-feedback	3
10343	vp.properties	1	com.atlassian.servicedesk:sd-request-feedback-date	3
10344	com.atlassian.servicedesk:Run.History	1	Run.History	6
10345	com.atlassian.servicedesk:Run.History	2	2019-06-17T03:11:33.555Z	6
10346	sd.notifications.plugin.properties	1	com.atlassian.servicedesk.servicedesk-notifications-plugin.notifications.upgrade.completed	1
10347	jira.properties	1	com.atlassian.upm:notifications:notification-edition.mismatch	5
10348	jira.properties	1	com.atlassian.upm:notifications:notification-plugin.request	5
10349	jira.properties	1	com.atlassian.upm:notifications:notification-evaluation.expired	5
10350	jira.properties	1	com.atlassian.upm:notifications:notification-evaluation.nearlyexpired	5
10351	jira.properties	1	com.atlassian.upm:notifications:notification-maintenance.expired	5
10352	jira.properties	1	com.atlassian.upm:notifications:notification-maintenance.nearlyexpired	5
10353	jira.properties	1	com.atlassian.upm:notifications:notification-license.expired	5
10354	jira.properties	1	com.atlassian.upm:notifications:notification-license.nearlyexpired	5
10355	jira.properties	1	com.atlassian.upm:notifications:notification-update	5
10356	ApplicationUser	10000	jira.onboarding.first.use.flow.started	5
10357	jira.properties	1	com.atlassian.jira.onboarding.postsetup.AppPropertiesPostSetupAnnouncementStore.all	6
10358	ApplicationUser	10000	newsletter.signup.first.view	3
10359	ApplicationUser	10000	jira.onboarding.first.use.flow.current.sequence	5
10360	ApplicationUser	10000	jira.onboarding.first.use.flow.completed	1
10400	jira.properties	1	com.atlassian.analytics.client.configuration..logged_base_analytics_data	5
10401	ApplicationUser	10000	com.atlassian.jira.flag.dismissals	6
10402	ApplicationUser	10000	jira.onboarding.first.use.flow.resolved	1
10403	jira.properties	1	org.apache.shindig.common.crypto.BlobCrypter:key	5
10404	jira.properties	1	jira.trustedapp.key.private.data	6
10405	jira.properties	1	jira.trustedapp.key.public.data	6
10406	jira.properties	1	jira.trustedapp.uid	5
10500	ApplicationUser	10000	jira.user.suppressedTips.qs-onboarding-tip	1
10501	com.atlassian.jira.plugins.jira-workflow-designer	1	jira.workflow.layout.v5:7bf86b1cc93f150a791e8eba5971489b	6
10502	vp.properties	1	com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker	3
10503	com.atlassian.jira.plugins.jira-workflow-designer	1	jira.workflow.layout.v5:aba1f9ae14495ac524e5ddc330b034c6	6
10504	com.atlassian.jira.plugins.jira-workflow-designer	1	jira.workflow.layout.v5:ba3acd3b7eead97e565048f681c65c2b	6
10505	ApplicationUser	10000	sd-onboarding-checklist-completion	5
\.


--
-- Data for Name: propertynumber; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY propertynumber (id, propertyvalue) FROM stdin;
9	0
11	1
12	0
15	1
16	1
23	1
10201	0
10212	1
10213	1
10216	10000
10217	10001
10218	10002
10226	0
10227	1
10228	1
10229	1
10222	1
10300	1
10304	10100
10328	1
10329	1
10330	45
10331	180
10332	14
10334	30000000
10335	50
10336	20
10339	10200
10341	1560741092646
10342	10101
10343	10102
10346	1
10358	1560741130859
10360	1
10223	0
10402	1
10500	1
10502	10200
\.


--
-- Data for Name: propertystring; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY propertystring (id, propertyvalue) FROM stdin;
2	10316
3	10011
4	10300
5	10122
6	10123
7	10000
22	0
10100	4
10103	B5T0-U5BT-3CKN-E98D
10104	10100
10105	1
10202	1
10203	
10204	1560740727177
10206	en_US
10102	english-moderate-stemming
10207	UTF-8
10208	Jira
10209	http://192.168.33.11:8080
10210	private
10211	/var/atlassian/application-data/jira/data/attachments
10215	/var/atlassian/application-data/jira/export
10214	true
10101	3
10224	true
10225	802002
10230	show
10301	00ea8d0d-1801-4ffe-84b0-d3a5fa914dc8
10302	B5T0-U5BT-3CKN-E98D
10303	true
10305	migrated
10306	4.2.2-REL-0002
10307	2
10309	1
10310	3
10311	3
10312	1
10313	6086
10314	5
10308	true
10315	4
10316	false
10317	false
10318	/favicon.ico
10319	/images/64jira.png
10320	64
10321	64
10200	6411e0087192541a09d88223fb51a6a0
10322	1
10323	2001
10324	2
10325	false
10326	1
10327	1
10347	#java.util.List\n
10348	#java.util.List\n
10349	#java.util.List\n
10350	#java.util.List\n
10351	#java.util.List\n
10352	#java.util.List\n
10353	#java.util.List\n
10354	#java.util.List\n
10356	cyoaFirstUseFlow
10359	cyoa
10205	UTC
10338	true
10340	true
10337	#java.util.List\n{"userKey":"JIRA","date":1560742111115,"i18nKey":"upm.auditLog.upm.startup","entryType":"UPM_STARTUP","params":[]}\n{"userKey":"JIRA","date":1560741091509,"i18nKey":"upm.auditLog.upm.startup","entryType":"UPM_STARTUP","params":[]}
10355	#java.util.List\n
10400	true
10403	Wv5jSfpAr9dcPabe+FY8/cKt28P7sObBrgIy8RAE4SI=
10406	jira:3986743
1	802002
13	8.2.2
14	7.1.2
10505	["project-settings"]
\.


--
-- Data for Name: propertytext; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY propertytext (id, propertyvalue) FROM stdin;
10	http://www.atlassian.com/*\n
21	{\n    "edgeMap": {\n        "1DEDB66F-FE5C-EDFD-54D0-4D19CDC8CECA": {\n            "actionId": 5,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1806.5,\n                    "y": 434.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1801.0,\n                    "y": 115.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "6DA64EEB-08FE-2870-C90C-4D19CDA2F72D",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1801.0,\n                "y": 115.0\n            },\n            "endStepId": 4,\n            "id": "1DEDB66F-FE5C-EDFD-54D0-4D19CDC8CECA",\n            "label": "Resolve Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1776.85,\n                "y": 355.25\n            },\n            "lineType": "straight",\n            "startNodeId": "A8B1A431-AC3A-6DCD-BFF0-4D19CDBCAADB",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1806.5,\n                "y": 434.0\n            },\n            "startStepId": 5\n        },\n        "3DF7CEC8-9FBC-C0D0-AFB1-4D19CE6EA230": {\n            "actionId": 2,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1469.5,\n                    "y": 113.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1614.0,\n                    "y": 226.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "1C846CFB-4F0D-2F40-D0AE-4D19CDAF5D34",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1614.0,\n                "y": 226.0\n            },\n            "endStepId": 6,\n            "id": "3DF7CEC8-9FBC-C0D0-AFB1-4D19CE6EA230",\n            "label": "Close Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1492.25,\n                "y": 154.25\n            },\n            "lineType": "straight",\n            "startNodeId": "778534F4-7595-88B6-45E1-4D19CD518712",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1469.5,\n                "y": 113.0\n            },\n            "startStepId": 1\n        },\n        "483797F1-1BF4-5E0F-86C6-4D19CE6023A2": {\n            "actionId": 5,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1469.5,\n                    "y": 113.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1763.0,\n                    "y": 113.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "6DA64EEB-08FE-2870-C90C-4D19CDA2F72D",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1763.0,\n                "y": 113.0\n            },\n            "endStepId": 4,\n            "id": "483797F1-1BF4-5E0F-86C6-4D19CE6023A2",\n            "label": "Resolve Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1551.0,\n                "y": 104.0\n            },\n            "lineType": "straight",\n            "startNodeId": "778534F4-7595-88B6-45E1-4D19CD518712",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1469.5,\n                "y": 113.0\n            },\n            "startStepId": 1\n        },\n        "517D7F32-20FB-309E-8639-4D19CE2ACB54": {\n            "actionId": 5,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1434.0,\n                    "y": 435.0\n                },\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1435.0,\n                    "y": 490.0\n                },\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1947.0,\n                    "y": 494.0\n                },\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1950.0,\n                    "y": 118.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1763.0,\n                    "y": 113.0\n                }\n            ],\n            "controlPoints": [\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1435.0,\n                    "y": 490.0\n                },\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1947.0,\n                    "y": 494.0\n                },\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1950.0,\n                    "y": 118.0\n                }\n            ],\n            "endNodeId": "6DA64EEB-08FE-2870-C90C-4D19CDA2F72D",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1763.0,\n                "y": 113.0\n            },\n            "endStepId": 4,\n            "id": "517D7F32-20FB-309E-8639-4D19CE2ACB54",\n            "label": "Resolve Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1631.25,\n                "y": 479.5\n            },\n            "lineType": "poly",\n            "startNodeId": "0740FFFA-2AA1-C90A-38ED-4D19CD61899B",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1434.0,\n                "y": 435.0\n            },\n            "startStepId": 3\n        },\n        "58BD4605-5FB9-84EA-6952-4D19CE7B454B": {\n            "actionId": 1,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1470.0,\n                    "y": 16.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1469.5,\n                    "y": 113.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "778534F4-7595-88B6-45E1-4D19CD518712",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1469.5,\n                "y": 113.0\n            },\n            "endStepId": 1,\n            "id": "58BD4605-5FB9-84EA-6952-4D19CE7B454B",\n            "label": "Create Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1475.5,\n                "y": 48.5\n            },\n            "lineType": "straight",\n            "startNodeId": "15174530-AE75-04E0-1D9D-4D19CD200835",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1470.0,\n                "y": 16.0\n            },\n            "startStepId": 1\n        },\n        "92D3DEFD-13AC-06A7-E5D8-4D19CE537791": {\n            "actionId": 4,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1439.5,\n                    "y": 116.0\n                },\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1393.0,\n                    "y": 116.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1390.0,\n                    "y": 434.0\n                }\n            ],\n            "controlPoints": [\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1393.0,\n                    "y": 116.0\n                }\n            ],\n            "endNodeId": "0740FFFA-2AA1-C90A-38ED-4D19CD61899B",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1390.0,\n                "y": 434.0\n            },\n            "endStepId": 3,\n            "id": "92D3DEFD-13AC-06A7-E5D8-4D19CE537791",\n            "label": "Start Progress",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1323.65,\n                "y": 193.75\n            },\n            "lineType": "poly",\n            "startNodeId": "778534F4-7595-88B6-45E1-4D19CD518712",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1439.5,\n                "y": 116.0\n            },\n            "startStepId": 1\n        },\n        "C049EE11-C5BB-F93B-36C3-4D19CDF12B8F": {\n            "actionId": 3,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1677.0,\n                    "y": 227.0\n                },\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1767.05,\n                    "y": 230.05\n                },\n                {\n                    "positiveController": null,\n                    "x": 1773.5,\n                    "y": 425.0\n                }\n            ],\n            "controlPoints": [\n                {\n                    "positiveController": {\n                        "positiveController": null,\n                        "x": 0.0,\n                        "y": 0.0\n                    },\n                    "x": 1767.05,\n                    "y": 230.05\n                }\n            ],\n            "endNodeId": "A8B1A431-AC3A-6DCD-BFF0-4D19CDBCAADB",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1773.5,\n                "y": 425.0\n            },\n            "endStepId": 5,\n            "id": "C049EE11-C5BB-F93B-36C3-4D19CDF12B8F",\n            "label": "Reopen Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1703.85,\n                "y": 218.5\n            },\n            "lineType": "poly",\n            "startNodeId": "1C846CFB-4F0D-2F40-D0AE-4D19CDAF5D34",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1677.0,\n                "y": 227.0\n            },\n            "startStepId": 6\n        },\n        "C9EA1792-2332-8B56-A04D-4D19CD725367": {\n            "actionId": 301,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1465.0,\n                    "y": 436.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1469.5,\n                    "y": 113.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "778534F4-7595-88B6-45E1-4D19CD518712",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1469.5,\n                "y": 113.0\n            },\n            "endStepId": 1,\n            "id": "C9EA1792-2332-8B56-A04D-4D19CD725367",\n            "label": "Stop Progress",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1407.8,\n                "y": 308.5\n            },\n            "lineType": "straight",\n            "startNodeId": "0740FFFA-2AA1-C90A-38ED-4D19CD61899B",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1465.0,\n                "y": 436.0\n            },\n            "startStepId": 3\n        },\n        "CAF37138-6321-E03A-8E41-4D19CDD7DC78": {\n            "actionId": 2,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1764.5,\n                    "y": 430.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1614.0,\n                    "y": 226.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "1C846CFB-4F0D-2F40-D0AE-4D19CDAF5D34",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1614.0,\n                "y": 226.0\n            },\n            "endStepId": 6,\n            "id": "CAF37138-6321-E03A-8E41-4D19CDD7DC78",\n            "label": "Close Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1677.65,\n                "y": 365.0\n            },\n            "lineType": "straight",\n            "startNodeId": "A8B1A431-AC3A-6DCD-BFF0-4D19CDBCAADB",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1764.5,\n                "y": 430.0\n            },\n            "startStepId": 5\n        },\n        "E1F8462A-8B0A-87EA-4F70-4D19CE423C83": {\n            "actionId": 2,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1488.0,\n                    "y": 430.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1614.0,\n                    "y": 226.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "1C846CFB-4F0D-2F40-D0AE-4D19CDAF5D34",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1614.0,\n                "y": 226.0\n            },\n            "endStepId": 6,\n            "id": "E1F8462A-8B0A-87EA-4F70-4D19CE423C83",\n            "label": "Close Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1492.0,\n                "y": 345.0\n            },\n            "lineType": "straight",\n            "startNodeId": "0740FFFA-2AA1-C90A-38ED-4D19CD61899B",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1488.0,\n                "y": 430.0\n            },\n            "startStepId": 3\n        },\n        "E27D8EB8-8E49-430B-8FCB-4D19CE127171": {\n            "actionId": 3,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1840.0,\n                    "y": 130.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1846.5,\n                    "y": 428.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "A8B1A431-AC3A-6DCD-BFF0-4D19CDBCAADB",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1846.5,\n                "y": 428.0\n            },\n            "endStepId": 5,\n            "id": "E27D8EB8-8E49-430B-8FCB-4D19CE127171",\n            "label": "Reopen Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1814.05,\n                "y": 169.5\n            },\n            "lineType": "straight",\n            "startNodeId": "6DA64EEB-08FE-2870-C90C-4D19CDA2F72D",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1840.0,\n                "y": 130.0\n            },\n            "startStepId": 4\n        },\n        "F79E742D-A9E4-0124-D7D4-4D19CDE48C9C": {\n            "actionId": 4,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1806.5,\n                    "y": 434.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1434.0,\n                    "y": 435.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "0740FFFA-2AA1-C90A-38ED-4D19CD61899B",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1434.0,\n                "y": 435.0\n            },\n            "endStepId": 3,\n            "id": "F79E742D-A9E4-0124-D7D4-4D19CDE48C9C",\n            "label": "Start Progress",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1607.25,\n                "y": 423.5\n            },\n            "lineType": "straight",\n            "startNodeId": "A8B1A431-AC3A-6DCD-BFF0-4D19CDBCAADB",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1806.5,\n                "y": 434.0\n            },\n            "startStepId": 5\n        },\n        "FD6BA267-475B-70B3-8AA4-4D19CE00BCD1": {\n            "actionId": 701,\n            "allPoints": [\n                {\n                    "positiveController": null,\n                    "x": 1763.0,\n                    "y": 113.0\n                },\n                {\n                    "positiveController": null,\n                    "x": 1614.0,\n                    "y": 226.0\n                }\n            ],\n            "controlPoints": [],\n            "endNodeId": "1C846CFB-4F0D-2F40-D0AE-4D19CDAF5D34",\n            "endPoint": {\n                "positiveController": null,\n                "x": 1614.0,\n                "y": 226.0\n            },\n            "endStepId": 6,\n            "id": "FD6BA267-475B-70B3-8AA4-4D19CE00BCD1",\n            "label": "Close Issue",\n            "labelPoint": {\n                "positiveController": null,\n                "x": 1635.75,\n                "y": 152.25\n            },\n            "lineType": "straight",\n            "startNodeId": "6DA64EEB-08FE-2870-C90C-4D19CDA2F72D",\n            "startPoint": {\n                "positiveController": null,\n                "x": 1763.0,\n                "y": 113.0\n            },\n            "startStepId": 4\n        }\n    },\n    "nodeMap": {\n        "0740FFFA-2AA1-C90A-38ED-4D19CD61899B": {\n            "id": "0740FFFA-2AA1-C90A-38ED-4D19CD61899B",\n            "inLinkIds": [\n                "F79E742D-A9E4-0124-D7D4-4D19CDE48C9C",\n                "92D3DEFD-13AC-06A7-E5D8-4D19CE537791"\n            ],\n            "isInitialAction": false,\n            "label": "In Progress",\n            "outLinkIds": [\n                "C9EA1792-2332-8B56-A04D-4D19CD725367",\n                "517D7F32-20FB-309E-8639-4D19CE2ACB54",\n                "E1F8462A-8B0A-87EA-4F70-4D19CE423C83"\n            ],\n            "rect": {\n                "height": 45.0,\n                "positiveController": null,\n                "width": 146.0,\n                "x": 1373.0,\n                "y": 419.0\n            },\n            "stepId": 3\n        },\n        "15174530-AE75-04E0-1D9D-4D19CD200835": {\n            "id": "15174530-AE75-04E0-1D9D-4D19CD200835",\n            "inLinkIds": [],\n            "isInitialAction": true,\n            "label": "Create Issue",\n            "outLinkIds": [\n                "58BD4605-5FB9-84EA-6952-4D19CE7B454B"\n            ],\n            "rect": {\n                "height": 45.0,\n                "positiveController": null,\n                "width": 157.0,\n                "x": 1405.0,\n                "y": 0.0\n            },\n            "stepId": 1\n        },\n        "1C846CFB-4F0D-2F40-D0AE-4D19CDAF5D34": {\n            "id": "1C846CFB-4F0D-2F40-D0AE-4D19CDAF5D34",\n            "inLinkIds": [\n                "CAF37138-6321-E03A-8E41-4D19CDD7DC78",\n                "E1F8462A-8B0A-87EA-4F70-4D19CE423C83",\n                "FD6BA267-475B-70B3-8AA4-4D19CE00BCD1",\n                "3DF7CEC8-9FBC-C0D0-AFB1-4D19CE6EA230"\n            ],\n            "isInitialAction": false,\n            "label": "Closed",\n            "outLinkIds": [\n                "C049EE11-C5BB-F93B-36C3-4D19CDF12B8F"\n            ],\n            "rect": {\n                "height": 45.0,\n                "positiveController": null,\n                "width": 120.0,\n                "x": 1569.0,\n                "y": 210.0\n            },\n            "stepId": 6\n        },\n        "6DA64EEB-08FE-2870-C90C-4D19CDA2F72D": {\n            "id": "6DA64EEB-08FE-2870-C90C-4D19CDA2F72D",\n            "inLinkIds": [\n                "517D7F32-20FB-309E-8639-4D19CE2ACB54",\n                "1DEDB66F-FE5C-EDFD-54D0-4D19CDC8CECA",\n                "483797F1-1BF4-5E0F-86C6-4D19CE6023A2"\n            ],\n            "isInitialAction": false,\n            "label": "Resolved",\n            "outLinkIds": [\n                "FD6BA267-475B-70B3-8AA4-4D19CE00BCD1",\n                "E27D8EB8-8E49-430B-8FCB-4D19CE127171"\n            ],\n            "rect": {\n                "height": 44.0,\n                "positiveController": null,\n                "width": 137.0,\n                "x": 1709.0,\n                "y": 97.0\n            },\n            "stepId": 4\n        },\n        "778534F4-7595-88B6-45E1-4D19CD518712": {\n            "id": "778534F4-7595-88B6-45E1-4D19CD518712",\n            "inLinkIds": [\n                "C9EA1792-2332-8B56-A04D-4D19CD725367",\n                "58BD4605-5FB9-84EA-6952-4D19CE7B454B"\n            ],\n            "isInitialAction": false,\n            "label": "Open",\n            "outLinkIds": [\n                "92D3DEFD-13AC-06A7-E5D8-4D19CE537791",\n                "483797F1-1BF4-5E0F-86C6-4D19CE6023A2",\n                "3DF7CEC8-9FBC-C0D0-AFB1-4D19CE6EA230"\n            ],\n            "rect": {\n                "height": 45.0,\n                "positiveController": null,\n                "width": 106.0,\n                "x": 1429.5,\n                "y": 97.0\n            },\n            "stepId": 1\n        },\n        "A8B1A431-AC3A-6DCD-BFF0-4D19CDBCAADB": {\n            "id": "A8B1A431-AC3A-6DCD-BFF0-4D19CDBCAADB",\n            "inLinkIds": [\n                "E27D8EB8-8E49-430B-8FCB-4D19CE127171",\n                "C049EE11-C5BB-F93B-36C3-4D19CDF12B8F"\n            ],\n            "isInitialAction": false,\n            "label": "Reopened",\n            "outLinkIds": [\n                "1DEDB66F-FE5C-EDFD-54D0-4D19CDC8CECA",\n                "CAF37138-6321-E03A-8E41-4D19CDD7DC78",\n                "F79E742D-A9E4-0124-D7D4-4D19CDE48C9C"\n            ],\n            "rect": {\n                "height": 45.0,\n                "positiveController": null,\n                "width": 142.0,\n                "x": 1749.5,\n                "y": 418.0\n            },\n            "stepId": 5\n        }\n    },\n    "rootIds": [\n        "15174530-AE75-04E0-1D9D-4D19CD200835"\n    ],\n    "width": 1136\n}\n
10108	{"com.atlassian.jira.reindex.required":1560740998941}
10333	[14,30,45,60,90,120,150,180]
10345	{"pluginVersion":"4.2.2-REL-0002","ranOn":"2019-06-17T03:11:33.548Z","buildDate":"2019-05-30T05:13:48.843Z","latestUpgradeTaskRun":"0","changeSet":"9183b678"}
10357	database.setup=AWAITS;app.properties.setup=FULLFILLED;create.user.mail.properties.setup=FULLFILLED;mail.properties.setup=FULLFILLED
10344	{"pluginVersion":"4.2.2-REL-0002","ranOn":"2019-06-17T03:28:33.486Z","buildDate":"2019-05-30T05:13:48.843Z","latestUpgradeTaskRun":"0","changeSet":"9183b678"}
10404	MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCedctUjeIP/8yCJG7X5GqCs2i8nz2LzTv02MveWWHfgqAdqwwZrw3wHDKWHfRnnO1tZ4ZCa8mPrIwLwg7IwAzfOOlyWzz3sEC+lctgfU3MT3ykuKk81Wl8Xp1+iSKhv8tVRPiwP1qd8ehLHu0Qtkoik7EFhXUvTzrwq18c2tgnO1bG9xR1Z4pZYi4Ia+osk/GM0wM18jVbkoMoi7TQYK9kL6cultCE5alk6FffCDyvpBvInJsMnjM1CCaYDTfq4n5U0LkAPyoDO8RFlN7S3qj9g3kwtVHZQfX/7bB6cSkPnxYmaL4FSW/fJ8Ycqp66De+ADjNa2Q50HPmtgZ97OqthAgMBAAECggEANPTM5Q8ds5cbM4sVeBUf+1oV8KOPH/+Qv/0L9N0ONnIjLIcTXioAkHP7++s62L27I0rrtZrsG5Xgn7VU14ZHRvpwGgWf1G8U1ByfgTmfHF6cGziTZn8z55p/K+CMsF22iGKqccxablp/9Ur6kxE4qWGOUXgOd6cU3YWIHc6NWl6R6rCHACWKPUUso2wzBr6cL4xMeuvdVgibQYMzFYp6+rInMf0zynSpc+X/6FYJxnwEEoH0SFUe3WXFxxI++3Bjp+GjCICaVbP3aqsNYbAZHZMGo6FK7t2WmYwFCx7ohkj60WMnfqa+5ss3/DesXshcnK0D52fYxmWLI6tE3intoQKBgQD/cZZWZOn/M4HoRYmx1pJ1DUHufxCYWU97xU04UUrfevt0IE6M87QUvQwJHu6DPiWM4P8t+WTL8O6wxoDiO3OK+yvspLZCS4T+7BqaCqXO32kOi3pVn5vJO2WOrOSCNkCHo8ecx7ylxkr11Q44Ez40zprOaOksEcptHy1tMn0sBQKBgQCeziM32ZhgK/v5o6R0AYypSVmhc8Mmd0l9OqIWkI2E24PkdgwqIxtTtU8FJ/bqOYhYvARMZBGCKq5tXqCUFPFta8TiToqgEGD89KXRtmo86Xrv6MRXUq39LrnBFFvGXttmTDtWMPhr9sEaWB52hOgMXBsvx/p+y7mZhBXGIEP8rQKBgC/ILIn+mHK/TMaI82fLrlus3i9hn1YQJrkNQBOPyUSieZTQUkE1ePMEJdMy8QSlxFpk3/bWls87QzrX9tXarxNJRn8elGU0QS16NsOnyDo5OiqZVonbsEVlEestVhUxVsLIlRczmYsc23q+nslIeDNijjycDTnMHiRpV5L8hcONAoGAIs20pDnZDkK1f3mgwhfQ1w0PSrYPL+/2uC5QdbeKfHKLjldcxSNaitbykYO3to+TVTcnjgJkX+2wuqGBedK5MqzqwlpKQVg1mVqYuMRMtjrJSaju1uB050cFSM6IaopBdUMBwhsLZsX2fiqPypPIM2VARBGE0aMecwmiqABMRiUCgYEAsVgk8rg7S46hnpe0sHnA87CJN0zJR1yQ1/cQ+UYA4vo2M6m3lsSho/QLddIVERb92JxvQc4ihO2odIUNTaGiciTIgdkq1Nwk5xAhZbk5fF21r4C2XYri4LzFoNX5ybaKdEBx0oKZ44Cj9hqszjg2qkb5RXUDZLnICIgessS/hng=
10405	MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnnXLVI3iD//MgiRu1+RqgrNovJ89i8079NjL3llh34KgHasMGa8N8Bwylh30Z5ztbWeGQmvJj6yMC8IOyMAM3zjpcls897BAvpXLYH1NzE98pLipPNVpfF6dfokiob/LVUT4sD9anfHoSx7tELZKIpOxBYV1L0868KtfHNrYJztWxvcUdWeKWWIuCGvqLJPxjNMDNfI1W5KDKIu00GCvZC+nLpbQhOWpZOhX3wg8r6QbyJybDJ4zNQgmmA036uJ+VNC5AD8qAzvERZTe0t6o/YN5MLVR2UH1/+2wenEpD58WJmi+BUlv3yfGHKqeug3vgA4zWtkOdBz5rYGfezqrYQIDAQAB
10501	{\n    "statuses": [\n        { "id": "S<10>", "x": -270.265625, "y": -198.1484375 },\n        { "id": "I<1>", "x": -470.53125, "y": -290.0 },\n        { "id": "S<14>", "x": -627.9495742304027, "y": 70.7687490463257 },\n        { "id": "S<11>", "x": -225.7578125, "y": -49.231250953674305 },\n        { "id": "S<13>", "x": -540.7265625, "y": -49.231250953674305 },\n        { "id": "S<8>", "x": -544.3046875, "y": -198.1484375 },\n        { "id": "S<16>", "x": -366.7578125, "y": 131.37498761713505 },\n        { "id": "S<12>", "x": -110.34374952316284, "y": 67.37498761713505 },\n        { "id": "S<15>", "x": -372.03124952316284, "y": -152.12187099456787 }\n    ],\n    "transitions": [\n        {\n            "id": "A<871:S<8>:S<11>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<8>",\n            "targetAngle": -154.88887877551932,\n            "targetId": "S<11>"\n        },\n        {\n            "id": "A<851:S<11>:S<10>>",\n            "sourceAngle": -70.01689347810002,\n            "sourceId": "S<11>",\n            "targetAngle": 78.11134196037203,\n            "targetId": "S<10>"\n        },\n        {\n            "id": "A<891:S<15>:S<13>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<15>",\n            "targetAngle": 3.458020947901192,\n            "targetId": "S<13>"\n        },\n        {\n            "id": "A<901:S<8>:S<14>>",\n            "sourceAngle": 177.29852571380187,\n            "sourceId": "S<8>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<14>"\n        },\n        {\n            "id": "IA<1:I<1>:S<8>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "I<1>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<8>"\n        },\n        {\n            "id": "A<891:S<11>:S<13>>",\n            "sourceAngle": 174.92652558324858,\n            "sourceId": "S<11>",\n            "targetAngle": 3.458020947901192,\n            "targetId": "S<13>"\n        },\n        {\n            "id": "A<781:S<10>:S<8>>",\n            "sourceAngle": 177.44278107666426,\n            "sourceId": "S<10>",\n            "targetAngle": 2.4688237007158302,\n            "targetId": "S<8>"\n        },\n        {\n            "id": "A<901:S<13>:S<14>>",\n            "sourceAngle": 176.06801282611218,\n            "sourceId": "S<13>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<14>"\n        },\n        {\n            "id": "A<851:S<8>:S<10>>",\n            "sourceAngle": 2.4688237007158302,\n            "sourceId": "S<8>",\n            "targetAngle": 177.44278107666426,\n            "targetId": "S<10>"\n        },\n        {\n            "id": "A<941:S<12>:S<16>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<12>",\n            "targetAngle": 4.492144589704093,\n            "targetId": "S<16>"\n        },\n        {\n            "id": "A<761:S<8>:S<12>>",\n            "sourceAngle": -11.573670140269954,\n            "sourceId": "S<8>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<921:S<8>:S<15>>",\n            "sourceAngle": 2.4688237007158302,\n            "sourceId": "S<8>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<15>"\n        },\n        {\n            "id": "A<891:S<8>:S<13>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<8>",\n            "targetAngle": -160.3634696591808,\n            "targetId": "S<13>"\n        },\n        {\n            "id": "A<901:S<10>:S<14>>",\n            "sourceAngle": -70.01689347810002,\n            "sourceId": "S<10>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<14>"\n        },\n        {\n            "id": "A<761:S<10>:S<12>>",\n            "sourceAngle": 2.347775348979277,\n            "sourceId": "S<10>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<761:S<11>:S<12>>",\n            "sourceAngle": 4.311595447066515,\n            "sourceId": "S<11>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<901:S<11>:S<14>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<11>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<14>"\n        },\n        {\n            "id": "A<911:S<10>:S<15>>",\n            "sourceAngle": 177.44278107666426,\n            "sourceId": "S<10>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<15>"\n        },\n        {\n            "id": "A<931:S<12>:S<13>>",\n            "sourceAngle": 175.41180155330412,\n            "sourceId": "S<12>",\n            "targetAngle": 26.07344000904233,\n            "targetId": "S<13>"\n        },\n        {\n            "id": "A<761:S<13>:S<12>>",\n            "sourceAngle": 26.07344000904233,\n            "sourceId": "S<13>",\n            "targetAngle": 175.41180155330412,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<941:S<14>:S<16>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<14>",\n            "targetAngle": 174.67487281477946,\n            "targetId": "S<16>"\n        }\n    ],\n    "updateAuthor": { "userName": "admin", "displayName": "admin" },\n    "updatedDate": 1477265502125,\n    "loopedTransitionContainer": null\n}
10503	{\n    "statuses": [\n        { "id": "S<13>", "x": -550.2943420410156, "y": -105.54050539433956 },\n        { "id": "S<10>", "x": -234.27505493164062, "y": -262.25 },\n        { "id": "S<15>", "x": -634.6594130975902, "y": -8.687512382864952 },\n        { "id": "S<16>", "x": -339.2385559082031, "y": -217.80417670607568 },\n        { "id": "S<12>", "x": -27.245086193084717, "y": -8.687512382864952 },\n        { "id": "S<8>", "x": -510.6604309082031, "y": -262.25 },\n        { "id": "I<1>", "x": -385.0, "y": -421.0 },\n        { "id": "S<14>", "x": -462.296875, "y": -357.6687509536743 },\n        { "id": "S<11>", "x": -155.46875, "y": -105.54050539433956 },\n        { "id": "S<17>", "x": -322.9651184082031, "y": 66.18725492060184 }\n    ],\n    "transitions": [\n        {\n            "id": "A<921:S<10>:S<15>>",\n            "sourceAngle": -167.19445465697783,\n            "sourceId": "S<10>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<15>"\n        },\n        {\n            "id": "A<921:S<13>:S<15>>",\n            "sourceAngle": 177.03168023483866,\n            "sourceId": "S<13>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<15>"\n        },\n        {\n            "id": "A<891:S<11>:S<13>>",\n            "sourceAngle": 174.92652558324858,\n            "sourceId": "S<11>",\n            "targetAngle": 2.6898407376428093,\n            "targetId": "S<13>"\n        },\n        {\n            "id": "A<871:S<8>:S<11>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<8>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<11>"\n        },\n        {\n            "id": "A<961:S<12>:S<17>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<12>",\n            "targetAngle": 4.492144589704093,\n            "targetId": "S<17>"\n        },\n        {\n            "id": "IA<1:I<1>:S<14>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "I<1>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<14>"\n        },\n        {\n            "id": "A<781:S<10>:S<8>>",\n            "sourceAngle": 177.44278107666426,\n            "sourceId": "S<10>",\n            "targetAngle": 2.4688237007158302,\n            "targetId": "S<8>"\n        },\n        {\n            "id": "A<851:S<11>:S<10>>",\n            "sourceAngle": -19.266170022040672,\n            "sourceId": "S<11>",\n            "targetAngle": 18.619149620969345,\n            "targetId": "S<10>"\n        },\n        {\n            "id": "A<901:S<14>:S<8>>",\n            "sourceAngle": 158.28483227364637,\n            "sourceId": "S<14>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<8>"\n        },\n        {\n            "id": "A<921:S<8>:S<15>>",\n            "sourceAngle": 177.29852571380187,\n            "sourceId": "S<8>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<15>"\n        },\n        {\n            "id": "A<851:S<8>:S<10>>",\n            "sourceAngle": 2.4688237007158302,\n            "sourceId": "S<8>",\n            "targetAngle": 177.44278107666426,\n            "targetId": "S<10>"\n        },\n        {\n            "id": "A<911:S<14>:S<12>>",\n            "sourceAngle": 2.3776334598428135,\n            "sourceId": "S<14>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<961:S<15>:S<17>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<15>",\n            "targetAngle": 174.67487281477946,\n            "targetId": "S<17>"\n        },\n        {\n            "id": "A<761:S<11>:S<12>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<11>",\n            "targetAngle": 175.41180155330412,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<761:S<8>:S<12>>",\n            "sourceAngle": -11.573670140269954,\n            "sourceId": "S<8>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<921:S<11>:S<15>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<11>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<15>"\n        },\n        {\n            "id": "A<891:S<8>:S<13>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<8>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<13>"\n        },\n        {\n            "id": "A<761:S<10>:S<12>>",\n            "sourceAngle": 2.347775348979277,\n            "sourceId": "S<10>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<951:S<12>:S<13>>",\n            "sourceAngle": 175.41180155330412,\n            "sourceId": "S<12>",\n            "targetAngle": 78.11134196037203,\n            "targetId": "S<13>"\n        },\n        {\n            "id": "A<931:S<8>:S<16>>",\n            "sourceAngle": 2.4688237007158302,\n            "sourceId": "S<8>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<16>"\n        },\n        {\n            "id": "A<761:S<13>:S<12>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<13>",\n            "targetAngle": 175.41180155330412,\n            "targetId": "S<12>"\n        },\n        {\n            "id": "A<921:S<14>:S<15>>",\n            "sourceAngle": 177.40732169058882,\n            "sourceId": "S<14>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<15>"\n        },\n        {\n            "id": "A<941:S<10>:S<16>>",\n            "sourceAngle": 177.44278107666426,\n            "sourceId": "S<10>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<16>"\n        },\n        {\n            "id": "A<891:S<16>:S<13>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<16>",\n            "targetAngle": 2.6898407376428093,\n            "targetId": "S<13>"\n        }\n    ],\n    "updateAuthor": { "userName": "admin", "displayName": "admin" },\n    "updatedDate": 1477266391440,\n    "loopedTransitionContainer": null\n}
10504	{\n    "statuses": [\n        { "id": "S<3>", "x": 636.359375, "y": 166.09999755859377 },\n        { "id": "S<7>", "x": 861.98828125, "y": 166.09999755859377 },\n        { "id": "I<1>", "x": 702.5, "y": -35.19999999999999 },\n        { "id": "S<2>", "x": 491.38671875, "y": 166.09999755859377 },\n        { "id": "S<1>", "x": 677.9140625, "y": 59.80000000000001 },\n        { "id": "S<6>", "x": 662.6328125, "y": 276.09999755859377 }\n    ],\n    "transitions": [\n        {\n            "id": "A<51:S<2>:S<1>>",\n            "sourceAngle": -70.01689347810002,\n            "sourceId": "S<2>",\n            "targetAngle": 173.58664771095746,\n            "targetId": "S<1>"\n        },\n        {\n            "id": "A<11:S<1>:S<3>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<1>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<3>"\n        },\n        {\n            "id": "A<41:S<1>:S<2>>",\n            "sourceAngle": 173.58664771095746,\n            "sourceId": "S<1>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<2>"\n        },\n        {\n            "id": "A<61:S<1>:S<7>>",\n            "sourceAngle": 5.243501734154274,\n            "sourceId": "S<1>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<7>"\n        },\n        {\n            "id": "A<11:S<2>:S<3>>",\n            "sourceAngle": 4.311595447066515,\n            "sourceId": "S<2>",\n            "targetAngle": 177.03168023483866,\n            "targetId": "S<3>"\n        },\n        {\n            "id": "A<41:S<3>:S<2>>",\n            "sourceAngle": 177.03168023483866,\n            "sourceId": "S<3>",\n            "targetAngle": 4.311595447066515,\n            "targetId": "S<2>"\n        },\n        {\n            "id": "A<61:S<6>:S<7>>",\n            "sourceAngle": 3.8872481919852424,\n            "sourceId": "S<6>",\n            "targetAngle": 78.11134196037203,\n            "targetId": "S<7>"\n        },\n        {\n            "id": "IA<1:I<1>:S<1>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "I<1>",\n            "targetAngle": -70.01689347810002,\n            "targetId": "S<1>"\n        },\n        {\n            "id": "A<11:S<6>:S<3>>",\n            "sourceAngle": -70.01689347810002,\n            "sourceId": "S<6>",\n            "targetAngle": 78.11134196037203,\n            "targetId": "S<3>"\n        },\n        {\n            "id": "A<71:S<7>:S<6>>",\n            "sourceAngle": 78.11134196037203,\n            "sourceId": "S<7>",\n            "targetAngle": 3.8872481919852424,\n            "targetId": "S<6>"\n        },\n        {\n            "id": "A<51:S<3>:S<1>>",\n            "sourceAngle": -70.01689347810002,\n            "sourceId": "S<3>",\n            "targetAngle": 78.11134196037203,\n            "targetId": "S<1>"\n        },\n        {\n            "id": "A<41:S<6>:S<2>>",\n            "sourceAngle": 175.50373278721605,\n            "sourceId": "S<6>",\n            "targetAngle": 78.11134196037203,\n            "targetId": "S<2>"\n        },\n        {\n            "id": "A<61:S<3>:S<7>>",\n            "sourceAngle": 2.6898407376428093,\n            "sourceId": "S<3>",\n            "targetAngle": 173.63927164256077,\n            "targetId": "S<7>"\n        }\n    ],\n    "updateAuthor": { "userName": "admin", "displayName": "admin" },\n    "updatedDate": 1482972967794,\n    "loopedTransitionContainer": null\n}
10401	{"com.atlassian.jira.tzdetect.-21600000,-18000000":1560742190330}
\.


--
-- Data for Name: qrtz_calendars; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY qrtz_calendars (id, calendar_name, calendar) FROM stdin;
\.


--
-- Data for Name: qrtz_cron_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY qrtz_cron_triggers (id, trigger_id, cronexperssion) FROM stdin;
\.


--
-- Data for Name: qrtz_fired_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY qrtz_fired_triggers (id, entry_id, trigger_id, trigger_listener, fired_time, trigger_state) FROM stdin;
\.


--
-- Data for Name: qrtz_job_details; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY qrtz_job_details (id, job_name, job_group, class_name, is_durable, is_stateful, requests_recovery, job_data) FROM stdin;
\.


--
-- Data for Name: qrtz_job_listeners; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY qrtz_job_listeners (id, job, job_listener) FROM stdin;
\.


--
-- Data for Name: qrtz_simple_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY qrtz_simple_triggers (id, trigger_id, repeat_count, repeat_interval, times_triggered) FROM stdin;
\.


--
-- Data for Name: qrtz_trigger_listeners; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY qrtz_trigger_listeners (id, trigger_id, trigger_listener) FROM stdin;
\.


--
-- Data for Name: qrtz_triggers; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY qrtz_triggers (id, trigger_name, trigger_group, job, next_fire, trigger_state, trigger_type, start_time, end_time, calendar_name, misfire_instr) FROM stdin;
\.


--
-- Data for Name: reindex_component; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY reindex_component (id, request_id, affected_index, entity_type) FROM stdin;
10100	10000	ISSUE	NONE
10101	10000	COMMENT	NONE
10102	10000	CHANGEHISTORY	NONE
10103	10000	WORKLOG	NONE
\.


--
-- Data for Name: reindex_request; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY reindex_request (id, type, request_time, start_time, completion_time, status, execution_node_id, query) FROM stdin;
10000	IMMEDIATE	2019-06-17 03:05:25.229+00	2019-06-17 03:11:20.451+00	2019-06-17 03:11:21.277+00	COMPLETE	\N	\N
\.


--
-- Data for Name: remembermetoken; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY remembermetoken (id, created, token, username) FROM stdin;
\.


--
-- Data for Name: remotelink; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY remotelink (id, issueid, globalid, title, summary, url, iconurl, icontitle, relationship, resolved, statusname, statusdescription, statusiconurl, statusicontitle, statusiconlink, statuscategorykey, statuscategorycolorname, applicationtype, applicationname) FROM stdin;
\.


--
-- Data for Name: replicatedindexoperation; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY replicatedindexoperation (id, index_time, node_id, affected_index, entity_type, affected_ids, operation, filename) FROM stdin;
\.


--
-- Data for Name: resolution; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY resolution (id, sequence, pname, description, iconurl) FROM stdin;
10000	1	Done	Work has been completed on this issue.	\N
10001	2	Won't Do	This issue won't be actioned.	\N
10002	3	Duplicate	The problem is a duplicate of an existing issue.	\N
10003	4	Declined	This issue was not approved.	\N
\.


--
-- Data for Name: rundetails; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY rundetails (id, job_id, start_time, run_duration, run_outcome, info_message) FROM stdin;
10002	com.atlassian.jira.service.JiraService:10001	2019-06-17 03:06:04.331+00	15190	S	
10003	d6ab377f-6e88-42ef-acb6-ae54bcbec763	2019-06-17 03:05:27.231+00	55042	S	
10105	PluginRequestCheckJob-job	2019-06-17 03:11:33.896+00	109	S	
10106	LocalPluginLicenseNotificationJob-job	2019-06-17 03:11:33.888+00	240	S	
10200	com.atlassian.jira.user.UserHistoryDatabaseCompactor	2019-06-17 03:28:39.936+00	14	S	
10201	com.atlassian.jira.cache.monitor.CacheStatisticsMonitor	2019-06-17 03:28:39.933+00	12	S	
10202	com.atlassian.scheduler.timedpromise.internal.TimedPromiseExecutor.job	2019-06-17 03:28:40.02+00	32	S	
10203	com.atlassian.scheduler.timedpromise.internal.TimedPromisePruner	2019-06-17 03:28:39.94+00	141	S	
10204	BundledUpdateCheckJob-job	2019-06-17 03:28:40.073+00	14	S	
10205	com.atlassian.scheduler.timedpromise.TimedPromiseHistoryPruner	2019-06-17 03:28:40.019+00	101	S	
10206	applink-status-analytics-job	2019-06-17 03:28:40.052+00	121	S	
10207	70671cb8-ded2-4028-b34a-81748f264690	2019-06-17 03:28:40.144+00	774	S	
10208	JiraPluginScheduler:com.atlassian.analytics.client.upload.RemoteFilterRead:job	2019-06-17 03:28:44.44+00	4088	S	
10210	CompatibilityPluginScheduler.JobId.hipchatRefreshConnectionStatusJob	2019-06-17 03:28:55.27+00	140	S	
10211	CompatibilityPluginScheduler.JobId.hipchatInstallGlancesJob	2019-06-17 03:28:58.657+00	139	S	
10213	b88b1093-8e3e-4c99-8164-838d405d260a	2019-06-17 03:28:40.153+00	37207	S	
10302	com.atlassian.jira.internal.mail.services.MailCleanerJobRunner	2019-06-17 03:29:30.748+00	1126	S	
10304	com.atlassian.jira.upgrade.UpgradeService	2019-06-17 03:29:30.189+00	2666	S	
10305	sd.custom.notification.batch.send	2019-06-17 03:29:34.803+00	121	S	
10306	JiraPluginScheduler:com.atlassian.troubleshooting.healthcheck.scheduler.HealthCheckSchedulerImpl:job	2019-06-17 03:29:35.17+00	3205	S	
10145	com.atlassian.diagnostics.internal.analytics.DailyAlertAnalyticsJob	2019-06-17 03:19:00.01+00	302	S	
10317	e440452d-df4d-4778-91d8-4d939143b5ab	2019-06-17 03:31:40.051+00	6098	S	
10325	com.atlassian.jira.plugins.inform.batching.cron.BatchNotificationJobSchedulerImpl	2019-06-17 03:33:30.797+00	75	S	
10329	com.atlassian.servicedesk.plugins.automation.internal.execution.engine.executors.PsmqOutstandingJobsScheduler	2019-06-17 03:33:35.774+00	235	S	
10330	com.atlassian.jira.service.JiraService:10000	2019-06-17 03:34:00.001+00	17	S	
10331	CompatibilityPluginScheduler.JobId.hipchatUpdateGlancesDataJob	2019-06-17 03:34:13.006+00	12	S	
10332	com.atlassian.jira.internal.mail.services.MailPullerJobRunner	2019-06-17 03:34:30.807+00	67	S	
10333	com.atlassian.jira.internal.mail.services.MailProcessorJobRunner	2019-06-17 03:34:30.806+00	67	S	
10334	com.atlassian.jira.plugins.inform.batching.cron.BatchNotificationJobSchedulerImpl.mentions	2019-06-17 03:34:32.968+00	14	S	
\.


--
-- Data for Name: schemeissuesecurities; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY schemeissuesecurities (id, scheme, security, sec_type, sec_parameter) FROM stdin;
\.


--
-- Data for Name: schemeissuesecuritylevels; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY schemeissuesecuritylevels (id, name, description, scheme) FROM stdin;
\.


--
-- Data for Name: schemepermissions; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY schemepermissions (id, scheme, permission, perm_type, perm_parameter, permission_key) FROM stdin;
10025	0	32	projectrole	10002	MANAGE_WATCHERS
10026	0	34	projectrole	10002	EDIT_ALL_COMMENTS
10027	0	35	applicationRole		EDIT_OWN_COMMENTS
10028	0	36	projectrole	10002	DELETE_ALL_COMMENTS
10029	0	37	applicationRole		DELETE_OWN_COMMENTS
10030	0	38	projectrole	10002	DELETE_ALL_ATTACHMENTS
10031	0	39	applicationRole		DELETE_OWN_ATTACHMENTS
10033	0	29	applicationRole		VIEW_DEV_TOOLS
10101	\N	44	group	jira-administrators	\N
10200	0	45	applicationRole		VIEW_READONLY_WORKFLOW
10300	0	46	applicationRole		TRANSITION_ISSUES
10000	\N	0	group	jira-administrators	\N
10004	0	23	projectrole	10002	ADMINISTER_PROJECTS
10005	0	10	applicationRole		BROWSE_PROJECTS
10006	0	11	applicationRole		CREATE_ISSUES
10007	0	15	applicationRole		ADD_COMMENTS
10008	0	19	applicationRole		CREATE_ATTACHMENTS
10009	0	13	applicationRole		ASSIGN_ISSUES
10010	0	17	applicationRole		ASSIGNABLE_USER
10011	0	14	applicationRole		RESOLVE_ISSUES
10012	0	21	applicationRole		LINK_ISSUES
10013	0	12	applicationRole		EDIT_ISSUES
10014	0	16	projectrole	10002	DELETE_ISSUES
10015	0	18	applicationRole		CLOSE_ISSUES
10016	0	25	applicationRole		MOVE_ISSUES
10017	0	28	applicationRole		SCHEDULE_ISSUES
10018	0	30	projectrole	10002	MODIFY_REPORTER
10019	0	20	applicationRole		WORK_ON_ISSUES
10020	0	43	projectrole	10002	DELETE_ALL_WORKLOGS
10021	0	42	applicationRole		DELETE_OWN_WORKLOGS
10022	0	41	projectrole	10002	EDIT_ALL_WORKLOGS
10023	0	40	applicationRole		EDIT_OWN_WORKLOGS
10024	0	31	applicationRole		VIEW_VOTERS_AND_WATCHERS
10400	10000	\N	projectrole	10002	ADMINISTER_PROJECTS
10401	10000	\N	projectrole	10002	SERVICEDESK_AGENT
10402	10000	\N	projectrole	10002	BROWSE_PROJECTS
10403	10000	\N	projectrole	10002	VIEW_DEV_TOOLS
10404	10000	\N	projectrole	10002	VIEW_READONLY_WORKFLOW
10405	10000	\N	projectrole	10002	CREATE_ISSUES
10406	10000	\N	projectrole	10002	EDIT_ISSUES
10407	10000	\N	projectrole	10002	SCHEDULE_ISSUES
10408	10000	\N	projectrole	10002	MOVE_ISSUES
10409	10000	\N	projectrole	10002	ASSIGN_ISSUES
10410	10000	\N	projectrole	10002	ASSIGNABLE_USER
10411	10000	\N	projectrole	10002	RESOLVE_ISSUES
10412	10000	\N	projectrole	10002	CLOSE_ISSUES
10413	10000	\N	projectrole	10002	MODIFY_REPORTER
10414	10000	\N	projectrole	10002	DELETE_ISSUES
10415	10000	\N	projectrole	10002	LINK_ISSUES
10416	10000	\N	projectrole	10002	TRANSITION_ISSUES
10417	10000	\N	projectrole	10002	SET_ISSUE_SECURITY
10418	10000	\N	projectrole	10002	VIEW_VOTERS_AND_WATCHERS
10419	10000	\N	projectrole	10002	MANAGE_WATCHERS
10420	10000	\N	projectrole	10002	ADD_COMMENTS
10421	10000	\N	projectrole	10002	EDIT_ALL_COMMENTS
10422	10000	\N	projectrole	10002	EDIT_OWN_COMMENTS
10423	10000	\N	projectrole	10002	DELETE_OWN_COMMENTS
10424	10000	\N	projectrole	10002	DELETE_ALL_COMMENTS
10425	10000	\N	projectrole	10002	DELETE_ALL_ATTACHMENTS
10426	10000	\N	projectrole	10002	CREATE_ATTACHMENTS
10427	10000	\N	projectrole	10002	DELETE_OWN_ATTACHMENTS
10428	10000	\N	projectrole	10002	WORK_ON_ISSUES
10429	10000	\N	projectrole	10002	EDIT_OWN_WORKLOGS
10430	10000	\N	projectrole	10002	DELETE_OWN_WORKLOGS
10431	10000	\N	projectrole	10002	EDIT_ALL_WORKLOGS
10432	10000	\N	projectrole	10002	DELETE_ALL_WORKLOGS
10433	10000	\N	projectrole	10101	BROWSE_PROJECTS
10434	10000	\N	projectrole	10101	SERVICEDESK_AGENT
10435	10000	\N	projectrole	10101	VIEW_READONLY_WORKFLOW
10436	10000	\N	projectrole	10101	CREATE_ISSUES
10437	10000	\N	projectrole	10101	EDIT_ISSUES
10438	10000	\N	projectrole	10101	SCHEDULE_ISSUES
10439	10000	\N	projectrole	10101	MOVE_ISSUES
10440	10000	\N	projectrole	10101	ASSIGN_ISSUES
10441	10000	\N	projectrole	10101	ASSIGNABLE_USER
10442	10000	\N	projectrole	10101	RESOLVE_ISSUES
10443	10000	\N	projectrole	10101	CLOSE_ISSUES
10444	10000	\N	projectrole	10101	MODIFY_REPORTER
10445	10000	\N	projectrole	10101	DELETE_ISSUES
10446	10000	\N	projectrole	10101	LINK_ISSUES
10447	10000	\N	projectrole	10101	TRANSITION_ISSUES
10448	10000	\N	projectrole	10101	SET_ISSUE_SECURITY
10449	10000	\N	projectrole	10101	VIEW_VOTERS_AND_WATCHERS
10450	10000	\N	projectrole	10101	MANAGE_WATCHERS
10451	10000	\N	projectrole	10101	ADD_COMMENTS
10452	10000	\N	projectrole	10101	EDIT_ALL_COMMENTS
10453	10000	\N	projectrole	10101	EDIT_OWN_COMMENTS
10454	10000	\N	projectrole	10101	DELETE_OWN_COMMENTS
10455	10000	\N	projectrole	10101	DELETE_ALL_COMMENTS
10456	10000	\N	projectrole	10101	DELETE_ALL_ATTACHMENTS
10457	10000	\N	projectrole	10101	CREATE_ATTACHMENTS
10458	10000	\N	projectrole	10101	DELETE_OWN_ATTACHMENTS
10459	10000	\N	projectrole	10101	WORK_ON_ISSUES
10460	10000	\N	projectrole	10101	EDIT_OWN_WORKLOGS
10461	10000	\N	projectrole	10101	DELETE_OWN_WORKLOGS
10462	10000	\N	sd.customer.portal.only	\N	BROWSE_PROJECTS
10463	10000	\N	sd.customer.portal.only	\N	CREATE_ISSUES
10464	10000	\N	sd.customer.portal.only	\N	EDIT_ISSUES
10465	10000	\N	sd.customer.portal.only	\N	SCHEDULE_ISSUES
10466	10000	\N	sd.customer.portal.only	\N	MOVE_ISSUES
10467	10000	\N	sd.customer.portal.only	\N	ASSIGN_ISSUES
10468	10000	\N	sd.customer.portal.only	\N	RESOLVE_ISSUES
10469	10000	\N	sd.customer.portal.only	\N	CLOSE_ISSUES
10470	10000	\N	sd.customer.portal.only	\N	MODIFY_REPORTER
10471	10000	\N	sd.customer.portal.only	\N	DELETE_ISSUES
10472	10000	\N	sd.customer.portal.only	\N	LINK_ISSUES
10473	10000	\N	sd.customer.portal.only	\N	TRANSITION_ISSUES
10474	10000	\N	sd.customer.portal.only	\N	SET_ISSUE_SECURITY
10475	10000	\N	sd.customer.portal.only	\N	VIEW_VOTERS_AND_WATCHERS
10476	10000	\N	sd.customer.portal.only	\N	MANAGE_WATCHERS
10477	10000	\N	sd.customer.portal.only	\N	ADD_COMMENTS
10478	10000	\N	sd.customer.portal.only	\N	EDIT_OWN_COMMENTS
10479	10000	\N	sd.customer.portal.only	\N	DELETE_OWN_COMMENTS
10480	10000	\N	sd.customer.portal.only	\N	CREATE_ATTACHMENTS
10481	10000	\N	sd.customer.portal.only	\N	DELETE_OWN_ATTACHMENTS
\.


--
-- Data for Name: searchrequest; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY searchrequest (id, filtername, authorname, description, username, groupname, projectid, reqcontent, fav_count, filtername_lower) FROM stdin;
\.


--
-- Data for Name: sequence_value_item; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY sequence_value_item (seq_name, seq_id) FROM stdin;
ApplicationUser	10100
User	10100
Membership	10100
FieldLayoutSchemeEntity	10100
LicenseRoleDefault	10100
MailServer	10100
ReindexComponent	10200
TaskIdSequence	10100
AuditLog	10400
Component	10100
AuditChangedValue	10600
ManagedConfigurationItem	10300
ProjectRole	10300
OSWorkflowEntry	10100
OSCurrentStep	10100
IssueLinkType	10300
Issue	10100
EventType	10000
GadgetUserPreference	10100
GenericConfiguration	10100
NotificationScheme	10100
OAuthConsumer	10100
PortalPage	10100
PortletConfiguration	10100
ServiceConfig	10200
SharePermissions	10100
CustomFieldValue	10100
UpgradeHistory	10200
UserAttribute	10300
RunDetails	10400
EntityProperty	10100
OSPropertyEntry	10600
ReindexRequest	10100
ClusteredJob	10500
UserHistoryItem	10200
Notification	10300
UpgradeVersionHistory	10100
EntityPropertyIndexDocument	10100
ListenerConfig	10300
ProductLicense	10100
Group	10110
LicenseRoleGroup	10100
GlobalPermissionEntry	10200
UpgradeTaskHistory	10200
UpgradeTaskHistoryAuditLog	10200
Feature	10300
Project	10100
PluginVersion	10300
ProjectKey	10100
ProjectRoleActor	10200
IssueType	10100
Avatar	10800
FieldConfiguration	10500
FieldConfigScheme	10500
FieldConfigSchemeIssueType	10600
OptionConfiguration	10400
ConfigurationContext	10500
FieldScreen	10100
FieldScreenTab	10200
FieldScreenLayoutItem	10300
FieldScreenScheme	10100
FieldScreenSchemeItem	10200
IssueTypeScreenScheme	10100
IssueTypeScreenSchemeEntity	10200
Resolution	10100
PermissionScheme	10100
SchemePermissions	10500
AuditItem	10200
Status	10100
WorkflowScheme	10200
Workflow	10200
CustomField	10300
WorkflowSchemeEntity	10200
Priority	10100
FieldLayout	10200
FieldLayoutItem	10300
FieldLayoutScheme	10100
\.


--
-- Data for Name: serviceconfig; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY serviceconfig (id, delaytime, clazz, servicename, cron_expression) FROM stdin;
10000	60000	com.atlassian.jira.service.services.mail.MailQueueService	Mail Queue Service	0 * * * * ?
10002	86400000	com.atlassian.jira.service.services.auditing.AuditLogCleaningService	Audit log cleaning service	0 0 0 * * ?
10001	43200000	com.atlassian.jira.service.services.export.ExportService	Backup Service	0 5 3/12 * * ?
\.


--
-- Data for Name: sharepermissions; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY sharepermissions (id, entityid, entitytype, sharetype, param1, param2, rights) FROM stdin;
10000	10000	PortalPage	global	\N	\N	1
\.


--
-- Data for Name: tempattachmentsmonitor; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY tempattachmentsmonitor (temporary_attachment_id, form_token, file_name, content_type, file_size, created_time) FROM stdin;
\.


--
-- Data for Name: trackback_ping; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY trackback_ping (id, issue, url, title, blogname, excerpt, created) FROM stdin;
\.


--
-- Data for Name: trustedapp; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY trustedapp (id, application_id, name, public_key, ip_match, url_match, timeout, created, created_by, updated, updated_by) FROM stdin;
\.


--
-- Data for Name: upgradehistory; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY upgradehistory (id, upgradeclass, targetbuild, status, downgradetaskrequired) FROM stdin;
10000	com.atlassian.jira.upgrade.tasks.UpgradeTask_Build70100	70100	complete	Y
10100	com.atlassian.jira.upgrade.tasks.UpgradeTask_Build70101	70101	complete	N
10101	com.atlassian.jira.upgrade.tasks.UpgradeTask_Build70102	70102	complete	N
10102	com.atlassian.jira.upgrade.tasks.UpgradeTask_Build71001	71001	complete	N
10103	com.atlassian.jira.upgrade.tasks.UpgradeTask_Build72001	72001	complete	N
10104	com.atlassian.jira.upgrade.tasks.UpgradeTask_Build72002	72002	complete	N
10105	com.atlassian.jira.upgrade.tasks.UpgradeTask_Build73002	73002	complete	N
\.


--
-- Data for Name: upgradetaskhistory; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY upgradetaskhistory (id, upgrade_task_factory_key, build_number, status, upgrade_type) FROM stdin;
10006	host	73010	COMPLETED	SERVER
10007	host	73011	COMPLETED	SERVER
10008	host	74002	COMPLETED	SERVER
10009	host	75002	COMPLETED	SERVER
10010	host	75003	COMPLETED	SERVER
10011	host	75004	COMPLETED	SERVER
10012	host	75005	COMPLETED	SERVER
10013	host	76001	COMPLETED	SERVER
10014	host	77001	COMPLETED	SERVER
10015	host	710001	COMPLETED	SERVER
10016	host	711001	COMPLETED	SERVER
10017	host	712001	COMPLETED	SERVER
10018	host	800000	COMPLETED	SERVER
10019	host	800003	COMPLETED	SERVER
10020	host	800004	COMPLETED	SERVER
10021	host	800005	COMPLETED	SERVER
10022	host	800006	COMPLETED	SERVER
10023	host	800007	COMPLETED	SERVER
10024	host	800009	COMPLETED	SERVER
10025	host	801000	COMPLETED	SERVER
10100	com.atlassian.servicedesk	1	COMPLETED	SERVER
10101	com.atlassian.servicedesk	2	COMPLETED	SERVER
10102	com.atlassian.servicedesk	3	COMPLETED	SERVER
10103	com.atlassian.servicedesk	7	COMPLETED	SERVER
10104	com.atlassian.servicedesk	8	COMPLETED	SERVER
10105	com.atlassian.servicedesk	9	COMPLETED	SERVER
10106	com.atlassian.servicedesk	10	COMPLETED	SERVER
10107	com.atlassian.servicedesk	11	COMPLETED	SERVER
10108	com.atlassian.servicedesk	12	COMPLETED	SERVER
\.


--
-- Data for Name: upgradetaskhistoryauditlog; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY upgradetaskhistoryauditlog (id, upgrade_task_factory_key, build_number, status, upgrade_type, timeperformed, action) FROM stdin;
10000	host	70101	COMPLETED	SERVER	2019-06-17 03:05:23.643+00	upgrade
10001	host	70102	COMPLETED	SERVER	2019-06-17 03:05:23.74+00	upgrade
10002	host	71001	COMPLETED	SERVER	2019-06-17 03:05:23.81+00	upgrade
10003	host	72001	COMPLETED	SERVER	2019-06-17 03:05:23.839+00	upgrade
10004	host	72002	COMPLETED	SERVER	2019-06-17 03:05:23.929+00	upgrade
10005	host	73002	COMPLETED	SERVER	2019-06-17 03:05:23.969+00	upgrade
10006	host	73010	COMPLETED	SERVER	2019-06-17 03:05:24.025+00	upgrade
10007	host	73011	COMPLETED	SERVER	2019-06-17 03:05:24.053+00	upgrade
10008	host	74002	COMPLETED	SERVER	2019-06-17 03:05:24.099+00	upgrade
10009	host	75002	COMPLETED	SERVER	2019-06-17 03:05:24.129+00	upgrade
10010	host	75003	COMPLETED	SERVER	2019-06-17 03:05:24.165+00	upgrade
10011	host	75004	COMPLETED	SERVER	2019-06-17 03:05:24.194+00	upgrade
10012	host	75005	COMPLETED	SERVER	2019-06-17 03:05:24.22+00	upgrade
10013	host	76001	COMPLETED	SERVER	2019-06-17 03:05:24.613+00	upgrade
10014	host	77001	COMPLETED	SERVER	2019-06-17 03:05:24.831+00	upgrade
10015	host	710001	COMPLETED	SERVER	2019-06-17 03:05:24.959+00	upgrade
10016	host	711001	COMPLETED	SERVER	2019-06-17 03:05:25.036+00	upgrade
10017	host	712001	COMPLETED	SERVER	2019-06-17 03:05:25.072+00	upgrade
10018	host	800000	COMPLETED	SERVER	2019-06-17 03:05:25.1+00	upgrade
10019	host	800003	COMPLETED	SERVER	2019-06-17 03:05:25.223+00	upgrade
10020	host	800004	COMPLETED	SERVER	2019-06-17 03:05:25.306+00	upgrade
10021	host	800005	COMPLETED	SERVER	2019-06-17 03:05:25.33+00	upgrade
10022	host	800006	COMPLETED	SERVER	2019-06-17 03:05:25.365+00	upgrade
10023	host	800007	COMPLETED	SERVER	2019-06-17 03:05:25.406+00	upgrade
10024	host	800009	COMPLETED	SERVER	2019-06-17 03:05:25.448+00	upgrade
10025	host	801000	COMPLETED	SERVER	2019-06-17 03:05:25.572+00	upgrade
10100	com.atlassian.servicedesk	1	COMPLETED	SERVER	2019-06-17 03:09:57.931+00	upgrade
10101	com.atlassian.servicedesk	2	COMPLETED	SERVER	2019-06-17 03:09:58.37+00	upgrade
10102	com.atlassian.servicedesk	3	COMPLETED	SERVER	2019-06-17 03:09:58.536+00	upgrade
10103	com.atlassian.servicedesk	7	COMPLETED	SERVER	2019-06-17 03:09:58.641+00	upgrade
10104	com.atlassian.servicedesk	8	COMPLETED	SERVER	2019-06-17 03:09:58.717+00	upgrade
10105	com.atlassian.servicedesk	9	COMPLETED	SERVER	2019-06-17 03:09:58.76+00	upgrade
10106	com.atlassian.servicedesk	10	COMPLETED	SERVER	2019-06-17 03:09:58.794+00	upgrade
10107	com.atlassian.servicedesk	11	COMPLETED	SERVER	2019-06-17 03:09:58.812+00	upgrade
10108	com.atlassian.servicedesk	12	COMPLETED	SERVER	2019-06-17 03:09:58.842+00	upgrade
\.


--
-- Data for Name: upgradeversionhistory; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY upgradeversionhistory (id, timeperformed, targetbuild, targetversion) FROM stdin;
10000	2019-06-17 03:05:25.592+00	801000	8.2.2
\.


--
-- Data for Name: userassociation; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY userassociation (source_name, sink_node_id, sink_node_entity, association_type, sequence, created) FROM stdin;
\.


--
-- Data for Name: userbase; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY userbase (id, username, password_hash) FROM stdin;
\.


--
-- Data for Name: userhistoryitem; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY userhistoryitem (id, entitytype, entityid, username, lastviewed, data) FROM stdin;
10000	Dashboard	10000	admin	1560742160071	\N
10100	Project	10000	admin	1560742236946	\N
\.


--
-- Data for Name: userpickerfilter; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY userpickerfilter (id, customfield, customfieldconfig, enabled) FROM stdin;
\.


--
-- Data for Name: userpickerfiltergroup; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY userpickerfiltergroup (id, userpickerfilter, groupname) FROM stdin;
\.


--
-- Data for Name: userpickerfilterrole; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY userpickerfilterrole (id, userpickerfilter, projectroleid) FROM stdin;
\.


--
-- Data for Name: versioncontrol; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY versioncontrol (id, vcsname, vcsdescription, vcstype) FROM stdin;
\.


--
-- Data for Name: votehistory; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY votehistory (id, issueid, votes, "timestamp") FROM stdin;
\.


--
-- Data for Name: workflowscheme; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY workflowscheme (id, name, description) FROM stdin;
10000	classic	classic
10100	Jira Service Desk IT Support Workflow Scheme generated for Project TEST	This Jira Service Desk IT Support Workflow Scheme was generated for Project TEST
\.


--
-- Data for Name: workflowschemeentity; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY workflowschemeentity (id, scheme, workflow, issuetype) FROM stdin;
10000	10000	classic default workflow	0
10100	10100	TEST: Service Request Fulfilment workflow for Jira Service Desk	10000
10101	10100	TEST: Service Request Fulfilment workflow for Jira Service Desk	10001
10102	10100	TEST: Service Request Fulfilment with Approvals workflow for Jira Service Desk	10002
10103	10100	TEST: Jira Service Desk default workflow	10003
10104	10100	TEST: Jira Service Desk default workflow	10004
\.


--
-- Data for Name: worklog; Type: TABLE DATA; Schema: public; Owner: jira
--

COPY worklog (id, issueid, author, grouplevel, rolelevel, worklogbody, created, updateauthor, updated, startdate, timeworked) FROM stdin;
\.


--
-- Name: AO_0201F0_KB_HELPFUL_AGGR_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_0201F0_KB_HELPFUL_AGGR"
    ADD CONSTRAINT "AO_0201F0_KB_HELPFUL_AGGR_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_0201F0_KB_VIEW_AGGR_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_0201F0_KB_VIEW_AGGR"
    ADD CONSTRAINT "AO_0201F0_KB_VIEW_AGGR_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_0201F0_STATS_EVENT_PARAM_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_0201F0_STATS_EVENT_PARAM"
    ADD CONSTRAINT "AO_0201F0_STATS_EVENT_PARAM_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_0201F0_STATS_EVENT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_0201F0_STATS_EVENT"
    ADD CONSTRAINT "AO_0201F0_STATS_EVENT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_0AC321_RECOMMENDATION_AO_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_0AC321_RECOMMENDATION_AO"
    ADD CONSTRAINT "AO_0AC321_RECOMMENDATION_AO_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_21D670_WHITELIST_RULES_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_21D670_WHITELIST_RULES"
    ADD CONSTRAINT "AO_21D670_WHITELIST_RULES_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_21F425_MESSAGE_AO_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_21F425_MESSAGE_AO"
    ADD CONSTRAINT "AO_21F425_MESSAGE_AO_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_21F425_MESSAGE_MAPPING_AO_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_21F425_MESSAGE_MAPPING_AO"
    ADD CONSTRAINT "AO_21F425_MESSAGE_MAPPING_AO_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_21F425_USER_PROPERTY_AO_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_21F425_USER_PROPERTY_AO"
    ADD CONSTRAINT "AO_21F425_USER_PROPERTY_AO_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_2C4E5C_MAILCHANNEL_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_2C4E5C_MAILCHANNEL"
    ADD CONSTRAINT "AO_2C4E5C_MAILCHANNEL_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_2C4E5C_MAILCONNECTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_2C4E5C_MAILCONNECTION"
    ADD CONSTRAINT "AO_2C4E5C_MAILCONNECTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_2C4E5C_MAILGLOBALHANDLER_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_2C4E5C_MAILGLOBALHANDLER"
    ADD CONSTRAINT "AO_2C4E5C_MAILGLOBALHANDLER_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_2C4E5C_MAILHANDLER_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_2C4E5C_MAILHANDLER"
    ADD CONSTRAINT "AO_2C4E5C_MAILHANDLER_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_2C4E5C_MAILITEMAUDIT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_2C4E5C_MAILITEMAUDIT"
    ADD CONSTRAINT "AO_2C4E5C_MAILITEMAUDIT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_2C4E5C_MAILITEMCHUNK_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_2C4E5C_MAILITEMCHUNK"
    ADD CONSTRAINT "AO_2C4E5C_MAILITEMCHUNK_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_2C4E5C_MAILITEM_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_2C4E5C_MAILITEM"
    ADD CONSTRAINT "AO_2C4E5C_MAILITEM_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_2C4E5C_MAILRUNAUDIT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_2C4E5C_MAILRUNAUDIT"
    ADD CONSTRAINT "AO_2C4E5C_MAILRUNAUDIT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_319474_MESSAGE_PROPERTY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_319474_MESSAGE_PROPERTY"
    ADD CONSTRAINT "AO_319474_MESSAGE_PROPERTY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_319474_MESSAGE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_319474_MESSAGE"
    ADD CONSTRAINT "AO_319474_MESSAGE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_319474_QUEUE_PROPERTY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_319474_QUEUE_PROPERTY"
    ADD CONSTRAINT "AO_319474_QUEUE_PROPERTY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_319474_QUEUE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_319474_QUEUE"
    ADD CONSTRAINT "AO_319474_QUEUE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_38321B_CUSTOM_CONTENT_LINK_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_38321B_CUSTOM_CONTENT_LINK"
    ADD CONSTRAINT "AO_38321B_CUSTOM_CONTENT_LINK_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_3B1893_LOOP_DETECTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_3B1893_LOOP_DETECTION"
    ADD CONSTRAINT "AO_3B1893_LOOP_DETECTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_4789DD_HEALTH_CHECK_STATUS_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_4789DD_HEALTH_CHECK_STATUS"
    ADD CONSTRAINT "AO_4789DD_HEALTH_CHECK_STATUS_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_4789DD_PROPERTIES_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_4789DD_PROPERTIES"
    ADD CONSTRAINT "AO_4789DD_PROPERTIES_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_4789DD_READ_NOTIFICATIONS_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_4789DD_READ_NOTIFICATIONS"
    ADD CONSTRAINT "AO_4789DD_READ_NOTIFICATIONS_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_4789DD_TASK_MONITOR_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_4789DD_TASK_MONITOR"
    ADD CONSTRAINT "AO_4789DD_TASK_MONITOR_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_4AEACD_WEBHOOK_DAO_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_4AEACD_WEBHOOK_DAO"
    ADD CONSTRAINT "AO_4AEACD_WEBHOOK_DAO_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_4E8AE6_NOTIF_BATCH_QUEUE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_4E8AE6_NOTIF_BATCH_QUEUE"
    ADD CONSTRAINT "AO_4E8AE6_NOTIF_BATCH_QUEUE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_4E8AE6_OUT_EMAIL_SETTINGS_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_4E8AE6_OUT_EMAIL_SETTINGS"
    ADD CONSTRAINT "AO_4E8AE6_OUT_EMAIL_SETTINGS_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_ASYNCUPGRADERECORD_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_ASYNCUPGRADERECORD"
    ADD CONSTRAINT "AO_54307E_ASYNCUPGRADERECORD_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_CAPABILITY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_CAPABILITY"
    ADD CONSTRAINT "AO_54307E_CAPABILITY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_CONFLUENCEKBENABLED_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBENABLED"
    ADD CONSTRAINT "AO_54307E_CONFLUENCEKBENABLED_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_CONFLUENCEKBLABELS_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBLABELS"
    ADD CONSTRAINT "AO_54307E_CONFLUENCEKBLABELS_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_CONFLUENCEKB_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKB"
    ADD CONSTRAINT "AO_54307E_CONFLUENCEKB_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_CUSTOMGLOBALTHEME_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_CUSTOMGLOBALTHEME"
    ADD CONSTRAINT "AO_54307E_CUSTOMGLOBALTHEME_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_CUSTOMTHEME_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_CUSTOMTHEME"
    ADD CONSTRAINT "AO_54307E_CUSTOMTHEME_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_EMAILCHANNELSETTING_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_EMAILCHANNELSETTING"
    ADD CONSTRAINT "AO_54307E_EMAILCHANNELSETTING_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_EMAILSETTINGS_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_EMAILSETTINGS"
    ADD CONSTRAINT "AO_54307E_EMAILSETTINGS_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_GOAL_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_GOAL"
    ADD CONSTRAINT "AO_54307E_GOAL_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_GROUPTOREQUESTTYPE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_GROUPTOREQUESTTYPE"
    ADD CONSTRAINT "AO_54307E_GROUPTOREQUESTTYPE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_GROUP_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_GROUP"
    ADD CONSTRAINT "AO_54307E_GROUP_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_IMAGES_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_IMAGES"
    ADD CONSTRAINT "AO_54307E_IMAGES_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_METRICCONDITION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_METRICCONDITION"
    ADD CONSTRAINT "AO_54307E_METRICCONDITION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_ORGANIZATION_MEMBER_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION_MEMBER"
    ADD CONSTRAINT "AO_54307E_ORGANIZATION_MEMBER_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_ORGANIZATION_PROJECT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION_PROJECT"
    ADD CONSTRAINT "AO_54307E_ORGANIZATION_PROJECT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_ORGANIZATION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION"
    ADD CONSTRAINT "AO_54307E_ORGANIZATION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_OUT_EMAIL_SETTINGS_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_OUT_EMAIL_SETTINGS"
    ADD CONSTRAINT "AO_54307E_OUT_EMAIL_SETTINGS_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_PARTICIPANTSETTINGS_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_PARTICIPANTSETTINGS"
    ADD CONSTRAINT "AO_54307E_PARTICIPANTSETTINGS_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_QUEUECOLUMN_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_QUEUECOLUMN"
    ADD CONSTRAINT "AO_54307E_QUEUECOLUMN_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_QUEUE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_QUEUE"
    ADD CONSTRAINT "AO_54307E_QUEUE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_REPORT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_REPORT"
    ADD CONSTRAINT "AO_54307E_REPORT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_SERIES_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_SERIES"
    ADD CONSTRAINT "AO_54307E_SERIES_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_SERVICEDESK_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_SERVICEDESK"
    ADD CONSTRAINT "AO_54307E_SERVICEDESK_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_SLAAUDITLOGDATA_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_SLAAUDITLOGDATA"
    ADD CONSTRAINT "AO_54307E_SLAAUDITLOGDATA_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_SLAAUDITLOG_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_SLAAUDITLOG"
    ADD CONSTRAINT "AO_54307E_SLAAUDITLOG_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_STATUSMAPPING_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_STATUSMAPPING"
    ADD CONSTRAINT "AO_54307E_STATUSMAPPING_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_SUBSCRIPTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_SUBSCRIPTION"
    ADD CONSTRAINT "AO_54307E_SUBSCRIPTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_SYNCUPGRADERECORD_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_SYNCUPGRADERECORD"
    ADD CONSTRAINT "AO_54307E_SYNCUPGRADERECORD_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_THRESHOLD_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_THRESHOLD"
    ADD CONSTRAINT "AO_54307E_THRESHOLD_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_TIMEMETRIC_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_TIMEMETRIC"
    ADD CONSTRAINT "AO_54307E_TIMEMETRIC_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_VIEWPORTFIELDVALUE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFIELDVALUE"
    ADD CONSTRAINT "AO_54307E_VIEWPORTFIELDVALUE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_VIEWPORTFIELD_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFIELD"
    ADD CONSTRAINT "AO_54307E_VIEWPORTFIELD_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_VIEWPORTFORM_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFORM"
    ADD CONSTRAINT "AO_54307E_VIEWPORTFORM_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_54307E_VIEWPORT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_VIEWPORT"
    ADD CONSTRAINT "AO_54307E_VIEWPORT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_550953_SHORTCUT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_550953_SHORTCUT"
    ADD CONSTRAINT "AO_550953_SHORTCUT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_563AEE_ACTIVITY_ENTITY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_563AEE_ACTIVITY_ENTITY"
    ADD CONSTRAINT "AO_563AEE_ACTIVITY_ENTITY_pkey" PRIMARY KEY ("ACTIVITY_ID");


--
-- Name: AO_563AEE_ACTOR_ENTITY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_563AEE_ACTOR_ENTITY"
    ADD CONSTRAINT "AO_563AEE_ACTOR_ENTITY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_563AEE_MEDIA_LINK_ENTITY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_563AEE_MEDIA_LINK_ENTITY"
    ADD CONSTRAINT "AO_563AEE_MEDIA_LINK_ENTITY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_563AEE_OBJECT_ENTITY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_563AEE_OBJECT_ENTITY"
    ADD CONSTRAINT "AO_563AEE_OBJECT_ENTITY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_563AEE_TARGET_ENTITY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_563AEE_TARGET_ENTITY"
    ADD CONSTRAINT "AO_563AEE_TARGET_ENTITY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_56464C_APPROVAL_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_56464C_APPROVAL"
    ADD CONSTRAINT "AO_56464C_APPROVAL_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_56464C_APPROVERDECISION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_56464C_APPROVERDECISION"
    ADD CONSTRAINT "AO_56464C_APPROVERDECISION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_56464C_APPROVER_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_56464C_APPROVER"
    ADD CONSTRAINT "AO_56464C_APPROVER_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_56464C_NOTIFICATIONRECORD_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_56464C_NOTIFICATIONRECORD"
    ADD CONSTRAINT "AO_56464C_NOTIFICATIONRECORD_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_587B34_GLANCE_CONFIG_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_587B34_GLANCE_CONFIG"
    ADD CONSTRAINT "AO_587B34_GLANCE_CONFIG_pkey" PRIMARY KEY ("ROOM_ID");


--
-- Name: AO_587B34_PROJECT_CONFIG_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_587B34_PROJECT_CONFIG"
    ADD CONSTRAINT "AO_587B34_PROJECT_CONFIG_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_5FB9D7_AOHIP_CHAT_LINK_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_5FB9D7_AOHIP_CHAT_LINK"
    ADD CONSTRAINT "AO_5FB9D7_AOHIP_CHAT_LINK_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_5FB9D7_AOHIP_CHAT_USER_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_5FB9D7_AOHIP_CHAT_USER"
    ADD CONSTRAINT "AO_5FB9D7_AOHIP_CHAT_USER_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_733371_EVENT_PARAMETER_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_733371_EVENT_PARAMETER"
    ADD CONSTRAINT "AO_733371_EVENT_PARAMETER_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_733371_EVENT_RECIPIENT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_733371_EVENT_RECIPIENT"
    ADD CONSTRAINT "AO_733371_EVENT_RECIPIENT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_733371_EVENT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_733371_EVENT"
    ADD CONSTRAINT "AO_733371_EVENT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_7A2604_CALENDAR_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_7A2604_CALENDAR"
    ADD CONSTRAINT "AO_7A2604_CALENDAR_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_7A2604_HOLIDAY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_7A2604_HOLIDAY"
    ADD CONSTRAINT "AO_7A2604_HOLIDAY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_7A2604_WORKINGTIME_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_7A2604_WORKINGTIME"
    ADD CONSTRAINT "AO_7A2604_WORKINGTIME_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_97EDAB_USERINVITATION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_97EDAB_USERINVITATION"
    ADD CONSTRAINT "AO_97EDAB_USERINVITATION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_EXEC_RULE_MSG_ITEM_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_EXEC_RULE_MSG_ITEM"
    ADD CONSTRAINT "AO_9B2E3B_EXEC_RULE_MSG_ITEM_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_IF_CONDITION_CONFIG_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_IF_CONDITION_CONFIG"
    ADD CONSTRAINT "AO_9B2E3B_IF_CONDITION_CONFIG_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_IF_COND_CONF_DATA_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_IF_COND_CONF_DATA"
    ADD CONSTRAINT "AO_9B2E3B_IF_COND_CONF_DATA_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_IF_COND_EXECUTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_IF_COND_EXECUTION"
    ADD CONSTRAINT "AO_9B2E3B_IF_COND_EXECUTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_IF_EXECUTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_IF_EXECUTION"
    ADD CONSTRAINT "AO_9B2E3B_IF_EXECUTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_IF_THEN_EXECUTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_IF_THEN_EXECUTION"
    ADD CONSTRAINT "AO_9B2E3B_IF_THEN_EXECUTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_IF_THEN_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_IF_THEN"
    ADD CONSTRAINT "AO_9B2E3B_IF_THEN_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_PROJECT_USER_CONTEXT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_PROJECT_USER_CONTEXT"
    ADD CONSTRAINT "AO_9B2E3B_PROJECT_USER_CONTEXT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_RSETREV_PROJ_CONTEXT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_RSETREV_PROJ_CONTEXT"
    ADD CONSTRAINT "AO_9B2E3B_RSETREV_PROJ_CONTEXT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_RSETREV_USER_CONTEXT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_RSETREV_USER_CONTEXT"
    ADD CONSTRAINT "AO_9B2E3B_RSETREV_USER_CONTEXT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_RULESET_REVISION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_RULESET_REVISION"
    ADD CONSTRAINT "AO_9B2E3B_RULESET_REVISION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_RULESET_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_RULESET"
    ADD CONSTRAINT "AO_9B2E3B_RULESET_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_RULE_EXECUTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_RULE_EXECUTION"
    ADD CONSTRAINT "AO_9B2E3B_RULE_EXECUTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_RULE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_RULE"
    ADD CONSTRAINT "AO_9B2E3B_RULE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_THEN_ACTION_CONFIG_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACTION_CONFIG"
    ADD CONSTRAINT "AO_9B2E3B_THEN_ACTION_CONFIG_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_THEN_ACT_CONF_DATA_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACT_CONF_DATA"
    ADD CONSTRAINT "AO_9B2E3B_THEN_ACT_CONF_DATA_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_THEN_ACT_EXECUTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACT_EXECUTION"
    ADD CONSTRAINT "AO_9B2E3B_THEN_ACT_EXECUTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_THEN_EXECUTION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_EXECUTION"
    ADD CONSTRAINT "AO_9B2E3B_THEN_EXECUTION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_WHEN_HANDLER_CONFIG_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_WHEN_HANDLER_CONFIG"
    ADD CONSTRAINT "AO_9B2E3B_WHEN_HANDLER_CONFIG_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_9B2E3B_WHEN_HAND_CONF_DATA_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_9B2E3B_WHEN_HAND_CONF_DATA"
    ADD CONSTRAINT "AO_9B2E3B_WHEN_HAND_CONF_DATA_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_A0B856_WEB_HOOK_LISTENER_AO_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_A0B856_WEB_HOOK_LISTENER_AO"
    ADD CONSTRAINT "AO_A0B856_WEB_HOOK_LISTENER_AO_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_B9A0F0_APPLIED_TEMPLATE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_B9A0F0_APPLIED_TEMPLATE"
    ADD CONSTRAINT "AO_B9A0F0_APPLIED_TEMPLATE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_C16815_ALERT_AO_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_C16815_ALERT_AO"
    ADD CONSTRAINT "AO_C16815_ALERT_AO_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_C7F17E_LINGO_REVISION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_C7F17E_LINGO_REVISION"
    ADD CONSTRAINT "AO_C7F17E_LINGO_REVISION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_C7F17E_LINGO_TRANSLATION_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_C7F17E_LINGO_TRANSLATION"
    ADD CONSTRAINT "AO_C7F17E_LINGO_TRANSLATION_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_C7F17E_LINGO_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_C7F17E_LINGO"
    ADD CONSTRAINT "AO_C7F17E_LINGO_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_C7F17E_PROJECT_LANGUAGE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_C7F17E_PROJECT_LANGUAGE"
    ADD CONSTRAINT "AO_C7F17E_PROJECT_LANGUAGE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_C7F17E_PROJECT_LANG_CONFIG_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_C7F17E_PROJECT_LANG_CONFIG"
    ADD CONSTRAINT "AO_C7F17E_PROJECT_LANG_CONFIG_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_C7F17E_PROJECT_LANG_REV_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_C7F17E_PROJECT_LANG_REV"
    ADD CONSTRAINT "AO_C7F17E_PROJECT_LANG_REV_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_D530BB_CANNEDRESPONSEAUDIT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_D530BB_CANNEDRESPONSEAUDIT"
    ADD CONSTRAINT "AO_D530BB_CANNEDRESPONSEAUDIT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_D530BB_CANNEDRESPONSEUSAGE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_D530BB_CANNEDRESPONSEUSAGE"
    ADD CONSTRAINT "AO_D530BB_CANNEDRESPONSEUSAGE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_D530BB_CANNEDRESPONSE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_D530BB_CANNEDRESPONSE"
    ADD CONSTRAINT "AO_D530BB_CANNEDRESPONSE_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_D530BB_CRAUDITACTIONDATA_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_D530BB_CRAUDITACTIONDATA"
    ADD CONSTRAINT "AO_D530BB_CRAUDITACTIONDATA_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_ED669C_SEEN_ASSERTIONS_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_ED669C_SEEN_ASSERTIONS"
    ADD CONSTRAINT "AO_ED669C_SEEN_ASSERTIONS_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_F1B27B_HISTORY_RECORD_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_F1B27B_HISTORY_RECORD"
    ADD CONSTRAINT "AO_F1B27B_HISTORY_RECORD_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_F1B27B_KEY_COMPONENT_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_F1B27B_KEY_COMPONENT"
    ADD CONSTRAINT "AO_F1B27B_KEY_COMPONENT_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_F1B27B_KEY_COMP_HISTORY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_F1B27B_KEY_COMP_HISTORY"
    ADD CONSTRAINT "AO_F1B27B_KEY_COMP_HISTORY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_F1B27B_PROMISE_HISTORY_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_F1B27B_PROMISE_HISTORY"
    ADD CONSTRAINT "AO_F1B27B_PROMISE_HISTORY_pkey" PRIMARY KEY ("ID");


--
-- Name: AO_F1B27B_PROMISE_pkey; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_F1B27B_PROMISE"
    ADD CONSTRAINT "AO_F1B27B_PROMISE_pkey" PRIMARY KEY ("ID");


--
-- Name: pk_app_user; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY app_user
    ADD CONSTRAINT pk_app_user PRIMARY KEY (id);


--
-- Name: pk_audit_changed_value; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY audit_changed_value
    ADD CONSTRAINT pk_audit_changed_value PRIMARY KEY (id);


--
-- Name: pk_audit_item; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY audit_item
    ADD CONSTRAINT pk_audit_item PRIMARY KEY (id);


--
-- Name: pk_audit_log; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY audit_log
    ADD CONSTRAINT pk_audit_log PRIMARY KEY (id);


--
-- Name: pk_avatar; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY avatar
    ADD CONSTRAINT pk_avatar PRIMARY KEY (id);


--
-- Name: pk_board; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY board
    ADD CONSTRAINT pk_board PRIMARY KEY (id);


--
-- Name: pk_boardproject; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY boardproject
    ADD CONSTRAINT pk_boardproject PRIMARY KEY (board_id, project_id);


--
-- Name: pk_changegroup; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY changegroup
    ADD CONSTRAINT pk_changegroup PRIMARY KEY (id);


--
-- Name: pk_changeitem; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY changeitem
    ADD CONSTRAINT pk_changeitem PRIMARY KEY (id);


--
-- Name: pk_clusteredjob; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY clusteredjob
    ADD CONSTRAINT pk_clusteredjob PRIMARY KEY (id);


--
-- Name: pk_clusterlockstatus; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY clusterlockstatus
    ADD CONSTRAINT pk_clusterlockstatus PRIMARY KEY (id);


--
-- Name: pk_clustermessage; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY clustermessage
    ADD CONSTRAINT pk_clustermessage PRIMARY KEY (id);


--
-- Name: pk_clusternode; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY clusternode
    ADD CONSTRAINT pk_clusternode PRIMARY KEY (node_id);


--
-- Name: pk_clusternodeheartbeat; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY clusternodeheartbeat
    ADD CONSTRAINT pk_clusternodeheartbeat PRIMARY KEY (node_id);


--
-- Name: pk_clusterupgradestate; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY clusterupgradestate
    ADD CONSTRAINT pk_clusterupgradestate PRIMARY KEY (id);


--
-- Name: pk_columnlayout; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY columnlayout
    ADD CONSTRAINT pk_columnlayout PRIMARY KEY (id);


--
-- Name: pk_columnlayoutitem; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY columnlayoutitem
    ADD CONSTRAINT pk_columnlayoutitem PRIMARY KEY (id);


--
-- Name: pk_component; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY component
    ADD CONSTRAINT pk_component PRIMARY KEY (id);


--
-- Name: pk_configurationcontext; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY configurationcontext
    ADD CONSTRAINT pk_configurationcontext PRIMARY KEY (id);


--
-- Name: pk_customfield; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY customfield
    ADD CONSTRAINT pk_customfield PRIMARY KEY (id);


--
-- Name: pk_customfieldoption; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY customfieldoption
    ADD CONSTRAINT pk_customfieldoption PRIMARY KEY (id);


--
-- Name: pk_customfieldvalue; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY customfieldvalue
    ADD CONSTRAINT pk_customfieldvalue PRIMARY KEY (id);


--
-- Name: pk_cwd_application; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_application
    ADD CONSTRAINT pk_cwd_application PRIMARY KEY (id);


--
-- Name: pk_cwd_application_address; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_application_address
    ADD CONSTRAINT pk_cwd_application_address PRIMARY KEY (application_id, remote_address);


--
-- Name: pk_cwd_directory; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_directory
    ADD CONSTRAINT pk_cwd_directory PRIMARY KEY (id);


--
-- Name: pk_cwd_directory_attribute; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_directory_attribute
    ADD CONSTRAINT pk_cwd_directory_attribute PRIMARY KEY (directory_id, attribute_name);


--
-- Name: pk_cwd_directory_operation; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_directory_operation
    ADD CONSTRAINT pk_cwd_directory_operation PRIMARY KEY (directory_id, operation_type);


--
-- Name: pk_cwd_group; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_group
    ADD CONSTRAINT pk_cwd_group PRIMARY KEY (id);


--
-- Name: pk_cwd_group_attributes; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_group_attributes
    ADD CONSTRAINT pk_cwd_group_attributes PRIMARY KEY (id);


--
-- Name: pk_cwd_membership; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_membership
    ADD CONSTRAINT pk_cwd_membership PRIMARY KEY (id);


--
-- Name: pk_cwd_user; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_user
    ADD CONSTRAINT pk_cwd_user PRIMARY KEY (id);


--
-- Name: pk_cwd_user_attributes; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY cwd_user_attributes
    ADD CONSTRAINT pk_cwd_user_attributes PRIMARY KEY (id);


--
-- Name: pk_deadletter; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY deadletter
    ADD CONSTRAINT pk_deadletter PRIMARY KEY (id);


--
-- Name: pk_draftworkflowscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY draftworkflowscheme
    ADD CONSTRAINT pk_draftworkflowscheme PRIMARY KEY (id);


--
-- Name: pk_draftworkflowschemeentity; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY draftworkflowschemeentity
    ADD CONSTRAINT pk_draftworkflowschemeentity PRIMARY KEY (id);


--
-- Name: pk_entity_property; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY entity_property
    ADD CONSTRAINT pk_entity_property PRIMARY KEY (id);


--
-- Name: pk_entity_property_index_docum; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY entity_property_index_document
    ADD CONSTRAINT pk_entity_property_index_docum PRIMARY KEY (id);


--
-- Name: pk_entity_translation; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY entity_translation
    ADD CONSTRAINT pk_entity_translation PRIMARY KEY (id);


--
-- Name: pk_external_entities; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY external_entities
    ADD CONSTRAINT pk_external_entities PRIMARY KEY (id);


--
-- Name: pk_externalgadget; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY externalgadget
    ADD CONSTRAINT pk_externalgadget PRIMARY KEY (id);


--
-- Name: pk_favouriteassociations; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY favouriteassociations
    ADD CONSTRAINT pk_favouriteassociations PRIMARY KEY (id);


--
-- Name: pk_feature; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY feature
    ADD CONSTRAINT pk_feature PRIMARY KEY (id);


--
-- Name: pk_fieldconfigscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldconfigscheme
    ADD CONSTRAINT pk_fieldconfigscheme PRIMARY KEY (id);


--
-- Name: pk_fieldconfigschemeissuetype; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldconfigschemeissuetype
    ADD CONSTRAINT pk_fieldconfigschemeissuetype PRIMARY KEY (id);


--
-- Name: pk_fieldconfiguration; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldconfiguration
    ADD CONSTRAINT pk_fieldconfiguration PRIMARY KEY (id);


--
-- Name: pk_fieldlayout; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldlayout
    ADD CONSTRAINT pk_fieldlayout PRIMARY KEY (id);


--
-- Name: pk_fieldlayoutitem; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldlayoutitem
    ADD CONSTRAINT pk_fieldlayoutitem PRIMARY KEY (id);


--
-- Name: pk_fieldlayoutscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldlayoutscheme
    ADD CONSTRAINT pk_fieldlayoutscheme PRIMARY KEY (id);


--
-- Name: pk_fieldlayoutschemeassociatio; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldlayoutschemeassociation
    ADD CONSTRAINT pk_fieldlayoutschemeassociatio PRIMARY KEY (id);


--
-- Name: pk_fieldlayoutschemeentity; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldlayoutschemeentity
    ADD CONSTRAINT pk_fieldlayoutschemeentity PRIMARY KEY (id);


--
-- Name: pk_fieldscreen; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldscreen
    ADD CONSTRAINT pk_fieldscreen PRIMARY KEY (id);


--
-- Name: pk_fieldscreenlayoutitem; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldscreenlayoutitem
    ADD CONSTRAINT pk_fieldscreenlayoutitem PRIMARY KEY (id);


--
-- Name: pk_fieldscreenscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldscreenscheme
    ADD CONSTRAINT pk_fieldscreenscheme PRIMARY KEY (id);


--
-- Name: pk_fieldscreenschemeitem; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldscreenschemeitem
    ADD CONSTRAINT pk_fieldscreenschemeitem PRIMARY KEY (id);


--
-- Name: pk_fieldscreentab; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fieldscreentab
    ADD CONSTRAINT pk_fieldscreentab PRIMARY KEY (id);


--
-- Name: pk_fileattachment; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY fileattachment
    ADD CONSTRAINT pk_fileattachment PRIMARY KEY (id);


--
-- Name: pk_filtersubscription; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY filtersubscription
    ADD CONSTRAINT pk_filtersubscription PRIMARY KEY (id);


--
-- Name: pk_gadgetuserpreference; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY gadgetuserpreference
    ADD CONSTRAINT pk_gadgetuserpreference PRIMARY KEY (id);


--
-- Name: pk_genericconfiguration; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY genericconfiguration
    ADD CONSTRAINT pk_genericconfiguration PRIMARY KEY (id);


--
-- Name: pk_globalpermissionentry; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY globalpermissionentry
    ADD CONSTRAINT pk_globalpermissionentry PRIMARY KEY (id);


--
-- Name: pk_groupbase; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY groupbase
    ADD CONSTRAINT pk_groupbase PRIMARY KEY (id);


--
-- Name: pk_issue_field_option; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issue_field_option
    ADD CONSTRAINT pk_issue_field_option PRIMARY KEY (id);


--
-- Name: pk_issue_field_option_scope; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issue_field_option_scope
    ADD CONSTRAINT pk_issue_field_option_scope PRIMARY KEY (id);


--
-- Name: pk_issuelink; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issuelink
    ADD CONSTRAINT pk_issuelink PRIMARY KEY (id);


--
-- Name: pk_issuelinktype; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issuelinktype
    ADD CONSTRAINT pk_issuelinktype PRIMARY KEY (id);


--
-- Name: pk_issuesecurityscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issuesecurityscheme
    ADD CONSTRAINT pk_issuesecurityscheme PRIMARY KEY (id);


--
-- Name: pk_issuestatus; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issuestatus
    ADD CONSTRAINT pk_issuestatus PRIMARY KEY (id);


--
-- Name: pk_issuetype; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issuetype
    ADD CONSTRAINT pk_issuetype PRIMARY KEY (id);


--
-- Name: pk_issuetypescreenscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issuetypescreenscheme
    ADD CONSTRAINT pk_issuetypescreenscheme PRIMARY KEY (id);


--
-- Name: pk_issuetypescreenschemeentity; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY issuetypescreenschemeentity
    ADD CONSTRAINT pk_issuetypescreenschemeentity PRIMARY KEY (id);


--
-- Name: pk_jiraaction; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jiraaction
    ADD CONSTRAINT pk_jiraaction PRIMARY KEY (id);


--
-- Name: pk_jiradraftworkflows; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jiradraftworkflows
    ADD CONSTRAINT pk_jiradraftworkflows PRIMARY KEY (id);


--
-- Name: pk_jiraeventtype; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jiraeventtype
    ADD CONSTRAINT pk_jiraeventtype PRIMARY KEY (id);


--
-- Name: pk_jiraissue; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jiraissue
    ADD CONSTRAINT pk_jiraissue PRIMARY KEY (id);


--
-- Name: pk_jiraperms; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jiraperms
    ADD CONSTRAINT pk_jiraperms PRIMARY KEY (id);


--
-- Name: pk_jiraworkflows; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jiraworkflows
    ADD CONSTRAINT pk_jiraworkflows PRIMARY KEY (id);


--
-- Name: pk_jiraworkflowstatuses; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jiraworkflowstatuses
    ADD CONSTRAINT pk_jiraworkflowstatuses PRIMARY KEY (id);


--
-- Name: pk_jquartz_blob_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_blob_triggers
    ADD CONSTRAINT pk_jquartz_blob_triggers PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: pk_jquartz_calendars; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_calendars
    ADD CONSTRAINT pk_jquartz_calendars PRIMARY KEY (calendar_name);


--
-- Name: pk_jquartz_cron_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_cron_triggers
    ADD CONSTRAINT pk_jquartz_cron_triggers PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: pk_jquartz_fired_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_fired_triggers
    ADD CONSTRAINT pk_jquartz_fired_triggers PRIMARY KEY (entry_id);


--
-- Name: pk_jquartz_job_details; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_job_details
    ADD CONSTRAINT pk_jquartz_job_details PRIMARY KEY (job_name, job_group);


--
-- Name: pk_jquartz_job_listeners; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_job_listeners
    ADD CONSTRAINT pk_jquartz_job_listeners PRIMARY KEY (job_name, job_group, job_listener);


--
-- Name: pk_jquartz_locks; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_locks
    ADD CONSTRAINT pk_jquartz_locks PRIMARY KEY (lock_name);


--
-- Name: pk_jquartz_paused_trigger_grps; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_paused_trigger_grps
    ADD CONSTRAINT pk_jquartz_paused_trigger_grps PRIMARY KEY (trigger_group);


--
-- Name: pk_jquartz_scheduler_state; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_scheduler_state
    ADD CONSTRAINT pk_jquartz_scheduler_state PRIMARY KEY (instance_name);


--
-- Name: pk_jquartz_simple_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_simple_triggers
    ADD CONSTRAINT pk_jquartz_simple_triggers PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: pk_jquartz_simprop_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_simprop_triggers
    ADD CONSTRAINT pk_jquartz_simprop_triggers PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: pk_jquartz_trigger_listeners; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_trigger_listeners
    ADD CONSTRAINT pk_jquartz_trigger_listeners PRIMARY KEY (trigger_group, trigger_listener);


--
-- Name: pk_jquartz_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY jquartz_triggers
    ADD CONSTRAINT pk_jquartz_triggers PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: pk_label; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY label
    ADD CONSTRAINT pk_label PRIMARY KEY (id);


--
-- Name: pk_licenserolesdefault; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY licenserolesdefault
    ADD CONSTRAINT pk_licenserolesdefault PRIMARY KEY (id);


--
-- Name: pk_licenserolesgroup; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY licenserolesgroup
    ADD CONSTRAINT pk_licenserolesgroup PRIMARY KEY (id);


--
-- Name: pk_listenerconfig; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY listenerconfig
    ADD CONSTRAINT pk_listenerconfig PRIMARY KEY (id);


--
-- Name: pk_mailserver; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY mailserver
    ADD CONSTRAINT pk_mailserver PRIMARY KEY (id);


--
-- Name: pk_managedconfigurationitem; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY managedconfigurationitem
    ADD CONSTRAINT pk_managedconfigurationitem PRIMARY KEY (id);


--
-- Name: pk_membershipbase; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY membershipbase
    ADD CONSTRAINT pk_membershipbase PRIMARY KEY (id);


--
-- Name: pk_moved_issue_key; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY moved_issue_key
    ADD CONSTRAINT pk_moved_issue_key PRIMARY KEY (id);


--
-- Name: pk_nodeassociation; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY nodeassociation
    ADD CONSTRAINT pk_nodeassociation PRIMARY KEY (source_node_id, source_node_entity, sink_node_id, sink_node_entity, association_type);


--
-- Name: pk_nodeindexcounter; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY nodeindexcounter
    ADD CONSTRAINT pk_nodeindexcounter PRIMARY KEY (id);


--
-- Name: pk_notification; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT pk_notification PRIMARY KEY (id);


--
-- Name: pk_notificationinstance; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY notificationinstance
    ADD CONSTRAINT pk_notificationinstance PRIMARY KEY (id);


--
-- Name: pk_notificationscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY notificationscheme
    ADD CONSTRAINT pk_notificationscheme PRIMARY KEY (id);


--
-- Name: pk_oauthconsumer; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY oauthconsumer
    ADD CONSTRAINT pk_oauthconsumer PRIMARY KEY (id);


--
-- Name: pk_oauthconsumertoken; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY oauthconsumertoken
    ADD CONSTRAINT pk_oauthconsumertoken PRIMARY KEY (id);


--
-- Name: pk_oauthspconsumer; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY oauthspconsumer
    ADD CONSTRAINT pk_oauthspconsumer PRIMARY KEY (id);


--
-- Name: pk_oauthsptoken; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY oauthsptoken
    ADD CONSTRAINT pk_oauthsptoken PRIMARY KEY (id);


--
-- Name: pk_optionconfiguration; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY optionconfiguration
    ADD CONSTRAINT pk_optionconfiguration PRIMARY KEY (id);


--
-- Name: pk_os_currentstep; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY os_currentstep
    ADD CONSTRAINT pk_os_currentstep PRIMARY KEY (id);


--
-- Name: pk_os_currentstep_prev; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY os_currentstep_prev
    ADD CONSTRAINT pk_os_currentstep_prev PRIMARY KEY (id, previous_id);


--
-- Name: pk_os_historystep; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY os_historystep
    ADD CONSTRAINT pk_os_historystep PRIMARY KEY (id);


--
-- Name: pk_os_historystep_prev; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY os_historystep_prev
    ADD CONSTRAINT pk_os_historystep_prev PRIMARY KEY (id, previous_id);


--
-- Name: pk_os_wfentry; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY os_wfentry
    ADD CONSTRAINT pk_os_wfentry PRIMARY KEY (id);


--
-- Name: pk_permissionscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY permissionscheme
    ADD CONSTRAINT pk_permissionscheme PRIMARY KEY (id);


--
-- Name: pk_permissionschemeattribute; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY permissionschemeattribute
    ADD CONSTRAINT pk_permissionschemeattribute PRIMARY KEY (id);


--
-- Name: pk_pluginstate; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY pluginstate
    ADD CONSTRAINT pk_pluginstate PRIMARY KEY (pluginkey);


--
-- Name: pk_pluginversion; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY pluginversion
    ADD CONSTRAINT pk_pluginversion PRIMARY KEY (id);


--
-- Name: pk_portalpage; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY portalpage
    ADD CONSTRAINT pk_portalpage PRIMARY KEY (id);


--
-- Name: pk_portletconfiguration; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY portletconfiguration
    ADD CONSTRAINT pk_portletconfiguration PRIMARY KEY (id);


--
-- Name: pk_priority; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY priority
    ADD CONSTRAINT pk_priority PRIMARY KEY (id);


--
-- Name: pk_productlicense; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY productlicense
    ADD CONSTRAINT pk_productlicense PRIMARY KEY (id);


--
-- Name: pk_project; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY project
    ADD CONSTRAINT pk_project PRIMARY KEY (id);


--
-- Name: pk_project_key; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY project_key
    ADD CONSTRAINT pk_project_key PRIMARY KEY (id);


--
-- Name: pk_projectcategory; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY projectcategory
    ADD CONSTRAINT pk_projectcategory PRIMARY KEY (id);


--
-- Name: pk_projectchangedtime; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY projectchangedtime
    ADD CONSTRAINT pk_projectchangedtime PRIMARY KEY (project_id);


--
-- Name: pk_projectrole; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY projectrole
    ADD CONSTRAINT pk_projectrole PRIMARY KEY (id);


--
-- Name: pk_projectroleactor; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY projectroleactor
    ADD CONSTRAINT pk_projectroleactor PRIMARY KEY (id);


--
-- Name: pk_projectversion; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY projectversion
    ADD CONSTRAINT pk_projectversion PRIMARY KEY (id);


--
-- Name: pk_propertydata; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY propertydata
    ADD CONSTRAINT pk_propertydata PRIMARY KEY (id);


--
-- Name: pk_propertydate; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY propertydate
    ADD CONSTRAINT pk_propertydate PRIMARY KEY (id);


--
-- Name: pk_propertydecimal; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY propertydecimal
    ADD CONSTRAINT pk_propertydecimal PRIMARY KEY (id);


--
-- Name: pk_propertyentry; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY propertyentry
    ADD CONSTRAINT pk_propertyentry PRIMARY KEY (id);


--
-- Name: pk_propertynumber; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY propertynumber
    ADD CONSTRAINT pk_propertynumber PRIMARY KEY (id);


--
-- Name: pk_propertystring; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY propertystring
    ADD CONSTRAINT pk_propertystring PRIMARY KEY (id);


--
-- Name: pk_propertytext; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY propertytext
    ADD CONSTRAINT pk_propertytext PRIMARY KEY (id);


--
-- Name: pk_qrtz_calendars; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY qrtz_calendars
    ADD CONSTRAINT pk_qrtz_calendars PRIMARY KEY (calendar_name);


--
-- Name: pk_qrtz_cron_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY qrtz_cron_triggers
    ADD CONSTRAINT pk_qrtz_cron_triggers PRIMARY KEY (id);


--
-- Name: pk_qrtz_fired_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY qrtz_fired_triggers
    ADD CONSTRAINT pk_qrtz_fired_triggers PRIMARY KEY (entry_id);


--
-- Name: pk_qrtz_job_details; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY qrtz_job_details
    ADD CONSTRAINT pk_qrtz_job_details PRIMARY KEY (id);


--
-- Name: pk_qrtz_job_listeners; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY qrtz_job_listeners
    ADD CONSTRAINT pk_qrtz_job_listeners PRIMARY KEY (id);


--
-- Name: pk_qrtz_simple_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY qrtz_simple_triggers
    ADD CONSTRAINT pk_qrtz_simple_triggers PRIMARY KEY (id);


--
-- Name: pk_qrtz_trigger_listeners; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY qrtz_trigger_listeners
    ADD CONSTRAINT pk_qrtz_trigger_listeners PRIMARY KEY (id);


--
-- Name: pk_qrtz_triggers; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY qrtz_triggers
    ADD CONSTRAINT pk_qrtz_triggers PRIMARY KEY (id);


--
-- Name: pk_reindex_component; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY reindex_component
    ADD CONSTRAINT pk_reindex_component PRIMARY KEY (id);


--
-- Name: pk_reindex_request; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY reindex_request
    ADD CONSTRAINT pk_reindex_request PRIMARY KEY (id);


--
-- Name: pk_remembermetoken; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY remembermetoken
    ADD CONSTRAINT pk_remembermetoken PRIMARY KEY (id);


--
-- Name: pk_remotelink; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY remotelink
    ADD CONSTRAINT pk_remotelink PRIMARY KEY (id);


--
-- Name: pk_replicatedindexoperation; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY replicatedindexoperation
    ADD CONSTRAINT pk_replicatedindexoperation PRIMARY KEY (id);


--
-- Name: pk_resolution; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY resolution
    ADD CONSTRAINT pk_resolution PRIMARY KEY (id);


--
-- Name: pk_rundetails; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY rundetails
    ADD CONSTRAINT pk_rundetails PRIMARY KEY (id);


--
-- Name: pk_schemeissuesecurities; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY schemeissuesecurities
    ADD CONSTRAINT pk_schemeissuesecurities PRIMARY KEY (id);


--
-- Name: pk_schemeissuesecuritylevels; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY schemeissuesecuritylevels
    ADD CONSTRAINT pk_schemeissuesecuritylevels PRIMARY KEY (id);


--
-- Name: pk_schemepermissions; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY schemepermissions
    ADD CONSTRAINT pk_schemepermissions PRIMARY KEY (id);


--
-- Name: pk_searchrequest; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY searchrequest
    ADD CONSTRAINT pk_searchrequest PRIMARY KEY (id);


--
-- Name: pk_sequence_value_item; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY sequence_value_item
    ADD CONSTRAINT pk_sequence_value_item PRIMARY KEY (seq_name);


--
-- Name: pk_serviceconfig; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY serviceconfig
    ADD CONSTRAINT pk_serviceconfig PRIMARY KEY (id);


--
-- Name: pk_sharepermissions; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY sharepermissions
    ADD CONSTRAINT pk_sharepermissions PRIMARY KEY (id);


--
-- Name: pk_tempattachmentsmonitor; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY tempattachmentsmonitor
    ADD CONSTRAINT pk_tempattachmentsmonitor PRIMARY KEY (temporary_attachment_id);


--
-- Name: pk_trackback_ping; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY trackback_ping
    ADD CONSTRAINT pk_trackback_ping PRIMARY KEY (id);


--
-- Name: pk_trustedapp; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY trustedapp
    ADD CONSTRAINT pk_trustedapp PRIMARY KEY (id);


--
-- Name: pk_upgradehistory; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY upgradehistory
    ADD CONSTRAINT pk_upgradehistory PRIMARY KEY (upgradeclass);


--
-- Name: pk_upgradetaskhistory; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY upgradetaskhistory
    ADD CONSTRAINT pk_upgradetaskhistory PRIMARY KEY (id);


--
-- Name: pk_upgradetaskhistoryauditlog; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY upgradetaskhistoryauditlog
    ADD CONSTRAINT pk_upgradetaskhistoryauditlog PRIMARY KEY (id);


--
-- Name: pk_upgradeversionhistory; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY upgradeversionhistory
    ADD CONSTRAINT pk_upgradeversionhistory PRIMARY KEY (targetbuild);


--
-- Name: pk_userassociation; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY userassociation
    ADD CONSTRAINT pk_userassociation PRIMARY KEY (source_name, sink_node_id, sink_node_entity, association_type);


--
-- Name: pk_userbase; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY userbase
    ADD CONSTRAINT pk_userbase PRIMARY KEY (id);


--
-- Name: pk_userhistoryitem; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY userhistoryitem
    ADD CONSTRAINT pk_userhistoryitem PRIMARY KEY (id);


--
-- Name: pk_userpickerfilter; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY userpickerfilter
    ADD CONSTRAINT pk_userpickerfilter PRIMARY KEY (id);


--
-- Name: pk_userpickerfiltergroup; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY userpickerfiltergroup
    ADD CONSTRAINT pk_userpickerfiltergroup PRIMARY KEY (id);


--
-- Name: pk_userpickerfilterrole; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY userpickerfilterrole
    ADD CONSTRAINT pk_userpickerfilterrole PRIMARY KEY (id);


--
-- Name: pk_versioncontrol; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY versioncontrol
    ADD CONSTRAINT pk_versioncontrol PRIMARY KEY (id);


--
-- Name: pk_votehistory; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY votehistory
    ADD CONSTRAINT pk_votehistory PRIMARY KEY (id);


--
-- Name: pk_workflowscheme; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY workflowscheme
    ADD CONSTRAINT pk_workflowscheme PRIMARY KEY (id);


--
-- Name: pk_workflowschemeentity; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY workflowschemeentity
    ADD CONSTRAINT pk_workflowschemeentity PRIMARY KEY (id);


--
-- Name: pk_worklog; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY worklog
    ADD CONSTRAINT pk_worklog PRIMARY KEY (id);


--
-- Name: u_ao_319474_queue_name; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_319474_QUEUE"
    ADD CONSTRAINT u_ao_319474_queue_name UNIQUE ("NAME");


--
-- Name: u_ao_4789dd_task_mo1827547914; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_4789DD_TASK_MONITOR"
    ADD CONSTRAINT u_ao_4789dd_task_mo1827547914 UNIQUE ("TASK_ID");


--
-- Name: u_ao_54307e_organiz2022034244; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION"
    ADD CONSTRAINT u_ao_54307e_organiz2022034244 UNIQUE ("SEARCH_NAME");


--
-- Name: u_ao_54307e_organization_name; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION"
    ADD CONSTRAINT u_ao_54307e_organization_name UNIQUE ("NAME");


--
-- Name: u_ao_54307e_viewport_key; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_54307E_VIEWPORT"
    ADD CONSTRAINT u_ao_54307e_viewport_key UNIQUE ("KEY");


--
-- Name: u_ao_587b34_project2070954277; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_587B34_PROJECT_CONFIG"
    ADD CONSTRAINT u_ao_587b34_project2070954277 UNIQUE ("NAME_UNIQUE_CONSTRAINT");


--
-- Name: u_ao_ed669c_seen_as1055534769; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_ED669C_SEEN_ASSERTIONS"
    ADD CONSTRAINT u_ao_ed669c_seen_as1055534769 UNIQUE ("ASSERTION_ID");


--
-- Name: u_ao_f1b27b_promise1620912431; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_F1B27B_PROMISE_HISTORY"
    ADD CONSTRAINT u_ao_f1b27b_promise1620912431 UNIQUE ("KEY_HASH");


--
-- Name: u_ao_f1b27b_promise_key_hash; Type: CONSTRAINT; Schema: public; Owner: jira; Tablespace: 
--

ALTER TABLE ONLY "AO_F1B27B_PROMISE"
    ADD CONSTRAINT u_ao_f1b27b_promise_key_hash UNIQUE ("KEY_HASH");


--
-- Name: action_author_created; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX action_author_created ON jiraaction USING btree (author, created);


--
-- Name: action_issue; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX action_issue ON jiraaction USING btree (issueid);


--
-- Name: attach_issue; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX attach_issue ON fileattachment USING btree (issueid);


--
-- Name: avatar_filename_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX avatar_filename_index ON avatar USING btree (filename, avatartype, systemavatar);


--
-- Name: avatar_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX avatar_index ON avatar USING btree (avatartype, owner);


--
-- Name: cf_cfoption; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX cf_cfoption ON customfieldoption USING btree (customfield);


--
-- Name: cf_userpickerfiltergroup; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX cf_userpickerfiltergroup ON userpickerfiltergroup USING btree (userpickerfilter);


--
-- Name: cf_userpickerfilterrole; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX cf_userpickerfilterrole ON userpickerfilterrole USING btree (userpickerfilter);


--
-- Name: cfvalue_issue; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX cfvalue_issue ON customfieldvalue USING btree (issue, customfield);


--
-- Name: chggroup_author_created; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX chggroup_author_created ON changegroup USING btree (author, created);


--
-- Name: chggroup_issue_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX chggroup_issue_id ON changegroup USING btree (issueid, id);


--
-- Name: chgitem_group_field; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX chgitem_group_field ON changeitem USING btree (groupid, field);


--
-- Name: cl_searchrequest; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX cl_searchrequest ON columnlayout USING btree (searchrequest);


--
-- Name: cl_username; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX cl_username ON columnlayout USING btree (username);


--
-- Name: cluster_lock_name_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX cluster_lock_name_idx ON clusterlockstatus USING btree (lock_name);


--
-- Name: clusteredjob_jobid_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX clusteredjob_jobid_idx ON clusteredjob USING btree (job_id);


--
-- Name: clusteredjob_jrk_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX clusteredjob_jrk_idx ON clusteredjob USING btree (job_runner_key);


--
-- Name: clusteredjob_nextrun_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX clusteredjob_nextrun_idx ON clusteredjob USING btree (next_run);


--
-- Name: confcontext; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX confcontext ON configurationcontext USING btree (projectcategory, project, customfield);


--
-- Name: confcontextfieldconfigscheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX confcontextfieldconfigscheme ON configurationcontext USING btree (fieldconfigscheme);


--
-- Name: confcontextkey; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX confcontextkey ON configurationcontext USING btree (customfield);


--
-- Name: confcontextprojectkey; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX confcontextprojectkey ON configurationcontext USING btree (project, customfield);


--
-- Name: deadletter_lastseen; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX deadletter_lastseen ON deadletter USING btree (last_seen);


--
-- Name: deadletter_msg_server_folder; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX deadletter_msg_server_folder ON deadletter USING btree (message_id, mail_server_id, folder_name);


--
-- Name: draft_workflow_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX draft_workflow_scheme ON draftworkflowschemeentity USING btree (scheme);


--
-- Name: draft_workflow_scheme_parent; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX draft_workflow_scheme_parent ON draftworkflowscheme USING btree (workflow_scheme_id);


--
-- Name: entityproperty_id_name_key; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX entityproperty_id_name_key ON entity_property USING btree (entity_id, entity_name, property_key);


--
-- Name: entityproperty_key_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX entityproperty_key_name ON entity_property USING btree (property_key, entity_name);


--
-- Name: entitytranslation_locale; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX entitytranslation_locale ON entity_translation USING btree (locale);


--
-- Name: entpropindexdoc_module; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX entpropindexdoc_module ON entity_property_index_document USING btree (plugin_key, module_key);


--
-- Name: ext_entity_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX ext_entity_name ON external_entities USING btree (name);


--
-- Name: favourite_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX favourite_index ON favouriteassociations USING btree (username, entitytype, entityid);


--
-- Name: fc_fieldid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fc_fieldid ON fieldconfiguration USING btree (fieldid);


--
-- Name: fcs_fieldid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fcs_fieldid ON fieldconfigscheme USING btree (fieldid);


--
-- Name: fcs_issuetype; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fcs_issuetype ON fieldconfigschemeissuetype USING btree (issuetype);


--
-- Name: fcs_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fcs_scheme ON fieldconfigschemeissuetype USING btree (fieldconfigscheme);


--
-- Name: feature_id_userkey; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX feature_id_userkey ON feature USING btree (id, user_key);


--
-- Name: fieldid_fieldconf; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fieldid_fieldconf ON optionconfiguration USING btree (fieldid, fieldconfig);


--
-- Name: fieldid_optionid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fieldid_optionid ON optionconfiguration USING btree (fieldid, optionid);


--
-- Name: fieldlayout_layout; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fieldlayout_layout ON fieldlayoutschemeentity USING btree (fieldlayout);


--
-- Name: fieldlayout_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fieldlayout_scheme ON fieldlayoutschemeentity USING btree (scheme);


--
-- Name: fieldscitem_tab; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fieldscitem_tab ON fieldscreenlayoutitem USING btree (fieldscreentab);


--
-- Name: fieldscreen_field; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fieldscreen_field ON fieldscreenlayoutitem USING btree (fieldidentifier);


--
-- Name: fieldscreen_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fieldscreen_scheme ON issuetypescreenschemeentity USING btree (fieldscreenscheme);


--
-- Name: fieldscreen_tab; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fieldscreen_tab ON fieldscreentab USING btree (fieldscreen);


--
-- Name: fl_scheme_assoc; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX fl_scheme_assoc ON fieldlayoutschemeassociation USING btree (project, issuetype);


--
-- Name: historystep_entryid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX historystep_entryid ON os_historystep USING btree (entry_id);


--
-- Name: idx_all_project_ids; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_all_project_ids ON project_key USING btree (project_id);


--
-- Name: idx_all_project_keys; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX idx_all_project_keys ON project_key USING btree (project_key);


--
-- Name: idx_audit_item_log_id2; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_audit_item_log_id2 ON audit_item USING btree (log_id);


--
-- Name: idx_audit_log_created; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_audit_log_created ON audit_log USING btree (created);


--
-- Name: idx_board_board_ids; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_board_board_ids ON boardproject USING btree (board_id);


--
-- Name: idx_board_project_ids; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_board_project_ids ON boardproject USING btree (project_id);


--
-- Name: idx_changed_value_log_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_changed_value_log_id ON audit_changed_value USING btree (log_id);


--
-- Name: idx_cli_fieldidentifier; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_cli_fieldidentifier ON columnlayoutitem USING btree (fieldidentifier);


--
-- Name: idx_component_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_component_name ON component USING btree (cname);


--
-- Name: idx_component_project; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_component_project ON component USING btree (project);


--
-- Name: idx_directory_active; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_directory_active ON cwd_directory USING btree (active);


--
-- Name: idx_directory_impl; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_directory_impl ON cwd_directory USING btree (lower_impl_class);


--
-- Name: idx_directory_type; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_directory_type ON cwd_directory USING btree (directory_type);


--
-- Name: idx_display_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_display_name ON cwd_user USING btree (lower_display_name);


--
-- Name: idx_email_address; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_email_address ON cwd_user USING btree (lower_email_address);


--
-- Name: idx_first_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_first_name ON cwd_user USING btree (lower_first_name);


--
-- Name: idx_fli_fieldidentifier; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_fli_fieldidentifier ON fieldlayoutitem USING btree (fieldidentifier);


--
-- Name: idx_fli_fieldlayout; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_fli_fieldlayout ON fieldlayoutitem USING btree (fieldlayout);


--
-- Name: idx_group_active; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_group_active ON cwd_group USING btree (lower_group_name, active);


--
-- Name: idx_group_attr_dir_name_lval; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_group_attr_dir_name_lval ON cwd_group_attributes USING btree (directory_id, attribute_name, lower_attribute_value);


--
-- Name: idx_group_dir_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_group_dir_id ON cwd_group USING btree (directory_id);


--
-- Name: idx_last_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_last_name ON cwd_user USING btree (lower_last_name);


--
-- Name: idx_mem_dir_child; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_mem_dir_child ON cwd_membership USING btree (lower_child_name, membership_type, directory_id);


--
-- Name: idx_mem_dir_parent; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_mem_dir_parent ON cwd_membership USING btree (lower_parent_name, membership_type, directory_id);


--
-- Name: idx_mem_dir_parent_child; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_mem_dir_parent_child ON cwd_membership USING btree (lower_parent_name, lower_child_name, membership_type, directory_id);


--
-- Name: idx_old_issue_key; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX idx_old_issue_key ON moved_issue_key USING btree (old_issue_key);


--
-- Name: idx_parent_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_parent_name ON jiraworkflowstatuses USING btree (parentname);


--
-- Name: idx_project_category_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_project_category_name ON projectcategory USING btree (cname);


--
-- Name: idx_project_key; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX idx_project_key ON project USING btree (pkey);


--
-- Name: idx_qrtz_ft_inst_job_req_rcvry; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_ft_inst_job_req_rcvry ON jquartz_fired_triggers USING btree (sched_name, instance_name, requests_recovery);


--
-- Name: idx_qrtz_ft_j_g; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_ft_j_g ON jquartz_fired_triggers USING btree (sched_name, job_name, job_group);


--
-- Name: idx_qrtz_ft_jg; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_ft_jg ON jquartz_fired_triggers USING btree (sched_name, job_group);


--
-- Name: idx_qrtz_ft_t_g; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_ft_t_g ON jquartz_fired_triggers USING btree (sched_name, trigger_name, trigger_group);


--
-- Name: idx_qrtz_ft_tg; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_ft_tg ON jquartz_fired_triggers USING btree (sched_name, trigger_group);


--
-- Name: idx_qrtz_ft_trig_inst_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_ft_trig_inst_name ON jquartz_fired_triggers USING btree (sched_name, instance_name);


--
-- Name: idx_qrtz_j_g; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_j_g ON jquartz_triggers USING btree (sched_name, trigger_group);


--
-- Name: idx_qrtz_j_grp; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_j_grp ON jquartz_job_details USING btree (sched_name, job_group);


--
-- Name: idx_qrtz_j_req_recovery; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_j_req_recovery ON jquartz_job_details USING btree (sched_name, requests_recovery);


--
-- Name: idx_qrtz_j_state; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_j_state ON jquartz_triggers USING btree (sched_name, trigger_state);


--
-- Name: idx_qrtz_t_c; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_c ON jquartz_triggers USING btree (sched_name, calendar_name);


--
-- Name: idx_qrtz_t_j; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_j ON jquartz_triggers USING btree (sched_name, job_name, job_group);


--
-- Name: idx_qrtz_t_jg; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_jg ON jquartz_triggers USING btree (sched_name, job_group);


--
-- Name: idx_qrtz_t_n_g_state; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_n_g_state ON jquartz_triggers USING btree (sched_name, trigger_group, trigger_state);


--
-- Name: idx_qrtz_t_n_state; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_n_state ON jquartz_triggers USING btree (sched_name, trigger_name, trigger_group, trigger_state);


--
-- Name: idx_qrtz_t_next_fire_time; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_next_fire_time ON jquartz_triggers USING btree (sched_name, next_fire_time);


--
-- Name: idx_qrtz_t_nft_misfire; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_nft_misfire ON jquartz_triggers USING btree (sched_name, misfire_instr, next_fire_time);


--
-- Name: idx_qrtz_t_nft_st; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_nft_st ON jquartz_triggers USING btree (sched_name, trigger_state, next_fire_time);


--
-- Name: idx_qrtz_t_nft_st_misfire; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_nft_st_misfire ON jquartz_triggers USING btree (sched_name, misfire_instr, next_fire_time, trigger_state);


--
-- Name: idx_qrtz_t_nft_st_misfire_grp; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_qrtz_t_nft_st_misfire_grp ON jquartz_triggers USING btree (sched_name, misfire_instr, next_fire_time, trigger_group, trigger_state);


--
-- Name: idx_reindex_component_req_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_reindex_component_req_id ON reindex_component USING btree (request_id);


--
-- Name: idx_tam_by_created_time; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_tam_by_created_time ON tempattachmentsmonitor USING btree (created_time);


--
-- Name: idx_tam_by_form_token; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_tam_by_form_token ON tempattachmentsmonitor USING btree (form_token);


--
-- Name: idx_user_attr_dir_name_lval; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_user_attr_dir_name_lval ON cwd_user_attributes USING btree (directory_id, attribute_name, lower_attribute_value);


--
-- Name: idx_version_project; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_version_project ON projectversion USING btree (project);


--
-- Name: idx_version_sequence; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX idx_version_sequence ON projectversion USING btree (sequence);


--
-- Name: index_ao_0201f0_sta1140767333; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_0201f0_sta1140767333 ON "AO_0201F0_STATS_EVENT" USING btree ("EVENT_TIME");


--
-- Name: index_ao_0201f0_sta556470213; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_0201f0_sta556470213 ON "AO_0201F0_STATS_EVENT_PARAM" USING btree ("STATS_EVENT_ID");


--
-- Name: index_ao_21f425_mes1965715920; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_21f425_mes1965715920 ON "AO_21F425_MESSAGE_MAPPING_AO" USING btree ("MESSAGE_ID");


--
-- Name: index_ao_21f425_mes223897723; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_21f425_mes223897723 ON "AO_21F425_MESSAGE_MAPPING_AO" USING btree ("USER_HASH");


--
-- Name: index_ao_21f425_use1458667739; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_21f425_use1458667739 ON "AO_21F425_USER_PROPERTY_AO" USING btree ("USER");


--
-- Name: index_ao_2c4e5c_mai1011024424; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_2c4e5c_mai1011024424 ON "AO_2C4E5C_MAILITEMAUDIT" USING btree ("MAIL_ITEM_ID");


--
-- Name: index_ao_2c4e5c_mai737633864; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_2c4e5c_mai737633864 ON "AO_2C4E5C_MAILHANDLER" USING btree ("MAIL_CHANNEL_ID");


--
-- Name: index_ao_2c4e5c_mai809659826; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_2c4e5c_mai809659826 ON "AO_2C4E5C_MAILCHANNEL" USING btree ("MAIL_CONNECTION_ID");


--
-- Name: index_ao_2c4e5c_mai9620346; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_2c4e5c_mai9620346 ON "AO_2C4E5C_MAILITEMCHUNK" USING btree ("MAIL_ITEM_ID");


--
-- Name: index_ao_319474_mes1143973536; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_mes1143973536 ON "AO_319474_MESSAGE_PROPERTY" USING btree ("MESSAGE_ID");


--
-- Name: index_ao_319474_mes1697012995; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_mes1697012995 ON "AO_319474_MESSAGE" USING btree ("CONTENT_TYPE");


--
-- Name: index_ao_319474_mes1814461114; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_mes1814461114 ON "AO_319474_MESSAGE" USING btree ("QUEUE_ID");


--
-- Name: index_ao_319474_mes1815442463; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_mes1815442463 ON "AO_319474_MESSAGE" USING btree ("PRIORITY");


--
-- Name: index_ao_319474_mes33041000; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_mes33041000 ON "AO_319474_MESSAGE" USING btree ("CLAIMANT");


--
-- Name: index_ao_319474_mes525710975; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_mes525710975 ON "AO_319474_MESSAGE" USING btree ("CREATED_TIME");


--
-- Name: index_ao_319474_message_msg_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_message_msg_id ON "AO_319474_MESSAGE" USING btree ("MSG_ID");


--
-- Name: index_ao_319474_que568114656; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_que568114656 ON "AO_319474_QUEUE_PROPERTY" USING btree ("QUEUE_ID");


--
-- Name: index_ao_319474_queue_claimant; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_queue_claimant ON "AO_319474_QUEUE" USING btree ("CLAIMANT");


--
-- Name: index_ao_319474_queue_topic; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_319474_queue_topic ON "AO_319474_QUEUE" USING btree ("TOPIC");


--
-- Name: index_ao_38321b_cus1828044926; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_38321b_cus1828044926 ON "AO_38321B_CUSTOM_CONTENT_LINK" USING btree ("CONTENT_KEY");


--
-- Name: index_ao_4789dd_tas42846517; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_4789dd_tas42846517 ON "AO_4789DD_TASK_MONITOR" USING btree ("TASK_MONITOR_KIND");


--
-- Name: index_ao_4e8ae6_not1081986701; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_4e8ae6_not1081986701 ON "AO_4E8AE6_NOTIF_BATCH_QUEUE" USING btree ("RECIPIENT_ID");


--
-- Name: index_ao_4e8ae6_not1193702477; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_4e8ae6_not1193702477 ON "AO_4E8AE6_NOTIF_BATCH_QUEUE" USING btree ("ISSUE_ID");


--
-- Name: index_ao_4e8ae6_not1949617122; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_4e8ae6_not1949617122 ON "AO_4E8AE6_NOTIF_BATCH_QUEUE" USING btree ("SENT_TIME");


--
-- Name: index_ao_4e8ae6_not850480572; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_4e8ae6_not850480572 ON "AO_4E8AE6_NOTIF_BATCH_QUEUE" USING btree ("EVENT_TIME");


--
-- Name: index_ao_54307e_cap794737505; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_cap794737505 ON "AO_54307E_CAPABILITY" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_con1117703894; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_con1117703894 ON "AO_54307E_CONFLUENCEKBLABELS" USING btree ("FORM_ID");


--
-- Name: index_ao_54307e_con1483953915; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_con1483953915 ON "AO_54307E_CONFLUENCEKBENABLED" USING btree ("CONFLUENCE_KBID");


--
-- Name: index_ao_54307e_con15134888; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_con15134888 ON "AO_54307E_CONFLUENCEKB" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_con1589156183; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_con1589156183 ON "AO_54307E_CONFLUENCEKBLABELS" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_con1935875239; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_con1935875239 ON "AO_54307E_CONFLUENCEKBENABLED" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_con534365480; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_con534365480 ON "AO_54307E_CONFLUENCEKBENABLED" USING btree ("FORM_ID");


--
-- Name: index_ao_54307e_con714018041; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_con714018041 ON "AO_54307E_CONFLUENCEKBLABELS" USING btree ("CONFLUENCE_KBID");


--
-- Name: index_ao_54307e_ema1343392873; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_ema1343392873 ON "AO_54307E_EMAILCHANNELSETTING" USING btree ("REQUEST_TYPE_ID");


--
-- Name: index_ao_54307e_ema1700110113; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_ema1700110113 ON "AO_54307E_EMAILSETTINGS" USING btree ("REQUEST_TYPE_ID");


--
-- Name: index_ao_54307e_ema1899742512; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_ema1899742512 ON "AO_54307E_EMAILCHANNELSETTING" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_ema648278202; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_ema648278202 ON "AO_54307E_EMAILSETTINGS" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_goa1428193453; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_goa1428193453 ON "AO_54307E_GOAL" USING btree ("TIME_METRIC_ID");


--
-- Name: index_ao_54307e_gro236699933; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_gro236699933 ON "AO_54307E_GROUP" USING btree ("VIEWPORT_ID");


--
-- Name: index_ao_54307e_gro66949553; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_gro66949553 ON "AO_54307E_GROUPTOREQUESTTYPE" USING btree ("REQUEST_TYPE_ID");


--
-- Name: index_ao_54307e_gro832680666; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_gro832680666 ON "AO_54307E_GROUPTOREQUESTTYPE" USING btree ("GROUP_ID");


--
-- Name: index_ao_54307e_met1835144061; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_met1835144061 ON "AO_54307E_METRICCONDITION" USING btree ("TIME_METRIC_ID");


--
-- Name: index_ao_54307e_org1226133886; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_org1226133886 ON "AO_54307E_ORGANIZATION_PROJECT" USING btree ("PROJECT_ID");


--
-- Name: index_ao_54307e_org1427239366; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_org1427239366 ON "AO_54307E_ORGANIZATION_PROJECT" USING btree ("ORGANIZATION_ID");


--
-- Name: index_ao_54307e_org1628402717; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_org1628402717 ON "AO_54307E_ORGANIZATION_MEMBER" USING btree ("ORGANIZATION_ID");


--
-- Name: index_ao_54307e_org240106076; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_org240106076 ON "AO_54307E_ORGANIZATION" USING btree ("LOWER_NAME");


--
-- Name: index_ao_54307e_org724569035; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_org724569035 ON "AO_54307E_ORGANIZATION_MEMBER" USING btree ("USER_KEY");


--
-- Name: index_ao_54307e_out808665536; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_out808665536 ON "AO_54307E_OUT_EMAIL_SETTINGS" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_par1577879197; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_par1577879197 ON "AO_54307E_PARTICIPANTSETTINGS" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_que1043532634; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_que1043532634 ON "AO_54307E_QUEUE" USING btree ("PROJECT_KEY");


--
-- Name: index_ao_54307e_que104885056; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_que104885056 ON "AO_54307E_QUEUE" USING btree ("PROJECT_ID");


--
-- Name: index_ao_54307e_que1582220046; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_que1582220046 ON "AO_54307E_QUEUECOLUMN" USING btree ("QUEUE_ID");


--
-- Name: index_ao_54307e_rep1824414723; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_rep1824414723 ON "AO_54307E_REPORT" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_ser1130618333; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_ser1130618333 ON "AO_54307E_SERVICEDESK" USING btree ("PROJECT_ID");


--
-- Name: index_ao_54307e_ser315001103; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_ser315001103 ON "AO_54307E_SERIES" USING btree ("REPORT_ID");


--
-- Name: index_ao_54307e_sla1843930481; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_sla1843930481 ON "AO_54307E_SLAAUDITLOG" USING btree ("SLA_ID");


--
-- Name: index_ao_54307e_sla384441158; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_sla384441158 ON "AO_54307E_SLAAUDITLOGDATA" USING btree ("SLA_AUDIT_LOG_ID");


--
-- Name: index_ao_54307e_sla400544864; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_sla400544864 ON "AO_54307E_SLAAUDITLOG" USING btree ("ISSUE_ID");


--
-- Name: index_ao_54307e_sta805003358; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_sta805003358 ON "AO_54307E_STATUSMAPPING" USING btree ("FORM_ID");


--
-- Name: index_ao_54307e_sub749517042; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_sub749517042 ON "AO_54307E_SUBSCRIPTION" USING btree ("ISSUE_ID");


--
-- Name: index_ao_54307e_thr78312131; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_thr78312131 ON "AO_54307E_THRESHOLD" USING btree ("TIME_METRIC_ID");


--
-- Name: index_ao_54307e_tim1369106182; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_tim1369106182 ON "AO_54307E_TIMEMETRIC" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_54307e_vie1002606617; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_vie1002606617 ON "AO_54307E_VIEWPORT" USING btree ("THEME_ID");


--
-- Name: index_ao_54307e_vie1086593335; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_vie1086593335 ON "AO_54307E_VIEWPORT" USING btree ("PROJECT_ID");


--
-- Name: index_ao_54307e_vie1674919217; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_vie1674919217 ON "AO_54307E_VIEWPORTFIELDVALUE" USING btree ("FIELD_ID");


--
-- Name: index_ao_54307e_vie1903424282; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_vie1903424282 ON "AO_54307E_VIEWPORTFIELD" USING btree ("FORM_ID");


--
-- Name: index_ao_54307e_vie877344976; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_vie877344976 ON "AO_54307E_VIEWPORTFORM" USING btree ("VIEWPORT_ID");


--
-- Name: index_ao_54307e_vie883303387; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_54307e_vie883303387 ON "AO_54307E_VIEWPORTFORM" USING btree ("KEY");


--
-- Name: index_ao_550953_sho1778115994; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_550953_sho1778115994 ON "AO_550953_SHORTCUT" USING btree ("PROJECT_ID");


--
-- Name: index_ao_563aee_act1642652291; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_563aee_act1642652291 ON "AO_563AEE_ACTIVITY_ENTITY" USING btree ("OBJECT_ID");


--
-- Name: index_ao_563aee_act1978295567; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_563aee_act1978295567 ON "AO_563AEE_ACTIVITY_ENTITY" USING btree ("TARGET_ID");


--
-- Name: index_ao_563aee_act972488439; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_563aee_act972488439 ON "AO_563AEE_ACTIVITY_ENTITY" USING btree ("ICON_ID");


--
-- Name: index_ao_563aee_act995325379; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_563aee_act995325379 ON "AO_563AEE_ACTIVITY_ENTITY" USING btree ("ACTOR_ID");


--
-- Name: index_ao_563aee_obj696886343; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_563aee_obj696886343 ON "AO_563AEE_OBJECT_ENTITY" USING btree ("IMAGE_ID");


--
-- Name: index_ao_563aee_tar521440921; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_563aee_tar521440921 ON "AO_563AEE_TARGET_ENTITY" USING btree ("IMAGE_ID");


--
-- Name: index_ao_56464c_app107561580; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_56464c_app107561580 ON "AO_56464C_APPROVAL" USING btree ("ISSUE_ID");


--
-- Name: index_ao_56464c_app1713999422; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_56464c_app1713999422 ON "AO_56464C_APPROVERDECISION" USING btree ("APPROVAL_ID");


--
-- Name: index_ao_56464c_app893380122; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_56464c_app893380122 ON "AO_56464C_APPROVER" USING btree ("APPROVAL_ID");


--
-- Name: index_ao_56464c_not5371299; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_56464c_not5371299 ON "AO_56464C_NOTIFICATIONRECORD" USING btree ("APPROVAL_ID");


--
-- Name: index_ao_587b34_pro1732672724; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_587b34_pro1732672724 ON "AO_587B34_PROJECT_CONFIG" USING btree ("ROOM_ID");


--
-- Name: index_ao_587b34_pro193829489; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_587b34_pro193829489 ON "AO_587B34_PROJECT_CONFIG" USING btree ("CONFIGURATION_GROUP_ID");


--
-- Name: index_ao_587b34_pro2093917684; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_587b34_pro2093917684 ON "AO_587B34_PROJECT_CONFIG" USING btree ("PROJECT_ID");


--
-- Name: index_ao_587b34_pro2115480362; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_587b34_pro2115480362 ON "AO_587B34_PROJECT_CONFIG" USING btree ("NAME");


--
-- Name: index_ao_5fb9d7_aoh1981563178; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_5fb9d7_aoh1981563178 ON "AO_5FB9D7_AOHIP_CHAT_USER" USING btree ("USER_KEY");


--
-- Name: index_ao_5fb9d7_aoh49772492; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_5fb9d7_aoh49772492 ON "AO_5FB9D7_AOHIP_CHAT_USER" USING btree ("HIP_CHAT_LINK_ID");


--
-- Name: index_ao_733371_eve1266474620; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_eve1266474620 ON "AO_733371_EVENT_RECIPIENT" USING btree ("CONSUMER_NAME");


--
-- Name: index_ao_733371_eve1423945899; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_eve1423945899 ON "AO_733371_EVENT_PARAMETER" USING btree ("NAME");


--
-- Name: index_ao_733371_eve1645451632; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_eve1645451632 ON "AO_733371_EVENT_RECIPIENT" USING btree ("EVENT_ID");


--
-- Name: index_ao_733371_eve525098581; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_eve525098581 ON "AO_733371_EVENT_RECIPIENT" USING btree ("SEND_DATE");


--
-- Name: index_ao_733371_eve673683319; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_eve673683319 ON "AO_733371_EVENT" USING btree ("EVENT_BUNDLE_ID");


--
-- Name: index_ao_733371_eve678699426; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_eve678699426 ON "AO_733371_EVENT_RECIPIENT" USING btree ("STATUS");


--
-- Name: index_ao_733371_eve704112384; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_eve704112384 ON "AO_733371_EVENT_PARAMETER" USING btree ("EVENT_ID");


--
-- Name: index_ao_733371_eve902883849; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_eve902883849 ON "AO_733371_EVENT" USING btree ("EVENT_TYPE");


--
-- Name: index_ao_733371_event_action; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_event_action ON "AO_733371_EVENT" USING btree ("ACTION");


--
-- Name: index_ao_733371_event_created; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_event_created ON "AO_733371_EVENT" USING btree ("CREATED");


--
-- Name: index_ao_733371_event_user_key; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_733371_event_user_key ON "AO_733371_EVENT" USING btree ("USER_KEY");


--
-- Name: index_ao_7a2604_hol2069343764; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_7a2604_hol2069343764 ON "AO_7A2604_HOLIDAY" USING btree ("CALENDAR_ID");


--
-- Name: index_ao_7a2604_wor1607107950; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_7a2604_wor1607107950 ON "AO_7A2604_WORKINGTIME" USING btree ("CALENDAR_ID");


--
-- Name: index_ao_9b2e3b_exe1939877636; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_exe1939877636 ON "AO_9B2E3B_EXEC_RULE_MSG_ITEM" USING btree ("RULE_EXECUTION_ID");


--
-- Name: index_ao_9b2e3b_if_1335518770; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_if_1335518770 ON "AO_9B2E3B_IF_THEN" USING btree ("RULE_ID");


--
-- Name: index_ao_9b2e3b_if_180675374; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_if_180675374 ON "AO_9B2E3B_IF_THEN_EXECUTION" USING btree ("RULE_EXECUTION_ID");


--
-- Name: index_ao_9b2e3b_if_1918018153; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_if_1918018153 ON "AO_9B2E3B_IF_COND_CONF_DATA" USING btree ("IF_CONDITION_CONFIG_ID");


--
-- Name: index_ao_9b2e3b_if_469154523; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_if_469154523 ON "AO_9B2E3B_IF_EXECUTION" USING btree ("IF_THEN_EXECUTION_ID");


--
-- Name: index_ao_9b2e3b_if_939960910; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_if_939960910 ON "AO_9B2E3B_IF_COND_EXECUTION" USING btree ("IF_EXECUTION_ID");


--
-- Name: index_ao_9b2e3b_if_94326430; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_if_94326430 ON "AO_9B2E3B_IF_CONDITION_CONFIG" USING btree ("IF_THEN_ID");


--
-- Name: index_ao_9b2e3b_rse1331505122; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_rse1331505122 ON "AO_9B2E3B_RSETREV_USER_CONTEXT" USING btree ("RULESET_REVISION_ID");


--
-- Name: index_ao_9b2e3b_rse1358405456; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_rse1358405456 ON "AO_9B2E3B_RSETREV_PROJ_CONTEXT" USING btree ("RULESET_REVISION_ID");


--
-- Name: index_ao_9b2e3b_rul106976704; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_rul106976704 ON "AO_9B2E3B_RULESET_REVISION" USING btree ("RULE_SET_ID");


--
-- Name: index_ao_9b2e3b_rul1074875245; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_rul1074875245 ON "AO_9B2E3B_RULE" USING btree ("RULESET_REVISION_ID");


--
-- Name: index_ao_9b2e3b_the1398825512; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_the1398825512 ON "AO_9B2E3B_THEN_ACT_CONF_DATA" USING btree ("THEN_ACTION_CONFIG_ID");


--
-- Name: index_ao_9b2e3b_the293379586; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_the293379586 ON "AO_9B2E3B_THEN_ACT_EXECUTION" USING btree ("THEN_EXECUTION_ID");


--
-- Name: index_ao_9b2e3b_the554290529; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_the554290529 ON "AO_9B2E3B_THEN_ACTION_CONFIG" USING btree ("IF_THEN_ID");


--
-- Name: index_ao_9b2e3b_the769956389; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_the769956389 ON "AO_9B2E3B_THEN_EXECUTION" USING btree ("IF_THEN_EXECUTION_ID");


--
-- Name: index_ao_9b2e3b_whe1231833619; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_whe1231833619 ON "AO_9B2E3B_WHEN_HAND_CONF_DATA" USING btree ("WHEN_HANDLER_CONFIG_ID");


--
-- Name: index_ao_9b2e3b_whe716528203; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_9b2e3b_whe716528203 ON "AO_9B2E3B_WHEN_HANDLER_CONFIG" USING btree ("RULE_ID");


--
-- Name: index_ao_c7f17e_lin1649844463; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_c7f17e_lin1649844463 ON "AO_C7F17E_LINGO_REVISION" USING btree ("LINGO_ID");


--
-- Name: index_ao_c7f17e_lin2056815817; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_c7f17e_lin2056815817 ON "AO_C7F17E_LINGO_TRANSLATION" USING btree ("LINGO_REVISION_ID");


--
-- Name: index_ao_c7f17e_lin523964875; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_c7f17e_lin523964875 ON "AO_C7F17E_LINGO" USING btree ("PROJECT_ID");


--
-- Name: index_ao_c7f17e_lin614942907; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_c7f17e_lin614942907 ON "AO_C7F17E_LINGO" USING btree ("LOGICAL_ID");


--
-- Name: index_ao_c7f17e_pro1274623457; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_c7f17e_pro1274623457 ON "AO_C7F17E_PROJECT_LANG_CONFIG" USING btree ("PROJECT_ID");


--
-- Name: index_ao_c7f17e_pro2026674159; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_c7f17e_pro2026674159 ON "AO_C7F17E_PROJECT_LANGUAGE" USING btree ("PROJECT_LANG_REV_ID");


--
-- Name: index_ao_c7f17e_pro847365574; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_c7f17e_pro847365574 ON "AO_C7F17E_PROJECT_LANG_REV" USING btree ("PROJECT_LANG_CONFIG_ID");


--
-- Name: index_ao_d530bb_can167094368; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_can167094368 ON "AO_D530BB_CANNEDRESPONSEUSAGE" USING btree ("CANNED_RESPONSE_ID");


--
-- Name: index_ao_d530bb_can1850228660; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_can1850228660 ON "AO_D530BB_CANNEDRESPONSEUSAGE" USING btree ("USAGE_TIME");


--
-- Name: index_ao_d530bb_can1880088006; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_can1880088006 ON "AO_D530BB_CANNEDRESPONSEAUDIT" USING btree ("USER_KEY");


--
-- Name: index_ao_d530bb_can1883970147; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_can1883970147 ON "AO_D530BB_CANNEDRESPONSE" USING btree ("SERVICE_DESK_ID");


--
-- Name: index_ao_d530bb_can474028001; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_can474028001 ON "AO_D530BB_CANNEDRESPONSEAUDIT" USING btree ("EVENT_TIME");


--
-- Name: index_ao_d530bb_can762862566; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_can762862566 ON "AO_D530BB_CANNEDRESPONSEAUDIT" USING btree ("CANNED_RESPONSE_ID");


--
-- Name: index_ao_d530bb_can955130060; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_can955130060 ON "AO_D530BB_CANNEDRESPONSEUSAGE" USING btree ("USER_KEY");


--
-- Name: index_ao_d530bb_cra1221426719; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_cra1221426719 ON "AO_D530BB_CRAUDITACTIONDATA" USING btree ("DATA_COLUMN_NAME");


--
-- Name: index_ao_d530bb_cra1494025551; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_cra1494025551 ON "AO_D530BB_CRAUDITACTIONDATA" USING btree ("DATA_TYPE");


--
-- Name: index_ao_d530bb_cra384497034; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_d530bb_cra384497034 ON "AO_D530BB_CRAUDITACTIONDATA" USING btree ("CANNED_RESPONSE_AUDIT_LOG_ID");


--
-- Name: index_ao_ed669c_see20117222; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_ed669c_see20117222 ON "AO_ED669C_SEEN_ASSERTIONS" USING btree ("EXPIRY_TIMESTAMP");


--
-- Name: index_ao_f1b27b_key473010270; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_f1b27b_key473010270 ON "AO_F1B27B_KEY_COMPONENT" USING btree ("TIMED_PROMISE_ID");


--
-- Name: index_ao_f1b27b_key828117107; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX index_ao_f1b27b_key828117107 ON "AO_F1B27B_KEY_COMP_HISTORY" USING btree ("TIMED_PROMISE_ID");


--
-- Name: issue_assignee; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_assignee ON jiraissue USING btree (assignee);


--
-- Name: issue_created; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_created ON jiraissue USING btree (created);


--
-- Name: issue_duedate; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_duedate ON jiraissue USING btree (duedate);


--
-- Name: issue_proj_num; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_proj_num ON jiraissue USING btree (issuenum, project);


--
-- Name: issue_proj_status; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_proj_status ON jiraissue USING btree (project, issuestatus);


--
-- Name: issue_reporter; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_reporter ON jiraissue USING btree (reporter);


--
-- Name: issue_resolutiondate; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_resolutiondate ON jiraissue USING btree (resolutiondate);


--
-- Name: issue_updated; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_updated ON jiraissue USING btree (updated);


--
-- Name: issue_votes; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_votes ON jiraissue USING btree (votes);


--
-- Name: issue_watches; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_watches ON jiraissue USING btree (watches);


--
-- Name: issue_workflow; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issue_workflow ON jiraissue USING btree (workflow_id);


--
-- Name: issuelink_dest; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issuelink_dest ON issuelink USING btree (destination);


--
-- Name: issuelink_src; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issuelink_src ON issuelink USING btree (source);


--
-- Name: issuelink_type; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX issuelink_type ON issuelink USING btree (linktype);


--
-- Name: label_fieldissue; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX label_fieldissue ON label USING btree (issue, fieldid);


--
-- Name: label_fieldissuelabel; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX label_fieldissuelabel ON label USING btree (issue, fieldid, label);


--
-- Name: label_issue; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX label_issue ON label USING btree (issue);


--
-- Name: label_label; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX label_label ON label USING btree (label);


--
-- Name: licenseroledefault_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX licenseroledefault_index ON licenserolesdefault USING btree (license_role_name);


--
-- Name: licenserolegroup_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX licenserolegroup_index ON licenserolesgroup USING btree (license_role_name, group_id);


--
-- Name: linktypename; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX linktypename ON issuelinktype USING btree (linkname);


--
-- Name: linktypestyle; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX linktypestyle ON issuelinktype USING btree (pstyle);


--
-- Name: managedconfigitem_id_type_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX managedconfigitem_id_type_idx ON managedconfigurationitem USING btree (item_id, item_type);


--
-- Name: mshipbase_group; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX mshipbase_group ON membershipbase USING btree (group_name);


--
-- Name: mshipbase_user; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX mshipbase_user ON membershipbase USING btree (user_name);


--
-- Name: node_id_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX node_id_idx ON nodeindexcounter USING btree (node_id, sending_node_id);


--
-- Name: node_operation_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX node_operation_idx ON replicatedindexoperation USING btree (node_id, affected_index, operation, index_time);


--
-- Name: node_sink; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX node_sink ON nodeassociation USING btree (sink_node_id, sink_node_entity);


--
-- Name: node_source; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX node_source ON nodeassociation USING btree (source_node_id, source_node_entity);


--
-- Name: notif_messageid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX notif_messageid ON notificationinstance USING btree (messageid);


--
-- Name: notif_source; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX notif_source ON notificationinstance USING btree (source);


--
-- Name: ntfctn_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX ntfctn_scheme ON notification USING btree (scheme);


--
-- Name: oauth_consumer_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX oauth_consumer_index ON oauthconsumer USING btree (consumer_key);


--
-- Name: oauth_consumer_service_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX oauth_consumer_service_index ON oauthconsumer USING btree (consumerservice);


--
-- Name: oauth_consumer_token_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX oauth_consumer_token_index ON oauthconsumertoken USING btree (token);


--
-- Name: oauth_consumer_token_key_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX oauth_consumer_token_key_index ON oauthconsumertoken USING btree (token_key);


--
-- Name: oauth_sp_consumer_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX oauth_sp_consumer_index ON oauthspconsumer USING btree (consumer_key);


--
-- Name: oauth_sp_consumer_key_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX oauth_sp_consumer_key_index ON oauthsptoken USING btree (consumer_key);


--
-- Name: oauth_sp_token_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX oauth_sp_token_index ON oauthsptoken USING btree (token);


--
-- Name: ordernumber_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX ordernumber_idx ON clusterupgradestate USING btree (order_number);


--
-- Name: osgroup_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX osgroup_name ON groupbase USING btree (groupname);


--
-- Name: osproperty_entid_name_propkey; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX osproperty_entid_name_propkey ON propertyentry USING btree (entity_id, entity_name, property_key);


--
-- Name: osproperty_propertykey; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX osproperty_propertykey ON propertyentry USING btree (property_key);


--
-- Name: osuser_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX osuser_name ON userbase USING btree (username);


--
-- Name: permission_key_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX permission_key_idx ON schemepermissions USING btree (permission_key);


--
-- Name: ppage_username; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX ppage_username ON portalpage USING btree (username);


--
-- Name: prmssn_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX prmssn_scheme ON schemepermissions USING btree (scheme);


--
-- Name: prmssn_scheme_attr_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX prmssn_scheme_attr_idx ON permissionschemeattribute USING btree (scheme);


--
-- Name: prmssn_scheme_attr_key_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX prmssn_scheme_attr_key_idx ON permissionschemeattribute USING btree (scheme, attribute_key);


--
-- Name: remembermetoken_username_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX remembermetoken_username_index ON remembermetoken USING btree (username);


--
-- Name: remotelink_globalid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX remotelink_globalid ON remotelink USING btree (globalid);


--
-- Name: remotelink_issueid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX remotelink_issueid ON remotelink USING btree (issueid, globalid);


--
-- Name: role_pid_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX role_pid_idx ON projectroleactor USING btree (pid);


--
-- Name: role_player_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX role_player_idx ON projectroleactor USING btree (projectroleid, pid);


--
-- Name: rundetails_jobid_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX rundetails_jobid_idx ON rundetails USING btree (job_id);


--
-- Name: rundetails_starttime_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX rundetails_starttime_idx ON rundetails USING btree (start_time);


--
-- Name: screenitem_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX screenitem_scheme ON fieldscreenschemeitem USING btree (fieldscreenscheme);


--
-- Name: searchrequest_filternamelower; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX searchrequest_filternamelower ON searchrequest USING btree (filtername_lower);


--
-- Name: sec_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX sec_scheme ON schemeissuesecurities USING btree (scheme);


--
-- Name: sec_security; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX sec_security ON schemeissuesecurities USING btree (security);


--
-- Name: share_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX share_index ON sharepermissions USING btree (entityid, entitytype);


--
-- Name: source_destination_node_idx; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX source_destination_node_idx ON clustermessage USING btree (source_node, destination_node);


--
-- Name: sr_author; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX sr_author ON searchrequest USING btree (authorname);


--
-- Name: subscrpt_user; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX subscrpt_user ON filtersubscription USING btree (filter_i_d, username);


--
-- Name: subscrptn_group; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX subscrptn_group ON filtersubscription USING btree (filter_i_d, groupname);


--
-- Name: trustedapp_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX trustedapp_id ON trustedapp USING btree (application_id);


--
-- Name: type_key; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX type_key ON genericconfiguration USING btree (datatype, datakey);


--
-- Name: uh_type_user_entity; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uh_type_user_entity ON userhistoryitem USING btree (entitytype, username, entityid);


--
-- Name: uk_application_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uk_application_name ON cwd_application USING btree (lower_application_name);


--
-- Name: uk_directory_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX uk_directory_name ON cwd_directory USING btree (lower_directory_name);


--
-- Name: uk_entitytranslation; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uk_entitytranslation ON entity_translation USING btree (entity_name, entity_id, locale);


--
-- Name: uk_group_attr_name_lval; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uk_group_attr_name_lval ON cwd_group_attributes USING btree (group_id, attribute_name, lower_attribute_value);


--
-- Name: uk_group_name_dir_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uk_group_name_dir_id ON cwd_group USING btree (lower_group_name, directory_id);


--
-- Name: uk_lower_user_name; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uk_lower_user_name ON app_user USING btree (lower_user_name);


--
-- Name: uk_mem_parent_child_type; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uk_mem_parent_child_type ON cwd_membership USING btree (parent_id, child_id, membership_type);


--
-- Name: uk_user_attr_name_lval; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX uk_user_attr_name_lval ON cwd_user_attributes USING btree (user_id, attribute_name);


--
-- Name: uk_user_externalid_dir_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX uk_user_externalid_dir_id ON cwd_user USING btree (external_id, directory_id);


--
-- Name: uk_user_key; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uk_user_key ON app_user USING btree (user_key);


--
-- Name: uk_user_name_dir_id; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE UNIQUE INDEX uk_user_name_dir_id ON cwd_user USING btree (lower_user_name, directory_id);


--
-- Name: upf_customfield; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX upf_customfield ON userpickerfilter USING btree (customfield);


--
-- Name: upf_fieldconfigid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX upf_fieldconfigid ON userpickerfilter USING btree (customfieldconfig);


--
-- Name: user_sink; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX user_sink ON userassociation USING btree (sink_node_id, sink_node_entity);


--
-- Name: user_source; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX user_source ON userassociation USING btree (source_name);


--
-- Name: userpref_portletconfiguration; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX userpref_portletconfiguration ON gadgetuserpreference USING btree (portletconfiguration);


--
-- Name: votehistory_issue_index; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX votehistory_issue_index ON votehistory USING btree (issueid);


--
-- Name: wf_entryid; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX wf_entryid ON os_currentstep USING btree (entry_id);


--
-- Name: workflow_scheme; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX workflow_scheme ON workflowschemeentity USING btree (scheme);


--
-- Name: worklog_author; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX worklog_author ON worklog USING btree (author);


--
-- Name: worklog_issue; Type: INDEX; Schema: public; Owner: jira; Tablespace: 
--

CREATE INDEX worklog_issue ON worklog USING btree (issueid);


--
-- Name: fk_ao_0201f0_stats_event_param_stats_event_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_0201F0_STATS_EVENT_PARAM"
    ADD CONSTRAINT fk_ao_0201f0_stats_event_param_stats_event_id FOREIGN KEY ("STATS_EVENT_ID") REFERENCES "AO_0201F0_STATS_EVENT"("ID");


--
-- Name: fk_ao_2c4e5c_mailchannel_mail_connection_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILCHANNEL"
    ADD CONSTRAINT fk_ao_2c4e5c_mailchannel_mail_connection_id FOREIGN KEY ("MAIL_CONNECTION_ID") REFERENCES "AO_2C4E5C_MAILCONNECTION"("ID");


--
-- Name: fk_ao_2c4e5c_mailhandler_mail_channel_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILHANDLER"
    ADD CONSTRAINT fk_ao_2c4e5c_mailhandler_mail_channel_id FOREIGN KEY ("MAIL_CHANNEL_ID") REFERENCES "AO_2C4E5C_MAILCHANNEL"("ID");


--
-- Name: fk_ao_2c4e5c_mailitemaudit_mail_item_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILITEMAUDIT"
    ADD CONSTRAINT fk_ao_2c4e5c_mailitemaudit_mail_item_id FOREIGN KEY ("MAIL_ITEM_ID") REFERENCES "AO_2C4E5C_MAILITEM"("ID");


--
-- Name: fk_ao_2c4e5c_mailitemchunk_mail_item_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_2C4E5C_MAILITEMCHUNK"
    ADD CONSTRAINT fk_ao_2c4e5c_mailitemchunk_mail_item_id FOREIGN KEY ("MAIL_ITEM_ID") REFERENCES "AO_2C4E5C_MAILITEM"("ID");


--
-- Name: fk_ao_319474_message_property_message_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_319474_MESSAGE_PROPERTY"
    ADD CONSTRAINT fk_ao_319474_message_property_message_id FOREIGN KEY ("MESSAGE_ID") REFERENCES "AO_319474_MESSAGE"("ID");


--
-- Name: fk_ao_319474_message_queue_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_319474_MESSAGE"
    ADD CONSTRAINT fk_ao_319474_message_queue_id FOREIGN KEY ("QUEUE_ID") REFERENCES "AO_319474_QUEUE"("ID");


--
-- Name: fk_ao_319474_queue_property_queue_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_319474_QUEUE_PROPERTY"
    ADD CONSTRAINT fk_ao_319474_queue_property_queue_id FOREIGN KEY ("QUEUE_ID") REFERENCES "AO_319474_QUEUE"("ID");


--
-- Name: fk_ao_54307e_capability_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CAPABILITY"
    ADD CONSTRAINT fk_ao_54307e_capability_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_confluencekb_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKB"
    ADD CONSTRAINT fk_ao_54307e_confluencekb_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_confluencekbenabled_confluence_kbid; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBENABLED"
    ADD CONSTRAINT fk_ao_54307e_confluencekbenabled_confluence_kbid FOREIGN KEY ("CONFLUENCE_KBID") REFERENCES "AO_54307E_CONFLUENCEKB"("ID");


--
-- Name: fk_ao_54307e_confluencekbenabled_form_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBENABLED"
    ADD CONSTRAINT fk_ao_54307e_confluencekbenabled_form_id FOREIGN KEY ("FORM_ID") REFERENCES "AO_54307E_VIEWPORTFORM"("ID");


--
-- Name: fk_ao_54307e_confluencekbenabled_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBENABLED"
    ADD CONSTRAINT fk_ao_54307e_confluencekbenabled_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_confluencekblabels_confluence_kbid; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBLABELS"
    ADD CONSTRAINT fk_ao_54307e_confluencekblabels_confluence_kbid FOREIGN KEY ("CONFLUENCE_KBID") REFERENCES "AO_54307E_CONFLUENCEKB"("ID");


--
-- Name: fk_ao_54307e_confluencekblabels_form_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBLABELS"
    ADD CONSTRAINT fk_ao_54307e_confluencekblabels_form_id FOREIGN KEY ("FORM_ID") REFERENCES "AO_54307E_VIEWPORTFORM"("ID");


--
-- Name: fk_ao_54307e_confluencekblabels_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_CONFLUENCEKBLABELS"
    ADD CONSTRAINT fk_ao_54307e_confluencekblabels_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_emailchannelsetting_request_type_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_EMAILCHANNELSETTING"
    ADD CONSTRAINT fk_ao_54307e_emailchannelsetting_request_type_id FOREIGN KEY ("REQUEST_TYPE_ID") REFERENCES "AO_54307E_VIEWPORTFORM"("ID");


--
-- Name: fk_ao_54307e_emailchannelsetting_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_EMAILCHANNELSETTING"
    ADD CONSTRAINT fk_ao_54307e_emailchannelsetting_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_emailsettings_request_type_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_EMAILSETTINGS"
    ADD CONSTRAINT fk_ao_54307e_emailsettings_request_type_id FOREIGN KEY ("REQUEST_TYPE_ID") REFERENCES "AO_54307E_VIEWPORTFORM"("ID");


--
-- Name: fk_ao_54307e_emailsettings_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_EMAILSETTINGS"
    ADD CONSTRAINT fk_ao_54307e_emailsettings_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_goal_time_metric_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_GOAL"
    ADD CONSTRAINT fk_ao_54307e_goal_time_metric_id FOREIGN KEY ("TIME_METRIC_ID") REFERENCES "AO_54307E_TIMEMETRIC"("ID");


--
-- Name: fk_ao_54307e_group_viewport_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_GROUP"
    ADD CONSTRAINT fk_ao_54307e_group_viewport_id FOREIGN KEY ("VIEWPORT_ID") REFERENCES "AO_54307E_VIEWPORT"("ID");


--
-- Name: fk_ao_54307e_grouptorequesttype_group_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_GROUPTOREQUESTTYPE"
    ADD CONSTRAINT fk_ao_54307e_grouptorequesttype_group_id FOREIGN KEY ("GROUP_ID") REFERENCES "AO_54307E_GROUP"("ID");


--
-- Name: fk_ao_54307e_grouptorequesttype_request_type_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_GROUPTOREQUESTTYPE"
    ADD CONSTRAINT fk_ao_54307e_grouptorequesttype_request_type_id FOREIGN KEY ("REQUEST_TYPE_ID") REFERENCES "AO_54307E_VIEWPORTFORM"("ID");


--
-- Name: fk_ao_54307e_metriccondition_time_metric_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_METRICCONDITION"
    ADD CONSTRAINT fk_ao_54307e_metriccondition_time_metric_id FOREIGN KEY ("TIME_METRIC_ID") REFERENCES "AO_54307E_TIMEMETRIC"("ID");


--
-- Name: fk_ao_54307e_organization_member_organization_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION_MEMBER"
    ADD CONSTRAINT fk_ao_54307e_organization_member_organization_id FOREIGN KEY ("ORGANIZATION_ID") REFERENCES "AO_54307E_ORGANIZATION"("ID");


--
-- Name: fk_ao_54307e_organization_project_organization_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_ORGANIZATION_PROJECT"
    ADD CONSTRAINT fk_ao_54307e_organization_project_organization_id FOREIGN KEY ("ORGANIZATION_ID") REFERENCES "AO_54307E_ORGANIZATION"("ID");


--
-- Name: fk_ao_54307e_out_email_settings_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_OUT_EMAIL_SETTINGS"
    ADD CONSTRAINT fk_ao_54307e_out_email_settings_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_participantsettings_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_PARTICIPANTSETTINGS"
    ADD CONSTRAINT fk_ao_54307e_participantsettings_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_queuecolumn_queue_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_QUEUECOLUMN"
    ADD CONSTRAINT fk_ao_54307e_queuecolumn_queue_id FOREIGN KEY ("QUEUE_ID") REFERENCES "AO_54307E_QUEUE"("ID");


--
-- Name: fk_ao_54307e_report_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_REPORT"
    ADD CONSTRAINT fk_ao_54307e_report_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_series_report_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_SERIES"
    ADD CONSTRAINT fk_ao_54307e_series_report_id FOREIGN KEY ("REPORT_ID") REFERENCES "AO_54307E_REPORT"("ID");


--
-- Name: fk_ao_54307e_slaauditlogdata_sla_audit_log_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_SLAAUDITLOGDATA"
    ADD CONSTRAINT fk_ao_54307e_slaauditlogdata_sla_audit_log_id FOREIGN KEY ("SLA_AUDIT_LOG_ID") REFERENCES "AO_54307E_SLAAUDITLOG"("ID");


--
-- Name: fk_ao_54307e_statusmapping_form_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_STATUSMAPPING"
    ADD CONSTRAINT fk_ao_54307e_statusmapping_form_id FOREIGN KEY ("FORM_ID") REFERENCES "AO_54307E_VIEWPORTFORM"("ID");


--
-- Name: fk_ao_54307e_threshold_time_metric_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_THRESHOLD"
    ADD CONSTRAINT fk_ao_54307e_threshold_time_metric_id FOREIGN KEY ("TIME_METRIC_ID") REFERENCES "AO_54307E_TIMEMETRIC"("ID");


--
-- Name: fk_ao_54307e_timemetric_service_desk_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_TIMEMETRIC"
    ADD CONSTRAINT fk_ao_54307e_timemetric_service_desk_id FOREIGN KEY ("SERVICE_DESK_ID") REFERENCES "AO_54307E_SERVICEDESK"("ID");


--
-- Name: fk_ao_54307e_viewport_theme_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_VIEWPORT"
    ADD CONSTRAINT fk_ao_54307e_viewport_theme_id FOREIGN KEY ("THEME_ID") REFERENCES "AO_54307E_CUSTOMTHEME"("ID");


--
-- Name: fk_ao_54307e_viewportfield_form_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFIELD"
    ADD CONSTRAINT fk_ao_54307e_viewportfield_form_id FOREIGN KEY ("FORM_ID") REFERENCES "AO_54307E_VIEWPORTFORM"("ID");


--
-- Name: fk_ao_54307e_viewportfieldvalue_field_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFIELDVALUE"
    ADD CONSTRAINT fk_ao_54307e_viewportfieldvalue_field_id FOREIGN KEY ("FIELD_ID") REFERENCES "AO_54307E_VIEWPORTFIELD"("ID");


--
-- Name: fk_ao_54307e_viewportform_viewport_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_54307E_VIEWPORTFORM"
    ADD CONSTRAINT fk_ao_54307e_viewportform_viewport_id FOREIGN KEY ("VIEWPORT_ID") REFERENCES "AO_54307E_VIEWPORT"("ID");


--
-- Name: fk_ao_563aee_activity_entity_actor_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_ACTIVITY_ENTITY"
    ADD CONSTRAINT fk_ao_563aee_activity_entity_actor_id FOREIGN KEY ("ACTOR_ID") REFERENCES "AO_563AEE_ACTOR_ENTITY"("ID");


--
-- Name: fk_ao_563aee_activity_entity_icon_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_ACTIVITY_ENTITY"
    ADD CONSTRAINT fk_ao_563aee_activity_entity_icon_id FOREIGN KEY ("ICON_ID") REFERENCES "AO_563AEE_MEDIA_LINK_ENTITY"("ID");


--
-- Name: fk_ao_563aee_activity_entity_object_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_ACTIVITY_ENTITY"
    ADD CONSTRAINT fk_ao_563aee_activity_entity_object_id FOREIGN KEY ("OBJECT_ID") REFERENCES "AO_563AEE_OBJECT_ENTITY"("ID");


--
-- Name: fk_ao_563aee_activity_entity_target_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_ACTIVITY_ENTITY"
    ADD CONSTRAINT fk_ao_563aee_activity_entity_target_id FOREIGN KEY ("TARGET_ID") REFERENCES "AO_563AEE_TARGET_ENTITY"("ID");


--
-- Name: fk_ao_563aee_object_entity_image_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_OBJECT_ENTITY"
    ADD CONSTRAINT fk_ao_563aee_object_entity_image_id FOREIGN KEY ("IMAGE_ID") REFERENCES "AO_563AEE_MEDIA_LINK_ENTITY"("ID");


--
-- Name: fk_ao_563aee_target_entity_image_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_563AEE_TARGET_ENTITY"
    ADD CONSTRAINT fk_ao_563aee_target_entity_image_id FOREIGN KEY ("IMAGE_ID") REFERENCES "AO_563AEE_MEDIA_LINK_ENTITY"("ID");


--
-- Name: fk_ao_56464c_approver_approval_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_56464C_APPROVER"
    ADD CONSTRAINT fk_ao_56464c_approver_approval_id FOREIGN KEY ("APPROVAL_ID") REFERENCES "AO_56464C_APPROVAL"("ID");


--
-- Name: fk_ao_56464c_approverdecision_approval_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_56464C_APPROVERDECISION"
    ADD CONSTRAINT fk_ao_56464c_approverdecision_approval_id FOREIGN KEY ("APPROVAL_ID") REFERENCES "AO_56464C_APPROVAL"("ID");


--
-- Name: fk_ao_56464c_notificationrecord_approval_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_56464C_NOTIFICATIONRECORD"
    ADD CONSTRAINT fk_ao_56464c_notificationrecord_approval_id FOREIGN KEY ("APPROVAL_ID") REFERENCES "AO_56464C_APPROVAL"("ID");


--
-- Name: fk_ao_5fb9d7_aohip_chat_user_hip_chat_link_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_5FB9D7_AOHIP_CHAT_USER"
    ADD CONSTRAINT fk_ao_5fb9d7_aohip_chat_user_hip_chat_link_id FOREIGN KEY ("HIP_CHAT_LINK_ID") REFERENCES "AO_5FB9D7_AOHIP_CHAT_LINK"("ID");


--
-- Name: fk_ao_733371_event_parameter_event_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_733371_EVENT_PARAMETER"
    ADD CONSTRAINT fk_ao_733371_event_parameter_event_id FOREIGN KEY ("EVENT_ID") REFERENCES "AO_733371_EVENT"("ID");


--
-- Name: fk_ao_733371_event_recipient_event_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_733371_EVENT_RECIPIENT"
    ADD CONSTRAINT fk_ao_733371_event_recipient_event_id FOREIGN KEY ("EVENT_ID") REFERENCES "AO_733371_EVENT"("ID");


--
-- Name: fk_ao_7a2604_holiday_calendar_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_7A2604_HOLIDAY"
    ADD CONSTRAINT fk_ao_7a2604_holiday_calendar_id FOREIGN KEY ("CALENDAR_ID") REFERENCES "AO_7A2604_CALENDAR"("ID");


--
-- Name: fk_ao_7a2604_workingtime_calendar_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_7A2604_WORKINGTIME"
    ADD CONSTRAINT fk_ao_7a2604_workingtime_calendar_id FOREIGN KEY ("CALENDAR_ID") REFERENCES "AO_7A2604_CALENDAR"("ID");


--
-- Name: fk_ao_9b2e3b_exec_rule_msg_item_rule_execution_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_EXEC_RULE_MSG_ITEM"
    ADD CONSTRAINT fk_ao_9b2e3b_exec_rule_msg_item_rule_execution_id FOREIGN KEY ("RULE_EXECUTION_ID") REFERENCES "AO_9B2E3B_RULE_EXECUTION"("ID");


--
-- Name: fk_ao_9b2e3b_if_cond_conf_data_if_condition_config_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_COND_CONF_DATA"
    ADD CONSTRAINT fk_ao_9b2e3b_if_cond_conf_data_if_condition_config_id FOREIGN KEY ("IF_CONDITION_CONFIG_ID") REFERENCES "AO_9B2E3B_IF_CONDITION_CONFIG"("ID");


--
-- Name: fk_ao_9b2e3b_if_cond_execution_if_execution_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_COND_EXECUTION"
    ADD CONSTRAINT fk_ao_9b2e3b_if_cond_execution_if_execution_id FOREIGN KEY ("IF_EXECUTION_ID") REFERENCES "AO_9B2E3B_IF_EXECUTION"("ID");


--
-- Name: fk_ao_9b2e3b_if_condition_config_if_then_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_CONDITION_CONFIG"
    ADD CONSTRAINT fk_ao_9b2e3b_if_condition_config_if_then_id FOREIGN KEY ("IF_THEN_ID") REFERENCES "AO_9B2E3B_IF_THEN"("ID");


--
-- Name: fk_ao_9b2e3b_if_execution_if_then_execution_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_EXECUTION"
    ADD CONSTRAINT fk_ao_9b2e3b_if_execution_if_then_execution_id FOREIGN KEY ("IF_THEN_EXECUTION_ID") REFERENCES "AO_9B2E3B_IF_THEN_EXECUTION"("ID");


--
-- Name: fk_ao_9b2e3b_if_then_execution_rule_execution_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_THEN_EXECUTION"
    ADD CONSTRAINT fk_ao_9b2e3b_if_then_execution_rule_execution_id FOREIGN KEY ("RULE_EXECUTION_ID") REFERENCES "AO_9B2E3B_RULE_EXECUTION"("ID");


--
-- Name: fk_ao_9b2e3b_if_then_rule_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_IF_THEN"
    ADD CONSTRAINT fk_ao_9b2e3b_if_then_rule_id FOREIGN KEY ("RULE_ID") REFERENCES "AO_9B2E3B_RULE"("ID");


--
-- Name: fk_ao_9b2e3b_rsetrev_proj_context_ruleset_revision_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RSETREV_PROJ_CONTEXT"
    ADD CONSTRAINT fk_ao_9b2e3b_rsetrev_proj_context_ruleset_revision_id FOREIGN KEY ("RULESET_REVISION_ID") REFERENCES "AO_9B2E3B_RULESET_REVISION"("ID");


--
-- Name: fk_ao_9b2e3b_rsetrev_user_context_ruleset_revision_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RSETREV_USER_CONTEXT"
    ADD CONSTRAINT fk_ao_9b2e3b_rsetrev_user_context_ruleset_revision_id FOREIGN KEY ("RULESET_REVISION_ID") REFERENCES "AO_9B2E3B_RULESET_REVISION"("ID");


--
-- Name: fk_ao_9b2e3b_rule_ruleset_revision_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RULE"
    ADD CONSTRAINT fk_ao_9b2e3b_rule_ruleset_revision_id FOREIGN KEY ("RULESET_REVISION_ID") REFERENCES "AO_9B2E3B_RULESET_REVISION"("ID");


--
-- Name: fk_ao_9b2e3b_ruleset_revision_rule_set_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_RULESET_REVISION"
    ADD CONSTRAINT fk_ao_9b2e3b_ruleset_revision_rule_set_id FOREIGN KEY ("RULE_SET_ID") REFERENCES "AO_9B2E3B_RULESET"("ID");


--
-- Name: fk_ao_9b2e3b_then_act_conf_data_then_action_config_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACT_CONF_DATA"
    ADD CONSTRAINT fk_ao_9b2e3b_then_act_conf_data_then_action_config_id FOREIGN KEY ("THEN_ACTION_CONFIG_ID") REFERENCES "AO_9B2E3B_THEN_ACTION_CONFIG"("ID");


--
-- Name: fk_ao_9b2e3b_then_act_execution_then_execution_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACT_EXECUTION"
    ADD CONSTRAINT fk_ao_9b2e3b_then_act_execution_then_execution_id FOREIGN KEY ("THEN_EXECUTION_ID") REFERENCES "AO_9B2E3B_THEN_EXECUTION"("ID");


--
-- Name: fk_ao_9b2e3b_then_action_config_if_then_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_ACTION_CONFIG"
    ADD CONSTRAINT fk_ao_9b2e3b_then_action_config_if_then_id FOREIGN KEY ("IF_THEN_ID") REFERENCES "AO_9B2E3B_IF_THEN"("ID");


--
-- Name: fk_ao_9b2e3b_then_execution_if_then_execution_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_THEN_EXECUTION"
    ADD CONSTRAINT fk_ao_9b2e3b_then_execution_if_then_execution_id FOREIGN KEY ("IF_THEN_EXECUTION_ID") REFERENCES "AO_9B2E3B_IF_THEN_EXECUTION"("ID");


--
-- Name: fk_ao_9b2e3b_when_hand_conf_data_when_handler_config_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_WHEN_HAND_CONF_DATA"
    ADD CONSTRAINT fk_ao_9b2e3b_when_hand_conf_data_when_handler_config_id FOREIGN KEY ("WHEN_HANDLER_CONFIG_ID") REFERENCES "AO_9B2E3B_WHEN_HANDLER_CONFIG"("ID");


--
-- Name: fk_ao_9b2e3b_when_handler_config_rule_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_9B2E3B_WHEN_HANDLER_CONFIG"
    ADD CONSTRAINT fk_ao_9b2e3b_when_handler_config_rule_id FOREIGN KEY ("RULE_ID") REFERENCES "AO_9B2E3B_RULE"("ID");


--
-- Name: fk_ao_c7f17e_lingo_revision_lingo_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_LINGO_REVISION"
    ADD CONSTRAINT fk_ao_c7f17e_lingo_revision_lingo_id FOREIGN KEY ("LINGO_ID") REFERENCES "AO_C7F17E_LINGO"("ID");


--
-- Name: fk_ao_c7f17e_lingo_translation_lingo_revision_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_LINGO_TRANSLATION"
    ADD CONSTRAINT fk_ao_c7f17e_lingo_translation_lingo_revision_id FOREIGN KEY ("LINGO_REVISION_ID") REFERENCES "AO_C7F17E_LINGO_REVISION"("ID");


--
-- Name: fk_ao_c7f17e_project_lang_rev_project_lang_config_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_PROJECT_LANG_REV"
    ADD CONSTRAINT fk_ao_c7f17e_project_lang_rev_project_lang_config_id FOREIGN KEY ("PROJECT_LANG_CONFIG_ID") REFERENCES "AO_C7F17E_PROJECT_LANG_CONFIG"("ID");


--
-- Name: fk_ao_c7f17e_project_language_project_lang_rev_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_C7F17E_PROJECT_LANGUAGE"
    ADD CONSTRAINT fk_ao_c7f17e_project_language_project_lang_rev_id FOREIGN KEY ("PROJECT_LANG_REV_ID") REFERENCES "AO_C7F17E_PROJECT_LANG_REV"("ID");


--
-- Name: fk_ao_d530bb_cannedresponseusage_canned_response_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_D530BB_CANNEDRESPONSEUSAGE"
    ADD CONSTRAINT fk_ao_d530bb_cannedresponseusage_canned_response_id FOREIGN KEY ("CANNED_RESPONSE_ID") REFERENCES "AO_D530BB_CANNEDRESPONSE"("ID");


--
-- Name: fk_ao_d530bb_crauditactiondata_canned_response_audit_log_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_D530BB_CRAUDITACTIONDATA"
    ADD CONSTRAINT fk_ao_d530bb_crauditactiondata_canned_response_audit_log_id FOREIGN KEY ("CANNED_RESPONSE_AUDIT_LOG_ID") REFERENCES "AO_D530BB_CANNEDRESPONSEAUDIT"("ID");


--
-- Name: fk_ao_f1b27b_key_comp_history_timed_promise_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_F1B27B_KEY_COMP_HISTORY"
    ADD CONSTRAINT fk_ao_f1b27b_key_comp_history_timed_promise_id FOREIGN KEY ("TIMED_PROMISE_ID") REFERENCES "AO_F1B27B_PROMISE_HISTORY"("ID");


--
-- Name: fk_ao_f1b27b_key_component_timed_promise_id; Type: FK CONSTRAINT; Schema: public; Owner: jira
--

ALTER TABLE ONLY "AO_F1B27B_KEY_COMPONENT"
    ADD CONSTRAINT fk_ao_f1b27b_key_component_timed_promise_id FOREIGN KEY ("TIMED_PROMISE_ID") REFERENCES "AO_F1B27B_PROMISE"("ID");


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

