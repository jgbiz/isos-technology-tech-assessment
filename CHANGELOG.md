## [0.8.0] - 2019-6-20
### Added java and install-jira role for archive install
  - added roles/installjira-tar to create users, directories, extract the archive and (in progress) start the service
  - added roles/java - geerlingguy java installation role

## [0.7.1] - 2019-6-19
### Refactoring installjira for platform agnostic install
  ^ defaults/main.yml - extenting defaults to separate data from code
  - removed tasks/restart-jira.yml - using handler and command module instead
  - added roles/os-prereq for use in ec2 instances and Ubuntu/RHEL modern releases with python3/python2 library installation based on facts
  ^ refactored handler to support both platform restarts using shell script, moved shell path references to defaults

## [0.7.0] - 2019-6-18
### Setup Jinja2 template for dbconfig
  - Added handlers/main.yml - Moved restart Jira section to handler to prevent errors during startup sequence
  - Moved dbconfig.xml to templates/dbconfig.xml.j2
  - Added database specific name, username, pass, host and port, and type vars to vault.yml
  - Added vault variables to default vars/vars.yml

## [0.6.0] - 2019-6-18
### Added OS specfic vars and tasks to vagrant/CensOS7-Ansible-Jira_v2
#### Commit error, included 0.6 files with 0.5.2 push
  ^ /vagrant
    - Added defaults/main.yml with default vars moved out of main /vars/main.yml file
    - Added install_Debian.yml with pip and postgresql psycopg2 connector as prereq for database
    - Added meta/main.yml

## [0.5.2] - 2019-6-17
### aws playbook - working ec2.py
  ^ Moved dynamic inventory scripts to inventory/hosts/ec2.*
  - Added RedHad-8.yml vars for current AWS RedHad version
  - Added roles/epel-repo to include epel in RHEL
  - Added roles/ec2 to open ports and configure firewall and python packages (in progress)
  - Added ansible.cfg to include vault password file during connection

## [0.5.1] - 2019-6-17
  ^ v3.md - corrected link

## [0.5.0] - 2019-6-17
### aws playbook
  - Added aws playbook with ec2.py
  - Added v3.md

## [0.4.1] - 2019-6-16
  - Clean-up readme and versions
  - Correct vars structure, consolidated in /vars/vars.yml, vault.yml
  ^ setup-databases.yml - added postgresql restart

## [0.4.0] - 2019-6-16
### installjira role
  - Added custom 'installjira' role
    - Added installjira/vars.yml vars
    - Added files/
      + response.varfile - selections for unattended installation
      + dbconfig.xml - database configuration used to override after the install
      + jiraclean.sql - a clean sql database with a license and base configuration settings
    - Added tasks/
      + main.yml - imports tasks
      + install-jira.xml - copies varfile, downloads jira from atlassian, sets permissions and installs jira using varfile
      + setup-database.yml - copies jira-clean.sql, imports data and copies dbconfig.xml
      + restart-jira.yml - restarts jira to apply dbconfig.xml
  ^ modified ansible-playbook.yml to include installjira role

## [0.3.0] - 2019-6-16
### Postfix role
  - Added postfix role from (https://github.com/geerlingguy/ansible-role-postfix)

## [0.2.1] - 2019-6-14
### Postgresql role
  - Added postgreSQL role from (https://github.com/geerlingguy/ansible-role-postgresql)

## [0.2.0] - 2019-6-14
### Postgresql role
  - Added main ansible-playbook file
  - Added vars, vault.yml for database passwords, and updated Vagrantfile to include a check for vault password during provision.
  - Added custom postgresql install role 'createdb'
  - Added V2.md

## [0.1.0] - 2019-06-12
### Initial Commit
  - Added v1.md for Version 1 Release
  - Added Vagrantfile for CentOS7-Jira_v1 VM
