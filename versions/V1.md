# Version 1

## Install Procedure:
This is the procedure for a manual install of Jira using Vagrant, CentOS7 and Postgres-9.6. First you initialize the vagrant box and configure, then you install PostgresSQL and finally install and setup Jira.

[Vagrantfile & Playbook](https://bitbucket.org/jgbiz/isos-technology-tech-assessment/src/master/vagrant/CentOS7-Jira_v1/)

### Initialize Vagrant VM

##### 1. Use vagrant and initialize the  bento/CentOS7 box.

``` bash
vagrant init bento/centos-7
```

##### 2. Enable auto rsync to watch for directory file changes.

``` bash
vagrant rsync-auto
```

##### 3. Edit the Vagrantfile to allow host machine access to the network, and add a shared folder from the host machine to the VM.

``` Bash
config.vm.network "private_network", ip: "192.168.33.10"
```

##### 4. Start the Vagrant VM

``` bash
vagrant up
```

##### 5. Access via ssh.

``` bash
vagrant ssh
```

### Install and Configure PostgreSQL


##### 1. Enable alternate PostgreSQL repository, install  PostgreSQL 9.6 server and PostgreSQL 9.6 via yum.

``` bash
sudo rpm -Uvh http://yum.postgresql.org/9.6/redhat/rhel-7-x86_64/pgdg-redhat96-9.6-3.noarch.rpm

sudo yum install postgresql96-server postgresql96
```

##### 2. Create a new PostgreSQL database cluster.

``` bash
sudo /usr/pgsql-9.6/bin/postgresql96-setup initdb
```

##### 3. Change authentication from `ident` to `trust` to support password auth later during  the Jira database setup.

``` bash
sudo vi /var/lib/pgsql/9.6/data/pg_hba.conf
```

Find the lines that looks like this, near the bottom of the file:

```
pg_hba.conf excerpt (original)
host    all             all             127.0.0.1/32            ident(trust)
host    all             all             ::1/128                 ident(trust)
```

##### 4. Start the service and enable PostgreSQL to run at boot.

``` bash
sudo systemctl start postgresql-9.6
sudo systemctl enable postgresql-9.6
```

##### 5. Switch to the `postgres` system user and access the command prompt

``` bash
sudo -i -u postgres
```

##### 6. Create a database to use in the Jira Install and a user for that database.

``` bash
createdb jira
createuser --interactive

```

##### 7. Access the `psql` prompt and change the password for the `postgres` user.

``` bash
\password
```

##### 8. Grant all privileges to the `jira` user for `jira` database and change `jira` user password.

``` bash
grant all privileges on database jira to jira;
ALTER USER jira WITH PASSWORD '*password*';
\q
```

### Install Atlassian Jira Service Desk

##### 1. Make the installer executable.

``` bash
cd /vagrant
chmod a+x atlassian-servicedesk-4.2.1-x64.bin
```

##### 2. Execute the installer.

``` bash
sudo ./atlassian-servicedesk-4.2.1-x64.bin
```

##### 3. Confirm installation details in the prompt.

    * (Enter) - To confirm overall install.
    * (1) - Express install selection.
        * Installation Directory: /opt/atlassian/jira
        *  Home Directory: /var/atlassian/application-data/jira
    *  HTTP Port: 8080
    * (Enter) - To confirm install as a service.
    * (Enter) - Installation Complete, start the service desk.

#### Configure Jira & Import Data

1. Access Jira through the VM network IP address `192.168.33.10:8080` .

2. Choose - I'll set it up myself.

3. Connect to the jira database created earlier and test the connection before running the setup. Use localhost, port 5432, user: jira, database: jira and the password.

4. On the screen to set the application properties choose to import data from a backup.

    * Move the .zip backup file provided from the vagrant shared directory into the correct directory for the import.

``` bash
cd /vagrant
mv jira-7-7-0-TIS.zip /var/atlassian/application-data/jira/import
```

    * Set the file name to jira-7-7-0-TIS.zip.

    * Paste the license key generated from the Atlassian website.

    * Run the import.

### Jira System Administrator Password

Since utilizing the import feature during the install, system admin credentials were not known.

##### 1. Set recovery mode in the Atlassian configuration.

``` bash
cd /opt/atlassian/jira/bin
sudo ./stop-jira.sh
vi setenv.sh
```

##### 2. In setenv.sh find the line `JVM_SUPPORT_RECOMMENDED_ARGS=` and set it equal to:

``` bash
-Datlassian.recovery.password=<your-password>
```

##### 3. Restart the Jira Service and login using the username recovery_admin and the password set above.

``` bash
sudo ./start-jira.sh
```

### Jira Setup & User Setup

1. First click Update IP Address to refresh IP set from the imported data.

2. Visit Settings > User Management and click `Create User`.

2. Click the `...` in the username's row and click `edit user groups`. Add the user to the jira-administrators group.

3. Shutdown Jira one more time, remove the recovery mode settings and restart Jira with the new adminstrator user.

``` bash
sudo ./stop-jira.#!/bin/sh
vi setenv.sh
sudo ./start-jira
```

### Resources
* [Installing Jira on Linux](https://confluence.atlassian.com/adminjiraserver/installing-jira-applications-on-linux-938846841.html)
* [Import Data](https://confluence.atlassian.com/adminjiraserver/restoring-data-from-an-xml-backup-938847707.html#Restoringdatafromanxmlbackup-restorexmldata)
* [PostgreSQL 9.2 Setup on CentOS 7](https://tecadmin.net/install-postgresql-on-centos-rhel-and-fedora/
)
* [rSync Shared Folders on Vagrant](https://www.vagrantup.com/docs/synced-folders/basic_usage.html)
* [Jira Administrator Password](https://confluence.atlassian.com/jira/retrieving-the-jira-administrator-192836.html?_ga=2.267795917.291624338.1560127240-352162059.1555303494)
* [Setting Jira Properties and Options on Startup](https://confluence.atlassian.com/adminjiraserver072/setting-properties-and-options-on-startup-828788225.html)
* [Restore Jira XML Data](https://confluence.atlassian.com/adminjiraserver/restoring-data-from-an-xml-backup-938847707.html#Restoringdatafromanxmlbackup-restorexmldata)
* [Vagrant Auto rSync](https://www.vagrantup.com/docs/cli/rsync-auto.html)
